extends ConfirmationDialog


@export var unhandled_key_input:Callable = Callable()


func _unhandled_key_input(event: InputEvent) -> void:
	unhandled_key_input.call(event)
