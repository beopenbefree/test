extends HBoxContainer


signal input_requested(id)

const ConfigClass := preload("res://addons/control_config.gd")


var editor_input_map: Node = null
var action: String
var input: InputEvent
var id: int


func _ready():
	$Edit.pressed.connect(_on_Edit_pressed)
	$Delete.pressed.connect(_on_Delete_pressed)


func init(a:String, i: InputEvent, can_delete_inputs: bool) -> void:
	action = a
	input = i
	if i is InputEventKey:
		id = 0
	elif i is InputEventJoypadButton:
		id = 1
	elif i is InputEventJoypadMotion:
		id = 2
	elif i is InputEventMouseButton:
		id = 3
	$Name.text = ConfigClass.get_input_text(i)
	$Delete.visible = can_delete_inputs
	set_input(i)


func _on_Edit_pressed() -> void:
	emit_signal("input_requested", id)


func _on_Delete_pressed() -> void:
	InputMap.action_erase_event(action, input)
	queue_free()
	editor_input_map.save_config()


func set_input(i: InputEvent) -> void:
	InputMap.action_erase_event(action, input)
	input = i
	InputMap.action_add_event(action, input)
	$Name.text = ConfigClass.get_input_text(i)
