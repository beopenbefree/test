extends VBoxContainer


signal input_requested(id)
signal input_added(input)

@export var InputClass := preload("res://test_inputs/control_config/editor_input_map/input.tscn")

var editor_input_map: Node = null
var action: String
var last_added_input: HBoxContainer
var allow_delete_inputs: bool

var can_add_input = true
var can_delete_action = true

@onready var add_pop: PopupMenu = $HBox/Add.get_popup()


func _ready() -> void:
	add_pop.id_pressed.connect(on_add_id_pressed)
	$HBox/Name.toggled.connect(_on_Name_toggled)
	$HBox/Deadzone.value_changed.connect(_on_Deadzone_value_changed)
	$HBox/Delete.pressed.connect(_on_Delete_pressed)


func init(a: String, can_add: bool, can_delete: bool, can_delete_inputs: bool) -> void:
	action = a
	$HBox/Name.text = a.replace("_", " ")
	$HBox/Deadzone.value = InputMap.action_get_deadzone(action)
	for input in InputMap.action_get_events(a):
		var i := InputClass.instantiate()
		i.editor_input_map = editor_input_map
		i.init(action, input, can_delete_inputs)
		$Inputs.add_child(i)
	$HBox/Add.visible = can_add
	can_add_input = can_add
	$HBox/Delete.visible = can_delete
	can_delete_action = can_delete
	allow_delete_inputs = can_delete_inputs
	# start with inputs & add hidden
	$HBox/Name.button_pressed = true
	_on_Name_toggled(true)


func get_all_inputs() -> Array:
	return $Inputs.get_children()


func _on_Name_toggled(button_pressed: bool) -> void:
	$Inputs.visible = not button_pressed
	$HBox/Add.visible = (not button_pressed) and can_add_input
	$HBox/Delete.visible = (not button_pressed) and can_delete_action


func _on_Deadzone_value_changed(value: float) -> void:
	InputMap.action_set_deadzone(action, value)
	# save input map
	editor_input_map.save_config()


func on_add_id_pressed(id: int) -> void:
	emit_signal("input_requested", id)


func _on_Delete_pressed() -> void:
	InputMap.erase_action(action)
	queue_free()
	# save input map
	editor_input_map.save_config()


func set_input(input: InputEvent) -> void:
	var i := InputClass.instantiate()
	i.editor_input_map = editor_input_map
	i.init(action, input, allow_delete_inputs)
	$Inputs.add_child(i)
	last_added_input = i
