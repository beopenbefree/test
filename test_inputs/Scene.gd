extends Node3D


@export var my_event: String

var dragging = false
var click_radius = 32

var arrow = load("res://test_2D_lights_and_shadows/spot.png")
var beam = load("res://test_2D_particle_systems/pngwing.com.png")
var drag = load("res://test_control_the_game_s_ui_with_code/icon.png")

const SPEED = 100


func _ready():
	pass
	
	Input.add_joy_mapping("03000000300f00001101000010010000,Jess Tech Colour Rumble Pad,a:b2,b:b3,y:b1,x:b0,start:b9,back:b8,leftstick:b10,rightstick:b11,leftshoulder:b4,rightshoulder:b6,dpup:b12,dpleft:b14,dpdown:b13,dpright:b13,leftx:a0,lefty:a1~,rightx:a3,righty:a2~,lefttrigger:b5,righttrigger:b7,platform:Linux", true)
	
	# Changes only the arrow shape of the cursor.
	# This is similar to changing it in the project settings.
	Input.set_custom_mouse_cursor(arrow, Input.CURSOR_ARROW,
			Vector2(64.0, 64.0))
	# Changes a specific shape of the cursor (here, the I-beam shape).
	Input.set_custom_mouse_cursor(beam, Input.CURSOR_IBEAM,
			Vector2(16.0, 16.0))
	Input.set_custom_mouse_cursor(drag, Input.CURSOR_DRAG,
			Vector2(32.0, 32.0))


func _process(delta):
	pass
	
	# Controllers, gamepads, and joysticks: Supporting universal input
	var velocity: Vector2
	# 1 When you have two axes
	# Godot >= 3.4
	velocity = Input.get_vector("movement_left", "movement_right",
			"movement_forward", "movement_backward")
	# Godot < 3.4
#	velocity = Vector2(Input.get_action_strength("movement_right") - 
#			Input.get_action_strength("movement_left"), 
#			Input.get_action_strength("movement_backward") -
#			Input.get_action_strength("movement_forward")).clamped(1)
	# 2 When you have one axis that can go both ways
	# Godot >= 3.4
#	velocity = Input.get_axis("movement_left", "movement_right")
	# Godot < 3.4
#	velocity = Vector2(Input.get_action_strength("movement_right") - 
#			Input.get_action_strength("movement_left"), 0.0)
	#
	$Sprite2D.position += velocity * delta * SPEED


func _input(event):
	pass
	
	# INTERRUPT (vs POLLING)
#	if event.is_action_pressed(my_event):
#		print("_input " + my_event)
	
	# INPUT EVENTS
#	print(event.as_text())
#	if event is InputEventMouseButton:
#		print("mouse button event at ", event.position)
	
	# InputMap
#	if event.is_action_pressed("ui_accept"):
#		print("ui_accept occurred!")
	
	# Keyboard events
#	if event is InputEventKey and event.pressed:
#		if event.scancode == KEY_T:
#			print("T was pressed")
	
	# Keyboard modifiers
#	if event is InputEventKey and event.pressed:
#		if event.scancode == KEY_T:
#			if event.shift:
#				print("Shift+T was pressed")
#			else:
#				print("T was pressed")
	
	# Mouse Buttons
#	if event is InputEventMouseButton:
#		if event.button_index == BUTTON_LEFT and event.pressed:
#			print("Left mouse button clicked at ", event.position)
#		if event.button_index == BUTTON_WHEEL_UP and event.pressed:
#			print("Wheel mouse button up")
#		if event.button_index == BUTTON_WHEEL_DOWN and event.pressed:
#			print("Wheel mouse button down")
	
	# Mouse motion
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT:
		if (event.position - $Sprite2D.position).length() < click_radius:
			# Start dragging if the click is on the sprite.
			if not dragging and event.pressed:
				dragging = true
		# Stop dragging if the button is released.
		if dragging and not event.pressed:
			dragging = false
	if event is InputEventMouseMotion and dragging:
		# While dragging, move the sprite with the mouse.
		$Sprite2D.position = event.position
	
	# Mouse in viewport display coordinates.
#	if event is InputEventMouseButton:
#		print("Mouse Click/Unclick at: ", event.position,
#				" - ", get_viewport().get_mouse_position())
#	elif event is InputEventMouseMotion:
#		print("Mouse Motion at: ", event.position,
#				" - ", get_viewport().get_mouse_position())
#	# Print the size of the viewport.
#	print("Viewport Resolution is: ", $Sprite.get_viewport_rect().size)


func _physics_process(delta):
	pass
	
	# POLLING (vs INTERRUPT)
#	if Input.is_action_pressed(my_event):
#		print("_physics_process " + my_event)
