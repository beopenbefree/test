extends Node


func _input(event):
	if (event is InputEventJoypadButton) or (event is InputEventJoypadMotion):
		print(event.as_text())
		var new_event = InputEventJoypadButton.new()
		new_event.button_index = event.button_index
		print("\t", new_event.as_text())
