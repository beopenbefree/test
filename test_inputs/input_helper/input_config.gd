extends Control


func _ready():
	InputHelper.device_changed.connect(_on_input_device_changed)


func _on_input_device_changed(device: String, device_index: int) -> void:
	var text = device + " (" + str(device_index) + ")"
	$VBoxContainer/HBoxContainer/Device.text = text
