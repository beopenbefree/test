extends AnimationPlayer


const StateMachineFactory = preload("res://addons/godot-finite-state-machine/fsm/StateMachineFactory.gd")


class Idle_unarmed extends "res://levels/tools/fsm_proxy_state.gd":
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)


class Pistol_equip extends "res://levels/tools/fsm_proxy_state.gd":
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.4
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Pistol_fire extends "res://levels/tools/fsm_proxy_state.gd":
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.8
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Pistol_idle extends "res://levels/tools/fsm_proxy_state.gd":
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Pistol_reload extends "res://levels/tools/fsm_proxy_state.gd":
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Pistol_unequip extends "res://levels/tools/fsm_proxy_state.gd":
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.4
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Rifle_equip extends "res://levels/tools/fsm_proxy_state.gd":
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 2.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Rifle_fire extends "res://levels/tools/fsm_proxy_state.gd":
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 6.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Rifle_idle extends "res://levels/tools/fsm_proxy_state.gd":
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Rifle_reload extends "res://levels/tools/fsm_proxy_state.gd":
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.45
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Rifle_unequip extends "res://levels/tools/fsm_proxy_state.gd":
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 2.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Knife_equip extends "res://levels/tools/fsm_proxy_state.gd":
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Knife_fire extends "res://levels/tools/fsm_proxy_state.gd":
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.35
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Knife_idle extends "res://levels/tools/fsm_proxy_state.gd":
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)

class Knife_unequip extends "res://levels/tools/fsm_proxy_state.gd":
	
	var animation_speed:float
	
	func _init():
		process_enabled = false
		physics_process_enabled = false
		input_enabled = false
		leave_state_enabled = false
		animation_speed = 1.0
	
	func _on_enter_state() -> void:
		var animation_name:String = id
		if target.has_animation(animation_name) == true:
			target.play(animation_name, -1, animation_speed)


var callback_function = null

var fsm: StateMachine
@onready var fsm_factory = StateMachineFactory.new()


func _ready():
	fsm = fsm_factory.create({
		"target": self,
		"current_state": "Idle_unarmed",
		"states": [
			{"id": "Idle_unarmed", "state": Idle_unarmed},
			
			{"id": "Pistol_equip", "state": Pistol_equip},
			{"id": "Pistol_fire", "state": Pistol_fire},
			{"id": "Pistol_idle", "state": Pistol_idle},
			{"id": "Pistol_reload", "state": Pistol_reload},
			{"id": "Pistol_unequip", "state": Pistol_unequip},
			
			{"id": "Rifle_equip", "state": Rifle_equip},
			{"id": "Rifle_fire", "state": Rifle_fire},
			{"id": "Rifle_idle", "state": Rifle_idle},
			{"id": "Rifle_reload", "state": Rifle_reload},
			{"id": "Rifle_unequip", "state": Rifle_unequip},
			
			{"id": "Knife_equip", "state": Knife_equip},
			{"id": "Knife_fire", "state": Knife_fire},
			{"id": "Knife_idle", "state": Knife_idle},
			{"id": "Knife_unequip", "state": Knife_unequip},
		],
		"transitions": [
			{"state_id": "Idle_unarmed", "to_states": ["Knife_equip", "Pistol_equip", "Rifle_equip", "Idle_unarmed"]},
			
			{"state_id": "Pistol_equip", "to_states": ["Pistol_idle"]},
			{"state_id": "Pistol_fire", "to_states": ["Pistol_idle"]},
			{"state_id": "Pistol_idle", "to_states": ["Pistol_fire", "Pistol_reload", "Pistol_unequip", "Pistol_idle"]},
			{"state_id": "Pistol_reload", "to_states": ["Pistol_idle"]},
			{"state_id": "Pistol_unequip", "to_states": ["Idle_unarmed"]},
			
			{"state_id": "Rifle_equip", "to_states": ["Rifle_idle"]},
			{"state_id": "Rifle_fire", "to_states": ["Rifle_idle"]},
			{"state_id": "Rifle_idle", "to_states": ["Rifle_fire", "Rifle_reload", "Rifle_unequip", "Rifle_idle"]},
			{"state_id": "Rifle_reload", "to_states": ["Rifle_idle"]},
			{"state_id": "Rifle_unequip", "to_states": ["Idle_unarmed"]},
			
			{"state_id": "Knife_equip", "to_states": ["Knife_idle"]},
			{"state_id": "Knife_fire", "to_states": ["Knife_idle"]},
			{"state_id": "Knife_idle", "to_states": ["Knife_fire", "Knife_unequip", "Knife_idle"]},
			{"state_id": "Knife_unequip", "to_states": ["Idle_unarmed"]},
		]
	})
	set_animation("Idle_unarmed")
	connect("animation_finished", Callable(self, "animation_ended"))


func set_animation(animation_name):
	fsm.transition(animation_name)


func animation_ended(anim_name):
	# UNARMED transitions
	if fsm.current_state == "Idle_unarmed":
		pass
	# KNIFE transitions
	elif fsm.current_state == "Knife_equip":
		set_animation("Knife_idle")
	elif fsm.current_state == "Knife_idle":
		pass
	elif fsm.current_state == "Knife_fire":
		set_animation("Knife_idle")
	elif fsm.current_state == "Knife_unequip":
		set_animation("Idle_unarmed")
	# PISTOL transitions
	elif fsm.current_state == "Pistol_equip":
		set_animation("Pistol_idle")
	elif fsm.current_state == "Pistol_idle":
		pass
	elif fsm.current_state == "Pistol_fire":
		set_animation("Pistol_idle")
	elif fsm.current_state == "Pistol_unequip":
		set_animation("Idle_unarmed")
	elif fsm.current_state == "Pistol_reload":
		set_animation("Pistol_idle")
	# RIFLE transitions
	elif fsm.current_state == "Rifle_equip":
		set_animation("Rifle_idle")
	elif fsm.current_state == "Rifle_idle":
		pass;
	elif fsm.current_state == "Rifle_fire":
		set_animation("Rifle_idle")
	elif fsm.current_state == "Rifle_unequip":
		set_animation("Idle_unarmed")
	elif fsm.current_state == "Rifle_reload":
		set_animation("Rifle_idle")


func animation_callback():
	if callback_function == null:
		print ("AnimationPlayer_Manager.gd -- WARNING: No callback function for the animation to call!")
	else:
		callback_function.call_func()
