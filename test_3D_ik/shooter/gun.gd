extends Node3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var fire_enabled: bool = true: get = _get_fire_enabled, set = _set_fire_enabled

@export var BULLET_IMPULSE_RANGE: Array = [150.0, 150.0]
@export var BULLET_MASS_RANGE: Array = [1.0, 2.0]

@export var AMMO_IN_MAG: int = 10

@export var DAMAGE: int = 6

var ammo_in_weapon:int = 10
var spare_ammo:int = 20

const CAN_RELOAD = true
const CAN_REFILL = true

const RELOADING_ANIM_NAME = "Pistol_reload"
const IDLE_ANIM_NAME = "Pistol_idle"
const FIRE_ANIM_NAME = "Pistol_fire"

var bullet_scene = preload("res://test_3D_ik/ik/fps/simple_bullet.tscn")

var player_node:Node = null


func fire_weapon():
	if fire_enabled:
		var bullet = bullet_scene.instantiate()
		bullet.DAMAGE = DAMAGE
		bullet.scaling *= 4.0
		bullet.collisions = 0b111001
		get_tree().add_child(bullet)
		bullet.mass = randf_range(BULLET_MASS_RANGE[0], BULLET_MASS_RANGE[1])
		
		bullet.global_transform = self.global_transform
		var impulse_size = randf_range(BULLET_IMPULSE_RANGE[0], BULLET_IMPULSE_RANGE[1])
		bullet.fire(impulse_size, bullet.global_transform.basis.z)
		# emit sound
#		globals.play_sound("gun_shot", null, true, false, bullet.global_transform.origin)
		
		ammo_in_weapon -= 1


func reload_weapon():
	var can_reload = false
	
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		can_reload = true
	
	if spare_ammo <= 0 or ammo_in_weapon == AMMO_IN_MAG:
		can_reload = false
	
	if can_reload == true:
		var ammo_needed = AMMO_IN_MAG - ammo_in_weapon
		
		if spare_ammo >= ammo_needed:
			spare_ammo -= ammo_needed
			ammo_in_weapon = AMMO_IN_MAG
		else:
			ammo_in_weapon += spare_ammo
			spare_ammo = 0
		
		player_node.animation_manager.set_animation(RELOADING_ANIM_NAME)
		
#		globals.play_sound("gun_cock", null, true, false, player_node.head.global_transform.origin)
		
		return true
	
	return false


func equip_weapon():
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		enabled = true
		return true
	
	if player_node.animation_manager.fsm.current_state == "Idle_unarmed":
		player_node.animation_manager.set_animation("Pistol_equip")
		
#		globals.play_sound("gun_cock", null, true, false, player_node.head.global_transform.origin)
	
	return false


func unequip_weapon():
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		if player_node.animation_manager.fsm.current_state != "Pistol_unequip":
			player_node.animation_manager.set_animation("Pistol_unequip")
	
	if player_node.animation_manager.fsm.current_state == "Idle_unarmed":
		enabled = false
		return true
	else:
		return false


func reset_weapon():
	ammo_in_weapon = 10
	spare_ammo = 20


func _set_fire_enabled(value):
	fire_enabled = value


func _get_fire_enabled():
	return fire_enabled


func _set_enabled(value):
	enabled = value


func _get_enabled():
	return enabled
