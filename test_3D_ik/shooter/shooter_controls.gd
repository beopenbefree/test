extends Node3D


func _ready():
	var shooter = get_node("../../Shooter")
	if shooter:
		set_signal_handling(shooter)


func set_signal_handling(shooter:Shooter):
	# fire
	$Fire.connect("analog_touch_true", Callable(self, "_feed_action").bind("fire", true))
	$Fire.connect("analog_touch_false", Callable(self, "_feed_action").bind("fire", false))
	# weapon up
	$WeaponUp.connect("analog_touch_true", self, "_feed_action", 
			["shift_weapon_positive", true])
	$WeaponUp.connect("analog_touch_false", self, "_feed_action", 
			["shift_weapon_positive", false])
	# weapon down
	$WeaponDown.connect("analog_touch_true", self, "_feed_action", 
			["shift_weapon_negative", true])
	$WeaponDown.connect("analog_touch_false", self, "_feed_action", 
			["shift_weapon_negative", false])
	# aim
	$Aim.connect("analog_move", Callable(shooter, "_on_VirtualAim_aim"))
	$Aim.connect("analog_touch", Callable(shooter, "_on_VirtualAim_aim_stop"))


func _feed_action(name, pressed):
	ScriptLibrary._feed_action(name, pressed)
