extends Node3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var fire_enabled: bool = true: get = _get_fire_enabled, set = _set_fire_enabled

@export var AMMO_IN_MAG: int = 1

@export var DAMAGE: int = 40

var ammo_in_weapon:int = 1
var spare_ammo:int = 1

const CAN_RELOAD = false
const CAN_REFILL = false

const RELOADING_ANIM_NAME = ""
const IDLE_ANIM_NAME = "Knife_idle"
const FIRE_ANIM_NAME = "Knife_fire"

var player_node:Node = null


func fire_weapon():
	if fire_enabled:
		var area = $Area3D
		var bodies = area.get_overlapping_bodies()
		for body in bodies:
			if body == player_node:
				continue
			
			if body.has_method("bullet_hit"):
				body.bullet_hit(DAMAGE, area.global_transform)


func reload_weapon():
	return false

func equip_weapon():
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		enabled = true
		return true
	
	if player_node.animation_manager.fsm.current_state == "Idle_unarmed":
		player_node.animation_manager.set_animation("Knife_equip")
	
	return false

func unequip_weapon():
	
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		player_node.animation_manager.set_animation("Knife_unequip")
	
	if player_node.animation_manager.fsm.current_state == "Idle_unarmed":
		enabled = false
		return true
	
	return false

func reset_weapon():
	ammo_in_weapon = 1
	spare_ammo = 1


func _set_fire_enabled(value):
	fire_enabled = value


func _get_fire_enabled():
	return fire_enabled


func _set_enabled(value):
	enabled = value


func _get_enabled():
	return enabled
