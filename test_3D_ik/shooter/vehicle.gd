extends Node3D


var weapon_scene = preload("res://test_3D_ik/shooter/pistol_scene.tscn")

var steer_angle = 0.0
var steer_speed = 2.0
const MAX_ABS_ANGLE = 80
const MIN_ABS_ANGLE = 2


func _physics_process(delta):
	var steer_left = Input.is_action_pressed("movement_left")
	var steer_right = Input.is_action_pressed("movement_right")
	if steer_left or steer_right:
		var left = 1 if steer_left else 0
		var right = 1 if steer_right else 0
		steer_angle += delta * steer_speed * (left - right)
	elif abs(steer_angle) >= deg_to_rad(MIN_ABS_ANGLE):
		steer_angle *= 0.95
	steer_angle = clamp(steer_angle, -deg_to_rad(MAX_ABS_ANGLE), deg_to_rad(MAX_ABS_ANGLE))
	$SteeringWheel.rotation.z = steer_angle


func _on_Button_pressed(equip):
	if equip:
		$SteeringWheel/RemoteTransformTargetRight.set_remote_node("")
		$Shooter.reset_aiming_targets_transform()
		$Shooter/AimingTargets/AnimationPlayer.play("Pistol_equip")
	else:
		$Shooter/AimingTargets/AnimationPlayer.play_backwards("Pistol_unequip")

func _take_weapon():
	for child in $Shooter/AimingTargets/TargetRight.get_children():
		child.queue_free()
	var weapon = weapon_scene.instantiate()
	weapon.scale = Vector3.ONE * 1.5
	weapon.position = Vector3(0.015, 0.029, -0.205)
	weapon.rotation = Vector3(deg_to_rad(81.069), deg_to_rad(125.31), deg_to_rad(34.368))
	$Shooter/AimingTargets/TargetRight.add_child(weapon)
	$Shooter/RotationHelper/AnimatedModel/AnimationPlayer.play("Aim-Pistol-Pose")


func _leave_weapon():
	for child in $Shooter/AimingTargets/TargetRight.get_children():
		child.queue_free()
	$Shooter/RotationHelper/AnimatedModel/AnimationPlayer.play("T-Pose.001")


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Pistol_unequip":
		$SteeringWheel/RemoteTransformTargetRight.set_remote_node("../../Shooter/AimingTargets/TargetRight")
