extends Node3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var fire_enabled: bool = true: get = _get_fire_enabled, set = _set_fire_enabled

@export var vision_area_radius = 36.0 # (float, 10, 100, 0.1)

@export var AMMO_IN_MAG: int = 50

@export var DAMAGE: int = 2

var ammo_in_weapon:int = 50
var spare_ammo:int = 100

const CAN_RELOAD = true
const CAN_REFILL = true

const RELOADING_ANIM_NAME = "Rifle_reload"
const IDLE_ANIM_NAME = "Rifle_idle"
const FIRE_ANIM_NAME = "Rifle_fire"

var player_node:Node = null
var node_raycast:Node = null


func _ready():
	node_raycast = $RayCast3D
#	node_raycast.cast_to = Vector3(0, 0, -vision_area_radius)


func fire_weapon():
	if fire_enabled:
		node_raycast.DAMAGE = DAMAGE
		node_raycast.shoot(player_node.aim_point)
		# emit sound
#		globals.play_sound("rifle_shot", null, true, false, node_raycast.global_transform.origin)
		
		ammo_in_weapon -= 1


func reload_weapon():
	var can_reload = false
	
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		can_reload = true
	
	if spare_ammo <= 0 or ammo_in_weapon == AMMO_IN_MAG:
		can_reload = false
	
	if can_reload == true:
		var ammo_needed = AMMO_IN_MAG - ammo_in_weapon
		
		if spare_ammo >= ammo_needed:
			spare_ammo -= ammo_needed
			ammo_in_weapon = AMMO_IN_MAG
		else:
			ammo_in_weapon += spare_ammo
			spare_ammo = 0
		
		player_node.animation_manager.set_animation(RELOADING_ANIM_NAME)
		
#		globals.play_sound("gun_cock", null, true, false, player_node.head.global_transform.origin)
		
		return true
	
	return false


func equip_weapon():
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		enabled = true
		return true
	
	if player_node.animation_manager.fsm.current_state == "Idle_unarmed":
		player_node.animation_manager.set_animation("Rifle_equip")
		
#		globals.play_sound("gun_cock", null, true, false, player_node.head.global_transform.origin)
	
	return false


func unequip_weapon():
	if player_node.animation_manager.fsm.current_state == IDLE_ANIM_NAME:
		if player_node.animation_manager.fsm.current_state != "Rifle_unequip":
			player_node.animation_manager.set_animation("Rifle_unequip")
	
	if player_node.animation_manager.fsm.current_state == "Idle_unarmed":
		enabled = false
		return true
	
	return false


func reset_weapon():
	ammo_in_weapon = 50
	spare_ammo = 100


func _set_fire_enabled(value):
	fire_enabled = value


func _get_fire_enabled():
	return fire_enabled


func _set_enabled(value):
	enabled = value


func _get_enabled():
	return enabled
