extends Node3D

class_name Shooter


@export var enabled: bool = true: get = _get_enabled, set = _set_enabled

@export var aim_enabled: bool = false: get = _get_aim_enabled, set = _set_aim_enabled

@export var MOUSE_SENSITIVITY: float = 0.05
@export var MOUSE_SENSITIVITY_SCROLL_WHEEL: float = 0.08

@export var GRENADE_AMOUNT: int = 5
var grenade_amounts:Dictionary = {
	"Grenade":0,
	"Sticky Grenade":0,
	}
@export var GRENADE_THROW_FORCE: float = 40.0

@export var OBJECT_THROW_FORCE: float = 120
@export var OBJECT_GRAB_DISTANCE: float = 7
@export var OBJECT_GRAB_RAY_DISTANCE: float = 10

@export var aim_max_yaw_angle = 30.0 # (float, 0.0, 60.0, 1.0)
@export var aim_yaw_speed = 0.3 # (float, 0.05, 1.0, 0.05)
@export var aim_max_pitch_angle = 15.0 # (float, 0.0, 60.0, 1.0)
@export var aim_pitch_speed = 0.3 # (float, 0.05, 1.0, 0.05)

var mouse_scroll_value:float = 0

var animation_manager:AnimationPlayer = null

var current_weapon_name:String = "UNARMED"
var weapons:Dictionary = {"UNARMED":null, "KNIFE":null, "GUN":null, "RIFLE":null}
const WEAPON_NUMBER_TO_NAME:Dictionary = {0:"UNARMED", 1:"KNIFE", 2:"GUN", 3:"RIFLE"}
const WEAPON_NAME_TO_NUMBER:Dictionary = {"UNARMED":0, "KNIFE":1, "GUN":2, "RIFLE":3}
var changing_weapon:bool = false
var changing_weapon_name:String = "UNARMED"
var reloading_weapon:bool = false

var current_grenade:String = "Grenade"
var grenade_scene = preload("res://test_3D_ik/ik/fps/simple_bullet.tscn")
var sticky_grenade_scene = preload("res://test_3D_ik/ik/fps/simple_bullet.tscn")

var grabbed_object:PhysicsBody3D = null

var head:Node3D = null
var aim_point:Node3D = null
var ui_status_label:Control = null

var aim_max_yaw_angle_rad: float = 0.0
var aim_max_pitch_angle_rad: float = 0.0

var touch_aim_direction:Vector2 = Vector2(0, 0)
var touch_touching:bool = false

var start_aiming_targets_transform:Transform3D = Transform3D.IDENTITY


func _ready():
	head = $RotationHelper/Head
	aim_point = $RotationHelper/AimPoint
	aim_max_yaw_angle_rad = deg_to_rad(aim_max_yaw_angle)
	aim_max_pitch_angle_rad = deg_to_rad(aim_max_pitch_angle)
	_set_aim_enabled(aim_enabled)

	animation_manager = $RotationHelper/AnimatedModel/AnimationPlayer
#	animation_manager.callback_function = funcref(self, "fire_weapon")
	
	weapons = {"GUN":1}
	
	if "KNIFE" in weapons:
		weapons["KNIFE"] = $RotationHelper/FirePoints/Knife
	if "GUN" in weapons:
		weapons["GUN"] = $RotationHelper/FirePoints/Gun
	if "RIFLE" in weapons:
		weapons["RIFLE"] = $RotationHelper/FirePoints/Rifle
	
	var aim_point_pos = $RotationHelper/AimPoint.global_transform.origin
	
#	for weapon in weapons:
#		var weapon_node = weapons[weapon]
#		if weapon_node != null:
#			weapon_node.player_node = self
#			weapon_node.look_at(aim_point_pos, Vector3(0, 1, 0))
#			weapon_node.rotate_object_local(Vector3(0, 1, 0), deg2rad(180))
	
	current_weapon_name = "UNARMED"
	changing_weapon_name = "UNARMED"
	current_grenade = "Grenade"
	# refill grenades
	for grenade in grenade_amounts:
		grenade_amounts[grenade] = GRENADE_AMOUNT
	# Get the UI label so we can show our health and ammo, and get the flashlight spotlight XXX
	#ui_status_label = globals.get_current_scene().get_node(
	#		"HUD/MarginContainer/VBoxContainer/HBoxContainer2/ScoreInfos/WeaponAmmo")
	
	
	start_aiming_targets_transform = $AimingTargets/TargetRight.transform
	$RotationHelper/AnimatedModel/AnimationPlayer.play("Steer-Pose")

func _input(event):
	if event is InputEventMouseButton :
		if event.button_index == MOUSE_BUTTON_WHEEL_UP or event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
			if event.button_index == MOUSE_BUTTON_WHEEL_UP:
				mouse_scroll_value += MOUSE_SENSITIVITY_SCROLL_WHEEL
			elif event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
				mouse_scroll_value -= MOUSE_SENSITIVITY_SCROLL_WHEEL
			
			mouse_scroll_value = clamp(mouse_scroll_value, 0, WEAPON_NUMBER_TO_NAME.size()-1)
			
			if changing_weapon == false:
				if reloading_weapon == false:
					var round_mouse_scroll_value = clamp(int(round(mouse_scroll_value)), 0, weapons.size() - 1)
					if WEAPON_NUMBER_TO_NAME[round_mouse_scroll_value] != current_weapon_name:
						changing_weapon_name = WEAPON_NUMBER_TO_NAME[round_mouse_scroll_value]
						changing_weapon = true
						mouse_scroll_value = round_mouse_scroll_value


func _physics_process(delta):
#	process_input(delta)
	
	process_aim_weapon(delta)
	
	if grabbed_object == null:
		process_changing_weapons(delta)
		process_reloading(delta)
	
	process_ui(delta)


func process_input(delta):
	var weapon_change_number = WEAPON_NAME_TO_NUMBER[current_weapon_name]
	if Input.is_key_pressed(KEY_1):
		weapon_change_number = 0
	if Input.is_key_pressed(KEY_2):
		weapon_change_number = 1
	if Input.is_key_pressed(KEY_3):
		weapon_change_number = 2
	if Input.is_key_pressed(KEY_4):
		weapon_change_number = 3
	if Input.is_action_just_pressed("shift_weapon_positive"):
		weapon_change_number += 1
	if Input.is_action_just_pressed("shift_weapon_negative"):
		weapon_change_number -= 1
	weapon_change_number = clamp(weapon_change_number, 0, weapons.size() - 1)
	
	if changing_weapon == false:
		if reloading_weapon == false:
			if WEAPON_NUMBER_TO_NAME[weapon_change_number] != current_weapon_name:
				changing_weapon_name = WEAPON_NUMBER_TO_NAME[weapon_change_number]
				changing_weapon = true
				mouse_scroll_value = weapon_change_number
	
	if reloading_weapon == false:
		if changing_weapon == false:
			if Input.is_action_just_pressed("reload"):
				var current_weapon = weapons[current_weapon_name]
				if current_weapon != null:
					if current_weapon.CAN_RELOAD == true:
						var current_anim_state = animation_manager.fsm.current_state
						var is_reloading = false
						for weapon in weapons:
							var weapon_node = weapons[weapon]
							if weapon_node != null:
								if current_anim_state == weapon_node.RELOADING_ANIM_NAME:
									is_reloading = true
						if is_reloading == false:
							reloading_weapon = true
	
	if Input.is_action_pressed("fire"):
		if reloading_weapon == false:
			if changing_weapon == false:
				var current_weapon = weapons[current_weapon_name]
				if current_weapon != null:
					if current_weapon.ammo_in_weapon > 0:
						if animation_manager.fsm.current_state == current_weapon.IDLE_ANIM_NAME:
							animation_manager.set_animation(current_weapon.FIRE_ANIM_NAME)
					else:
						reloading_weapon = true
	
	if Input.is_action_just_pressed("change_grenade"):
		if current_grenade == "Grenade":
			current_grenade = "Sticky Grenade"
		elif current_grenade == "Sticky Grenade":
			current_grenade = "Grenade"
	
	if Input.is_action_just_pressed("fire_grenade"):
		if grenade_amounts[current_grenade] > 0:
			grenade_amounts[current_grenade] -= 1
			
			var grenade_clone
			if (current_grenade == "Grenade"):
				grenade_clone = grenade_scene.instantiate()
			elif (current_grenade == "Sticky Grenade"):
				grenade_clone = sticky_grenade_scene.instantiate()
				grenade_clone.player_body = self
			
			get_tree().add_child(grenade_clone)
			grenade_clone.global_transform = $RotationHelper/GrenadeTossPosition.global_transform
			grenade_clone.scaling = scale
			grenade_clone.apply_impulse(Vector3(0,0,0), 
					grenade_clone.global_transform.basis.z.normalized() * GRENADE_THROW_FORCE)
	
	if Input.is_action_just_pressed("fire") and current_weapon_name == "UNARMED":
		if grabbed_object == null:
			var state = get_world_3d().direct_space_state
			var ray_from = head.get_global_transform().origin 
			var ray_to = ray_from + head.get_global_transform().basis.z * OBJECT_GRAB_RAY_DISTANCE
			var ray_result = state.intersect_ray(ray_from, ray_to, [self, $RotationHelper/FirePoints/Knife/Area3D])
			if ray_result:
				if ray_result["collider"] is RigidBody3D:
					grabbed_object = ray_result["collider"]
					grabbed_object.mode = RigidBody3D.FREEZE_MODE_STATIC
					grabbed_object.collision_layer = 0
					grabbed_object.collision_mask = 0
		else:
			grabbed_object.mode = RigidBody3D.MODE_RIGID
			grabbed_object.apply_impulse(Vector3(0,0,0), 
					head.global_transform.basis.z.normalized() * OBJECT_THROW_FORCE)
			grabbed_object.collision_layer = 1
			grabbed_object.collision_mask = 1
			grabbed_object = null
	
	if grabbed_object != null:
		grabbed_object.global_transform.origin = (head.global_transform.origin + 
				(head.global_transform.basis.z.normalized() * OBJECT_GRAB_DISTANCE))


func process_aim_weapon(delta):
	if aim_enabled:
		var left = Input.is_action_pressed("aim_left")
		var right = Input.is_action_pressed("aim_right")
		var up = Input.is_action_pressed("aim_up")
		var down = Input.is_action_pressed("aim_down")
		# check if aiming is requested
		if (left or right or up or down or touch_touching):
			var yaw:float = 0.0
			var pitch:float = 0.0
			if !touch_touching:
				# keyboard & joystick
				yaw = Input.get_action_strength("aim_left") - Input.get_action_strength("aim_right")
				pitch = Input.get_action_strength("aim_down") - Input.get_action_strength("aim_up")
			else:
				# virtual analog & mouse
				yaw = -touch_aim_direction.x
				pitch = touch_aim_direction.y
			yaw *= aim_yaw_speed * delta
			pitch *= aim_pitch_speed * delta
			# clamp
			var new_rotation_y = rotation.y + yaw
			if new_rotation_y > aim_max_yaw_angle_rad:
				new_rotation_y = aim_max_yaw_angle_rad
			elif new_rotation_y < -aim_max_yaw_angle_rad:
				new_rotation_y = -aim_max_yaw_angle_rad
			var new_rotation_x = $RotationHelper.rotation.x + pitch
			if new_rotation_x > aim_max_pitch_angle_rad:
				new_rotation_x = aim_max_pitch_angle_rad
			elif new_rotation_x < -aim_max_pitch_angle_rad:
				new_rotation_x = -aim_max_pitch_angle_rad
			# actual rotation
			rotation.y = new_rotation_y
			$RotationHelper.rotation.x = new_rotation_x


func process_changing_weapons(delta):
	if changing_weapon == true:
		
		var weapon_unequipped = false
		var current_weapon = weapons[current_weapon_name]
		if current_weapon == null:
			weapon_unequipped = true
		else:
			if current_weapon.enabled == true:
				weapon_unequipped = current_weapon.unequip_weapon()
			else:
				weapon_unequipped = true
		
		if weapon_unequipped == true:
			
			var weapon_equiped = false
			var weapon_to_equip = weapons[changing_weapon_name]
			
			if weapon_to_equip == null:
				weapon_equiped = true
			else:
				if weapon_to_equip.enabled == false:
					weapon_equiped = weapon_to_equip.equip_weapon()
				else:
					weapon_equiped = true
			
			if weapon_equiped == true:
				changing_weapon = false
				current_weapon_name = changing_weapon_name
				changing_weapon_name = ""


func process_reloading(delta):
	if reloading_weapon == true:
		var current_weapon = weapons[current_weapon_name]
		if current_weapon != null:
			current_weapon.reload_weapon()
		reloading_weapon = false


func process_ui(delta):
	pass
#	ui_status_label.text = "Weapon: " + current_weapon_name
#
#	if (current_weapon_name != "UNARMED") and (current_weapon_name != "KNIFE"):
#		var current_weapon = weapons[current_weapon_name]
#		ui_status_label.text += "\nAMMO:" + str(current_weapon.ammo_in_weapon) + "/" + str(current_weapon.spare_ammo)
#
#	ui_status_label.text += "\n" + current_grenade + ": " + str(grenade_amounts[current_grenade])


func fire_weapon():
	if changing_weapon == true:
		return
	weapons[current_weapon_name].fire_weapon()


func add_ammo(additional_ammo):
	if (current_weapon_name != "UNARMED"):
		if (weapons[current_weapon_name].CAN_REFILL == true):
			weapons[current_weapon_name].spare_ammo += weapons[current_weapon_name].AMMO_IN_MAG * additional_ammo


func add_grenade(additional_grenade):
	grenade_amounts[current_grenade] += additional_grenade
	grenade_amounts[current_grenade] = clamp(grenade_amounts[current_grenade], 
			0, GRENADE_AMOUNT)


func reset_aiming_targets_transform():
	$AimingTargets/TargetRight.transform = start_aiming_targets_transform


func _on_VirtualAim_aim(direction):
	touch_aim_direction = direction


func _on_VirtualAim_aim_stop(touching):
	touch_touching = touching
	if not touching:
		touch_aim_direction = Vector2(0, 0)


func _set_aim_enabled(value):
	aim_enabled = value
#	$Controls/Aim.enabled = value


func _get_aim_enabled():
	return aim_enabled


func _set_enabled(value):
	enabled = value
	set_physics_process(enabled)


func _get_enabled():
	return enabled
