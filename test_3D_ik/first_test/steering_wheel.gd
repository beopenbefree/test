extends Node3D


var steer_angle = 0.0
var steer_speed = 2.0
const MAX_ABS_ANGLE = 80
const MIN_ABS_ANGLE = 2


func _physics_process(delta):
	var steer_left = Input.is_action_pressed("movement_left")
	var steer_right = Input.is_action_pressed("movement_right")
	if steer_left or steer_right:
		var left = 1 if steer_left else 0
		var right = 1 if steer_right else 0
		steer_angle += delta * steer_speed * (left - right)
	elif abs(steer_angle) >= deg_to_rad(MIN_ABS_ANGLE):
		steer_angle *= 0.95
	steer_angle = clamp(steer_angle, -deg_to_rad(MAX_ABS_ANGLE), deg_to_rad(MAX_ABS_ANGLE))
	rotation.z = steer_angle
