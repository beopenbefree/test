extends Node3D


var weapon_scene = preload("res://test_3D_ik/first_test/gun.tscn")


func _on_Button_pressed():
	$SteeringWheel/RemoteTarget_R.set_remote_node("")
	$AimingTargets/Target_R.transform = $AimingTargets/Target_R.start_transform
	$Arms/AnimationPlayer.play("pistol_arming")


func _take_weapon():
	var weapon = weapon_scene.instantiate()
	weapon.scale = Vector3.ONE * 10
	weapon.position = Vector3(0, 1.5, 0)
	weapon.rotation = Vector3(deg_to_rad(10), deg_to_rad(165), deg_to_rad(65))
	$AimingTargets/Target_R.add_child(weapon)
