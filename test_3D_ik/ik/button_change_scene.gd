extends Button

@export var scene_to_change_to = null # (String, FILE)


func _ready():
	# warning-ignore:return_value_discarded
	connect("pressed", Callable(self, "change_scene_to_file"))


func change_scene_to_file():
	if scene_to_change_to != null:
		# warning-ignore:return_value_discarded
		get_tree().change_scene_to_file(scene_to_change_to)
