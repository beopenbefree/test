extends MarginContainer


@onready var number_label = $HBoxContainer/Bars/LifeBar/Count/Background/Number
@onready var bar = $HBoxContainer/Bars/LifeBar/Gauge
@onready var tween = $Tween

var animated_health: float = 0.0


func _ready():
	bar.max_value = $"../Characters/Player".max_health
	number_label.text = str(bar.max_value)
	animated_health = bar.max_value


func _process(delta):
	var round_value = round(animated_health)
	number_label.text = str(round_value)
	bar.value = round_value


func _on_Player_health_changed(health):
	_update_health(health)


func _update_health(value):
	tween.interpolate_property(self, "animated_health", animated_health, value, 0.6)
	if not tween.is_active():
		tween.start()


func _on_Player_died():
	var start_color = Color(1.0, 1.0, 1.0, 1.0)
	var end_color = Color(1.0, 1.0, 1.0, 0.0)
	tween.interpolate_property(self, "modulate", start_color, end_color, 1.0)
