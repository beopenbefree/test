extends VBoxContainer


var toggle_delay: Timer = null
var forward: bool = true


func _ready():
	toggle_delay = Timer.new()
	toggle_delay.one_shot = true
	toggle_delay.wait_time = 5.0
	add_child(toggle_delay)
	toggle_delay.connect("timeout", Callable(self, "_on_toggle_delay_timeout"))
	toggle_delay.start()


func _on_toggle_delay_timeout():
	if forward:
		$TextureProgressBar/Tween.interpolate_property($TextureProgressBar, "value", 0.0, 100.0, 3)
	else:
		$TextureProgressBar/Tween.interpolate_property($TextureProgressBar, "value", 100.0, 0.0, 1)
	$TextureProgressBar/Tween.start()
	forward = !forward


func _on_Tween_tween_completed(object, key):
	toggle_delay.start()
