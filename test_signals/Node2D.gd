extends Node2D


func _ready():
	$Timer.connect("timeout", Callable(self, "_on_Timer_timeout"))


func _on_Timer_timeout():
	$Sprite2D.visible = not $Sprite2D.visible

