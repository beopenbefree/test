extends Button


var vsync = true


func _on_pressed():
	vsync = not vsync
	if vsync:
		text = "VSYNC ON"
		DisplayServer.window_set_vsync_mode(DisplayServer.VSYNC_ENABLED)
	else:
		text = "VSYNC OFF"
		DisplayServer.window_set_vsync_mode(DisplayServer.VSYNC_DISABLED)
