extends Node3D


@export var target_radius: float = 1.0
@export var slow_radius: float = 10.0
@export var min_angle_turn = 5.0 # (float, 0.0, 15.0, 0.5)
@export var current_sound_name: String = ""

var vehicle_body:VehicleAI = null
var rabbit:Node3D = null

var cos_limit:float = 0.0
var one_minus_cos_limit_inv:float = 1.0
var max_cos_turn:float = 0.0
var max_cos_turn_inv:float = 0.0


func _ready():
	vehicle_body = get_node("VehicleAI/Body")
	rabbit = get_node("Path3D/PathFollow3D/Rabbit")
	# put vehicle at rabbit location
	vehicle_body.global_transform.origin = rabbit.global_transform.origin
	# get steering limit
	cos_limit = cos(deg_to_rad(45.0))
	one_minus_cos_limit_inv = 1.0 / (1 - cos_limit)
	# set min angle to start turning
	max_cos_turn = cos(deg_to_rad(min_angle_turn))
	max_cos_turn_inv = 1.0 / max_cos_turn
	# play engine sound for vehicle body
	vehicle_body.play_sound_engine(current_sound_name)


func _physics_process(delta):
	vehicle_body.action_pressed_turn = false
	vehicle_body.action_strength_turn = 0.0
	vehicle_body.action_pressed_motion = false
	vehicle_body.action_strength_motion = 0.0
	vehicle_body._set_current_brake(0.0)
	# compute the direction/distance to the target.
	var direction = rabbit.global_transform.origin - vehicle_body.global_transform.origin
	var distance = direction.length()
	direction = direction.normalized()
	
	# compute steering
	var vehicle_direction = vehicle_body.global_transform.basis.z
	var looking_at_diff_cos = vehicle_direction.dot(direction)
	if looking_at_diff_cos <= max_cos_turn:
		# turn toward rabbit
		vehicle_body.action_pressed_turn = true
		if vehicle_direction.cross(direction).y >= 0.0:
			if looking_at_diff_cos >= cos_limit:
				vehicle_body.action_strength_turn = (max_cos_turn - looking_at_diff_cos) * one_minus_cos_limit_inv
			else:
				vehicle_body.action_strength_turn = 1.0
		else:
			if looking_at_diff_cos >= cos_limit:
				vehicle_body.action_strength_turn = (looking_at_diff_cos - max_cos_turn) * one_minus_cos_limit_inv
			else:
				vehicle_body.action_strength_turn = -1.0
	
	# compute movement
	if distance < target_radius:
		vehicle_body._set_current_brake(vehicle_body.brake_size)
	# Otherwise calculate a scaled speed.
	elif distance < slow_radius:
		vehicle_body.action_pressed_motion = true
		vehicle_body.action_strength_motion = -1.0
	# If we are outside the slowRadius, then move at max speed.
	else:
		# distance >= slow_radius
		vehicle_body.action_pressed_motion = true
		vehicle_body.action_strength_motion = 1.0
