extends VehicleBody3D


class_name VehicleAI


@export var enabled: bool = true: get = _get_enabled, set = _set_enabled

@export var steer_speed = 1.5 # (float, 0.5, 5.0, 0.1)
@export var steer_limit = 0.4 # (float, 0.0, 1.0, 0.05)
@export var tilt_angle_max = 25 # (float, 10.0, 60.0, 1.0)
@export var tilt_engine_force_factor = 1.0 # (float, 1.0, 3.0, 0.1)
@export var accel_steer_speed = 1.0 # (float, 0.5, 2.0, 0.05)
@export var engine_force_value = 40 # (float, 2.0, 200.0, 1.0)
@export var drag_force_factor = 20.0 # (float, 1.0, 120.0, 0.5)
@export var lift_force: Vector3 = Vector3(0.0, 500.0, 0.0)
@export var lift_force_position: Vector3 = Vector3.ZERO

@export var gear_max_speed1 = 3.0 # (float, 1.0, 20.0, 0.25)
@export var gear_max_speed2 = 6.0 # (float, 1.0, 40.0, 0.25)
@export var gear_max_speed3 = 9.0 # (float, 1.0, 60.0, 0.5)
@export var gear_max_speed4 = 12.0 # (float, 1.0, 80.0, 0.5)
@export var gear_speed_factor = 0.3 # (float, 0.1, 1.0, 0.05)
@export var gear_engine_factor = 0.4 # (float, 0.01, 4.0, 0.01)
@export var gear_max_low_factor = 0.9 # (float, 0.8, 1.0, 0.001)

@export var steering_wheel_angle_max = 80 # (float, 30, 90.0, 1.0)
@export var steering_wheel_scale: Vector3 = Vector3.ONE

@export var MAX_NO_DAMAGE_SPEED = 5.0 # m/s # (float, 1.0, 10.0, 0.5)
@export var DAMAGE_SPEED_FACTOR = 0.5 # (float, 0, 1, 0.01)

var motion_target:float = 0
var steer_target:float = 0
var touch_move_direction:Vector2 = Vector2.ZERO
var touch_steer_direction:Vector2 = Vector2.ZERO
var accel_steer:float = 0
const max_accel_steer:float = 9.81 # Android accelerometer's component max value
const max_accel_steer_inv:float = 1.0 / max_accel_steer
var brake_size:float = 1.0
# boost area stuff
var engine_force_boost:float = 1.0
var boost_timer:Timer
# blast data
var do_blast:bool = false
var blast_impulse:Vector3 = Vector3.ZERO
const BLAST_TORQUE_IMPULSE:float = 50.0
# lift data
var lift_up:bool = false
# sound data for gears
var current_sound:Node3D = null
# steering wheel stuff
var steering_wheel_node:Node3D = null
var steering_wheel_angle_factor:float = 0.0

# AI: when used as target
var velocity:Vector3 = Vector3.ZERO

# current data
@onready var shooter_node:Shooter = find_child("Shooter")
@onready var engine_force_value_backward:float = engine_force_value * 0.4
@onready var drag_force_correction = drag_force_factor * 1e-5
@onready var rolling_resistance_correction:float = drag_force_factor * 30.0 * 1e-5
@onready var tilt_max:float = sin(deg_to_rad(tilt_angle_max))
@onready var tilt_engine_force_factor_backward:float = tilt_engine_force_factor * 0.8
@onready var current_speed:float = 0.0
@onready var current_gear:String = "0"
@onready var changing_gear:bool = false
@onready var gear_max_low_speed1:float = gear_max_low_factor * gear_max_speed1
@onready var gear_max_low_speed2:float = gear_max_low_factor * gear_max_speed2
@onready var gear_max_low_speed3:float = gear_max_low_factor * gear_max_speed3
@onready var gear_max_low_speed4:float = gear_max_low_factor * gear_max_speed4

# AI
var action_pressed_turn:bool = false
var action_strength_turn:float = 0.0
var action_pressed_motion:bool = false
var action_strength_motion:float = 0.0


func _ready():
	# boost area stuff
	boost_timer = Timer.new()
	boost_timer.connect("timeout", Callable(self, "_on_boost_timer_timeout"))
	boost_timer.one_shot = true
	add_child(boost_timer)
	# compute brake size
	brake_size = mass / 1000.0 * 30.0
	# get current engine sound
	current_sound = globals.get_sound(globals.current_vehicle_sound, self)
	# connect body_entered signal
	connect("body_entered", Callable(self, "_on_Body_body_entered"))
	# steering wheel node (if any)
	steering_wheel_node = get_node("SteeringWheel")
	if steering_wheel_node:
		steering_wheel_angle_factor = ((1.0 / steer_limit) * 
				deg_to_rad(steering_wheel_angle_max))
		var scaling = Vector3.ONE * steering_wheel_scale
		steering_wheel_node.scale = scaling
	# map target
	var map_target = get_node("MapTarget")
	if map_target:
		map_target.show()
	# enablers' side effects
	_set_enabled(enabled)


func _physics_process(delta):
	# reset targets
	steer_target = 0.0
	motion_target = 0.0
	if action_pressed_turn:
		steer_target = action_strength_turn
	steer_target *= steer_limit
	
	var speed = linear_velocity.length()
	var speed_inv = 1.0 / speed if speed != 0.0 else INF
	# AI
	velocity = linear_velocity
	var motion_direction = linear_velocity.dot(transform.basis.z)
	engine_force = 0
	
	if not changing_gear:
		if action_pressed_motion:
			motion_target = action_strength_motion
		engine_force = motion_target * engine_force_boost
		if motion_target > 0.0:
			engine_force *= engine_force_value
			if get_tilt() > tilt_max:
				engine_force *= max(10.0 * speed_inv, 
						tilt_engine_force_factor)
		else:
			engine_force *= engine_force_value_backward
			if get_tilt() < -tilt_max:
				engine_force *= max(8.0 * speed_inv, 
						tilt_engine_force_factor_backward)
	
	# motion resistance = drag force + rolling resistance
	var drag_force = pow(speed, 2.0) * drag_force_correction
	var rolling_resistance = speed * rolling_resistance_correction
	linear_damp = drag_force + rolling_resistance
	
	steering = move_toward(steering, steer_target, steer_speed * delta)
	
	# turn the steering wheel
	if steering_wheel_node:
		steering_wheel_node.update(steering * steering_wheel_angle_factor)
	
	# set engine sound pitch
	if current_sound:
		var pitch_scale = 1.0
		# (simple) fsm to change gears
		if current_gear == "0":
			if speed > 0.15:
				if motion_direction < 0:
					_change_gear("reverse")
				else:
					_change_gear("1")
		elif current_gear == "reverse":
			if motion_direction > 0:
				_change_gear("0")
			pitch_scale = 1 + gear_speed_factor * speed + gear_engine_factor
		elif current_gear == "1":
			if speed > gear_max_speed1:
				_change_gear("2")
			elif speed < 0.15:
				_change_gear("0")
			pitch_scale = 1 + gear_speed_factor * speed + gear_engine_factor
		elif current_gear == "2":
			if speed > gear_max_speed2:
				_change_gear("3")
			elif speed < gear_max_low_speed1:
				_change_gear("1")
			pitch_scale = 1 + gear_speed_factor * (speed - gear_max_speed1) + gear_engine_factor
		elif current_gear == "3":
			if speed > gear_max_speed3:
				_change_gear("4")
			elif speed < gear_max_low_speed2:
				_change_gear("2")
			pitch_scale = 1 + gear_speed_factor * (speed - gear_max_speed2) + gear_engine_factor
		elif current_gear == "4":
			if speed > gear_max_speed4:
				_change_gear("5")
			elif speed < gear_max_low_speed3:
				_change_gear("3")
			pitch_scale = 1 + gear_speed_factor * (speed - gear_max_speed3) + gear_engine_factor
		elif current_gear == "5":
			if speed < gear_max_low_speed4:
				_change_gear("4")
			pitch_scale = 1 + gear_speed_factor * (speed - gear_max_speed4) + gear_engine_factor
		current_sound.set_pitch_scale(pitch_scale)
	current_speed = speed
	
	# NOTE: _integrate_forces() not used (TODO)
	if do_blast:
		apply_central_impulse(blast_impulse)
		apply_torque_impulse(Vector3(0.0, abs(randf()), 0.0) * BLAST_TORQUE_IMPULSE)
		do_blast = false
	if lift_up:
		apply_force(lift_force_position, lift_force)


func _input(event):
	if event.is_action_pressed("brake"):
		_set_current_brake(brake_size)
	elif event.is_action_released("brake"):
		_set_current_brake(0.0)


func get_tilt():
	var forward  = get_global_transform().basis.z
	var up = Vector3(0, 1.0, 0)
	return up.dot(forward)


func apply_accelerometer(acc):
	accel_steer = acc.x * accel_steer_speed
	accel_steer = clamp(accel_steer, -max_accel_steer, 
			max_accel_steer)
	var x = accel_steer * max_accel_steer_inv
	var y = sqrt(1.0 - x * x)
	touch_steer_direction = Vector2(x, y)


func apply_boost(factor, duration):
	if (not boost_timer.is_stopped()) or (factor == 0.0):
		return
	
	engine_force_boost = factor
	boost_timer.start(duration)
	add_health(factor)


func apply_blast(magnitude):
	# blast direction 
	var y = global_transform.basis.y.y * randf()
	var impulse_dir = Vector3(randf(), y, randf()).normalized()
	blast_impulse = impulse_dir * magnitude * mass
	do_blast = true
	add_health(-magnitude)


func bullet_hit(damage, bullet_global_transform):
	if damage:
		# subtract -damage to scene's current_count
		add_health(-damage)
	# play sound
	globals.play_sound("vehicle_collision_dynamic", null, true, false, 
			bullet_global_transform.origin)


func rocket_hit(damage, rocket_global_transform):
	if damage:
		# subtract -damage to scene's current_count
		add_health(-damage)
	# play sound
	globals.play_sound("vehicle_collision_dynamic", null, true, false, 
			rocket_global_transform.origin)


func raycast_hit(damage, hit_position):
	if damage:
		# subtract -damage to scene's current_count
		add_health(-damage)
	# play sound
	globals.play_sound("vehicle_collision_dynamic", null, true, false, 
			hit_position)


func add_health(delta):
	var scene = globals.get_current_scene()
	scene.current_count += delta
	clamp(scene.current_count, -1, scene.total_count_down)


func play_sound_engine(current_sound_name):
	globals.play_sound(current_sound_name, self, true, true)
	current_sound = globals.get_sound(current_sound_name, self)


func _on_boost_timer_timeout():
	engine_force_boost = 1.0


func _on_VirtualSteer_steer(direction):
	touch_steer_direction = direction


func _on_VirtualSteer_steer_stop(touching):
	if not touching:
		touch_steer_direction = Vector2(0, 0)


func _on_VirtualMove_move(direction):
	touch_move_direction = direction


func _on_VirtualMove_move_stop(touching):
	if not touching:
		touch_move_direction = Vector2(0, 0)


func _on_VirtualBrake_brake(enabled):
	brake = enabled


func _on_Body_body_entered(body:PhysicsBody3D):
	# damage if too speedy
	var damage = 0.0
	if current_speed > MAX_NO_DAMAGE_SPEED:
		damage = (current_speed - MAX_NO_DAMAGE_SPEED) * DAMAGE_SPEED_FACTOR
	# physics body: environment
	if body.collision_layer == 0b1:
		if ("mass" in body) and (body.mode == RigidBody3D.MODE_RIGID):
			# body is a (dynamic) RigidBody
			damage *= body.mass * DAMAGE_SPEED_FACTOR
		add_health(-damage)
		globals.play_sound("vehicle_collision_static", globals.current_vehicle)
	# static body: turret
	elif body.collision_layer == 0b100000:
		body.bullet_hit(damage, global_transform)
		add_health(-damage)
	# ai vehicle (body & trailer)
	elif body.collision_layer & 0b11000:
		body.bullet_hit(damage, global_transform)
		add_health(-damage)


func _set_current_brake(value):
	brake = value


func _change_gear(gear):
	current_gear = gear
	changing_gear = true
	await get_tree().create_timer(0.2).timeout
	changing_gear = false


func _set_enabled(value):
	enabled = value
	set_physics_process(enabled)
	set_process_input(enabled)


func _get_enabled():
	return enabled


#<HACK
class Globals extends RefCounted:
	class Scene extends RefCounted:
		var current_count = 0
		var total_count_down = 1
	var scene = Scene.new()
	var current_vehicle_sound
	var current_vehicle
	func get_sound(a=null, b=null, c=null, d=null, e=null):
		pass
	func play_sound(a=null, b=null):
		pass
	func get_current_scene():
		return scene
@onready var globals = Globals.new()
#HACK>
