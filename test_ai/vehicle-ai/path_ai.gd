extends Path3D


var time_elapsed = 0.0
const max_time = 60.0


func _physics_process(delta):
	time_elapsed += delta
	if time_elapsed > max_time:
		time_elapsed = 0.0
	var alfa = time_elapsed / max_time
	$PathFollow3D.set_progress_ratio(alfa)
