extends RigidBody3D

class_name Bullet


@export var impulse_size = 50.0 # (float, 0.0, 200.0, 1.0)
@export var DAMAGE = 10 # (int, 0, 100, 1)
@export var scaling: Vector3 = Vector3.ONE: set = _set_scaling
@export var collisions = 0b0 # (int, LAYERS_3D_PHYSICS)

var direction:Vector3 = Vector3()
var firing:bool = false

const KILL_TIMER:float = 4.0
var timer:float = 0
var hit_something:bool = false

#var bullet_hole_scene:PackedScene = preload("res://levels/elements/bullet_hole.tscn")
var decal_added:bool = false


func _ready():
	# connect body_entered signal
	connect("body_entered", Callable(self, "_on_Body_body_entered"))
	# set collision mask
	collision_mask = collisions


func _integrate_forces(state):
	if firing:
		apply_impulse(impulse_size * direction, Vector3(0.0, 0.0, 0.0))
		firing = false
	
	timer += state.step
	if timer > KILL_TIMER:
		queue_free()


func fire(imp_size:float, dir:Vector3):
	impulse_size = imp_size
	direction = dir.normalized()
	firing = true


func _on_Body_body_entered(body:PhysicsBody3D):
	if hit_something == false:
		if body.has_method("bullet_hit"):
			body.bullet_hit(DAMAGE, global_transform)
	hit_something = true
	if body.collision_layer != 0b1:
		# NO collided with static body: environment
		queue_free()
	if not decal_added:
		# add bullet hole decal (using decalco)
#		var bullet_hole = bullet_hole_scene.instance()
#		body.add_child(bullet_hole)
#		bullet_hole.global_transform = global_transform
#		decal_added = true
		pass


func _set_scaling(value):
	$MeshInstance3D.scale = value
	$CollisionShape3D.scale = value
