extends Node3D

class_name Turret


@export var enabled: bool = true: get = _get_enabled, set = _set_enabled

@export (bool) var use_raycast = false

@export var fire_enabled: bool = true: get = _get_fire_enabled, set = _set_fire_enabled

@export var vision_area_radius = 36.0: get = _get_vision_area_radius, set = _set_vision_area_radius # (float, 10, 100, 0.1)

@export var DAMAGE_BULLET: int = 20
@export var DAMAGE_RAYCAST: int = 5

@export var BULLET_IMPULSE_RANGE: Array = [50.0, 150.0]
@export var BULLET_MASS_RANGE: Array = [1.0, 10.0]

#<AI
@export var ai_enabled: bool = false
@export var ai_dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)
@export var ai_damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var ai_max_angular_acceleration: float = 5.0
@export var ai_max_rotation: float = 5.0
@export var ai_target_radius: float = 5.0 # degrees
@export var ai_slow_radius: float = 45.0 # degrees
@export var ai_time_to_target: float = 0.1

var ai_face: AI.Face
var basis = Basis(Vector3.UP, deg_to_rad(180.0))
#AI>

const FLASH_TIME: float = 0.1
var flash_timer: float = 0

const FIRE_TIME: float = 0.8
var fire_timer: float = 0

var node_turret_head: Node = null
var node_raycast: Node = null
var node_flash_one: Node = null
var node_flash_two: Node = null

var ammo_in_turret: int = 20
const AMMO_IN_FULL_TURRET: int = 20
const AMMO_RELOAD_TIME: int = 4
var ammo_reload_timer: float = 0

var is_active: bool = false

var current_target: Node = null
var visibility_check: RayCast3D = null
var visibility_check_position: Vector3
var visible_target: Marker3D = null

var smoke_particles: Particles = null

const MAX_TURRET_HEALTH: int = 60
var turret_health: int = MAX_TURRET_HEALTH

const DESTROYED_TIME: int = 20
var destroyed_timer: float = 0

var bullet_scene = preload("res://test_ai/turret-ai/bullet.tscn")


func _ready():

	$VisionArea.connect("body_entered", Callable(self, "_body_entered_vision"))
	$VisionArea.connect("body_exited", Callable(self, "_body_exited_vision"))

	node_turret_head = $Head
	node_raycast = $Head/RayCast3D
	node_flash_one = $Head/Flash
	node_flash_two = $Head/Flash_2
	
	node_raycast.add_exception(self)
	node_raycast.add_exception($Base/StaticBody3D)
	node_raycast.add_exception($Head/StaticBody3D)
	node_raycast.add_exception($VisionArea)
	node_raycast.target_position = Vector3(0, 0, -vision_area_radius)
	
	node_flash_one.visible = false
	node_flash_two.visible = false
	
	smoke_particles = $Smoke
	smoke_particles.emitting = false
	
	turret_health = MAX_TURRET_HEALTH
	
	# re-add vision collision shape taking into account any scaling effect
	var sphere_shape = SphereShape3D.new()
	sphere_shape.radius = vision_area_radius
	var new_collision_shape = CollisionShape3D.new()
	new_collision_shape.shape = sphere_shape
	$VisionArea.add_child(new_collision_shape)
	$VisionArea/CollisionShape3D.queue_free()
	#<AI
	if ai_enabled:
		ai_face = AI.Face.new()
		ai_face.setup(ai_max_angular_acceleration, ai_max_rotation, 
				deg_to_rad(ai_target_radius), deg_to_rad(ai_slow_radius),
				ai_time_to_target, ai_damping, ai_dims)
		_ai_initialize()
	#AI>
	# enablers' side effects
	_set_fire_enabled(fire_enabled)
	_set_enabled(enabled)
	# optimization
	set_physics_process(false)


func _physics_process(delta):
	if flash_timer > 0:
		flash_timer -= delta
	
		if flash_timer <= 0:
			node_flash_one.visible = false
		node_flash_two.visible = false
	
	if turret_health <= 0.0:
		if destroyed_timer > 0:
			destroyed_timer -= delta
			return
		else:
			turret_health = MAX_TURRET_HEALTH
			smoke_particles.emitting = false
	
	if (current_target != null) and (visible_target != null):
		# check if current_target is really visible
		visibility_check.target_position = visible_target.global_transform.origin - visibility_check_position
		if (visibility_check.is_colliding()):
			
			if not (visibility_check.get_collider() == current_target):
				return
			
			#<AI
			if ai_enabled:
				_ai_pre_update(delta)
				_ai_update(delta)
				_ai_post_update(delta)
			else:
			#AI>
				node_turret_head.look_at(visible_target.global_transform.origin, Vector3.UP)
			
			if fire_enabled:
				if turret_health> 0:
					
					if ammo_in_turret > 0:
						if fire_timer > 0:
							fire_timer -= delta
						else:
							fire()
					else:
						if ammo_reload_timer > 0:
							ammo_reload_timer -= delta
						else:
							ammo_in_turret = AMMO_IN_FULL_TURRET


func fire():
	if use_raycast == true:
		node_raycast.DAMAGE = DAMAGE_RAYCAST
#		node_raycast.shoot(visible_target) HACK
		node_raycast.shoot($Head/BarrelEnd)#HACK
		# emit sound
		globals.play_sound("gun_shot", null, true, false, node_raycast.global_transform.origin)
		
		ammo_in_turret -= 1
	else:
		var bullet = bullet_scene.instantiate()
		bullet.DAMAGE = DAMAGE_BULLET
		bullet.scaling *= 2.0
		bullet.collisions = 0b111
		#var scene_root = get_tree().root.get_children()[0]
#		globals.get_current_scene().add_child(bullet) HACK
		get_tree().root.add_child(bullet)#HACK
		bullet.mass = randf_range(BULLET_MASS_RANGE[0], BULLET_MASS_RANGE[1])
		
		bullet.global_transform = $Head/BarrelEnd.global_transform
		var impulse_size = randf_range(BULLET_IMPULSE_RANGE[0], BULLET_IMPULSE_RANGE[1])
		bullet.fire(impulse_size, bullet.global_transform.basis.z)
		# emit sound
		globals.play_sound("cannon_shot", null, true, false, bullet.global_transform.origin)
		
		ammo_in_turret -= 1
		
	node_flash_one.visible = true
	node_flash_two.visible = true
	
	flash_timer = FLASH_TIME
	fire_timer = FIRE_TIME
	
	if ammo_in_turret <= 0:
		ammo_reload_timer = AMMO_RELOAD_TIME


func bullet_hit(damage, bullet_global_transform):
	if enabled:
		if damage:
			turret_health -= damage
		
		if turret_health <= 0:
			smoke_particles.emitting = true
			destroyed_timer = DESTROYED_TIME
		# play sound
		globals.play_sound("vehicle_collision_static", null, true, false, 
				bullet_global_transform.origin)


func rocket_hit(damage, rocket_global_transform):
	if enabled:
		if damage:
			turret_health -= damage
		
		if turret_health <= 0:
			smoke_particles.emitting = true
			destroyed_timer = DESTROYED_TIME
		# play sound
		globals.play_sound("vehicle_collision_static", null, true, false, 
				rocket_global_transform.origin)


func raycast_hit(damage, hit_position):
	if enabled:
		if damage:
			turret_health -= damage
		
		if turret_health <= 0:
			smoke_particles.emitting = true
			destroyed_timer = DESTROYED_TIME
		# play sound
		globals.play_sound("vehicle_collision_static", null, true, false, 
				hit_position)


func _body_entered_vision(body):
	if enabled:
		if current_target == null:
#			if (body is RigidBody) and (body.collision_layer == 0b10): HACK
			if true:#HACK
				current_target = body
				set_physics_process(true)
				# create a visibility check and place it at turret's lookout global position
				visibility_check = RayCast3D.new()
#				globals.get_current_scene().add_child(visibility_check) HACK
				get_tree().root.add_child(visibility_check)#HACK
				
				# remove any turret's scaling effect
				var pos = $Head/BarrelEnd/LookOut.global_transform.origin
				var scale = $Head/BarrelEnd/LookOut.global_transform.basis.get_scale()
				pos = Vector3(pos.x / scale.x, pos.y / scale.y, pos.z / scale.z)
				visibility_check.global_transform.origin = pos
				visibility_check_position = pos
				
				# get visible target
				visible_target = current_target.get_node("VisibleTarget")
				visibility_check.collision_mask = 0b111
				visibility_check.enabled = true


func _body_exited_vision(body):
	if enabled:
		if current_target != null:
			if body == current_target:
				current_target = null
				set_physics_process(false)
				
				flash_timer = 0
				fire_timer = 0
				node_flash_one.visible = false
				node_flash_two.visible = false
				#
				visibility_check.enabled = false
				visible_target = null
				visibility_check.queue_free()


#<AI
func _ai_initialize():
	var character_position = node_turret_head.global_transform.origin
	if ai_dims == AI_TOOLS.Dimensions.TWO:
		ai_face.character.position = Vector2(character_position.x,
				character_position.z)
	else:
		ai_face.character.position = character_position
	ai_face.character.orientation = AI_TOOLS.map_to_range(
			node_turret_head.global_transform.basis.get_euler().y)


func _ai_pre_update(delta):
	# steering depends on: target.position, character.position
	var position = visible_target.global_transform.origin
	var character_position = node_turret_head.global_transform.origin
	if ai_dims == AI_TOOLS.Dimensions.TWO:
		ai_face.target.position = Vector2(position.x, position.z)
		ai_face.character.position = Vector2(character_position.x,
				character_position.z)
	else:
		ai_face.target.position = position
		ai_face.character.position = character_position


func _ai_update(delta):
	var output:AI.SteeringOutput = ai_face.get_steering()
	ai_face.character.update(output, delta)


func _ai_post_update(delta):
	node_turret_head.global_transform.basis = basis.rotated(Vector3.UP, 
			ai_face.character.orientation)
#AI>


func _set_fire_enabled(value):
	fire_enabled = value


func _get_fire_enabled():
	return fire_enabled


func _set_vision_area_radius(value):
	vision_area_radius = value


func _get_vision_area_radius():
	return vision_area_radius


func _set_enabled(value):
	enabled = value
	set_physics_process(enabled)


func _get_enabled():
	return enabled

#<HACK
class Globals extends RefCounted:
	class Scene extends RefCounted:
		var current_count = 0
		var total_count_down = 1
	var scene = Scene.new()
	var current_vehicle_sound
	var current_vehicle
	func get_sound(a=null, b=null, c=null, d=null, e=null):
		pass
	func play_sound(a=null, b=null, c=null, d=null, e=null):
		pass
	func get_current_scene():
		return scene
@onready var globals = Globals.new()
#HACK>
