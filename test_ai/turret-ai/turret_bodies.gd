extends StaticBody3D


@export (NodePath) var path_to_turret_root

var turret_root:Node = null


func _ready():
	turret_root = get_node(path_to_turret_root)


func bullet_hit(damage, bullet_global_transform):
	if path_to_turret_root != null:
		turret_root.bullet_hit(damage, bullet_global_transform)


func rocket_hit(damage, rocket_global_transform):
	if path_to_turret_root != null:
		turret_root.rocket_hit(damage, rocket_global_transform)


func raycast_hit(damage, hit_position):
	if path_to_turret_root != null:
		turret_root.raycast_hit(damage, hit_position)
