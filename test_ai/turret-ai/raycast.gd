extends RayCast3D

class_name ShootRayCast


@export var shot_trail_speed_scale = 1.0 # (float, 0.0, 20.0, 0.05)

var DAMAGE:int = 10
@onready var shoot_trail: Particles = $ShotTrail


func _ready():
	shoot_trail.emitting = false
	shoot_trail.one_shot = true
	shoot_trail.speed_scale = shot_trail_speed_scale


func shoot(target):
	look_at(target.global_transform.origin, Vector3(0, 1, 0))
	var end_start_scale = 1
	force_raycast_update()
	if is_colliding():
		var body = get_collider()
		var collision_pos = get_collision_point()
		end_start_scale = min((collision_pos - global_transform.origin).length() * 0.01, 1.0)
		if body.has_method("raycast_hit"):
			body.raycast_hit(DAMAGE, collision_pos)
	# emit shoot particles
	emit_shot_trail(end_start_scale)


func emit_shot_trail(end_start_scale):
	shoot_trail.scale.z = end_start_scale
	shoot_trail.emitting = true	
