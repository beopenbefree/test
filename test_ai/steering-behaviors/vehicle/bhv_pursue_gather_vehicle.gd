extends Node3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var target_scene: NodePath = "../../../Target"

@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)
@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var epsilon: float = 1.0e-3
@export var max_acceleration: float = 5.0
@export var max_rotation: float = 2.0
#
@export var speed: float = 5.0
#
@export var placement_boundary: float = 50.0

var previous_target: Node3D = null
var character: Node = null

var objects_scene = preload("res://test_ai/steering-behaviors/objects.tscn")


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		randomize()
		var objects = objects_scene.instantiate()
		add_child(objects)
		objects.global_transform.origin += Vector3(0,-0.5,0)
		# create the characters
		character = $PrioritySteeringVehicle
		character.enabled = _get_enabled()
		character.damping = damping
		character.dims = dims
		character.max_acceleration = max_acceleration
		character.max_rotation = max_rotation
		character.max_speed = speed
		# place it randomly
		var x = AI_TOOLS.random_binomial() * placement_boundary
		var z = AI_TOOLS.random_binomial() * placement_boundary
		character.transform.origin = Vector3(x, 2, z)
		add_child(character)
		character.target = get_node(target_scene)
		#
		# complete the inner behaviors' configuration
		# create a common collision detector: for obstacle avoidance behaviors
		var collision_detector = AI.CollisionDetector.new()
		collision_detector.collision_mask = 0x10000
		add_child(collision_detector.detector)
		# iterate over all inner behaviors' of the characters
		var all_targets = []
		for group in character.priority_steering.groups:
			for bhv_wght in group.behaviors:
				var bhv = bhv_wght.behavior
				# check if collision_detector is needed
				if "detector" in bhv:
					bhv.detector = collision_detector
				# append bhv's character to all targets
				all_targets.append(bhv.character)
		# if needed, assign to behavior its targets: i.e. the others characters 
		for group in character.priority_steering.groups:
			for bhv_wght in group.behaviors:
				var bhv = bhv_wght.behavior
				# check if targets is needed
				if "targets" in bhv:
					var targets = []
					for other in all_targets:
						if other != bhv.character:
							targets.append(other)
					bhv.targets = targets
		#
		var gather_area = $PrioritySteeringVehicle/CarBaseAI/Body/GatherArea
		gather_area.connect("area_entered", Callable(self, "_on_Area_area_entered"))
		gather_area.connect("area_exited", Callable(self, "_on_Area_area_exited"))
		previous_target = character.target


func object_gathered():
	character.target = previous_target


func _on_Area_area_entered(area):
	character.target = area.get_parent().get_parent()


func _on_Area_area_exited(area):
	character.target = previous_target


func _set_enabled(value):
	enabled = value
	visible = value
	for child in get_children():
		if "visible" in child:
			child.visible = value
		if "enabled" in child:
			child.enabled = value


func _get_enabled():
	return enabled
