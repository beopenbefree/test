extends Node3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var target_scene: NodePath = "../../../Target"
@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var evade: bool = false

@export var signal_enabled: bool = false
@export var max_acceleration: float = 5.0
@export var max_prediction: float = 1.0
@export var max_speed: float = -1.0

# vehicle's parameters
@export var aggressiveness = 1.0 # (float, 0.01, 1.2, 0.01)
# AI4G: 3.8.3 COMMON ACTUATION PROPERTIES (pp. 175-177)
@export var forward_arc_max_angle_deg = 145.0 # (float, 0.0, 180.0, 1.0)
@export var arc_dec_factor_by_speed = 250 # (float, 100, 1000, 50)
# HACK: to prevent the steering wheels from wagging
@export var turn_min_angle = 5.0 # (float, 0.0, 15.0, 0.5)
@export var turn_max_angle: float = 45.0

var pursue_evade:AI.PursueEvade
var steering_output:AI.SteeringOutput
var vehicle_lib = preload("res://test_ai/steering-behaviors/vehicle/bhv_vehicle_lib.gd")
var motion_state = vehicle_lib.MotionState.FORWARD

# vehicle's stationary max forward/rear arcs
@onready var forward_arc_max_angle = deg_to_rad(forward_arc_max_angle_deg)
@onready var rear_arc_max_angle = PI - forward_arc_max_angle
# vehicle's turn limits
@onready var turn_max_angle_cos = cos(deg_to_rad(turn_max_angle))
@onready var turn_one_minus_max_angle_inv = 1.0 / (1 - turn_max_angle_cos)
@onready var turn_min_angle_cos = cos(deg_to_rad(turn_min_angle))

@onready var target = get_node(target_scene)
@onready var body = $CarBaseAI/Body

#<DEBUG
var debug_lib = preload("res://test_ai/steering-behaviors/debug_draw.gd")
@onready var segment_output = ImmediateMesh.new()
#DEBUG>


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		pursue_evade = AI.PursueEvade.new()
		pursue_evade.setup(max_acceleration, max_prediction, 0.0, dims,
				AI_TOOLS.Behavior.EVADE if evade else AI_TOOLS.Behavior.PURSUE)
		if signal_enabled:
			pursue_evade.signal_enable = signal_enabled
			pursue_evade.connect("pursue_evade", self, "_on_AI_pursue_evade",
					["pursue_evade"])
		_initialize()
		
		#<DEBUG
		var mat = StandardMaterial3D.new()
		mat.albedo_color = Color(1,0,0,1)
		segment_output.material_override = mat
		body.add_child(segment_output)
		#DEBUG>


func _physics_process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _initialize():
	var position = body.global_transform.origin
	var velocity = body.linear_velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		pursue_evade.character.position = Vector2(position.x, position.z)
		pursue_evade.character.velocity = Vector2(velocity.x, velocity.z)
	else:
		pursue_evade.character.position = position
		pursue_evade.character.velocity = velocity


func _pre_update(delta):
	var position = target.global_transform.origin
	var velocity = target.velocity
	var body_position = body.global_transform.origin
	var body_velocity = body.linear_velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		pursue_evade.target.position = Vector2(position.x, position.z)
		pursue_evade.target.velocity = Vector2(velocity.x, velocity.z)
		pursue_evade.character.position = Vector2(body_position.x,
				body_position.z)
		pursue_evade.character.velocity = Vector2(body_velocity.x,
				body_velocity.z)
	else:
		pursue_evade.target.position = position
		pursue_evade.target.velocity = velocity
		pursue_evade.character.position = body_position
		pursue_evade.character.velocity = body_velocity


func _update(delta):
	steering_output = pursue_evade.get_steering()
	
	#<DEBUG
	debug_lib.debug_draw_obstacle_avoidance(null, body, steering_output,
			null, null, segment_output, dims)
	#DEBUG>


func _post_update(delta):
	motion_state = vehicle_lib.post_update(delta, motion_state, body, 
			steering_output, aggressiveness, arc_dec_factor_by_speed, 
			forward_arc_max_angle, rear_arc_max_angle, turn_min_angle_cos, 
			turn_max_angle_cos, turn_one_minus_max_angle_inv)


func _on_AI_pursue_evade(object_name, action, signal_name):
	print(signal_name, ": (",object_name, ", ", action, ")")


func _set_enabled(value):
	enabled = value
	visible = value
	set_physics_process(value)


func _get_enabled():
	return enabled
