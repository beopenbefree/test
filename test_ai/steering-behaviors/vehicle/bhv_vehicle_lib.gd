extends Node

enum MotionState {STEADY, FORWARD, BACKWARD}
const MIN_SPEED = 0.1


static func post_update(delta, state, body, steering_output, aggressiveness,
			arc_dec_factor_by_speed, forward_arc_max_angle,
			rear_arc_max_angle, turn_min_angle_cos, turn_max_angle_cos,
			turn_one_minus_max_angle_inv):
	body.action_pressed_turn = false
	body.action_strength_turn = 0.0
	body.action_pressed_motion = false
	body.action_strength_motion = 0.0
	body._set_current_brake(0.0)
	# get the desired_dir (normalized) and desired_speed to the target.
	var desired_dir = steering_output.linear.normalized()
	# get vehicle's forward orientation
	var forward_orientation = body.global_transform.basis.z
	# compute desired dir w.r.t. vehicle's forward orientation: the (cosine of)
	# difference angle
	var direction_diff_cos = forward_orientation.dot(desired_dir)
	
	# get current velocity/speed
	var body_velocity = body.linear_velocity
	var body_speed = body_velocity.length()
	
	# compute the driving's commands 
	# get the motion direction: i.e. velocity w.r.t forward orientation
	var new_state = state
	if state == MotionState.FORWARD:
		# going forward direction: compute current forward arc
		var forward_arc_angle_cos = cos(
				arc_dec_factor_by_speed * forward_arc_max_angle /
				(forward_arc_max_angle * body_speed + arc_dec_factor_by_speed))
		# we're moving in the forward direction
		if direction_diff_cos >= forward_arc_angle_cos:
			# we are into the forward arc: compute turning's commands
			if direction_diff_cos <= turn_min_angle_cos:
				# turn toward the target
				body.action_pressed_turn = true
				if forward_orientation.cross(desired_dir).y >= 0.0:
					if direction_diff_cos >= turn_max_angle_cos:
						body.action_strength_turn = (
								(turn_min_angle_cos - direction_diff_cos) * 
								turn_one_minus_max_angle_inv)
					else:
						body.action_strength_turn = 1.0
				else:
					if direction_diff_cos >= turn_max_angle_cos:
						body.action_strength_turn = ((
								direction_diff_cos - turn_min_angle_cos) * 
								turn_one_minus_max_angle_inv)
					else:
						body.action_strength_turn = -1.0
			# compute motion's commands: go forward
			body.action_pressed_motion = true
			body.action_strength_motion = aggressiveness
		else:
			# we are outside the forward arc: let's brake
			body._set_current_brake(body.brake_size)
			# change state if necessary
			if body_speed < MIN_SPEED:
				new_state = MotionState.BACKWARD
	elif state == MotionState.BACKWARD:
		# going rear direction: compute current rear arc
		var rear_arc_angle_cos = cos(
				PI - (arc_dec_factor_by_speed * rear_arc_max_angle) /
				(rear_arc_max_angle * body_speed + arc_dec_factor_by_speed))
			# we're moving in the rear direction
		if direction_diff_cos <= rear_arc_angle_cos:
			# we are in the rear arc: compute turning's commands
			# turn opposite the target
			body.action_pressed_turn = true
			if forward_orientation.cross(desired_dir).y >= 0.0:
				body.action_strength_turn = -1.0
			else:
				body.action_strength_turn = 1.0
			# compute motion's commands: go backward
			body.action_pressed_motion = true
			body.action_strength_motion = -aggressiveness				
		else:
			# we are outside the rear arc: let's brake
			body._set_current_brake(body.brake_size)
			# change state if necessary
			if body_speed < MIN_SPEED:
				new_state = MotionState.FORWARD
	return new_state


static func post_update_old(delta, body, steering_output, aggressiveness,
			arc_dec_factor_by_speed, forward_arc_max_angle,
			rear_arc_max_angle, turn_min_angle_cos, turn_max_angle_cos,
			turn_one_minus_max_angle_inv):
	body.action_pressed_turn = false
	body.action_strength_turn = 0.0
	body.action_pressed_motion = false
	body.action_strength_motion = 0.0
	body._set_current_brake(0.0)
	# get the desired_dir (normalized) and desired_speed to the target.
	var desired_dir = steering_output.linear.normalized()
	var desired_speed = steering_output.linear.length() * aggressiveness
	# get current dir (normalized)
	var current_dir = body.global_transform.basis.z
	# get directions' difference
	var direction_diff_cos = current_dir.dot(desired_dir)
	
	# compute current forward/rear arcs
	var body_speed = body.linear_velocity.length()
	var forward_arc_angle_cos = cos(arc_dec_factor_by_speed * forward_arc_max_angle /
			(forward_arc_max_angle * body_speed + arc_dec_factor_by_speed))
	var rear_arc_angle_cos = cos(PI - 
			(arc_dec_factor_by_speed * rear_arc_max_angle) /
					(rear_arc_max_angle * body_speed + arc_dec_factor_by_speed))
	
	# compute movement
	if direction_diff_cos >= forward_arc_angle_cos:
		# we are in the forward arc: we can turn
		if direction_diff_cos <= turn_min_angle_cos:
			# turn toward the target
			body.action_pressed_turn = true
			if current_dir.cross(desired_dir).y >= 0.0:
				if direction_diff_cos >= turn_max_angle_cos:
					body.action_strength_turn = (
							(turn_min_angle_cos - direction_diff_cos) * 
							turn_one_minus_max_angle_inv)
				else:
					body.action_strength_turn = 1.0
			else:
				if direction_diff_cos >= turn_max_angle_cos:
					body.action_strength_turn = ((
							direction_diff_cos - turn_min_angle_cos) * 
							turn_one_minus_max_angle_inv)
				else:
					body.action_strength_turn = -1.0
		# compute motion: go forward
		body.action_pressed_motion = true
		body.action_strength_motion = desired_speed
	elif direction_diff_cos <= rear_arc_angle_cos:
		# we are in the rear arc
		# turn opposite the target
		body.action_pressed_turn = true
		if current_dir.cross(desired_dir).y >= 0.0:
			body.action_strength_turn = -1.0
		else:
			body.action_strength_turn = 1.0
		# compute motion: go backward
		body.action_pressed_motion = true
		body.action_strength_motion = -desired_speed
	else:
		# we in a braking zone
		body._set_current_brake(body.brake_size)
