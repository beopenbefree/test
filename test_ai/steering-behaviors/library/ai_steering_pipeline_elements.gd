class_name AISteeringPipelineElements
extends Node


### TARGETERS ###
class ChaseTargeter extends AI.Targeter:
	var chased_character: AI.Kinematic

	# Controls how much to anticipate the movement.
	var lookahead: float

	func _init(_lookahead:float, dims):
		lookahead = _lookahead
		chased_character = AI.Kinematic.new(dims)

	func get_goal(kinematic:AI.Kinematic) -> AI.Goal:
		var goal = AI.Goal.new(kinematic.dims)
		goal.position = (chased_character.position + 
				chased_character.velocity * lookahead)
		goal.has_position = true
		return goal


### DECOMPOSERS ###
class PlanningDecomposer extends AI.Decomposer:
	var map: RID
	var optimize: bool
	var path: AI.NavMapPath

	func _init(world:World, _optimize=false):
		map = world.get_navigation_map()
		optimize = _optimize

	func decompose(character: AI.Kinematic, goal: AI.Goal) -> AI.Goal:
		# First we quantize our current location and our goal into
		# nodes of the map.
		var char_pos = character.position
		var goal_pos = goal.position
		if goal.dims == AI_TOOLS.Dimensions.TWO:
			char_pos = Vector3(char_pos.x, 0.0, char_pos.y)
			goal_pos = Vector3(goal_pos.x, 0.0, goal_pos.y)
		var start: Vector3 = NavigationServer3D.map_get_closest_point(
				map, char_pos)
		var end: Vector3 = NavigationServer3D.map_get_closest_point(
				map, goal_pos)

		# If they are equal, we don’t need to plan.
		if start == end:
			return goal

		# Otherwise plan the route.
		path = AI.NavMapPath.new(NavigationServer3D.map_get_path(
				map, start, end, optimize))

		# Get the first node (beyond the start one) in the path and localize it.
		var first_node: Vector3 = path.get_point(1)
		var position = first_node

		# Update the goal and return.
		if goal.dims == AI_TOOLS.Dimensions.TWO:
			position = Vector2(position.x, position.z)
		goal.position = position
		goal.has_position = true
		return goal


### CONSTRAINTS ###
class BaseConstraint extends AI.Constraint:
	var dims = AI_TOOLS.Dimensions.THREE
	var character: AI.Kinematic
	var map: RID
	var optimize: bool
	var path: AI.NavMapPath

	func _init(_character, _dims, world:World, _optimize):
		dims = _dims
		character = _character
		map = world.get_navigation_map()
		optimize = _optimize

	func suggest(character:AI.Kinematic, path:AI.NavMapPath, 
			goal:AI.Goal) -> AI.Goal:
		# First we quantize our current location and our goal into
		# nodes of the map.
		var char_pos = character.position
		var goal_pos = goal.position
		if goal.dims == AI_TOOLS.Dimensions.TWO:
			char_pos = Vector3(char_pos.x, 0.0, char_pos.y)
			goal_pos = Vector3(goal_pos.x, 0.0, goal_pos.y)
		var start: Vector3 = NavigationServer3D.map_get_closest_point(
				map, char_pos)
		var end: Vector3 = NavigationServer3D.map_get_closest_point(
				map, goal_pos)

		# If they are equal, we don’t need to plan.
		if start == end:
			return goal

		# Otherwise plan the route.
		path = AI.NavMapPath.new(NavigationServer3D.map_get_path(
				map, start, end, optimize))

		# Set up the goal and return.
		var new_position = path.get_point(1)
		if goal.dims == AI_TOOLS.Dimensions.TWO:
			new_position = Vector2(new_position.x, new_position.z)
		goal.position = new_position
		goal.has_position = true
		return goal


class ObstacleAvoidanceConstraint extends BaseConstraint:
	# the inner obstacle avoidance behavior
	var num_rays: int
	var whiskers_angle: float
	var lookahead: float
	var detector: AI.CollisionDetector
	# private variables
	var _collision = null
	var _collision1 = null
	var _ray
	var _ray1

	func _init(_character, _collision_detector, _lookahead, 
			_num_rays, _whiskers_angle, _dims, world:World, 
			_optimize=false).(_character, _dims, world, _optimize):
		num_rays = _num_rays
		whiskers_angle = _whiskers_angle
		lookahead = _lookahead
		detector = _collision_detector

	func will_violate(path: AI.NavMapPath) -> bool:
		_collision = null
		_collision1 = null
		if num_rays == 2:
			# 1. Calculate the target to delegate to seek
			# Calculate the collision ray vector.
			var _y
			var _z
			if (dims == AI_TOOLS.Dimensions.TWO):
				var velocity = Vector3(character.velocity.x, 0, 
						character.velocity.y)
				_z = velocity.normalized() * lookahead + velocity
				_y = Vector3.UP
			else:
				var velocity = character.velocity
				_z = velocity.normalized() * lookahead + velocity
				var _x = _z.cross(Vector3.UP)
				_y = _x.cross(_z).normalized()
			_ray = Basis(_y, -whiskers_angle) * (_z)
			_ray1 = Basis(_y, whiskers_angle) * (_z)
			# Find the collision.
			if dims == AI_TOOLS.Dimensions.TWO:
				_collision = detector.get_collision(
						Vector3(character.position.x, 0,character.position.y), 
						_ray)
				_collision1 = detector.get_collision(
						Vector3(character.position.x, 0,character.position.y), 
						_ray1)
			else:
				_collision = detector.get_collision(character.position, _ray)
				_collision1 = detector.get_collision(character.position, _ray1)
			return (_collision != null) or (_collision1 != null)
		else:
			# only 1 ray by default
			if (dims == AI_TOOLS.Dimensions.TWO):
				var velocity = Vector3(character.velocity.x, 0, character.velocity.y)
				_ray = velocity.normalized() * lookahead + velocity
			else:
				var velocity = character.velocity
				_ray = velocity.normalized() * lookahead + velocity
			_ray1 = _ray
			if dims == AI_TOOLS.Dimensions.TWO:
				_collision = detector.get_collision(
						Vector3(character.position.x, 0,character.position.y), 
						_ray)
			else:
				# Find the collision.
				_collision = detector.get_collision(character.position, _ray)
			#
			return _collision != null


class SeparationConstraint extends BaseConstraint:
	# the inner separation behavior
	var targets: Array
	var threshold: float
	# private variables
	var _steering_needed

	func _init(_character, _targets, _threshold, _dims, 
			world:World, _optimize=false).(_character, _dims, 
			world, _optimize):
		targets = _targets
		threshold = _threshold

	func add_target(target):
		targets.append(target)

	func remove_target(target):
		targets.erase(target)

	func will_violate(path: AI.NavMapPath) -> bool:
		# Loop through each target.
		_steering_needed = false
		for target in targets:
			# Check if the target is close.
			var direction = target.position - character.position
			var distance = direction.length()
			if distance < threshold:
				_steering_needed = true
		return _steering_needed


class AvoidObstacleConstraint extends AI.Constraint:
	# The obstacle bounding sphere.
	var center: Vector3
	var radius: float
	var radius_2: float
	
	# Holds a margin of error by which we’d ideally like
	# to clear the obstacle. Given as a proportion of the
	# radius (i.e. should be > 1.0).
	var margin: float
	
	# If a violation occurs, stores the part of the path
	# that caused the problem.
	var problem_index: int
	
	func _init(_center:Vector3, _radius:float, _margin:float=0.4):
		center = _center
		radius = _radius
		radius_2 = _radius * _radius
		margin = _margin
	
	func will_violate(path: AI.NavMapPath) -> bool:
		# Check each segment of the path in turn.
		for i in range(path.size - 1):
			var segment = path.get_segment(i)
			# If we have a clash, store the current segment.
			if AI_TOOLS.point_segment_distance_squared(center, segment) < radius_2:
				problem_index = i
				return true
		# No segments caused a problem.
		return false
	
	func suggest(character:AI.Kinematic, path:AI.NavMapPath, 
			goal:AI.Goal) -> AI.Goal:
		# Find the closest point on the segment to the sphere center.
		var segment = path.get_segment(problem_index)
		var closest = AI_TOOLS.point_segment_closest_point(segment, center,
				false)
		
		# Check if we pass through the center point.
		var new_position
		var offset = closest - center
		var offset_length = offset.length()
		if offset_length == 0:
			# Any vector at right angles to the segment will do.
			var direction = segment.direction
			var new_direction = direction.cross(Vector3.UP).normalized()
			new_position = center + new_direction * radius * margin
		# Otherwise project the point out beyond the radius.
		else:
			new_position = center + offset * radius * margin / offset_length
		# Set up the goal and return.
		if goal.dims == AI_TOOLS.Dimensions.TWO:
			new_position = Vector2(new_position.x, new_position.z)
		goal.position = new_position
		goal.has_position = true
		return goal


### ACTUATORS ###
class SeekActuator extends AI.Actuator:
	var seek: AI.SeekFlee
	var map: RID
	var optimize: bool

	func _init(max_acceleration, damping, dims, 
			world:World, _optimize=false):
		seek = AI.SeekFlee.new()
		seek.setup(max_acceleration, damping, dims, AI_TOOLS.Behavior.SEEK)
		map = world.get_navigation_map()
		optimize = _optimize

	func get_path(character:AI.Kinematic, goal:AI.Goal) -> AI.NavMapPath:
		var char_pos = character.position
		var goal_pos = goal.position
		if goal.dims == AI_TOOLS.Dimensions.TWO:
			char_pos = Vector3(char_pos.x, 0.0, char_pos.y)
			goal_pos = Vector3(goal_pos.x, 0.0, goal_pos.y)
		var start: Vector3 = NavigationServer3D.map_get_closest_point(
				map, char_pos)
		var end: Vector3 = NavigationServer3D.map_get_closest_point(
				map, goal_pos)
		return AI.NavMapPath.new(PackedVector3Array([start, end]))

	func output(character:AI.Kinematic, path:AI.NavMapPath, 
			goal:AI.Goal) -> AI.SteeringOutput:
		seek.character = character
		var path_point = path.get_point(1)
		if goal.dims == AI_TOOLS.Dimensions.TWO:
			path_point = Vector2(path_point.x, path_point.z)
		seek.target.position = path_point
		return seek.get_steering()
