# see: AI4G - (Millington, I. (2019). Ai for games, Third edition. CRC Press.)
class_name AI
extends RefCounted


class SteeringOutput extends RefCounted:
	var dims = AI_TOOLS.Dimensions.THREE
	# used by Kinematic behaviors
	var velocity
	var rotation := 0.0
	# used by Steering behaviors
	var linear
	var angular := 0.0
	
	func _init(_dims=AI_TOOLS.Dimensions.THREE):
		dims = _dims
		if dims == AI_TOOLS.Dimensions.TWO:
			velocity = Vector2.ZERO
			linear = Vector2.ZERO
		else:
			velocity = Vector3.ZERO
			linear = Vector3.ZERO
	
	# operator overloading
	func _add(other:SteeringOutput) -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		result.velocity = velocity + other.velocity
		result.rotation = rotation + other.rotation
		result.linear = linear + other.linear
		result.angular = angular + other.angular
		return result
		
	func _iadd(other:SteeringOutput) -> SteeringOutput:
		velocity += other.velocity
		rotation += other.rotation
		linear += other.linear
		angular += other.angular
		return self
		
	func _mul(scalar:float) -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		result.velocity = velocity * scalar
		result.rotation = rotation * scalar
		result.linear = linear * scalar
		result.angular = angular * scalar
		return result
		
	func _imul(scalar:float) -> SteeringOutput:
		velocity *= scalar
		rotation *= scalar
		linear *= scalar
		angular *= scalar
		return self
		
	func reset():
		if dims == AI_TOOLS.Dimensions.TWO:
			velocity = Vector2.ZERO
			linear = Vector2.ZERO
		else:
			velocity = Vector3.ZERO
			linear = Vector3.ZERO
		rotation = 0.0
		angular = 0.0


class Static extends RefCounted:
	var dims
	var position
	var orientation := 0.0
	
	func _init(_dims=AI_TOOLS.Dimensions.THREE):
		dims = _dims
		if _dims == AI_TOOLS.Dimensions.TWO:
			position = Vector2.ZERO
		else:
			position = Vector3.ZERO
	
	func update(steering:SteeringOutput, time:float, 
			max_speed:float=-1.0):
		# Update the position and orientation.
		position += steering.velocity * time
		orientation = AI_TOOLS.map_to_range(orientation + steering.rotation * time)


class Kinematic extends Static:
	var velocity
	var rotation := 0.0
	var damping := 0.0: set = _set_damping
	
	func _init(dims=AI_TOOLS.Dimensions.THREE):
		super(dims)
		if dims == AI_TOOLS.Dimensions.TWO:
			velocity = Vector2.ZERO
		else:
			velocity = Vector3.ZERO
	
	func _set_damping(value:float):
		damping = clamp(1.0 - value, 0.0, 1.0)
	
	func update(steering:SteeringOutput, time:float, 
			max_speed:float=-1.0):
		# Update the position and orientation.
		position += velocity * time
		orientation = AI_TOOLS.map_to_range(orientation + rotation * time)
		
		# and the velocity and rotation.
		var linear_accel = steering.linear
		var angular_accel = steering.angular
		if linear_accel.dot(linear_accel) != 0.0:# i.e. linear_accel.length() != 0.0
			velocity += linear_accel * time
		else:
			velocity *= damping
		if angular_accel != 0.0:
			rotation += angular_accel * time
		else:
			rotation *= damping
		
		# Check for speeding and clip.
		if (max_speed > 0.0) and (velocity.length() > max_speed):
			velocity = velocity.normalized() * max_speed


class SteeringBehavior extends RefCounted:
	var type: int
	var dims: int
	var character: Static: set = _set_character
	var name: String = str(self): set = _set_name
	var signal_enable = false
	
	func get_steering() -> SteeringOutput:
		push_warning("To be implemented!")
		return SteeringOutput.new()
	
	func _send_signal(value:String, action:String, last_signal_action:Array):
		if signal_enable and (last_signal_action[0] != action):
			emit_signal(value, self.name, action)
			last_signal_action[0] = action
	
	func _set_character(value:Static):
		character = value
	
	func _set_name(value):
		name = value


class KinematicSeekFlee extends SteeringBehavior:
	var target:Static
	var max_speed:float
	
	func setup(_max_speed, _dims=AI_TOOLS.Dimensions.THREE,
			_type=AI_TOOLS.Behavior.SEEK):
		type = _type
		dims = _dims
		character = Static.new(dims)
		target = Static.new(dims)
		max_speed = _max_speed
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Get the direction to the target.
		result.velocity = target.position - character.position
		if type == AI_TOOLS.Behavior.FLEE:
			result.velocity = -result.velocity
		# The velocity is along this direction, at full speed.
		result.velocity = result.velocity.normalized() * max_speed
		# Face in the direction we want to move.
		character.orientation = AI_TOOLS.new_orientation(
			character.orientation, result.velocity, dims)
		result.rotation = SteeringOutput.new(dims).rotation
		return result


class KinematicArriveLeave extends SteeringBehavior:
	signal arrive_leave(object_name, signal_action)
	# start/stop steering action
	var last_start_stop: Array = [""]
	
	var target: Static
	var max_speed: float
	# The satisfaction radius.
	var radius: float
	# The time to target constant.
	var time_to_target: float
	
	func setup(_max_speed, _radius, _time_to_target,
			_dims=AI_TOOLS.Dimensions.THREE, _type=AI_TOOLS.Behavior.ARRIVE):
		type = _type
		dims = _dims
		character = Static.new(dims)
		target = Static.new(dims)
		max_speed = _max_speed
		radius = _radius
		time_to_target = _time_to_target
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Get the direction to the target.
		if type == AI_TOOLS.Behavior.LEAVE:
			result.velocity = -target.position + character.position
			# Check if we’re within radius.
			if result.velocity.length() > radius:
				# send signal
				_send_signal("arrive_leave", "stop_steering", last_start_stop)
				# Request no steering.
				return SteeringOutput.new(dims)
		else:
			result.velocity = target.position - character.position
			# Check if we’re within radius.
			if result.velocity.length() < radius:
				# send signal
				_send_signal("arrive_leave", "stop_steering", last_start_stop)
				# Request no steering.
				return SteeringOutput.new(dims)
		# send signal
		_send_signal("arrive_leave", "start_steering", last_start_stop)
		# We need to move to our target, we’d like to
		# get there in timeToTarget seconds.
		result.velocity /= time_to_target
		# If this is too fast, clip it to the max speed.
		if result.velocity.length() > max_speed:
			result.velocity = result.velocity.normalized() * max_speed
		# Face in the direction we want to move.
		character.orientation = AI_TOOLS.new_orientation(
			character.orientation, result.velocity, dims)
		result.rotation = SteeringOutput.new(dims).rotation
		return result


class KinematicWander extends SteeringBehavior:
	var max_speed: float
	# The maximum rotation speed we’d like, probably should be smaller
	# than the maximum possible, for a leisurely change in direction.
	var max_rotation: float
	
	func setup(_max_speed, _max_rotation, _dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.WANDER
		dims = _dims
		character = Static.new(dims)
		max_speed = _max_speed
		max_rotation = _max_rotation
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Get velocity from the vector form of the orientation.
		if dims == AI_TOOLS.Dimensions.TWO:
			result.velocity = (AI_TOOLS.as_vector2(character.orientation) *
					max_speed)
		else:
			result.velocity = (AI_TOOLS.as_vector3(character.orientation) * 
					max_speed)
		# Change our orientation randomly.
		result.rotation = AI_TOOLS.random_binomial() * max_rotation
		return result


class SeekFlee extends SteeringBehavior:
	var target: Kinematic
	var max_acceleration: float
	
	func setup(_max_acceleration, _damping = 0.0,
			_dims=AI_TOOLS.Dimensions.THREE, _type=AI_TOOLS.Behavior.SEEK):
		type = _type
		dims = _dims
		character = Kinematic.new(dims)
		character.damping = _damping
		target = Kinematic.new(dims)
		max_acceleration = _max_acceleration
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Get the direction to the target.
		result.linear = target.position - character.position
		if type == AI_TOOLS.Behavior.FLEE:
			result.linear = -result.linear
		# Give full acceleration along this direction.
		result.linear = result.linear.normalized() * max_acceleration
		result.angular = SteeringOutput.new(dims).angular
		return result


class ArriveLeave extends SteeringBehavior:
	signal arrive_leave(object_name, signal_action)
	# start/stop steering action
	var last_start_stop: Array = [""]
	# in/out slow radius action
	var last_in_out_slow_radius: Array = [""]
	
	var target: Kinematic
	var max_acceleration: float
	var max_speed: float
	# The radius for arriving at the target.
	var target_radius: float
	# The radius for beginning to slow down.
	var slow_radius: float
	# The time over which to achieve target speed.
	var time_to_target: float
	# private variables
	var _direction
	var _distance
	var _target_speed
	var _target_velocity
	
	func setup(_max_acceleration, _max_speed, _target_radius, _slow_radius, 
			_time_to_target, _damping = 0.0, _dims=AI_TOOLS.Dimensions.THREE,
			_type=AI_TOOLS.Behavior.ARRIVE):
		type = _type
		dims = _dims
		character = Kinematic.new(dims)
		character.damping = _damping
		target = Kinematic.new(dims)
		max_acceleration = _max_acceleration
		max_speed = _max_speed
		target_radius = _target_radius
		slow_radius = _slow_radius
		time_to_target = _time_to_target
		# initialize private variables
		_direction = Vector3.ZERO if dims == AI_TOOLS.Dimensions.THREE else Vector2.ZERO
		_distance = 0.0
		_target_speed = 0.0
		_target_velocity = _direction
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Get the direction to the target.
		_direction = target.position - character.position
		if type == AI_TOOLS.Behavior.LEAVE:
			_direction = -_direction
		_distance = _direction.length()
		# Check if we are there, return no steering.
		if _distance < target_radius:
			# send signal
			_send_signal("arrive_leave", "stop_steering", last_start_stop)
			return SteeringOutput.new(dims)
		# send signal
		_send_signal("arrive_leave", "start_steering", last_start_stop)
		if _distance > slow_radius:
			# If we are outside the slowRadius, then move at max speed.
			_target_speed = max_speed
			# send signal
			_send_signal("arrive_leave", "out_slow_radius", last_in_out_slow_radius)
		else:
			# Otherwise calculate a scaled speed.
			_target_speed = max_speed * _distance / slow_radius
			# send signal
			_send_signal("arrive_leave", "in_slow_radius", last_in_out_slow_radius)
		# The target velocity combines speed and direction.
		_target_velocity = _direction
		_target_velocity = _target_velocity.normalized() * _target_speed
		# Acceleration tries to get to the target velocity.
		result.linear = _target_velocity - character.velocity
		result.linear /= time_to_target
		# Check if the acceleration is too fast.
		if result.linear.length() > max_acceleration:
			result.linear = result.linear.normalized() * max_acceleration
		result.angular = SteeringOutput.new(dims).angular
		return result


class Align extends SteeringBehavior:
	signal align(object_name, signal_action)
	# start/stop steering action
	var last_start_stop: Array = [""]
	# in/out slow radius action
	var last_in_out_slow_radius: Array = [""]
	
	var target: Kinematic
	var max_angular_acceleration: float
	var max_rotation: float
	# The radius for arriving at the target.
	var target_radius: float
	# The radius for beginning to slow down.
	var slow_radius: float
	# The time over which to achieve target speed.
	var time_to_target: float
	# private variables
	var _rotation
	var _rotation_size
	
	func setup(_max_angular_acceleration, _max_rotation, _target_radius, 
			_slow_radius, _time_to_target, _damping = 0.0, 
			_dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.ALIGN
		dims = _dims
		character = Kinematic.new(dims)
		character.damping = _damping
		target = Kinematic.new(dims)
		max_angular_acceleration = _max_angular_acceleration
		max_rotation = _max_rotation
		target_radius = _target_radius
		slow_radius = _slow_radius
		time_to_target = _time_to_target
		# initialize private variables
		_rotation = 0.0
		_rotation_size = 0.0
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Get the naive direction to the target.
		_rotation = target.orientation - character.orientation
		# Map the result to the (-pi, pi) interval.
		_rotation = AI_TOOLS.map_to_range(_rotation)
		_rotation_size = abs(_rotation)
		# Check if we are there, return no steering.
		if _rotation_size < target_radius:
			# send signal
			_send_signal("align", "stop_steering", last_start_stop)
			return SteeringOutput.new(dims)
		var target_rotation
		if _rotation_size > slow_radius:
			# If we are outside the slowRadius, then use maximum rotation.
			target_rotation = max_rotation
			# send signal
			_send_signal("align", "out_slow_radius", last_in_out_slow_radius)
		else:
			# Otherwise calculate a scaled rotation.
			target_rotation = max_rotation * _rotation_size / slow_radius
			# send signal
			_send_signal("align", "in_slow_radius", last_in_out_slow_radius)
		# send signal
		_send_signal("align", "start_steering", last_start_stop)
		# The final target rotation combines speed (already in the
		# variable) and direction.
		target_rotation *= _rotation / _rotation_size
		# Acceleration tries to get to the target rotation.
		result.angular = target_rotation - character.rotation
		result.angular /= time_to_target
		# Check if the acceleration is too great.
		var angular_acceleration = abs(result.angular)
		if angular_acceleration > max_angular_acceleration:
			result.angular /= angular_acceleration
			result.angular *= max_angular_acceleration
		result.linear = SteeringOutput.new(dims).linear
		return result


class VelocityMatch extends SteeringBehavior:
	var target: Kinematic
	var max_acceleration: float
	# The time over which to achieve target speed.
	var time_to_target: float
	
	func setup(_max_acceleration, _time_to_target, 
			_damping = 0.0, _dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.VELOCITY_MATCH
		dims = _dims
		character = Kinematic.new(dims)
		character.damping = _damping
		target = Kinematic.new(dims)
		max_acceleration = _max_acceleration
		time_to_target = _time_to_target
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Acceleration tries to get to the target velocity.
		result.linear = target.velocity - character.velocity
		result.linear /= time_to_target
		# Check if the acceleration is too fast.
		if result.linear.length() > max_acceleration:
			result.linear = result.linear.normalized() * max_acceleration
		result.angular = SteeringOutput.new(dims).angular
		return result


class PursueEvade extends SteeringBehavior:
	# This implementation uses SeekFlee sub-behavior component
	signal pursue_evade(object_name, signal_action)
	# on/off max prediction action
	var last_on_off_max_prediction: Array = [""]
	
	var target: Kinematic
	# The maximum prediction time.
	var max_prediction: float
	# delegation
	var seek_flee: SeekFlee
	# private variables
	var _direction
	var _distance
	var _speed
	var _prediction
	
	func _set_character(value):
		super._set_character(value)
		# override delegate's character
		seek_flee.character = value
	
	func _set_name(value):
		super._set_name(value)
		seek_flee.name = name
	
	func setup(max_acceleration_delegate, _max_prediction, _damping = 0.0,
			_dims=AI_TOOLS.Dimensions.THREE, _type=AI_TOOLS.Behavior.PURSUE):
		type = _type
		dims = _dims
		character = Kinematic.new(dims)
		character.damping = _damping
		target = Kinematic.new(dims)
		max_prediction = _max_prediction
		# delegation setup
		var type_sf = AI_TOOLS.Behavior.SEEK
		if type == AI_TOOLS.Behavior.EVADE:
			type_sf = AI_TOOLS.Behavior.FLEE
		seek_flee = SeekFlee.new()
		seek_flee.setup(max_acceleration_delegate, _damping, dims, type_sf)
		# override delegate's character
		seek_flee.character = character
		seek_flee.name = name
		# initialize private variables
		_direction = Vector3.ZERO if dims == AI_TOOLS.Dimensions.THREE else Vector2.ZERO
		_distance = 0.0
		_speed = 0.0
		_prediction = 0.0
	
	func get_steering() -> SteeringOutput:
		# 1. Calculate the target to delegate to seek
		# Work out the distance to target.
		_direction = target.position - character.position
		if type == AI_TOOLS.Behavior.EVADE:
			_direction = -_direction
		_distance = _direction.length()
		# Work out our current speed.
		_speed = character.velocity.length()
		if _speed <= _distance / max_prediction:
			# Check if speed gives a reasonable prediction time.
			_prediction = max_prediction
			# send signal
			_send_signal("pursue_evade", "on_max_prediction", last_on_off_max_prediction)
		else:
			# Otherwise calculate the prediction time.
			_prediction = _distance / _speed
			# send signal
			_send_signal("pursue_evade", "off_max_prediction", last_on_off_max_prediction)
		# Put the target together.
		seek_flee.target.position = target.position
		seek_flee.target.velocity = target.velocity
		seek_flee.target.position += target.velocity * _prediction
		# 2. Delegate to seek.
		return seek_flee.get_steering()


class PursueEvade2 extends SteeringBehavior:
	# This implementation uses ArriveLeave sub-behavior component
	signal pursue_evade_2(object_name, signal_action)
	# on/off max prediction action
	var last_on_off_max_prediction: Array = [""]
	
	var target: Kinematic
	# The maximum prediction time.
	var max_prediction: float
	# delegation
	var arrive_leave: ArriveLeave
	# private variables
	var _direction
	var _distance
	var _speed
	var _prediction
	
	func _set_character(value):
		super._set_character(value)
		# override delegate's character
		arrive_leave.character = value
	
	func _set_name(value):
		super._set_name(value)
		arrive_leave.name = name
	
	func setup(max_acceleration_delegate, max_speed_delegate, 
			target_radius_delegate, slow_radius_delegate,
			time_to_target_delegate, _max_prediction, _damping = 0.0,
			_dims=AI_TOOLS.Dimensions.THREE, _type=AI_TOOLS.Behavior.PURSUE):
		type = _type
		dims = _dims
		character = Kinematic.new(dims)
		character.damping = _damping
		target = Kinematic.new(dims)
		max_prediction = _max_prediction
		# delegation setup
		var type_sf = AI_TOOLS.Behavior.ARRIVE
		if type == AI_TOOLS.Behavior.EVADE2:
			type_sf = AI_TOOLS.Behavior.LEAVE
		arrive_leave = ArriveLeave.new()
		arrive_leave.setup(max_acceleration_delegate, max_speed_delegate,
				target_radius_delegate, slow_radius_delegate, 
				time_to_target_delegate, _damping, dims, type_sf)
		# override delegate's character
		arrive_leave.character = character
		arrive_leave.name = name
		# initialize private variables
		_direction = Vector3.ZERO if dims == AI_TOOLS.Dimensions.THREE else Vector2.ZERO
		_distance = 0.0
		_speed = 0.0
		_prediction = 0.0
	
	func get_steering() -> SteeringOutput:
		# 1. Calculate the target to delegate to seek
		# Work out the distance to target.
		_direction = target.position - character.position
		if type == AI_TOOLS.Behavior.EVADE:
			_direction = -_direction
		_distance = _direction.length()
		# Work out our current speed.
		_speed = character.velocity.length()
		if _speed <= _distance / max_prediction:
			# Check if speed gives a reasonable prediction time.
			_prediction = max_prediction
			# send signal
			_send_signal("pursue_evade_2", "on_max_prediction", last_on_off_max_prediction)
		else:
			# Otherwise calculate the prediction time.
			_prediction = _distance / _speed
			# send signal
			_send_signal("pursue_evade_2", "off_max_prediction", last_on_off_max_prediction)
		# Put the target together.
		arrive_leave.target.position = target.position
		arrive_leave.target.velocity = target.velocity
		arrive_leave.target.position += target.velocity * _prediction
		# 2. Delegate to seek.
		return arrive_leave.get_steering()


class Face extends SteeringBehavior:
	signal face(object_name, signal_action)
	# start/stop steering action
	var last_start_stop: Array = [""]
	
	var target: Kinematic
	# delegation
	# signal align
	var align: Align
	# private variables
	var _direction
	
	func _set_character(value):
		super._set_character(value)
		# override delegate's character
		align.character = value
	
	func _set_name(value):
		super._set_name(value)
		align.name = name
	
	func setup(max_angular_acceleration_delegate, max_rotation_delegate, 
			target_radius_delegate, slow_radius_delegate, time_to_target_delegate, 
			_damping = 0.0, _dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.FACE
		dims = _dims
		character = Kinematic.new(dims)
		character.damping = _damping
		target = Kinematic.new(dims)
		# delegation setup
		align = Align.new()
		align.setup(max_angular_acceleration_delegate, max_rotation_delegate, 
				target_radius_delegate, slow_radius_delegate, 
				time_to_target_delegate, _damping, dims)
		# override delegate's character
		align.character = character
		align.name = name
		# initialize private variables
		_direction = Vector3.ZERO if dims == AI_TOOLS.Dimensions.THREE else Vector2.ZERO
	
	func get_steering() -> SteeringOutput:
		# 1. Calculate the target to delegate to align
		# Work out the distance to target.
		var last_direction = _direction
		_direction = target.position - character.position
		# Check for a zero direction, and make no change if so.
		if _direction == last_direction:
			# send signal
			_send_signal("face", "stop_steering", last_start_stop)
			return SteeringOutput.new(dims)
		# send signal
		_send_signal("face", "start_steering", last_start_stop)
		# Put the target together.
		if dims == AI_TOOLS.Dimensions.TWO:
			align.target.orientation = atan2(_direction.x, _direction.y)
		else:
			align.target.orientation = atan2(_direction.x, _direction.z)
		# 2. Delegate to seek.
		return align.get_steering()


class LookWhereYoureGoing extends SteeringBehavior:
	signal look_where_youre_going(object_name, signal_action)
	# start/stop steering action
	var last_start_stop: Array = [""]
	
	var max_angular_acceleration: float
	var max_rotation: float
	# The radius for arriving at the target.
	var target_radius: float
	# The radius for beginning to slow down.
	var slow_radius: float
	# The time over which to achieve target speed.
	var time_to_target: float
	# delegation
	# signal align
	var align: Align
	
	func _set_character(value):
		super._set_character(value)
		# override delegate's character
		align.character = value
	
	func _set_name(value):
		super._set_name(value)
		align.name = name
	
	func setup(max_angular_acceleration_delegate, max_rotation_delegate, 
			target_radius_delegate, slow_radius_delegate, time_to_target_delegate, 
			_damping = 0.0, _dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.LOOK_WHERE_YOURE_GOING
		dims = _dims
		character = Kinematic.new(dims)
		character.damping = _damping
		# delegation setup
		align = Align.new()
		align.setup(max_angular_acceleration_delegate, max_rotation_delegate, 
				target_radius_delegate, slow_radius_delegate, 
				time_to_target_delegate, _damping, dims)
		# override delegate's character
		align.character = character
		align.name = name
	
	func get_steering() -> SteeringOutput:
		# 1. Calculate the target to delegate to align
		# Check for a zero direction, and make no change if so.
		var velocity = character.velocity
		if velocity.length() == 0.0:
			# send signal
			_send_signal("look_where_youre_going", "stop_steering", last_start_stop)
			return SteeringOutput.new(dims)
		# send signal
		_send_signal("face", "start_steering", last_start_stop)
		# Otherwise set the target based on the velocity.
		align.target.orientation = atan2(velocity.x, velocity.z)
		# 2. Delegate to seek.
		return align.get_steering()


class Wander extends SteeringBehavior:
	# The radius and forward offset of the wander circle.
	var wander_offset: float
	var wander_radius: float
	# The maximum rate at which the wander orientation can change.
	var wander_rate: float
	# The current orientation of the wander target.
	var wander_orientation: float
	# The maximum acceleration of the character.
	var max_acceleration: float
	# delegation
	# signal face, align
	var face: Face
	# private variables
	var _target_orientation
	var _target_orientation_as_vector
	var _character_orientation_as_vector
	
	func _set_character(value):
		super._set_character(value)
		# override delegate's character
		face.character = value
	
	func _set_name(value):
		super._set_name(value)
		face.name = name
	
	func setup(max_angular_acceleration_delegate, max_rotation_delegate, 
			target_radius_delegate, slow_radius_delegate, time_to_target_delegate,
			_wander_offset, _wander_radius, _wander_rate, _wander_orientation,
			_max_acceleration, _damping = 0.0, _dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.WANDER
		dims = _dims
		character = Kinematic.new(dims)
		character.damping = _damping
		wander_offset = _wander_offset
		wander_radius = _wander_radius
		wander_rate = _wander_rate
		wander_orientation = _wander_orientation
		max_acceleration = _max_acceleration
		# delegation setup
		face = Face.new()
		face.setup(max_angular_acceleration_delegate, max_rotation_delegate, 
				target_radius_delegate, slow_radius_delegate, 
				time_to_target_delegate, _damping, dims)
		# override delegate's character
		face.character = character
		face.name = name
		# initialize private variables
		_target_orientation = 0.0
		_target_orientation_as_vector = Vector3.ZERO if dims == AI_TOOLS.Dimensions.THREE else Vector2.ZERO
		_character_orientation_as_vector = _target_orientation_as_vector
	
	func get_steering() -> SteeringOutput:
		# 1. Calculate the target to delegate to face
		# Update the wander orientation.
		wander_orientation += AI_TOOLS.random_binomial() * wander_rate
		# Calculate the combined target orientation.
		_target_orientation = AI_TOOLS.map_to_range(wander_orientation + 
				character.orientation)

		# OPTIMIZATION!
		if dims == AI_TOOLS.Dimensions.TWO:
			_target_orientation_as_vector = AI_TOOLS.as_vector2(
					_target_orientation)
			_character_orientation_as_vector = AI_TOOLS.as_vector2(
					character.orientation)
		else:
			_target_orientation_as_vector = AI_TOOLS.as_vector3(
					_target_orientation)
			_character_orientation_as_vector = AI_TOOLS.as_vector3(
					character.orientation)
		
		# Calculate the center of the wander circle.
		face.target.position = (character.position + 
				_character_orientation_as_vector * wander_offset)
		# Calculate the target location.
		face.target.position += _target_orientation_as_vector * wander_radius
		# 2. Delegate to face.
		var result = face.get_steering()
		# 3. Now set the linear acceleration to be at full
		# acceleration in the direction of the orientation.
		result.linear = _character_orientation_as_vector * max_acceleration
		# Return it.
		return result


class PathAI extends RefCounted:
	# Note: path is supposed to lay in global space
	var path: Path3D
	var path_length: float
	var path_length_inv: float
	# coherence >0 (<0) -> param is monotonically increasing(decreasing)
	var coherence: float
	
	func _init(_path):
		path = _path
		path_length = path.curve.get_baked_length()
		path_length_inv = (1.0 / path.curve.get_baked_length())
	
	func get_param(position: Vector3, last_param: float) -> float:
		var param = path.curve.get_closest_offset(position) * path_length_inv
		if coherence > 0.0:
			if param < last_param:
				param = last_param
		if coherence < 0.0:
			if param > last_param:
				param = last_param
		return param
	
	func get_position(param: float) -> Vector3:
		return path.curve.interpolate_baked(param * path_length)


class FollowPath extends SteeringBehavior:
	# The path to follow
	var path: PathAI
	# The distance along the path to generate the target. Can be
	# negative if the character is moving in the reverse direction.
	var path_offset: float: set = _set_path_offset
	# The time in the future to predict the character’s position.
	var predict_time: float
	# The current position on the path.
	var current_param: float
	# delegation
	var seek_flee: SeekFlee
	# private variables
	var _future_pos
	var _target_param
	
	func _set_character(value):
		super._set_character(value)
		# override delegate's character
		seek_flee.character = value
	
	func _set_name(value):
		super._set_name(value)
		seek_flee.name = name
	
	func _set_path_offset(_path_offset:float):
		path_offset = clamp(_path_offset, -1.0, 1.0)
		path.coherence = path_offset
	
	func setup(max_acceleration_delegate, _path, _path_offset,
			_predict_time=0.1, _damping = 0.0, 
			_dims=AI_TOOLS.Dimensions.THREE, _type=AI_TOOLS.Behavior.FOLLOW_PATH):
		type = _type
		dims = _dims
		character = Kinematic.new(dims)
		character.damping = _damping
		path = _path
		_set_path_offset(_path_offset)
		current_param = 0.0 if path_offset >= 0.0 else 1.0
		predict_time = _predict_time
		# delegation setup
		seek_flee = SeekFlee.new()
		seek_flee.setup(max_acceleration_delegate, _damping, dims, 
				AI_TOOLS.Behavior.SEEK)
		# override delegate's character
		seek_flee.character = character
		seek_flee.name = name
		# initialize private variables
		_future_pos = Vector3.ZERO if dims == AI_TOOLS.Dimensions.THREE else Vector2.ZERO
		_target_param = 0.0
	
	func get_steering() -> SteeringOutput:
		# 1. Calculate the target to delegate to seek.
		# Find the current position on the path.
		if dims == AI_TOOLS.Dimensions.TWO:
			if type == AI_TOOLS.Behavior.FOLLOW_PATH_PREDICTIVE:
				_future_pos = (Vector3(character.position.x, 0.0,
						character.position.y) +
						Vector3(character.velocity.x, 0.0,
						character.velocity.y) * predict_time)
			else:
				_future_pos = Vector3(character.position.x, 0.0,
					character.position.y)
			current_param = path.get_param(_future_pos, current_param)
		else:
			if type == AI_TOOLS.Behavior.FOLLOW_PATH_PREDICTIVE:
				_future_pos = (character.position +
						character.velocity * predict_time)
			else:
				_future_pos = character.position
			current_param = path.get_param(_future_pos, current_param)
		# Offset it.
		_target_param = current_param + path_offset
		# Get the target position.
		if dims == AI_TOOLS.Dimensions.TWO:
			var target_position = path.get_position(_target_param)
			seek_flee.target.position = Vector2(target_position.x,
					target_position.z)
		else:
			seek_flee.target.position = path.get_position(_target_param)
		# 2. Delegate to seek.
		return seek_flee.get_steering()


class SeparationAttraction extends SteeringBehavior:
	signal separation_attraction(object_name, signal_action)
	# start/stop steering action
	var last_start_stop: Array = [""]
	
	# A list of potential targets.
	var targets: Array
	var max_acceleration: float
	# The (squared) threshold to take action.
	var threshold: float
	# The constant coefficient of decay for the inverse square law.
	var decay_coefficient: float
	# private variables
	var _steering_needed:bool
	
	func setup(_max_acceleration, _threshold, _decay_coefficient,
			_damping = 0.0, _dims=AI_TOOLS.Dimensions.THREE,
			_type=AI_TOOLS.Behavior.SEPARATION):
		type = _type
		dims = _dims
		character = Kinematic.new(dims)
		character.damping = _damping
		targets = []
		max_acceleration = _max_acceleration
		threshold = _threshold
		decay_coefficient = _decay_coefficient
		# initialize private variables
		_steering_needed = false
	
	func add_target(target):
		targets.append(target)
	
	func remove_target(target):
		targets.erase(target)
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Loop through each target.
		_steering_needed = false
		for target in targets:
			# Check if the target is close.
			var direction = target.position - character.position
			var distance = direction.length()
			if distance < threshold:
				_steering_needed = true
				# Calculate the strength of repulsion
				# (here using the inverse square law).
				var strength = min(decay_coefficient / (distance * distance), 
						max_acceleration)
				if type == AI_TOOLS.Behavior.ATTRACTION:
					strength = -strength
				# Add the acceleration.
				result.linear += strength * (-direction.normalized())
		result.angular = SteeringOutput.new(dims).angular
		if _steering_needed:
			# send signal
			_send_signal("separation_attraction", "start_steering", 
				last_start_stop)
		else:
			# send signal
			_send_signal("separation_attraction", "stop_steering", 
				last_start_stop)
		return result


class CollisionAvoidance extends SteeringBehavior:
	signal collision_avoidance(object_name, signal_action)
	# start/stop steering action
	var last_start_stop: Array = [""]
	
	# A list of potential targets.
	var targets: Array
	var max_acceleration: float
	# The collision radius of a character (assuming all characters
	# have the same radius here).
	var radius: float
	var epsilon_2: float
	# private variables
	var _shortest_time
	var _first_target: Kinematic
	var _first_min_separation
	var _first_distance
	var _first_relative_pos
	var _first_relative_vel
	var _relative_pos
	
	func setup(_max_acceleration, _radius, _epsilon = 0.1,
			_damping = 0.0, _dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.COLLISION_AVOIDANCE
		dims = _dims
		character = Kinematic.new(dims)
		character.damping = _damping
		targets = []
		max_acceleration = _max_acceleration
		radius = _radius
		epsilon_2 = _epsilon * _epsilon
		# initialize private variables
		_shortest_time = 0.0
		_first_target = null
		_first_min_separation = 0.0
		_first_distance = 0.0
		_first_relative_pos = Vector3.ZERO if dims == AI_TOOLS.Dimensions.THREE else Vector2.ZERO
		_first_relative_vel = _first_relative_pos
		_relative_pos = _first_relative_pos
	
	func add_target(target):
		targets.append(target)
	
	func remove_target(target):
		targets.erase(target)
	
	func get_steering() -> SteeringOutput:
		# 1. Find the target that’s closest to collision
		# Store the first collision time.
		_shortest_time = INF
		# Store the target that collides then, and other data that we
		# will need and can avoid recalculating.
		_first_target = null
		# Loop through each target.
		for target in targets:
			# Calculate the time to collision.
			var relative_pos = target.position - character.position
			var relative_vel = target.velocity - character.velocity
			var relative_speed_2 = relative_vel.dot(relative_vel)
			if abs(relative_speed_2) < epsilon_2:
				# corner case: both character and target are stopped
				# or they are moving with the same velocity
				continue
			var time_to_collision = -(relative_pos.dot(relative_vel) /
					relative_speed_2)
			# Check if it is going to be a collision at all.
			var min_separation = (relative_pos + 
					relative_vel * time_to_collision).length()
			if min_separation > 2 * radius:
				continue
			# Check if it is the shortest.
			if (time_to_collision > 0) and (time_to_collision < _shortest_time):
				# Store the time, target and other data.
				_shortest_time = time_to_collision
				_first_target = target
				_first_min_separation = min_separation
				_first_distance = relative_pos.length()
				_first_relative_pos = relative_pos
				_first_relative_vel = relative_vel
		# 2. Calculate the steering
		var result = SteeringOutput.new(dims)
		# If we have no target, then exit.
		if not _first_target:
			# send signal
			_send_signal("collision_avoidance", "stop_steering", 
					last_start_stop)
			return result
		# send signal
		_send_signal("collision_avoidance", "start_steering", 
				last_start_stop)
		# If we’re going to hit exactly, or if we’re already
		# colliding, then do the steering based on current position.
		if (_first_min_separation <= 0) or (_first_distance < 2 * radius):
			_relative_pos = _first_target.position - character.position
		# Otherwise calculate the future relative position.
		else:
			_relative_pos = (_first_relative_pos + 
					_first_relative_vel * _shortest_time)
		# Avoid the target.
		result.linear = (-_relative_pos.normalized()) * max_acceleration
		result.angular = SteeringOutput.new(dims).angular
		return result


class Collision extends RefCounted:
	var position: Vector3
	var normal: Vector3
	
	func _init(p, n):
		position = p
		normal = n


class CollisionDetector extends RefCounted:
	var detector := Node3D.new()
	var collision_mask := 0x7FFFFFFF # 0b01111111111111111111111111111111
	# private variables
	var _result = {}
		
	func get_collision(position: Vector3, move_amount: Vector3) -> Collision:
		# NOTE: should be during physics' tick (i.e. _physics_process)
		var space_state = detector.get_world_3d().direct_space_state
		# use global coordinates, not local to node
		_result = space_state.intersect_ray(position, position + move_amount, 
				[], collision_mask)
		if not _result:
			return null
		return Collision.new(_result["position"], _result["normal"])


class ObstacleAvoidance extends SteeringBehavior:
	signal obstacle_avoidance(object_name, signal_action)
	# start/stop steering action
	var last_start_stop: Array = [""]
	
	var detector: CollisionDetector
	var num_rays: int
	# The minimum distance to a wall (i.e., how far to avoid
	# collision) should be greater than the radius of the character.
	var avoid_distance: float
	# The distance to look ahead for a collision
	# (i.e., the length of the collision ray).
	var lookahead: float
	# whiskers' angle
	var whiskers_angle: float
	# delegation
	var seek_flee: SeekFlee
	# private variables
	var _ray
	var _ray1
	var _collision
	var _collision1
	
	func _set_character(value):
		super._set_character(value)
		# override delegate's character
		seek_flee.character = value
	
	func _set_name(value):
		super._set_name(value)
		seek_flee.name = name
	
	func setup(max_acceleration_delegate, _detector, _avoid_distance, 
			_lookahead, _num_rays=1, _whiskers_angle=deg_to_rad(15),
			_damping = 0.0, _dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.OBSTACLE_AVOIDANCE
		dims = _dims
		character = Kinematic.new(dims)
		character.damping = _damping
		detector = _detector
		avoid_distance = _avoid_distance
		lookahead = _lookahead
		num_rays = _num_rays
		whiskers_angle = _whiskers_angle
		# delegation setup
		seek_flee = SeekFlee.new()
		seek_flee.setup(max_acceleration_delegate, _damping, dims,
				AI_TOOLS.Behavior.SEEK)
		# override delegate's character
		seek_flee.character = character
		seek_flee.name = name
		# initialize private variables
		_ray = Vector3.ZERO if dims == AI_TOOLS.Dimensions.THREE else Vector2.ZERO
		_ray1 = _ray
		_collision = Collision.new(Vector3.ZERO, Vector3.ZERO)
		_collision1 = _collision
	
	func get_steering() -> SteeringOutput:
		if num_rays == 2:
			# 1. Calculate the target to delegate to seek
			# Calculate the collision ray vector.
			var _y
			var _z
			if (dims == AI_TOOLS.Dimensions.TWO):
				var velocity = Vector3(character.velocity.x, 0, 
						character.velocity.y)
				_z = velocity.normalized() * lookahead + velocity
				_y = Vector3.UP
			else:
				var velocity = character.velocity
				_z = velocity.normalized() * lookahead + velocity
				var _x = _z.cross(Vector3.UP)
				_y = _x.cross(_z).normalized()
			_ray = Basis(_y, -whiskers_angle) * (_z)
			_ray1 = Basis(_y, whiskers_angle) * (_z)
			# Find the collision.
			if dims == AI_TOOLS.Dimensions.TWO:
				_collision = detector.get_collision(
						Vector3(character.position.x, 0,character.position.y), 
						_ray)
				_collision1 = detector.get_collision(
						Vector3(character.position.x, 0,character.position.y), 
						_ray1)
			else:
				_collision = detector.get_collision(character.position, _ray)
				_collision1 = detector.get_collision(character.position, _ray1)
			# If have no collision, do nothing.
			if not (_collision or _collision1):
				# send signal
				_send_signal("obstacle_avoidance", "stop_steering", 
						last_start_stop)
				return SteeringOutput.new(dims)
			# 2. Otherwise create a target and delegate to seek.
			# send signal
			_send_signal("obstacle_avoidance", "start_steering", 
					last_start_stop)
			var position_r
			var normal_r
			var position_l
			var normal_l
			var position
			var normal
			if dims == AI_TOOLS.Dimensions.TWO:
				if _collision:
					position_r = Vector2(_collision.position.x, _collision.position.z)
					normal_r = Vector2(_collision.normal.x, _collision.normal.z)
				else:
					position_r = Vector2.ZERO
					normal_r = Vector2.ZERO
				if _collision1:
					position_l = Vector2(_collision1.position.x, _collision1.position.z)
					normal_l = Vector2(_collision1.normal.x, _collision1.normal.z)
				else:
					position_l = Vector2.ZERO
					normal_l = Vector2.ZERO
			else:
				if _collision:
					position_r = _collision.position
					normal_r = _collision.normal
				else:
					position_r = Vector3.ZERO
					normal_r = Vector3.ZERO
				if _collision1:
					position_l = _collision1.position
					normal_l = _collision1.normal
				else:
					position_l = Vector3.ZERO
					normal_l = Vector3.ZERO
			position = (position_r + position_l) * 0.5
			#normal = (normal_r + normal_l).normalized()
			normal = normal_r + normal_l # optimization
			seek_flee.target.position = (position + normal * avoid_distance)
		else:
			# only 1 ray by default
			if (dims == AI_TOOLS.Dimensions.TWO):
				var velocity = Vector3(character.velocity.x, 0, character.velocity.y)
				_ray = velocity.normalized() * lookahead + velocity
			else:
				var velocity = character.velocity
				_ray = velocity.normalized() * lookahead + velocity
			_ray1 = _ray
			if dims == AI_TOOLS.Dimensions.TWO:
				_collision = detector.get_collision(
						Vector3(character.position.x, 0,character.position.y), 
						_ray)
			else:
				# Find the collision.
				_collision = detector.get_collision(character.position, _ray)
			_collision1 = _collision
			# If have no collision, do nothing.
			if not _collision:
				# send signal
				_send_signal("obstacle_avoidance", "stop_steering", 
						last_start_stop)
				return SteeringOutput.new(dims)
			# 2. Otherwise create a target and delegate to seek.
			# send signal
			_send_signal("obstacle_avoidance", "start_steering", 
					last_start_stop)
			if dims == AI_TOOLS.Dimensions.TWO:
				seek_flee.target.position = (
						Vector2(_collision.position.x, _collision.position.z) + 
						Vector2(_collision.normal.x, _collision.normal.z) * 
								avoid_distance)
			else:
				seek_flee.target.position = (_collision.position + 
						_collision.normal * avoid_distance)
		#
		return seek_flee.get_steering()


class BlendedSteering extends SteeringBehavior:
	class BehaviorAndWeight extends RefCounted:
		var behavior: SteeringBehavior
		var weight: float
	
	var behaviors: Array = []: set = _set_behaviors
	# The overall maximum acceleration and rotation.
	var max_acceleration: float
	var max_rotation: float
	
	func _set_character(value):
		super._set_character(value)
		# override behaviors' characters
		_set_behaviors(behaviors)
	
	func _set_behaviors(_behaviors:Array):
		behaviors = _behaviors
		for bhv_wgth in _behaviors:
			# override behavior's character
			bhv_wgth.behavior.character = character
	
	func setup(_behaviors, _max_acceleration, _max_rotation,
			_damping = 0.0, _dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.BLENDED_STEERING
		dims = _dims
		character = Kinematic.new(dims)
		character.damping = _damping
		_set_behaviors(_behaviors)
		max_acceleration = _max_acceleration
		max_rotation = _max_rotation
	
	func get_steering() -> SteeringOutput:
		var result = SteeringOutput.new(dims)
		# Accumulate all accelerations.
		for b in behaviors:
			# result += b.weight * b.behavior.get_steering()
			result._iadd(b.behavior.get_steering()._mul(b.weight))
		# Crop the result and return.
		var linear_size = result.linear.length()
		result.linear = (min(linear_size, max_acceleration) * 
				result.linear.normalized())
		result.angular = clamp(result.angular, -max_rotation, max_rotation)
		return result


class PrioritySteering extends SteeringBehavior:
	# Holds a list of BlendedSteering instances, which in turn
	# contain sets of behaviors with their blending weights.
	var groups: Array = []: set = _set_groups
	# A small value, effectively zero.
	var epsilon: float
	
	func _set_character(value):
		super._set_character(value)
		# override groups' characters
		_set_groups(groups)
	
	func _set_groups(value:Array):
		groups = value
		for group in value:
			# override group's character
			group.character = character
	
	func setup(_groups, _epsilon, _damping = 0.0, 
			_dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.PRIORITY_STEERING
		dims = _dims
		character = Kinematic.new(dims)
		character.damping = _damping
		_set_groups(_groups)
		epsilon = _epsilon
	
	func get_steering() -> SteeringOutput:
		var steering = SteeringOutput.new(dims)
		for group in groups:
			# Create the steering structure for accumulation.
			steering = group.get_steering()
			# Check if we’re above the threshold, if so return.
			if (steering.linear.length() > epsilon or
					abs(steering.angular) > epsilon):
				return steering
		# If we get here, it means that no group had a large enough
		# acceleration, so return the small acceleration from the
		# final group.
		return steering


class NavMapPath extends RefCounted:
	var path: PackedVector3Array
	var size: int
	
	func _init(_path:PackedVector3Array=PackedVector3Array()):
		path = _path
		size = _path.size()
	
	func get_point(idx:int) -> Vector3:
		var _idx = int(clamp(idx, 0, size - 1))
		return path[_idx]
	
	func get_segment(idx:int) -> AI_TOOLS.Segment:
		var _idx = int(clamp(idx, 0, size - 2))
		return AI_TOOLS.Segment.new(path[_idx], path[_idx + 1])


class Goal extends RefCounted:
	var dims = AI_TOOLS.Dimensions.THREE
	
	# Flags to indicate if each channel is to be used.
	var has_position: bool = false
	var has_orientation: bool = false
	var has_velocity: bool = false
	var has_rotation: bool = false
	
	# Data for each channel.
	var position
	var orientation: float
	var velocity
	var rotation: float
	
	func _init(_dims=AI_TOOLS.Dimensions.THREE):
		dims = _dims
		if dims == AI_TOOLS.Dimensions.TWO:
			position = Vector2.ZERO
			velocity = Vector2.ZERO
		else:
			position = Vector3.ZERO
			velocity = Vector3.ZERO
	
	# Updates this goal.
	func update_channels(other: Goal):
		if other.has_position:
			position = other.position
			has_position = true
		if other.has_orientation:
			orientation = other.orientation
			has_orientation = true
		if other.has_velocity:
			velocity = other.velocity
			has_velocity = true
		if other.has_rotation:
			rotation = other.rotation
			has_rotation = true


class Targeter extends RefCounted:
	
	func get_goal(character:Kinematic) -> Goal:
		push_warning("Unimplemented get_goal")
		return Goal.new()


class Decomposer extends RefCounted:
	
	func decompose(character:Kinematic, goal:Goal) -> Goal:
		push_warning("Unimplemented decompose")
		return Goal.new()


class Constraint extends RefCounted:
	
	func will_violate(path:NavMapPath) -> bool:
		push_warning("Unimplemented will_violate")
		return false
	
	func suggest(character:Kinematic, path:NavMapPath, goal:Goal) -> Goal:
		push_warning("Unimplemented suggest")
		return Goal.new()


class Actuator extends RefCounted:
	
	func get_path(character:Kinematic, goal:Goal) -> NavMapPath:
		push_warning("Unimplemented get_path")
		return NavMapPath.new()
	
	func output(character:Kinematic, path:NavMapPath, goal:Goal) -> SteeringOutput:
		push_warning("Unimplemented output")
		return SteeringOutput.new()


class SteeringPipeline extends SteeringBehavior:
	# Lists of components at each stage of the pipe.
	var targeters: Array = []
	var decomposers: Array = [] 
	var constraints: Array = []
	var actuator: Actuator
	
	# The number of attempts the algorithm will make to find an
	# unconstrained route.
	var constraint_steps: int
	
	# The deadlock steering behavior.
	var deadlock: SteeringBehavior: set = _set_deadlock
	
	func _set_deadlock(value:SteeringBehavior):
		deadlock = value
		deadlock.character = character
	
	func setup(_constraint_steps, _deadlock, _damping = 0.0, 
			_dims=AI_TOOLS.Dimensions.THREE):
		type = AI_TOOLS.Behavior.STEERING_PIPELINE
		dims = _dims
		constraint_steps = _constraint_steps
		character = Kinematic.new(dims)
		character.damping = _damping
		_set_deadlock(_deadlock)
	
	func get_steering() -> SteeringOutput:
		# Firstly we get the top level goal.
		var goal: Goal = Goal.new(dims)
		for targeter in targeters:
			var targeter_goal = targeter.get_goal(character)
			goal.update_channels(targeter_goal)
		
		# Now we decompose it.
		for decomposer in decomposers:
			goal = decomposer.decompose(character, goal)
		
		# Now we loop through the actuation and constraint process.
		for i in range(constraint_steps):
			# Get the path from the actuator.
			var path = actuator.get_path(character, goal)
			
			# Check for constraint violation.
			var contraint_violated: bool = false
			for constraint in constraints:
			# If we find a violation, get a suggestion.
				if constraint.will_violate(path):
					goal = constraint.suggest(character, path, goal)
					
					# Go to the next iteration of the ’for i in ...’
					# loop to try for the new goal.
					contraint_violated = true
					break
			if contraint_violated:
				continue
					
			# If we’re here it is because we found a valid path.
			return actuator.output(character, path, goal)
			
		# We arrive here if we ran out of constraint steps.
		# We delegate to the deadlock behavior.
		return deadlock.get_steering()


static func _behavior_factory(type:String, setup_args:Array, 
		kinematic=false) -> SteeringBehavior:
	var bhv: SteeringBehavior = null
	var type_name = type.to_upper()
	match type_name:
		"SEEK":
			if !kinematic:
				bhv = SeekFlee.new()
			else:
				bhv = KinematicSeekFlee.new()
			setup_args.append(AI_TOOLS.Behavior.SEEK)
		"FLEE":
			if !kinematic:
				bhv = SeekFlee.new()
			else:
				bhv = KinematicSeekFlee.new()
			setup_args.append(AI_TOOLS.Behavior.FLEE)
		"ARRIVE":
			if !kinematic:
				bhv = ArriveLeave.new()
			else:
				bhv = KinematicArriveLeave.new()
			setup_args.append(AI_TOOLS.Behavior.ARRIVE)
		"LEAVE":
			if !kinematic:
				bhv = ArriveLeave.new()
			else:
				bhv = KinematicArriveLeave.new()
			bhv = ArriveLeave.new()
			setup_args.append(AI_TOOLS.Behavior.LEAVE)
		"WANDER":
			if !kinematic:
				bhv = Wander.new()
			else:
				bhv = KinematicWander.new()
		"ALIGN":
			bhv = Align.new()
		"VELOCITY_MATCH":
			bhv = VelocityMatch.new()
		"PURSUE":
			bhv = PursueEvade.new()
			setup_args.append(AI_TOOLS.Behavior.PURSUE)
		"PURSUE2":
			bhv = PursueEvade2.new()
			setup_args.append(AI_TOOLS.Behavior.PURSUE2)
		"EVADE":
			bhv = PursueEvade.new()
			setup_args.append(AI_TOOLS.Behavior.EVADE)
		"EVADE2":
			bhv = PursueEvade2.new()
			setup_args.append(AI_TOOLS.Behavior.EVADE2)
		"FACE":
			bhv = Face.new()
		"LOOK_WHERE_YOURE_GOING":
			bhv = LookWhereYoureGoing.new()
		"FOLLOW_PATH":
			bhv = FollowPath.new()
			setup_args.append(AI_TOOLS.Behavior.FOLLOW_PATH)
		"FOLLOW_PATH_PREDICTIVE":
			bhv = FollowPath.new()
			setup_args.append(AI_TOOLS.Behavior.FOLLOW_PATH_PREDICTIVE)
		"SEPARATION":
			bhv = SeparationAttraction.new()
			setup_args.append(AI_TOOLS.Behavior.SEPARATION)
		"ATTRACTION":
			bhv = SeparationAttraction.new()
			setup_args.append(AI_TOOLS.Behavior.ATTRACTION)
		"COLLISION_AVOIDANCE":
			bhv = CollisionAvoidance.new()
		"OBSTACLE_AVOIDANCE":
			bhv = ObstacleAvoidance.new()
		_:
			bhv = null
	if bhv:
		# call setup
		bhv.callv("setup", setup_args)
	return bhv
