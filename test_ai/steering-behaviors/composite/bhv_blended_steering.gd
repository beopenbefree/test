extends MeshInstance3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var target_scene: NodePath = "../../../Target"
@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var behaviors_file = "res://test_ai/steering-behaviors/blended_steering_behaviors.json" # (String, FILE)
@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var max_acceleration: float = 5.0
@export var max_rotation: float = 2.0
@export var max_speed: float = 5.0

var blended_steering: AI.BlendedSteering

var behaviors: Array = []
var behaviors_pre_update: Dictionary = {}

var velocity: Vector3
@onready var last_origin := global_transform.origin

@onready var target = get_node(target_scene)


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		var behaviors_dict = _load_behaviors(behaviors_file)
		for type_name in behaviors_dict:
			var setup_args = []
			for arg in behaviors_dict[type_name]["setup_args"]:
				var value = arg.values()[0]
				if (value is String) and (value == "null"):
					value = null
				setup_args.append(value)
			setup_args[-2] = damping
			setup_args[-1] = dims
			var bhv = AI._behavior_factory(type_name, setup_args)
			var bhv_wght = AI.BlendedSteering.BehaviorAndWeight.new()
			bhv_wght.behavior = bhv
			# name
			bhv.name = behaviors_dict[type_name]["name"]
			# weight
			bhv_wght.weight = behaviors_dict[type_name]["weight"] 
			behaviors.append(bhv_wght)
			# _pre_update
			var _pre_update()_func = behaviors_dict[type_name]["_pre_update"]
			behaviors_pre_update[bhv_wght] = funcref(self, _pre_update_func)
			# signal_enable
			var _signal = behaviors_dict[type_name]["signal"]
			if (_signal is String) and (not _signal.is_empty()):
				bhv.signal_enable = true
				bhv.connect(_signal, Callable(self, "_on_steering_behavior_signal"))
			else:
				bhv.signal_enable = false
			blended_steering = AI.BlendedSteering.new()
		blended_steering.setup(behaviors, max_acceleration, max_rotation, damping, dims)
		_initialize()


func _physics_process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)
	# update velocities
	var origin = global_transform.origin
	velocity = (origin - last_origin) / delta
	last_origin = origin


func _initialize():
	var origin = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		blended_steering.character.position = Vector2(origin.x, origin.z)
	else:
		blended_steering.character.position = origin
	blended_steering.character.orientation = AI_TOOLS.map_to_range(
			global_transform.basis.get_euler().y)


func _pre_update(delta):
	for bhv_wght in blended_steering.behaviors:
		var bhv = bhv_wght.behavior
		behaviors_pre_update[bhv_wght].call_func(bhv, delta)


func _pre_update_pos(bhv, delta):
	# steering depends on: target.position
	var position = target.global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		bhv.target.position = Vector2(position.x, position.z)
	else:
		bhv.target.position = position


func _pre_update_ori(bhv, delta):
	# steering depends on: target.orientation
	bhv.target.orientation = AI_TOOLS.map_to_range(
			target.global_transform.basis.get_euler().y)


func _pre_update_vel(bhv, delta):
	# steering depends on: target.velocity
	var velocity = target.velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		bhv.target.velocity = Vector2(velocity.x, velocity.z)
	else:
		bhv.target.velocity = velocity


func _pre_update_char_vel(bhv, delta):
	# steering depends on: character.velocity
	bhv.character.velocity = velocity


func _pre_update_pos_char_pos(bhv, delta):
	# steering depends on: target.position, character.position
	var position = target.global_transform.origin
	var character_position = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		bhv.target.position = Vector2(position.x, position.z)
		bhv.character.position = Vector2(character_position.x,
				character_position.z)
	else:
		bhv.target.position = position
		bhv.character.position = character_position


func _pre_update_pos_vel(bhv, delta):
	# steering depends on: target.position, target.velocity
	var position = target.global_transform.origin
	var velocity = target.velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		bhv.target.position = Vector2(position.x, position.z)
		bhv.target.velocity = Vector2(velocity.x, velocity.z)
	else:
		bhv.target.position = position
		bhv.target.velocity = velocity


func _pre_update_pass(bhv, delta):
	# steering depends on: nothing
	pass


func _update(delta):
	var output:AI.SteeringOutput = blended_steering.get_steering()
	blended_steering.character.update(output, delta, max_speed)


func _post_update(delta):
	var basis = Basis(Vector3.RIGHT, deg_to_rad(90.0))
	var position = blended_steering.character.position
	if dims == AI_TOOLS.Dimensions.TWO:
		global_transform.origin.x = position.x
		global_transform.origin.z = position.y
	else:
		global_transform.origin = position
	global_transform.basis = basis.rotated(Vector3.UP, 
			blended_steering.character.orientation)


func _on_steering_behavior_signal(_name, action):
	print(_name, " - ", action)


func _load_behaviors(json_file):
	var saved_game = File.new()
	if saved_game.open(json_file, File.READ) != OK:
			return false
	var test_json_conv = JSON.new()
	test_json_conv.parse(saved_game.get_as_text())
	var res = test_json_conv.get_data()
	if res.error != OK:
		print("Error ID: ", res.error)
		print("Error string: ", res.error_string)
		print("Error line: ", res.error_line)
		return {}
	saved_game.close()
	return res.result


func _set_enabled(value):
	enabled = value
	visible = value
	set_physics_process(value)


func _get_enabled():
	return enabled
