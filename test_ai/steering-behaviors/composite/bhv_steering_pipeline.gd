extends Node3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var damping = 0.5 # (float, 0.0, 1.0, 0.01)
@export var constraint_steps: int = 1
@export var navmesh_path_optimize: bool = true

@export var max_acceleration: float = 5.0
@export var max_speed: float = 5.0
@export var oa_lookahead: float = 1.0
@export var oa_num_rays = 1 # (int, 1, 2)
@export var oa_whiskers_angle: float = 15
@export var s_targets: Array = []
@export var s_threshold: float = 20.0

var steering_pipeline: AI.SteeringPipeline = null
var target:Node3D = null
var collision_detector:AI.CollisionDetector = null

#<DEBUG
@export var debug_draw_scene: NodePath = null
var debug_draw = null
var debug_lib = preload("res://test_ai/steering-behaviors/debug_draw.gd")
var obstacle_avoidance_constraint = null
@onready var segment_r = ImmediateMesh.new()
@onready var segment_l = ImmediateMesh.new()
@onready var segment_output = ImmediateMesh.new()
@onready var collision_point:MeshInstance3D = MeshInstance3D.new()
#DEBUG>


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		steering_pipeline = AI.SteeringPipeline.new()
		steering_pipeline.setup(constraint_steps,
				AI.SteeringBehavior.new(), damping, dims)
		# set targeters
		var targeter = AISteeringPipelineElements.ChaseTargeter.new(1.0, dims)
		steering_pipeline.targeters.append(targeter)
		# set decomposers
		var decomposer = AISteeringPipelineElements.PlanningDecomposer.new(get_world_3d(),
				navmesh_path_optimize)
		steering_pipeline.decomposers.append(decomposer)
		# set constraints
		var oa_constraint = AISteeringPipelineElements.ObstacleAvoidanceConstraint.new(
				steering_pipeline.character, collision_detector, 
				oa_lookahead, oa_num_rays, oa_whiskers_angle,
				dims, get_world_3d(), navmesh_path_optimize)
		steering_pipeline.constraints.append(oa_constraint)
		
		#<DEBUG
		obstacle_avoidance_constraint = oa_constraint
		#DEBUG>
		
		var s_constarint = AISteeringPipelineElements.SeparationConstraint.new(
				steering_pipeline.character, s_targets, s_threshold, dims, 
				get_world_3d(), navmesh_path_optimize)
		steering_pipeline.constraints.append(s_constarint)
		# set actuator
		var actuator = AISteeringPipelineElements.SeekActuator.new(max_acceleration, damping, dims, 
			get_world_3d())
		steering_pipeline.actuator = actuator
		# set deadlock
		var deadlock = _create_deadlock()
		deadlock.character = steering_pipeline.character
		steering_pipeline.deadlock = deadlock
		_initialize()
		
		#<DEBUG
		if debug_draw_scene != null:
			debug_draw = get_node(debug_draw_scene)
		add_child(segment_r)
		add_child(segment_l)
		var mat = StandardMaterial3D.new()
		mat.albedo_color = Color(1,0,0,1)
		segment_output.material_override = mat
		add_child(segment_output)
		# collision point
		var point = SphereMesh.new()
		point.height = 0.5
		point.radius = 0.25
		mat = StandardMaterial3D.new()
		mat.albedo_color = Color(0,0,0,1)
		point.material = mat
		collision_point.mesh = point
		add_child(collision_point)
		#DEBUG>


func _physics_process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _initialize():
	var position = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		steering_pipeline.character.position = Vector2(position.x, position.z)
	else:
		steering_pipeline.character.position = position


func _pre_update(delta):
	# steering depends on: target.position
	var position = target.global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		steering_pipeline.targeters[0].chased_character.position = (
				Vector2(position.x, position.z))
	else:
		steering_pipeline.targeters[0].chased_character.position = position


func _update(delta):
	var output:AI.SteeringOutput = steering_pipeline.get_steering()
	steering_pipeline.character.update(output, delta, max_speed)
	
	#<DEBUG
	if debug_draw:
		debug_draw.draw_path(steering_pipeline.decomposers[0].path.path)
	debug_lib.debug_draw_obstacle_avoidance(obstacle_avoidance_constraint, 
		self, output, segment_r, segment_l, segment_output, dims)
	var collision = obstacle_avoidance_constraint._collision
	if collision:
		collision_point.show()
		collision_point.global_transform.origin = collision.position
	else:
		collision_point.hide()
	#DEBUG>


func _post_update(delta):
	var origin = steering_pipeline.character.position
	if dims == AI_TOOLS.Dimensions.TWO:
		global_transform.origin.x = origin.x
		global_transform.origin.z = origin.y
	else:
		global_transform.origin = origin


func _create_deadlock() -> AI.SteeringBehavior:
	var wander_offset = 15.0
	var wander_radius = 7.5 
	var wander_rate = 30.0 # degrees
	var wander_orientation = 0.0 # degrees
	var max_angular_acceleration = 5.0
	var max_rotation = 5.0
	var target_radius = 5.0 # degrees
	var slow_radius = 45.0 # degrees
	var time_to_target = 0.1
	var deadlock:AI.SteeringBehavior = AI._behavior_factory("WANDER",
			[max_angular_acceleration, max_rotation, deg_to_rad(target_radius), 
					deg_to_rad(slow_radius), time_to_target, wander_offset, wander_radius,
					deg_to_rad(wander_rate), deg_to_rad(wander_orientation), max_acceleration, 
					damping, dims])
	return deadlock


func _set_enabled(value):
	enabled = value
	visible = value
	set_physics_process(value)


func _get_enabled():
	return enabled
