extends Node3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var target_scene: NodePath = "../Target"
@export var nav_mesh_scene: NodePath = "../NavigationRegion3D"
@export var obstacles_scene: PackedScene = preload("res://test_ai/steering-behaviors/obstacles_navmesh.tscn")
@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var damping = 0.5 # (float, 0.0, 1.0, 0.01)
@export var constraint_steps: int = 1
@export var navmesh_agent_radius: float = 2.0
@export var navmesh_path_optimize: bool = true

@export var max_acceleration: float = 5.0
@export var max_speed: float = 5.0

var pipeline: AI.SteeringPipeline


@onready var target:Node3D = get_node(target_scene)
@onready var nav_mesh:NavigationRegion3D = get_node(nav_mesh_scene)
@onready var obstacles:Node3D = obstacles_scene.instantiate()

#<DEBUG
@export var debug_draw_scene: NodePath = null
var debug_draw = null
#DEBUG>


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		# add obstacles
		nav_mesh.add_child(obstacles)
		# bake the NavigationMeshInstance
		nav_mesh.connect("bake_finished", Callable(self, "_on_nav_mesh_ready"))
		nav_mesh.navigation_mesh.agent_radius = navmesh_agent_radius
		nav_mesh.bake_navigation_mesh(true)
		set_process(false)
		
		#<DEBUG
		if debug_draw_scene != null:
			 debug_draw = get_node(debug_draw_scene)
		#DEBUG>


func _process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _initialize():
	var position = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		pipeline.character.position = Vector2(position.x, position.z)
	else:
		pipeline.character.position = position


func _pre_update(delta):
	# steering depends on: target.position
	var position = target.global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		pipeline.targeters[0].chased_character.position = (
				Vector2(position.x, position.z))
	else:
		pipeline.targeters[0].chased_character.position = position


func _update(delta):
	var output:AI.SteeringOutput = pipeline.get_steering()
	pipeline.character.update(output, delta, max_speed)
	
	#<DEBUG
	if debug_draw:
		debug_draw.draw_path(pipeline.decomposers[0].path.path)
	#DEBUG>


func _post_update(delta):
	var origin = pipeline.character.position
	if dims == AI_TOOLS.Dimensions.TWO:
		global_transform.origin.x = origin.x
		global_transform.origin.z = origin.y
	else:
		global_transform.origin = origin


func _on_nav_mesh_ready():
	pipeline = AI.SteeringPipeline.new()
	pipeline.setup(constraint_steps, AI.SteeringBehavior.new(),
			damping, dims)
	# set targeters
	var targeter = AISteeringPipelineElements.ChaseTargeter.new(1.0, dims)
	pipeline.targeters.append(targeter)
	# set decomposers
	var decomposer = AISteeringPipelineElements.PlanningDecomposer.new(get_world_3d(),
			navmesh_path_optimize)
	pipeline.decomposers.append(decomposer)
	# set constraints
	var margin = 0.4
	for mesh_inst in obstacles.get_children():
		if mesh_inst is MeshInstance3D:
			var center = mesh_inst.global_transform.origin
			var radius = mesh_inst.mesh.radius
			var constraint = AISteeringPipelineElements.AvoidObstacleConstraint.new(center, 
					radius, margin)
			pipeline.constraints.append(constraint)
	# set actuator
	var actuator = AISteeringPipelineElements.SeekActuator.new(max_acceleration, damping, dims, 
		get_world_3d())
	pipeline.actuator = actuator
	# set deadlock
	var deadlock = _create_deadlock()
	pipeline.deadlock = deadlock
	_initialize()
	set_process(true)


func _create_deadlock() -> AI.SteeringBehavior:
	var wander_offset = 15.0
	var wander_radius = 7.5 
	var wander_rate = 30.0 # degrees
	var wander_orientation = 0.0 # degrees
	var max_angular_acceleration = 5.0
	var max_rotation = 5.0
	var target_radius = 5.0 # degrees
	var slow_radius = 45.0 # degrees
	var time_to_target = 0.1
	var deadlock:AI.SteeringBehavior = AI._behavior_factory("WANDER",
			[max_angular_acceleration, max_rotation, deg_to_rad(target_radius), 
					deg_to_rad(slow_radius), time_to_target, wander_offset, wander_radius,
					deg_to_rad(wander_rate), deg_to_rad(wander_orientation), max_acceleration, 
					damping, dims])
	return deadlock


func _set_enabled(value):
	enabled = value
	visible = value
	set_process(value)


func _get_enabled():
	return enabled
