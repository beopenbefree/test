extends Node3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var target_scene: NodePath = "../Target"
@export var nav_mesh_scene: NodePath = "../NavigationRegion3D"
@export var navmesh_agent_radius: float = 2.0
@export var obstacles_scene: PackedScene = preload(
		"res://test_ai/steering-behaviors/obstacles_navmesh.tscn")
@export var character_scene: PackedScene = preload(
		"res://test_ai/steering-behaviors/composite/bhv_steering_pipeline.tscn")

@export var flock_num: int = 5

@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)
@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var constraint_steps: int = 2
@export var navmesh_path_optimize: bool = true
#
@export var max_acceleration: float = 5.0
@export var max_speed: float = 5.0
@export var oa_lookahead: float = 1.0
@export var oa_num_rays = 1 # (int, 1, 2)
@export var oa_whiskers_angle: float = 15
@export var s_threshold: float = 5.0

@export var placement_boundary: float = 50.0

@onready var target:Node3D = get_node(target_scene)
@onready var nav_mesh:NavigationRegion3D = get_node(nav_mesh_scene)
@onready var obstacles:Node3D = obstacles_scene.instantiate()

#<DEBUG
@export var debug_draw_scene: PackedScene = preload(
		"res://test_ai/steering-behaviors/debug_draw_path.tscn")
#DEBUG>


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		# add obstacles
		nav_mesh.add_child(obstacles)
		# bake the NavigationMeshInstance
		nav_mesh.connect("bake_finished", Callable(self, "_on_nav_mesh_ready"))
		nav_mesh.navigation_mesh.agent_radius = navmesh_agent_radius
		nav_mesh.bake_navigation_mesh(true)


func _on_nav_mesh_ready():
	randomize()
	# create a common collision detector: for obstacle avoidance constraints
	var collision_detector = AI.CollisionDetector.new()
	add_child(collision_detector.detector)
	# create the characters
	var all_targets = []
	for i in range(flock_num):
		var character = character_scene.instantiate()
		character.enabled = _get_enabled()
		character.dims = dims
		character.damping = damping
		character.constraint_steps = constraint_steps
		character.navmesh_path_optimize = navmesh_path_optimize
		character.max_acceleration = max_acceleration
		character.max_speed = max_speed
		character.collision_detector = collision_detector
		character.oa_lookahead = oa_lookahead
		character.oa_num_rays = oa_num_rays
		character.oa_whiskers_angle = oa_whiskers_angle
		character.s_targets = []
		character.s_threshold = s_threshold
		
		#<DEBUG
		var debug_draw = debug_draw_scene.instantiate()
		add_child(debug_draw)
		character.debug_draw = debug_draw
		#DEBUG>
		
		# place it randomly
		var x = AI_TOOLS.random_binomial() * placement_boundary
		var z = AI_TOOLS.random_binomial() * placement_boundary
		character.transform.origin = Vector3(x, 2, z)
		add_child(character)
		character.target = target
		all_targets.append(character.steering_pipeline.character)
	# updates s_targets: i.e. the others characters
	for character in get_children():
		if not "steering_pipeline" in character:
			continue
		for constraint in character.steering_pipeline.constraints:
			if constraint is AISteeringPipelineElements.SeparationConstraint:
				var targets = []
				for other in all_targets:
					if other != character.steering_pipeline.character:
						targets.append(other)
				constraint.targets.append_array(targets)


func _set_enabled(value):
	enabled = value
	visible = value
	for child in get_children():
		if "visible" in child:
			child.visible = value
		if "enabled" in child:
			child.enabled = value


func _get_enabled():
	return enabled
