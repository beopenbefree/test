extends MeshInstance3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var target_scene: NodePath = "../../../Target"
@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

# priority steering parameters
@export var behaviors_file = ( # (String, FILE)
		"res://test_ai/steering-behaviors/priority_steering_behaviors.json")
@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var epsilon: float = 1.0e-3
@export var max_acceleration: float = 5.0
@export var max_rotation: float = 2.0
@export var max_speed: float = 5.0

var priority_steering: AI.PrioritySteering

var behaviors_dict: Dictionary = {}
var behaviors_pre_update: Dictionary = {}
var velocity: Vector3

# groups of behaviors
var behavior_groups_dict = {
	"Separation":{
		"behavior_set": ["separation"],
		"priority": 2
	},
	"Pursuit":{
		"behavior_set": ["pursue"],
		"priority": 3
	},
	"Collision Avoidance":{
		"behavior_set": ["collision_avoidance", "obstacle_avoidance"],
		"priority": 1
	},
}

@onready var last_origin := global_transform.origin
@onready var target = get_node(target_scene)

#<DEBUG
var debug_lib = preload("res://test_ai/steering-behaviors/debug_draw.gd")
var obstacle_avoidance: AI.ObstacleAvoidance
@onready var circle = MeshInstance3D.new()
@onready var segment_r = ImmediateMesh.new()
@onready var segment_l = ImmediateMesh.new()
@onready var segment_output = ImmediateMesh.new()
#DEBUG>


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		behaviors_dict = _load_behaviors(behaviors_file)
		# setup each behavior group in order
		var priority_group_list = []
		var behavior_groups = []
		for group_name in behavior_groups_dict:
			var behavior_names = behavior_groups_dict[group_name]["behavior_set"]
			var behavior_group = _behavior_group_setup(group_name, behavior_names,
					max_acceleration, max_rotation, damping, dims)
			# insert the behavior group into priority list
			var priority = behavior_groups_dict[group_name]["priority"]
			var pos = 0
			while pos < priority_group_list.size():
				if priority <= priority_group_list[pos]:
					break
				pos += 1
			priority_group_list.insert(pos, priority)
			behavior_groups.insert(pos, behavior_group)
			
			#<DEBUG
			var idx = behavior_names.find("obstacle_avoidance")
			if idx >= 0:
				obstacle_avoidance = behavior_group.behaviors[idx].behavior
			#DEBUG>
		
		priority_steering = AI.PrioritySteering.new()
		priority_steering.setup(behavior_groups, epsilon, damping, dims)
		_initialize()
		
		#<DEBUG
		circle.mesh = debug_lib.get_circle_mesh(
				obstacle_avoidance.avoid_distance)
		add_child(circle)
		circle.rotate(Vector3.RIGHT, deg_to_rad(90))
		add_child(segment_r)
		add_child(segment_l)
		var mat = StandardMaterial3D.new()
		mat.albedo_color = Color(1,0,0,1)
		segment_output.material_override = mat
		add_child(segment_output)
		#DEBUG>


func _behavior_group_setup(group_name:String, behavior_names:Array,
		max_acceleration, max_rotation, damping, dims):
	print(group_name + " group construction...")
	var group = AI.BlendedSteering.new()
	var group_behaviors: Array = []
	for bhv_name in behavior_names:
		var bhv_name_lower = bhv_name.to_lower()
		#
		print("... " + bhv_name + " behavior construction...")
		var setup_args = []
		for arg in behaviors_dict[bhv_name_lower]["setup_args"]:
			var value = arg.values()[0]
			if (value is String) and (value == "null"):
				value = null
			setup_args.append(value)
		setup_args[-2] = damping
		setup_args[-1] = dims
		var behavior = AI._behavior_factory(bhv_name, setup_args)
		#
		print("... " + bhv_name + " setting name...")
		behavior.name = behaviors_dict[bhv_name_lower]["name"]
		#
		print("... " + bhv_name + " BehaviorAndWeight construction...")
		var bhv_wght = AI.BlendedSteering.BehaviorAndWeight.new()
		bhv_wght.behavior = behavior
		bhv_wght.weight = behaviors_dict[bhv_name_lower]["weight"]
		group_behaviors.append(bhv_wght)
		#
		print("... " + bhv_name + " pre_update function setting...")
		var pre_update()_func = behaviors_dict[bhv_name_lower]["_pre_update"]
		behaviors_pre_update[bhv_wght] = funcref(self, pre_update_func)
		#
		print("... " + bhv_name + " setting signals...")
		var _signal = behaviors_dict[bhv_name_lower]["signal"]
		if (_signal is String) and (not _signal.is_empty()):
			behavior.signal_enable = true
			behavior.connect(_signal, Callable(self, "_on_steering_behavior_signal"))
		else:
			behavior.signal_enable = false
	#
	print(group_name + " group final setup")
	group.setup(group_behaviors, max_acceleration, max_rotation, damping, dims)
	return group


func _physics_process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)
	# update velocities
	var origin = global_transform.origin
	velocity = (origin - last_origin) / delta
	last_origin = origin


func _initialize():
	var origin = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		priority_steering.character.position = Vector2(origin.x, origin.z)
	else:
		priority_steering.character.position = origin
	priority_steering.character.orientation = AI_TOOLS.map_to_range(
			global_transform.basis.get_euler().y)


func _pre_update(delta):
	for group in priority_steering.groups:
		for bhv_wght in group.behaviors:
			var bhv = bhv_wght.behavior
			behaviors_pre_update[bhv_wght].call_func(bhv, delta)


func _pre_update_pos(bhv, delta):
	# steering depends on: target.position
	var position = target.global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		bhv.target.position = Vector2(position.x, position.z)
	else:
		bhv.target.position = position


func _pre_update_ori(bhv, delta):
	# steering depends on: target.orientation
	bhv.target.orientation = AI_TOOLS.map_to_range(
			target.global_transform.basis.get_euler().y)


func _pre_update_vel(bhv, delta):
	# steering depends on: target.velocity
	var velocity = target.velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		bhv.target.velocity = Vector2(velocity.x, velocity.z)
	else:
		bhv.target.velocity = velocity


func _pre_update_char_vel(bhv, delta):
	# steering depends on: character.velocity
	bhv.character.velocity = velocity


func _pre_update_pass(bhv, delta):
	# steering depends on: nothing
	pass


func _pre_update_pos_vel(bhv, delta):
	# steering depends on: target.position, target.velocity
	var position = target.global_transform.origin
	var velocity = target.velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		bhv.target.position = Vector2(position.x, position.z)
		bhv.target.velocity = Vector2(velocity.x, velocity.z)
	else:
		bhv.target.position = position
		bhv.target.velocity = velocity


func _update(delta):
	var output:AI.SteeringOutput = priority_steering.get_steering()
	
	#<DEBUG
	debug_lib.debug_draw_obstacle_avoidance(obstacle_avoidance, 
		self, output, segment_r, segment_l, segment_output, dims)
	#DEBUG>
	
	priority_steering.character.update(output, delta, max_speed)


func _post_update(delta):
	var basis = Basis(Vector3.RIGHT, deg_to_rad(90.0))
	var position = priority_steering.character.position
	if dims == AI_TOOLS.Dimensions.TWO:
		global_transform.origin.x = position.x
		global_transform.origin.z = position.y
	else:
		global_transform.origin = position
	global_transform.basis = basis.rotated(Vector3.UP, 
			priority_steering.character.orientation)


func _on_steering_behavior_signal(_name, action):
	print(_name, " - ", action)


func _load_behaviors(json_file):
	var saved_game = File.new()
	if saved_game.open(json_file, File.READ) != OK:
			return false
	var test_json_conv = JSON.new()
	test_json_conv.parse(saved_game.get_as_text())
	var res = test_json_conv.get_data()
	if res.error != OK:
		print("Error ID: ", res.error)
		print("Error string: ", res.error_string)
		print("Error line: ", res.error_line)
		return {}
	saved_game.close()
	return res.result


func _set_enabled(value):
	enabled = value
	visible = value
	set_physics_process(value)


func _get_enabled():
	return enabled
