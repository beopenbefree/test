extends StaticBody3D


@export var linear_speed: float = 15.0
@export var angular_speed: float = 2.0

var velocity: Vector3
var rotation_velocity: float
@onready var last_origin := global_transform.origin
@onready var last_orientation := global_transform.basis.get_euler().y


func _process(delta):
	if Input.is_action_pressed("ui_up"):
		translate_object_local(Vector3.UP * delta * linear_speed)
	if Input.is_action_pressed("ui_down"):
		translate_object_local(-Vector3.UP * delta * linear_speed)
	if Input.is_action_pressed("ui_left"):
		rotate_object_local(Vector3.FORWARD, delta * angular_speed)
	if Input.is_action_pressed("ui_right"):
		rotate_object_local(Vector3.FORWARD, -delta * angular_speed)
	if Input.is_action_pressed("ui_page_up"):
		translate_object_local(Vector3.FORWARD * delta * linear_speed)
	if Input.is_action_pressed("ui_page_down"):
		translate_object_local(Vector3.FORWARD * -delta * linear_speed)
	# update velocities
	var origin = global_transform.origin
	velocity = (origin - last_origin) / delta
	last_origin = origin
	#
	var orientation = -global_transform.basis.get_euler().y
	rotation_velocity = (orientation - last_orientation) / delta
	last_orientation = orientation
