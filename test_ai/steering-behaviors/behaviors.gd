extends Node3D


@onready var cameras = [
	$OrbitCamera,
	$BehaviorList/Vehicle/PursueGatherVehicle/PrioritySteeringVehicle/CarBaseAI/Body/OrbitCamera,
	$Target/OrbitCamera
]
@onready var camera_idx := 0


func _input(event:InputEvent):
	# Receives key input
	if event.is_action_pressed("toggle_camera"):
		cameras[camera_idx].current = false
		var new_camera = null
		while new_camera == null:
			camera_idx = (camera_idx + 1) % cameras.size()
			new_camera = cameras[camera_idx]
		new_camera.current = true
