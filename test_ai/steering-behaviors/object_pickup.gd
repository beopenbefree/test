extends Node3D


@export var enabled: bool = true: get = _get_enabled, set = _set_enabled

@export (int, "full size", "small") var kit_size = 0: set = _set_kit_size

# 0 = full size pickup, 1 = small pickup
@export var AMMO_AMOUNTS: Array = [4, 1]
# 0 = full size pickup, 1 = small pickup
@export var GRENADE_AMOUNTS: Array = [2, 0]

@export var RESPAWN_TIME: float = 20
var respawn_timer = 0

var is_ready = false
var velocity = Vector3.ZERO


func _ready():
	$Holder/AmmoPickupTrigger.connect("body_entered", Callable(self, "_trigger_body_entered"))
	
	is_ready = true
	
	# Hide all of the possible kit sizes
	_set_kit_size_values(0, false)
	_set_kit_size_values(1, false)
	
	_set_kit_size_values(kit_size, true)
	# enablers' side effects
	_set_enabled(enabled)
	# optimization
	set_physics_process(false)


func _physics_process(delta):
	if respawn_timer > 0:
		respawn_timer -= delta
		
		if respawn_timer <= 0:
			_set_kit_size_values(kit_size, true)
			set_physics_process(false)


func _trigger_body_entered(body):
	if enabled:
		var grand_parent = body.get_parent().get_parent().get_parent()
		if grand_parent and grand_parent.has_method("object_gathered"):
			grand_parent.object_gathered()
		_do_respawn()


func _do_respawn():
	respawn_timer = RESPAWN_TIME
	_set_kit_size_values(kit_size, false)
	set_physics_process(true)


func _set_kit_size(value):
	if enabled:
		if is_ready:
			_set_kit_size_values(kit_size, false)
			kit_size = value
			
			_set_kit_size_values(kit_size, true)
		else:
			kit_size = value


func _set_kit_size_values(size, enable):
	if enabled:
		if size == 0:
			$Holder/AmmoPickupTrigger/ShapeKit.disabled = !enable
			$Holder/AmmoKit.visible = enable
		elif size == 1:
			$Holder/AmmoPickupTrigger/ShapeKitSmall.disabled = !enable
			$Holder/AmmoKitSmall.visible = enable


func _set_enabled(value):
	enabled = value
	set_physics_process(enabled)


func _get_enabled():
	return enabled
