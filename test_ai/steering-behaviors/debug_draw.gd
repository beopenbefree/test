extends Node


static func get_segment(a, b):
	var segment = PackedVector3Array()
	segment.append(a)
	segment.append(b)
	return segment


static func get_segment_mesh(a,b):
	return _get_mesh(get_segment(a, b))


static func get_circle(radius, num_points=16):
	var delta = 2 * PI / num_points
	var circle = PackedVector3Array()
	for i in range(num_points):
		var p = Vector3(sin(delta * i), 0, cos(delta * i)) * radius
		circle.append(p)
	return circle


static func get_circle_mesh(radius, num_points=16):
	return _get_mesh(get_circle(radius, num_points))


static func _get_mesh(vertices):
	# Initialize the ArrayMesh.
	var arr_mesh = ArrayMesh.new()
	var arrays = []
	arrays.resize(ArrayMesh.ARRAY_MAX)
	arrays[ArrayMesh.ARRAY_VERTEX] = vertices
	# Create the Mesh.
	arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_LINE_LOOP, arrays)
	return arr_mesh


static func debug_draw_immediate(drawable:ImmediateMesh, primitive, points):
	drawable.clear()
	drawable.begin(primitive)
	for p in points:
		drawable.add_vertex(p)
	drawable.end()


static func debug_draw_obstacle_avoidance(obstacle_avoidance, object,
		output, segment_r, segment_l, segment_output, dims):
	var a = (object.global_transform.origin) * object.global_transform
	if obstacle_avoidance:
		var whisker_r = obstacle_avoidance._ray
		var whisker_l = obstacle_avoidance._ray1
		var b_r = object.global_transform.xform_inv(object.global_transform.origin + 
				whisker_r)
		var b_l = object.global_transform.xform_inv(object.global_transform.origin + 
				whisker_l)
		var points_r = get_segment(a, b_r)
		debug_draw_immediate(segment_r, Mesh.PRIMITIVE_LINE_LOOP, points_r)
		var points_l = get_segment(a, b_l)
		debug_draw_immediate(segment_l, Mesh.PRIMITIVE_LINE_LOOP, points_l)	
		#
	var output_linear
	if dims == AI_TOOLS.Dimensions.TWO:
		output_linear = Vector3(output.linear.x, 0, output.linear.y)
	else:
		output_linear = output.linear
	var b = object.global_transform.xform_inv(object.global_transform.origin + 
			output_linear)
	var points = get_segment(a, b)
	debug_draw_immediate(segment_output, Mesh.PRIMITIVE_LINE_LOOP, points)
	#DEBUG>
