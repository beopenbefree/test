extends MeshInstance3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var target_scene: NodePath = "../../../Target"
@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var flee: bool = false

@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var max_acceleration: float = 5.0
@export var max_speed: float = -1.0

var seek_flee: AI.SeekFlee

@onready var target = get_node(target_scene)


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		seek_flee = AI.SeekFlee.new()
		seek_flee.setup(max_acceleration, damping, dims,
				AI_TOOLS.Behavior.FLEE if flee else AI_TOOLS.Behavior.SEEK)
		_initialize()


func _process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _initialize():
	var position = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		seek_flee.character.position = Vector2(position.x, position.z)
	else:
		seek_flee.character.position = position


func _pre_update(delta):
	# steering depends on: target.position
	var position = target.global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		seek_flee.target.position = Vector2(position.x, position.z)
	else:
		seek_flee.target.position = position


func _update(delta):
	var output:AI.SteeringOutput = seek_flee.get_steering()
	seek_flee.character.update(output, delta, max_speed)


func _post_update(delta):
	var origin = seek_flee.character.position
	if dims == AI_TOOLS.Dimensions.TWO:
		global_transform.origin.x = origin.x
		global_transform.origin.z = origin.y
	else:
		global_transform.origin = origin


func _set_enabled(value):
	enabled = value
	visible = value
	set_process(value)


func _get_enabled():
	return enabled
