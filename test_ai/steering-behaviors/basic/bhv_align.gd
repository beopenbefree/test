extends MeshInstance3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var target_scene: NodePath = "../../../Target"
@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var signal_enabled: bool = false
@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var max_angular_acceleration: float = 5.0
@export var max_rotation: float = 5.0
@export var target_radius: float = 5.0 # degrees
@export var slow_radius: float = 45.0 # degrees
@export var time_to_target: float = 0.1

var align: AI.Align

@onready var target = get_node(target_scene)


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		align = AI.Align.new()
		align.setup(max_angular_acceleration, max_rotation, deg_to_rad(target_radius), 
				deg_to_rad(slow_radius), time_to_target, damping, dims)
		if signal_enabled:
			align.signal_enable = signal_enabled
			align.connect("align", Callable(self, "_on_AI_align").bind("align"))
		_initialize()


func _process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _initialize():
	align.character.orientation = AI_TOOLS.map_to_range(
			global_transform.basis.get_euler().y)


func _pre_update(delta):
	# steering depends on: target.orientation
	align.target.orientation = AI_TOOLS.map_to_range(
			target.global_transform.basis.get_euler().y)


func _update(delta):
	var output:AI.SteeringOutput = align.get_steering()
	align.character.update(output, delta)


func _post_update(delta):
	var basis = Basis(Vector3.RIGHT, deg_to_rad(90.0))
	global_transform.basis = basis.rotated(Vector3.UP, 
			align.character.orientation)


func _on_AI_align(object_name, action, signal_name):
	print(signal_name, ": (",object_name, ", ", action, ")")


func _set_enabled(value):
	enabled = value
	visible = value
	set_process(value)


func _get_enabled():
	return enabled
