extends Node3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var character_scene: PackedScene = preload(
		"res://test_ai/steering-behaviors/basic/bhv_obstacle_avoidance.tscn")
@export var flock_num: int = 5

@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)
@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var max_acceleration: float = 50.0
@export var avoid_distance: float = 25.0
@export var lookahead: float = 1.0
@export var num_rays = 1 # (int, 1, 2)
@export var whiskers_angle: float = 15
#
@export var speed: float = 15.0
#
@export var enable_first: bool = false
@export var damping_first = 0.0 # (float, 0.0, 1.0, 0.01)
@export var max_acceleration_first: float = 50.0
@export var avoid_distance_first: float = 25.0
@export var lookahead_first: float = 15.0
@export var num_rays_first = 1 # (int, 1, 2)
@export var whiskers_angle_first: float = 15
#
@export var speed_first: float = 15.0
#
@export var motion_boundary: float = 150.0

var obstacles_scene = preload("res://test_ai/steering-behaviors/obstacles.tscn")


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		randomize()
		var obstacles = obstacles_scene.instantiate()
		add_child(obstacles)
		# create a common collision detector
		var collision_detector = AI.CollisionDetector.new()
		add_child(collision_detector.detector)
		# create the characters
		for i in range(flock_num):
			var character = character_scene.instantiate()
			character.enabled = _get_enabled()
			character.dims = dims
			character.damping = damping
			character.max_acceleration = max_acceleration
			character.collision_detector = collision_detector
			character.avoid_distance = avoid_distance
			character.lookahead = lookahead
			character.num_rays = num_rays
			character.whiskers_angle = whiskers_angle
			character.boundary = motion_boundary
			character.max_speed = speed
			# place it randomly
			var x = AI_TOOLS.random_binomial() * motion_boundary
			var z = AI_TOOLS.random_binomial() * motion_boundary
			character.transform.origin = Vector3(x, 2, z)
			add_child(character)
			# enable the first character for special behavior
			if enable_first:
				if i == 0:
					character.obstacle_avoidance.signal_enable = true
					character.obstacle_avoidance.character.damping = damping_first
					character.obstacle_avoidance.seek_flee.max_acceleration = max_acceleration_first
					character.obstacle_avoidance.avoid_distance = avoid_distance_first
					character.obstacle_avoidance.lookahead = lookahead_first
					character.obstacle_avoidance.num_rays = num_rays_first
					character.obstacle_avoidance.whiskers_angle = whiskers_angle_first
					character.max_speed = speed_first
					var mat = StandardMaterial3D.new()
					mat.albedo_color = Color(0,0,0,1)
					character.material_override = mat
				else:
					character.obstacle_avoidance.signal_enable = false
			else:
				# for all characters the same behavior
				character.obstacle_avoidance.signal_enable = true
			# give some random motion
			var vx = AI_TOOLS.random_binomial()
			var vz = AI_TOOLS.random_binomial()
			var dir
			if dims == AI_TOOLS.Dimensions.TWO:
				dir = Vector2(vx, vz).normalized()
			else:
				dir = Vector3(vx, 0, vz).normalized()
			if enable_first and (i == 0):
					character.obstacle_avoidance.character.velocity = dir * speed_first
			else:
				character.obstacle_avoidance.character.velocity = dir * speed


func _set_enabled(value):
	enabled = value
	visible = value
	for child in get_children():
		if "visible" in child:
			child.visible = value
		if "enabled" in child:
			child.enabled = value


func _get_enabled():
	return enabled
