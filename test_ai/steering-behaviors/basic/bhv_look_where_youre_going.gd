extends MeshInstance3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var signal_enabled: bool = false
@export var linear_speed: float = 15.0
@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var max_angular_acceleration: float = 5.0
@export var max_rotation: float = 5.0
@export var target_radius: float = 5.0 # degrees
@export var slow_radius: float = 45.0 # degrees
@export var time_to_target: float = 0.1

var look_where_youre_going: AI.LookWhereYoureGoing

var velocity: Vector3
@onready var last_origin := global_transform.origin


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		look_where_youre_going = AI.LookWhereYoureGoing.new()
		look_where_youre_going.setup(max_angular_acceleration, max_rotation, 
				deg_to_rad(target_radius), deg_to_rad(slow_radius), time_to_target, 
				damping, dims)
		if signal_enabled:
			look_where_youre_going.signal_enable = signal_enabled
			look_where_youre_going.connect("look_where_youre_going", self, 
					"_on_AI_look_where_youre_going", ["look_where_youre_going"])
			look_where_youre_going.align.connect("align", self, 
					"_on_AI_look_where_youre_going", ["align"])
		_initialize()


func _process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)
	# update position
	if Input.is_action_pressed("up"):
		global_transform.origin.z -= delta * linear_speed
	if Input.is_action_pressed("down"):
		global_transform.origin.z += delta * linear_speed
	if Input.is_action_pressed("left"):
		global_transform.origin.x -= delta * linear_speed
	if Input.is_action_pressed("right"):
		global_transform.origin.x += delta * linear_speed
	# update velocities
	var origin = global_transform.origin
	velocity = (origin - last_origin) / delta
	last_origin = origin


func _initialize():
	look_where_youre_going.character.orientation = AI_TOOLS.map_to_range(
			global_transform.basis.get_euler().y)


func _pre_update(delta):
	# steering depends on: character.velocity
	look_where_youre_going.character.velocity = velocity


func _update(delta):
	var output:AI.SteeringOutput = look_where_youre_going.get_steering()
	look_where_youre_going.character.update(output, delta)


func _post_update(delta):
	var basis = Basis(Vector3.RIGHT, deg_to_rad(90.0))
	global_transform.basis = basis.rotated(Vector3.UP, 
			look_where_youre_going.character.orientation)


func _on_AI_look_where_youre_going(object_name, action, signal_name):
	print(signal_name, ": (",object_name, ", ", action, ")")


func _set_enabled(value):
	enabled = value
	visible = value
	set_process(value)


func _get_enabled():
	return enabled
