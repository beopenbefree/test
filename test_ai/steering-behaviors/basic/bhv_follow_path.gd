extends MeshInstance3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var predictive: bool = false

@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var max_acceleration: float = 50.0
@export var path: NodePath = "../../../Path3D"
@export var path_offset = 0.05 # (float, -0.3, 0.3, 0.001)
@export var predict_time: float = 0.1
@export var max_speed: float = 10.0

var follow_path: AI.FollowPath

#<DEBUG
@export var debug_scene: NodePath = "../../../DebugScene"
var sphere: SphereMesh
var mat1: StandardMaterial3D
var rabbit: MeshInstance3D
#DEBUG>


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		follow_path = AI.FollowPath.new()
		var path_ai = AI.PathAI.new(get_node(path))
		var behavior_type = AI_TOOLS.Behavior.FOLLOW_PATH
		if predictive:
			behavior_type = AI_TOOLS.Behavior.FOLLOW_PATH_PREDICTIVE
		follow_path.setup(max_acceleration, path_ai, path_offset, predict_time, 
				damping, dims, behavior_type)
		_initialize()
		
		#<DEBUG
		sphere = SphereMesh.new()
		mat1 = StandardMaterial3D.new()
		rabbit = MeshInstance3D.new()
		sphere.radial_segments = 8
		sphere.rings = 4
		mat1.albedo_color = Color(1, 0, 0, 1)
		_draw(path_ai.path.curve.get_baked_points(), 0.5, get_node(debug_scene), mat1)
		#
		get_node(debug_scene).add_child(rabbit)
		rabbit.mesh = sphere 
		rabbit.scale = Vector3.ONE
		var mat2 := StandardMaterial3D.new()
		mat2.albedo_color = Color(1, 1, 0, 1)
		rabbit.material_overlay = mat2
		var start_offset
		if path_offset > 0:
			start_offset = 0
		else:
			start_offset = 1.0
		rabbit.global_transform.origin = path_ai.get_position(start_offset)
		#DEBUG>


func _process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)
	
	#<DEBUG
	rabbit.global_transform.origin = follow_path.path.get_position(
			follow_path.current_param + follow_path.path_offset)
	#DEBUG>


func _initialize():
	var position = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		follow_path.character.position = Vector2(position.x, position.z)
	else:
		follow_path.character.position = position


func _pre_update(delta):
	pass


func _update(delta):
	var output:AI.SteeringOutput = follow_path.get_steering()
	follow_path.character.update(output, delta, max_speed)


func _post_update(delta):
	var origin = follow_path.character.position
	if dims == AI_TOOLS.Dimensions.TWO:
		global_transform.origin.x = origin.x
		global_transform.origin.z = origin.y
	else:
		global_transform.origin = origin

#<DEBUG
func _draw(items, scaling, scene, mat=null):
	for p in items:
		var i = MeshInstance3D.new()
		scene.add_child(i)
		i.mesh = sphere
		i.scale = Vector3.ONE * scaling
		if mat:
			i.material_overlay = mat
		i.global_transform.origin = p
#DEBUG>

func _set_enabled(value):
	enabled = value
	visible = value
	set_process(value)
	
	#<DEBUG
	var debug_node = get_node(debug_scene)
	if debug_node:
		debug_node.visible = value
	#DEBUG>



func _get_enabled():
	return enabled
