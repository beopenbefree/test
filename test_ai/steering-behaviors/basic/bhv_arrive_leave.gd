extends MeshInstance3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var target_scene: NodePath = "../../../Target"
@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var leave: bool = false

@export var signal_enabled: bool = false
@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var max_acceleration: float = 5.0
@export var max_speed: float = 5.0
@export var target_radius: float = 7.5
@export var slow_radius: float = 15.0
@export var time_to_target: float = 0.1

var arrive_leave: AI.ArriveLeave

@onready var target = get_node(target_scene)


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		arrive_leave = AI.ArriveLeave.new()
		arrive_leave.setup(max_acceleration, max_speed, target_radius,
				slow_radius, time_to_target, damping, dims,
				AI_TOOLS.Behavior.LEAVE if leave else AI_TOOLS.Behavior.ARRIVE)
		if signal_enabled:
			arrive_leave.signal_enable = signal_enabled
			arrive_leave.connect("arrive_leave", self, "_on_AI_arrive_leave", 
					["arrive_leave"])
		_initialize()


func _process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _initialize():
	var position = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		arrive_leave.character.position = Vector2(position.x, position.z)
	else:
		arrive_leave.character.position = position


func _pre_update(delta):
	# steering depends on: target.position
	var position = target.global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		arrive_leave.target.position = Vector2(position.x, position.z)
	else:
		arrive_leave.target.position = position


func _update(delta):
	var output:AI.SteeringOutput = arrive_leave.get_steering()
	arrive_leave.character.update(output, delta, max_speed)


func _post_update(delta):
	var origin = arrive_leave.character.position
	if dims == AI_TOOLS.Dimensions.TWO:
		global_transform.origin.x = origin.x
		global_transform.origin.z = origin.y
	else:
		global_transform.origin = origin


func _on_AI_arrive_leave(object_name, action, signal_name):
	print(signal_name, ": (",object_name, ", ", action, ")")


func _set_enabled(value):
	enabled = value
	visible = value
	set_process(value)


func _get_enabled():
	return enabled
