extends MeshInstance3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var target_scene: NodePath = "../../../Target"
@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var max_acceleration: float = 5.0
@export var time_to_target: float = 0.1

var velocity_match: AI.VelocityMatch

@onready var target = get_node(target_scene)


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		velocity_match = AI.VelocityMatch.new()
		velocity_match.setup(max_acceleration, time_to_target, damping, dims)
		_initialize()


func _process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _initialize():
	var position = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		velocity_match.character.position = Vector2(position.x, position.z)
	else:
		velocity_match.character.position = position


func _pre_update(delta):
	# steering depends on: target.velocity
	var velocity = target.velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		velocity_match.target.velocity = Vector2(velocity.x, velocity.z)
	else:
		velocity_match.target.velocity = velocity


func _update(delta):
	var output:AI.SteeringOutput = velocity_match.get_steering()
	velocity_match.character.update(output, delta)


func _post_update(delta):
	var origin = velocity_match.character.position
	if dims == AI_TOOLS.Dimensions.TWO:
		global_transform.origin.x = origin.x
		global_transform.origin.z = origin.y
	else:
		global_transform.origin = origin


func _set_enabled(value):
	enabled = value
	visible = value
	set_process(value)


func _get_enabled():
	return enabled
