extends MeshInstance3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var target_scene: NodePath = "../../../Target"
@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var evade: bool = false

@export var signal_enabled: bool = false
@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var max_acceleration: float = 5.0
@export var max_prediction: float = 1.0
@export var max_speed: float = -1.0

var pursue_evade: AI.PursueEvade

@onready var target = get_node(target_scene)


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		pursue_evade = AI.PursueEvade.new()
		pursue_evade.setup(max_acceleration, max_prediction, damping, dims,
				AI_TOOLS.Behavior.EVADE if evade else AI_TOOLS.Behavior.PURSUE)
		if signal_enabled:
			pursue_evade.signal_enable = signal_enabled
			pursue_evade.connect("pursue_evade", self, "_on_AI_pursue_evade",
					["pursue_evade"])
		_initialize()


func _process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _initialize():
	var position = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		pursue_evade.character.position = Vector2(position.x, position.z)
	else:
		pursue_evade.character.position = position


func _pre_update(delta):
	# steering depends on: target.position, target.velocity
	var position = target.global_transform.origin
	var velocity = target.velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		pursue_evade.target.position = Vector2(position.x, position.z)
		pursue_evade.target.velocity = Vector2(velocity.x, velocity.z)
	else:
		pursue_evade.target.position = position
		pursue_evade.target.velocity = velocity


func _update(delta):
	var output:AI.SteeringOutput = pursue_evade.get_steering()
	pursue_evade.character.update(output, delta, max_speed)


func _post_update(delta):
	var origin = pursue_evade.character.position
	if dims == AI_TOOLS.Dimensions.TWO:
		global_transform.origin.x = origin.x
		global_transform.origin.z = origin.y
	else:
		global_transform.origin = origin


func _on_AI_pursue_evade(object_name, action, signal_name):
	print(signal_name, ": (",object_name, ", ", action, ")")


func _set_enabled(value):
	enabled = value
	visible = value
	set_process(value)


func _get_enabled():
	return enabled
