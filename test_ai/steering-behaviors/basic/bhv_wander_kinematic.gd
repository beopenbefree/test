extends MeshInstance3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var max_speed: float = 3.0
@export var max_rotation: float = 1.5

var wander: AI.KinematicWander


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		wander = AI.KinematicWander.new()
		wander.setup(max_speed, max_rotation, dims)
		_initialize()


func _process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _initialize():
	randomize()
	var position = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		wander.character.position = Vector2(position.x, position.z)
	else:
		wander.character.position = position
	wander.character.orientation = AI_TOOLS.map_to_range(
			-global_transform.basis.get_euler().y)


func _pre_update(delta):
	pass


func _update(delta):
	var output:AI.SteeringOutput = wander.get_steering()
	wander.character.update(output, delta)


func _post_update(delta):
	var origin = wander.character.position
	var basis = Basis(Vector3.RIGHT, deg_to_rad(90.0))
	if dims == AI_TOOLS.Dimensions.TWO:
		global_transform.origin.x = origin.x
		global_transform.origin.z = origin.y
	else:
		global_transform.origin = origin
	global_transform.basis = basis.rotated(Vector3.UP, 
			AI_TOOLS.map_to_range(wander.character.orientation))


func _set_enabled(value):
	enabled = value
	visible = value
	set_process(value)


func _get_enabled():
	return enabled
