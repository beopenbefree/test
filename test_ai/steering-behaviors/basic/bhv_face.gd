extends MeshInstance3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var target_scene: NodePath = "../../../Target"
@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var signal_enabled: bool = false
@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var max_angular_acceleration: float = 5.0
@export var max_rotation: float = 5.0
@export var target_radius: float = 5.0 # degrees
@export var slow_radius: float = 45.0 # degrees
@export var time_to_target: float = 0.1

var face: AI.Face

@onready var target = get_node(target_scene)


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		face = AI.Face.new()
		face.setup(max_angular_acceleration, max_rotation, deg_to_rad(target_radius), 
				deg_to_rad(slow_radius), time_to_target, damping, dims)
		face.connect("face", Callable(self, "_on_AI_face").bind("face"))
		if signal_enabled:
			face.signal_enable = signal_enabled
			face.align.connect("align", Callable(self, "_on_AI_face").bind("align"))
		_initialize()


func _process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _initialize():
	var position = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		face.character.position = Vector2(position.x, position.z)
	else:
		face.character.position = position
	face.character.orientation = AI_TOOLS.map_to_range(
			global_transform.basis.get_euler().y)


func _pre_update(delta):
	# steering depends on: target.position, character.position
	var position = target.global_transform.origin
	var character_position = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		face.target.position = Vector2(position.x, position.z)
		face.character.position = Vector2(character_position.x,
				character_position.z)
	else:
		face.target.position = position
		face.character.position = character_position


func _update(delta):
	var output:AI.SteeringOutput = face.get_steering()
	face.character.update(output, delta)


func _post_update(delta):
	var basis = Basis(Vector3.RIGHT, deg_to_rad(90.0))
	global_transform.basis = basis.rotated(Vector3.UP, 
			face.character.orientation)


func _on_AI_face(object_name, action, signal_name):
	print(signal_name, ": (",object_name, ", ", action, ")")


func _set_enabled(value):
	enabled = value
	visible = value
	set_process(value)


func _get_enabled():
	return enabled
