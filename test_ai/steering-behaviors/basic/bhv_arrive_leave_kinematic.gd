extends MeshInstance3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var target_scene: NodePath = "../../../Target"
@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var leave: bool = false

@export var max_speed: float = 5.0
@export var radius: float = 10.0
@export var time_to_target: float = 0.5

var arrive_leave: AI.KinematicArriveLeave

@onready var target = get_node(target_scene)


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		arrive_leave = AI.KinematicArriveLeave.new()
		arrive_leave.setup(max_speed, radius, time_to_target, dims,
				AI_TOOLS.Behavior.LEAVE if leave else AI_TOOLS.Behavior.ARRIVE)
		arrive_leave.connect("arrive_leave", self, "_on_AI_arrive_leave", 
				["arrive_leave"])
		_initialize()


func _process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _initialize():
	var position = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		arrive_leave.character.position = Vector2(position.x, position.z)
	else:
		arrive_leave.character.position = position
	arrive_leave.character.orientation = AI_TOOLS.map_to_range(
			-global_transform.basis.get_euler().y)


func _pre_update(delta):
	# steering depends on: target.position
	var position = target.global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		arrive_leave.target.position = Vector2(position.x, position.z)
	else:
		arrive_leave.target.position = position


func _update(delta):
	var output:AI.SteeringOutput = arrive_leave.get_steering()
	arrive_leave.character.update(output, delta)


func _post_update(delta):
	var origin = arrive_leave.character.position
	var basis = Basis(Vector3.RIGHT, deg_to_rad(90.0))
	if dims == AI_TOOLS.Dimensions.TWO:
		global_transform.origin.x = origin.x
		global_transform.origin.z = origin.y
	else:
		global_transform.origin = origin
	global_transform.basis = basis.rotated(Vector3.UP, 
			-AI_TOOLS.map_to_range(arrive_leave.character.orientation))


func _on_AI_arrive_leave(object_name, action, signal_name):
	print(signal_name, ": (",object_name, ", ", action, ")")


func _set_enabled(value):
	enabled = value
	visible = value
	set_process(value)


func _get_enabled():
	return enabled
