extends MeshInstance3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var target_scenes = [] # (Array, NodePath)
@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var attraction: bool = false

@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var max_acceleration: float = 5.0
@export var threshold: float = 5.0
@export var decay_coefficient: float = 15
@export var boundary: float = 50.0
@export var max_speed: float = -1.0

var separation_attraction: AI.SeparationAttraction

var targets:Array = []: set = _set_targets


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		for scene in target_scenes:
			var target = get_node(scene)
			targets.append(target)
		var behavior_type = AI_TOOLS.Behavior.SEPARATION
		if attraction:
			behavior_type = AI_TOOLS.Behavior.ATTRACTION
		separation_attraction = AI.SeparationAttraction.new()
		separation_attraction.setup(max_acceleration, threshold, 
				decay_coefficient, damping, dims, behavior_type)
		separation_attraction.connect("separation_attraction", self, 
				"_on_AI_separation_attraction", ["separation_attraction"])
		_set_targets(targets)
		_initialize()


func _process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _initialize():
	var position = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		separation_attraction.character.position = Vector2(position.x, position.z)
	else:
		separation_attraction.character.position = position


func _pre_update(delta):
	pass


func _update(delta):
	var output:AI.SteeringOutput = separation_attraction.get_steering()
	separation_attraction.character.update(output, delta, max_speed)
	# limits movement to a square boundary
	var c = separation_attraction.character
	if c.position.x > boundary:
		c.position.x = boundary
		c.velocity.x = -c.velocity.x
	elif c.position.x < -boundary:
		c.position.x = -boundary
		c.velocity.x = -c.velocity.x
	if dims == AI_TOOLS.Dimensions.TWO:
		if c.position.y > boundary:
			c.position.y = boundary
			c.velocity.y = -c.velocity.y
		elif c.position.y < -boundary:
			c.position.y = -boundary
			c.velocity.y = -c.velocity.y
	else:
		if c.position.z > boundary:
			c.position.z = boundary
			c.velocity.z = -c.velocity.z
		elif c.position.z < -boundary:
			c.position.z = -boundary
			c.velocity.z = -c.velocity.z


func _post_update(delta):
	var origin = separation_attraction.character.position
	if dims == AI_TOOLS.Dimensions.TWO:
		global_transform.origin.x = origin.x
		global_transform.origin.z = origin.y
	else:
		global_transform.origin = origin


func _set_targets(value:Array):
	targets = value
	separation_attraction.targets.clear()
	for target in value:
		var kinematic = target.separation_attraction.character 
		separation_attraction.add_target(kinematic)


func _on_AI_separation_attraction(object_name, action, signal_name):
	print(signal_name, ": (",object_name, ", ", action, ")")


func _set_enabled(value):
	enabled = value
	visible = value
	set_process(value)


func _get_enabled():
	return enabled
