extends MeshInstance3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var target_scenes = [] # (Array, NodePath)
@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var max_acceleration: float = 5.0
@export var avoid_distance: float = 5.0
@export var lookahead: float = 1.0
@export var num_rays = 1 # (int, 1, 2)
@export var whiskers_angle: float = 15.0
@export var boundary: float = 150.0
@export var max_speed: float = -1.0

var obstacle_avoidance: AI.ObstacleAvoidance

var collision_detector: AI.CollisionDetector = null

#<DEBUG
var debug_lib = preload("res://test_ai/steering-behaviors/debug_draw.gd")
@onready var circle = MeshInstance3D.new()
@onready var segment_r = ImmediateMesh.new()
@onready var segment_l = ImmediateMesh.new()
@onready var segment_output = ImmediateMesh.new()
#DEBUG>


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		obstacle_avoidance = AI.ObstacleAvoidance.new()
		obstacle_avoidance.setup(max_acceleration, collision_detector, 
				avoid_distance, lookahead, num_rays, deg_to_rad(whiskers_angle), 
				damping, dims)
		obstacle_avoidance.connect("obstacle_avoidance", self, 
				"_on_AI_obstacle_avoidance", ["obstacle_avoidance"])
		_initialize()
		
		#<DEBUG
		circle.mesh = debug_lib.get_circle_mesh(avoid_distance)
		add_child(circle)
		circle.rotate(Vector3.FORWARD, deg_to_rad(90))
		add_child(segment_r)
		add_child(segment_l)
		var mat = StandardMaterial3D.new()
		mat.albedo_color = Color(1,0,0,1)
		segment_output.material_override = mat
		add_child(segment_output)
		#DEBUG>


func _physics_process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _initialize():
	var position = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		obstacle_avoidance.character.position = Vector2(position.x, position.z)
	else:
		obstacle_avoidance.character.position = position


func _pre_update(delta):
	pass


func _update(delta):
	var output:AI.SteeringOutput = obstacle_avoidance.get_steering()
	
	#<DEBUG
	debug_lib.debug_draw_obstacle_avoidance(obstacle_avoidance, 
		self, output, segment_r, segment_l, segment_output, dims)
	#DEBUG>
	
	obstacle_avoidance.character.update(output, delta, max_speed)
	# limits movement to a square boundary
	var c = obstacle_avoidance.character
	if c.position.x > boundary:
		c.position.x = boundary
		c.velocity.x = -c.velocity.x
	elif c.position.x < -boundary:
		c.position.x = -boundary
		c.velocity.x = -c.velocity.x
	if dims == AI_TOOLS.Dimensions.TWO:
		if c.position.y > boundary:
			c.position.y = boundary
			c.velocity.y = -c.velocity.y
		elif c.position.y < -boundary:
			c.position.y = -boundary
			c.velocity.y = -c.velocity.y
	else:
		if c.position.z > boundary:
			c.position.z = boundary
			c.velocity.z = -c.velocity.z
		elif c.position.z < -boundary:
			c.position.z = -boundary
			c.velocity.z = -c.velocity.z
	var speed = c.velocity.length()
	if speed < max_speed:
		c.velocity = c.velocity.normalized() * (speed + delta * 2.0)


func _post_update(delta):
	var origin = obstacle_avoidance.character.position
	if dims == AI_TOOLS.Dimensions.TWO:
		global_transform.origin.x = origin.x
		global_transform.origin.z = origin.y
	else:
		global_transform.origin = origin


func _on_AI_obstacle_avoidance(object_name, action, signal_name):
	print(signal_name, ": (",object_name, ", ", action, ", ", 
			str(Time.get_ticks_msec()),")")


func _set_enabled(value):
	enabled = value
	visible = value
	set_physics_process(value)


func _get_enabled():
	return enabled
