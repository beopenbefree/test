extends MeshInstance3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var target_scenes = [] # (Array, NodePath)
@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var max_acceleration: float = 5.0
@export var radius: float = 5.0
@export var boundary: float = 50.0
@export var max_speed: float = -1.0

var collision_avoidance: AI.CollisionAvoidance

var targets:Array = []: set = _set_targets

#<DEBUG
var debug_lib = preload("res://test_ai/steering-behaviors/debug_draw.gd")
var circle: MeshInstance3D
#DEBUG>


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		for scene in target_scenes:
			var target = get_node(scene)
			targets.append(target)
		collision_avoidance = AI.CollisionAvoidance.new()
		collision_avoidance.setup(max_acceleration, radius, damping, dims)
		collision_avoidance.connect("collision_avoidance", self, 
				"_on_AI_collision_avoidance", ["collision_avoidance"])
		_set_targets(targets)
		_initialize()
		
		#<DEBUG
		circle = MeshInstance3D.new()
		circle.mesh = debug_lib.get_circle_mesh(radius)
		add_child(circle)
		circle.rotate(Vector3.FORWARD, deg_to_rad(90))
		#DEBUG>


func _process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _initialize():
	var position = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		collision_avoidance.character.position = Vector2(position.x, position.z)
	else:
		collision_avoidance.character.position = position


func _pre_update(delta):
	pass


func _update(delta):
	var output:AI.SteeringOutput = collision_avoidance.get_steering()
	collision_avoidance.character.update(output, delta, max_speed)
	# limits movement to a square boundary
	var c = collision_avoidance.character
	if c.position.x > boundary:
		c.position.x = boundary
		c.velocity.x = -c.velocity.x
	elif c.position.x < -boundary:
		c.position.x = -boundary
		c.velocity.x = -c.velocity.x
	if dims == AI_TOOLS.Dimensions.TWO:
		if c.position.y > boundary:
			c.position.y = boundary
			c.velocity.y = -c.velocity.y
		elif c.position.y < -boundary:
			c.position.y = -boundary
			c.velocity.y = -c.velocity.y
	else:
		if c.position.z > boundary:
			c.position.z = boundary
			c.velocity.z = -c.velocity.z
		elif c.position.z < -boundary:
			c.position.z = -boundary
			c.velocity.z = -c.velocity.z
	var speed = c.velocity.length()
	if speed < max_speed:
		c.velocity = c.velocity.normalized() * (speed + delta * 2.0)


func _post_update(delta):
	var origin = collision_avoidance.character.position
	if dims == AI_TOOLS.Dimensions.TWO:
		global_transform.origin.x = origin.x
		global_transform.origin.z = origin.y
	else:
		global_transform.origin = origin


func _set_targets(value:Array):
	targets = value
	collision_avoidance.targets.clear()
	for target in value:
		var kinematic = target.collision_avoidance.character 
		collision_avoidance.add_target(kinematic)


func _on_AI_collision_avoidance(object_name, action, signal_name):
	print(signal_name, ": (",object_name, ", ", action, ", ", 
			str(Time.get_ticks_msec()),")")


func _set_enabled(value):
	enabled = value
	visible = value
	set_process(value)


func _get_enabled():
	return enabled
