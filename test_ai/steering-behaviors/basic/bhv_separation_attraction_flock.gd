extends Node3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var character_scene: PackedScene = preload(
		"res://test_ai/steering-behaviors/basic/bhv_separation_attraction.tscn")
@export var flock_num: int = 5

@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)
@export var attraction: bool = false
@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var max_acceleration: float = 5.0
@export var threshold: float = 20.0
@export var decay_coefficient: float = 1000.0
#
@export var speed: float = 15.0
#
@export var enable_first: bool = false
@export var damping_first = 0.0 # (float, 0.0, 1.0, 0.01)
@export var max_acceleration_first: float = 5.0
@export var threshold_first: float = 5.0
@export var decay_coefficient_first: float = 1000.0
#
@export var speed_first: float = 15.0
#
@export var motion_boundary: float = 50.0


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		randomize()
		# create the characters
		for i in range(flock_num):
			var character = character_scene.instantiate()
			character.enabled = _get_enabled()
			character.dims = dims
			character.attraction = attraction
			character.damping = damping
			character.max_acceleration = max_acceleration
			character.threshold = threshold
			character.decay_coefficient = decay_coefficient
			character.boundary = motion_boundary
			character.max_speed = speed
			# place it randomly
			var x = AI_TOOLS.random_binomial() * motion_boundary
			var z = AI_TOOLS.random_binomial() * motion_boundary
			character.transform.origin = Vector3(x, 2, z)
			add_child(character)
			# enable the first character for special behavior
			if enable_first:
				if i == 0:
					character.separation_attraction.signal_enable = true
					character.separation_attraction.character.damping = damping_first
					character.separation_attraction.threshold = threshold_first
					character.separation_attraction.decay_coefficient = decay_coefficient_first
					character.separation_attraction.max_acceleration = max_acceleration_first
					character.max_speed = speed_first
					var mat = StandardMaterial3D.new()
					mat.albedo_color = Color(0,0,0,1)
					character.material_override = mat
				else:
					character.separation_attraction.signal_enable = false
			else:
				# for all characters the same behavior
				character.separation_attraction.signal_enable = true
			# give some random motion
			var vx = AI_TOOLS.random_binomial()
			var vz = AI_TOOLS.random_binomial()
			var dir
			if dims == AI_TOOLS.Dimensions.TWO:
				dir = Vector2(vx, vz).normalized()
			else:
				dir = Vector3(vx, 0, vz).normalized()
			if enable_first and (i == 0):
					character.separation_attraction.character.velocity = dir * speed_first
			else:
				character.separation_attraction.character.velocity = dir * speed
		# assign each character its targets: i.e. the others characters
		for character in get_children():
			var targets = []
			for other in get_children():
				if other != character:
					targets.append(other)
			character.targets = targets


func _set_enabled(value):
	enabled = value
	visible = value
	for child in get_children():
		child.enabled = value


func _get_enabled():
	return enabled
