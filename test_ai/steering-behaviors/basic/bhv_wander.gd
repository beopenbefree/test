extends MeshInstance3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

@export var max_speed: float = 3.0

@export var damping = 0.5 # (float, 0.0, 1.0, 0.01)
@export var wander_offset: float = 15.0
@export var wander_radius: float = 7.5 
@export var wander_rate: float = 30.0 # degrees
@export var wander_orientation: float = 0.0 # degrees
@export var max_acceleration: float = 5.0

@export var max_angular_acceleration: float = 5.0
@export var max_rotation: float = 5.0
@export var target_radius: float = 5.0 # degrees
@export var slow_radius: float = 45.0 # degrees
@export var time_to_target: float = 0.1

var wander: AI.Wander


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		wander = AI.Wander.new()
		wander.setup(max_angular_acceleration, max_rotation, deg_to_rad(target_radius), 
				deg_to_rad(slow_radius), time_to_target, wander_offset, wander_radius,
				deg_to_rad(wander_rate), deg_to_rad(wander_orientation), max_acceleration, 
				damping, dims)
		wander.face.connect("face", Callable(self, "_on_AI_wander").bind("face"))
		wander.face.align.connect("align", Callable(self, "_on_AI_wander").bind("align"))
		_initialize()


func _process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _initialize():
	var position = global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		wander.character.position = Vector2(position.x, position.z)
	else:
		wander.character.position = position
	wander.character.orientation = AI_TOOLS.map_to_range(
			global_transform.basis.get_euler().y)


func _pre_update(delta):
	pass


func _update(delta):
	var output:AI.SteeringOutput = wander.get_steering()
	wander.character.update(output, delta, max_speed)
	#<DEBUG
	$Debug.global_transform.origin = wander.face.target.position
	#DEBUG>


func _post_update(delta):
	var origin = wander.character.position
	var basis = Basis(Vector3.RIGHT, deg_to_rad(90.0))
	if dims == AI_TOOLS.Dimensions.TWO:
		global_transform.origin.x = origin.x
		global_transform.origin.z = origin.y
	else:
		global_transform.origin = origin
	global_transform.basis = basis.rotated(Vector3.UP, 
			wander.character.orientation)


func _on_AI_wander(object_name, action, signal_name):
	print(signal_name, ": (",object_name, ", ", action, ")")


func _set_enabled(value):
	enabled = value
	visible = value
	set_process(value)


func _get_enabled():
	return enabled
