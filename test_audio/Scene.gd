extends AudioStreamPlayer


var time_begin
var time_delay


func _ready():
	# 1 Using the system clock to sync
#	time_begin = OS.get_ticks_usec()
#	time_delay = AudioServer.get_time_to_next_mix() + AudioServer.get_output_latency()
	
	play()


func _process(delta):
	# 1 Using the system clock to sync
#	# Obtain from ticks.
#	var time = (OS.get_ticks_usec() - time_begin) / 1000000.0
#	# Compensate for latency.
#	time -= time_delay
#	# May be below 0 (did not begin yet).
#	time = max(0, time)
#	print("Time is: ", time)
	
	# 2 Using the sound hardware clock to sync
	var time = get_playback_position() + AudioServer.get_time_since_last_mix()
	# Compensate for output latency.
	time -= AudioServer.get_output_latency()
	print("Time is: ", time)
