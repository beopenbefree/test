extends AudioStreamPlayer


@export var PERIOD = 1.0 
var elapsed = 0.0

var time_begin
var time_delay


func _ready():
	#Using the system clock to sync
	time_begin = Time.get_ticks_usec()
	time_delay = AudioServer.get_time_to_next_mix() + AudioServer.get_output_latency()
	play()


func _process(delta):
	elapsed += delta
	if playing:
		return
	if elapsed > PERIOD:
		elapsed = 0.0
		play()



