extends Node3D


func _on_button_pressed():
	$Explosion/Sparks.set_deferred("emitting", true)
	$Explosion/Flash.set_deferred("emitting", true)
	$Explosion/Fire.set_deferred("emitting", true)
	$Explosion/Smoke.set_deferred("emitting", true)
