extends Node3D


func _on_button_pressed():
	var is_emitting = $Explosion/Flares.emitting or $Explosion/Smoke.emitting
	if is_emitting:
		$Explosion/Flares.emitting = false
		$Explosion/Smoke.emitting = false
	await get_tree().process_frame
	$Explosion/Flares.set_deferred("emitting", true)
	$Explosion/Smoke.set_deferred("emitting", true)
