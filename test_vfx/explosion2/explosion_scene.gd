extends Node3D


func _on_button_pressed():
	$Explosion/Sparks.set_deferred("emitting", true)
	$Explosion/Flames.set_deferred("emitting", true)
	$Explosion/Smoke.set_deferred("emitting", true)
