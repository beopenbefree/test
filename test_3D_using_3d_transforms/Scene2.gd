extends Node3D


@onready var quatStart = Quaternion($RotationStart.transform.basis)
@onready var posStart = $RotationStart.transform.origin

@onready var quatStop = Quaternion($RotationStop.transform.basis)
@onready var posStop = $RotationStop.transform.origin

@export var duration: float = 10.0
@onready var durationInv = 1.0 / duration
@onready var elapsed = 0.0


func _process(delta):
	if elapsed <= duration:
		var weight = elapsed * durationInv
		var quat = quatStart.slerp(quatStop, weight)
		var pos = lerp(posStart, posStop, weight)
		$Rotation.transform = Transform3D(Basis(quat), pos)
	# update
	elapsed += delta
