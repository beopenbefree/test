#tool
extends MeshInstance3D


func _ready():
	# 1
	if name == "Cylinder":
		transform.basis.y = Vector3(0, 1, 1).normalized()
		transform.basis.z = Vector3(0, -1, 1).normalized()
		transform.origin = Vector3(0, 0, 5.0)
	# 2
	if name == "Cylinder2":
		transform.basis = (Basis(Vector3(1, 0, 0), deg_to_rad(20)) * 
				Basis(Vector3(1, 0, 0), deg_to_rad(25)) * transform.basis)
		transform.origin = Vector3(0, 0, 2.5)
	# 2a
	if name == "Cylinder3":
		transform.basis = transform.basis.rotated(Vector3(1, 0, 0), deg_to_rad(45))
		transform.origin = Vector3(0, 0, 0.0)
	# 3
	if name == "Cylinder4":
#		rotate(Vector3(1, 0, 0), deg2rad(20))
#		rotate(Vector3(1, 0, 0), deg2rad(25))
		rotate_x(deg_to_rad(20))
		rotate_x(deg_to_rad(25))
		transform.origin = Vector3(0, 0, -2.5)
	if name == "Cylinder5":
		rotate_object_local(Vector3(1, 0, 0), deg_to_rad(45))
		transform.origin = Vector3(0, 0, -5.0)
