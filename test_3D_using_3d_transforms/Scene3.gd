extends Node3D


func _ready():
#	test_compose_transform()
#	test_model_global_transforms()

#	test_transforms("rot_trans")
	test_transforms("rot_rot")
#	test_transforms("trans_rot")


func test_compose_transform():
	var trans = Transform3D(Basis(), Vector3(3.0, 0.0, 0.0))
	var rot = Transform3D(Basis(Vector3(0.0, 1.0, 0.0), deg_to_rad(-45)), Vector3())
	# transform order: trans -> rot == rot(trans(P))
	var t = rot * trans
	$FinalModel2.transform = t
	print("FinalModel2 global transform:", $FinalModel2.global_transform)
	print()
	# transform order: rot -> trans == trans(rot(P))
	t = trans * rot
	$FinalModel2.transform = t
	print("FinalModel2 global transform:", $FinalModel2.global_transform)


func test_model_global_transforms():
	$FinalModel2.global_rotate(Vector3(0.0, 1.0, 0.0), deg_to_rad(-45))
	$FinalModel2.global_translate(Vector3(3.0, 0.0, 0.0))
	print("FinalModel2 global transform:", $FinalModel2.global_transform)


## DON'T USE rotate & translate
# translate = translate_object_local
# rotate = global_rotate
func test_transforms(t):
	if t == "rot_trans":
		print("rotate -> *translate*")
		$FinalModel1.transform = Transform3D()
		$FinalModel1.rotate(Vector3(0.0, 1.0, 0.0), deg_to_rad(-45))
		$FinalModel1.translate(Vector3(0.0, 0.0, 1.0))
		print("FinalModel2 global origin (translate):", $FinalModel2.global_transform.origin)
		$FinalModel2.transform = Transform3D()
		$FinalModel2.rotate(Vector3(0.0, 1.0, 0.0), deg_to_rad(-45))
		$FinalModel2.translate_object_local(Vector3(0.0, 0.0, 1.0))
		print("FinalModel2 global origin (translate_object_local):", $FinalModel2.global_transform.origin)
		$FinalModel3.transform = Transform3D()
		$FinalModel3.rotate(Vector3(0.0, 1.0, 0.0), deg_to_rad(-45))
		$FinalModel3.global_translate(Vector3(0.0, 0.0, 1.0))
		print("FinalModel2 global origin (global_translate):", $FinalModel2.global_transform.origin)
	
	if t == "rot_rot":
		print("rotate -> *rotate*")
		$FinalModel1.transform = Transform3D()
		$FinalModel1.rotate(Vector3(0.0, 1.0, 0.0), deg_to_rad(-90.0))
		$FinalModel1.rotate(Vector3(1.0, 0.0, 0.0), deg_to_rad(-90.0))
		print("FinalModel2 global basis (rotate):", $FinalModel2.global_transform.basis)
		$FinalModel2.transform = Transform3D()
		$FinalModel2.rotate(Vector3(0.0, 1.0, 0.0), deg_to_rad(-90.0))
		$FinalModel2.rotate_object_local(Vector3(1.0, 0.0, 0.0), deg_to_rad(-90.0))
		print("FinalModel2 global basis (rotate_object_local):", $FinalModel2.global_transform.basis)
		$FinalModel3.transform = Transform3D()
		$FinalModel3.rotate(Vector3(0.0, 1.0, 0.0), deg_to_rad(-90.0))
		$FinalModel3.global_rotate(Vector3(1.0, 0.0, 0.0), deg_to_rad(-90.0))
		print("FinalModel2 global basis (global_rotate):", $FinalModel2.global_transform.basis)
	
	if t == "trans_rot":
		print("translate -> *rotate*")
		$FinalModel1.transform = Transform3D()
		$FinalModel1.translate(Vector3(1.0, 0.0, 0.0))
		$FinalModel1.rotate(Vector3(1.0, 0.0, 0.0), deg_to_rad(-90.0))
		print("FinalModel2 global basis (rotate):", $FinalModel2.global_transform.basis)
		$FinalModel2.transform = Transform3D()
		$FinalModel2.translate(Vector3(1.0, 0.0, 0.0))
		$FinalModel2.rotate_object_local(Vector3(1.0, 0.0, 0.0), deg_to_rad(-90.0))
		print("FinalModel2 global basis (rotate_object_local):", $FinalModel2.global_transform.basis)
		$FinalModel3.transform = Transform3D()
		$FinalModel3.translate(Vector3(1.0, 0.0, 0.0))
		$FinalModel3.global_rotate(Vector3(1.0, 0.0, 0.0), deg_to_rad(-90.0))
		print("FinalModel2 global basis (global_rotate):", $FinalModel2.global_transform.basis)
	
	$FinalModel1.global_translate(Vector3(-10.0, 0.0, 0.0))
	$FinalModel3.global_translate(Vector3(10.0, 0.0, 0.0))
