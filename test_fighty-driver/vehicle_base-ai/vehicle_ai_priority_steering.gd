extends Node3D


@export var enabled: bool = false: get = _get_enabled, set = _set_enabled

@export var dims = AI_TOOLS.Dimensions.THREE # (AI_TOOLS.Dimensions)

# priority steering parameters
@export var behaviors_file = ( # (String, FILE)
		"res://test_fighty-driver/car_base_ai_behaviors.json")
@export var behavior_groups_file = ( # (String, FILE)
		"res://test_fighty-driver/car_base_ai_behavior_groups.json")
@export var damping = 0.0 # (float, 0.0, 1.0, 0.01)
@export var epsilon: float = 1.0e-3
@export var max_acceleration: float = 5.0
@export var max_rotation: float = 2.0
@export var max_speed: float = 5.0

# vehicle's parameters
@export var aggression = 0.5 # (float, 0.01, 1.0, 0.01)
# AI4G: 3.8.3 COMMON ACTUATION PROPERTIES (pp. 175-177)
@export var forward_arc_max_angle_deg = 145.0 # (float, 0.0, 180.0, 1.0)
@export var arc_dec_factor_by_speed = 250 # (float, 100, 1000, 50)
# HACK: to prevent the steering wheels from wagging
@export var turn_min_angle = 5.0 # (float, 0.0, 15.0, 0.5)
@export var turn_max_angle: float = 45.0

# post update (actuator) items
enum MotionState {STEADY, FORWARD, BACKWARD}
const MIN_SPEED = 0.1

# ai data definitions
var priority_steering: AI.PrioritySteering
var steering_output:AI.SteeringOutput

# behaviors' data definitions
var behaviors_dict: Dictionary = {}
var behavior_groups_dict: Dictionary = {}
var behaviors_pre_update: Dictionary = {}

@onready var motion_state = MotionState.FORWARD

@onready var target = null

# vehicle's stationary max forward/rear arcs
@onready var forward_arc_max_angle = deg_to_rad(forward_arc_max_angle_deg)
@onready var rear_arc_max_angle = PI - forward_arc_max_angle
# vehicle's turn limits
@onready var turn_max_angle_cos = cos(deg_to_rad(turn_max_angle))
@onready var turn_one_minus_max_angle_inv = 1.0 / (1 - turn_max_angle_cos)
@onready var turn_min_angle_cos = cos(deg_to_rad(turn_min_angle))
@onready var body = $Body


func _ready():
	_set_enabled(enabled)
	if _get_enabled():
		behaviors_dict = _load_behaviors(behaviors_file)
		behavior_groups_dict = _load_behaviors(behavior_groups_file)
		# setup each behavior group in order
		var priority_group_list = []
		var behavior_groups = []
		for group_name in behavior_groups_dict:
			var behavior_names = behavior_groups_dict[group_name]["behavior_set"]
			var behavior_group = _behavior_group_setup(group_name, behavior_names,
					max_acceleration, max_rotation, damping, dims)
			# insert the behavior group into priority list
			var priority = behavior_groups_dict[group_name]["priority"]
			var pos = 0
			while pos < priority_group_list.size():
				if priority <= priority_group_list[pos]:
					break
				pos += 1
			priority_group_list.insert(pos, priority)
			behavior_groups.insert(pos, behavior_group)
		priority_steering = AI.PrioritySteering.new()
		priority_steering.setup(behavior_groups, epsilon, damping, dims)
		_initialize()


func _behavior_group_setup(group_name:String, behavior_names:Array,
		max_acceleration, max_rotation, damping, dims):
	print(group_name + " group construction...")
	var group = AI.BlendedSteering.new()
	var group_behaviors: Array = []
	for bhv_name in behavior_names:
		var bhv_name_lower = bhv_name.to_lower()
		#
		print("... " + bhv_name + " behavior construction...")
		var setup_args = []
		for arg in behaviors_dict[bhv_name_lower]["setup_args"]:
			var value = arg.values()[0]
			if (value is String) and (value == "null"):
				value = null
			setup_args.append(value)
		setup_args[-2] = damping
		setup_args[-1] = dims
		var behavior = AI._behavior_factory(bhv_name, setup_args)
		#
		print("... " + bhv_name + " BehaviorAndWeight construction...")
		var bhv_wght = AI.BlendedSteering.BehaviorAndWeight.new()
		bhv_wght.behavior = behavior
		bhv_wght.weight = behaviors_dict[bhv_name_lower]["weight"]
		group_behaviors.append(bhv_wght)
		#
		print("... " + bhv_name + " pre_update function setting...")
		var pre_update()_func = behaviors_dict[bhv_name_lower]["_pre_update"]
		behaviors_pre_update[bhv_wght] = funcref(self, pre_update_func)
	#
	print(group_name + " group final setup")
	group.setup(group_behaviors, max_acceleration, max_rotation, damping, dims)
	return group


func _physics_process(delta):
	_pre_update(delta)
	_update(delta)
	_post_update(delta)


func _initialize():
	var position = body.global_transform.origin
	var velocity = body.linear_velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		priority_steering.character.position = Vector2(position.x, position.z)
		priority_steering.character.velocity = Vector2(velocity.x, velocity.z)
	else:
		priority_steering.character.position = position
		priority_steering.character.velocity = velocity


func _pre_update(delta):
	# synchronize character's motion state with the body's one
	var position = body.global_transform.origin
	var velocity = body.linear_velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		priority_steering.character.position = Vector2(position.x, position.z)
		priority_steering.character.velocity = Vector2(velocity.x, velocity.z)
	else:
		priority_steering.character.position = position
		priority_steering.character.velocity = velocity
	# pre-update every inner behavior
	for group in priority_steering.groups:
		for bhv_wght in group.behaviors:
			var bhv = bhv_wght.behavior
			behaviors_pre_update[bhv_wght].call_func(bhv, delta)


func _pre_update_pos(bhv, delta):
	# steering depends on: target.position
	var position = target.global_transform.origin
	if dims == AI_TOOLS.Dimensions.TWO:
		bhv.target.position = Vector2(position.x, position.z)
	else:
		bhv.target.position = position


func _pre_update_ori(bhv, delta):
	# steering depends on: target.orientation
	bhv.target.orientation = AI_TOOLS.map_to_range(
			target.global_transform.basis.get_euler().y)


func _pre_update_vel(bhv, delta):
	# steering depends on: target.velocity
	var velocity = target.velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		bhv.target.velocity = Vector2(velocity.x, velocity.z)
	else:
		bhv.target.velocity = velocity


func _pre_update_char_vel(bhv, delta):
	# steering depends on: character.velocity
	bhv.character.velocity = body.linear_velocity


func _pre_update_pos_vel(bhv, delta):
	# steering depends on: target.position, target.velocity
	var position = target.global_transform.origin
	var velocity = target.velocity
	if dims == AI_TOOLS.Dimensions.TWO:
		bhv.target.position = Vector2(position.x, position.z)
		bhv.target.velocity = Vector2(velocity.x, velocity.z)
	else:
		bhv.target.position = position
		bhv.target.velocity = velocity


func _pre_update_pass(bhv, delta):
	# steering depends on: nothing
	pass


func _update(delta):
	steering_output = priority_steering.get_steering()


func _post_update(delta):
	body.action_pressed_turn = false
	body.action_strength_turn = 0.0
	body.action_pressed_motion = false
	body.action_strength_motion = 0.0
	body._set_current_brake(0.0)
	# get the desired_dir (normalized) and desired_speed to the target.
	var desired_dir = steering_output.linear.normalized()
	var desired_speed = steering_output.linear.length() * aggression
	# get vehicle's forward orientation
	var forward_orientation = body.global_transform.basis.z
	# compute desired dir w.r.t. vehicle's forward orientation: the (cosine of)
	# difference angle
	var direction_diff_cos = forward_orientation.dot(desired_dir)
	
	# get current velocity/speed
	var body_velocity = body.linear_velocity
	var body_speed = body_velocity.length()
	
	# compute the driving's commands 
	# get the motion direction: i.e. velocity w.r.t forward orientation
	if motion_state == MotionState.FORWARD:
		# going forward direction: compute current forward arc
		var forward_arc_angle_cos = cos(
				arc_dec_factor_by_speed * forward_arc_max_angle /
				(forward_arc_max_angle * body_speed + arc_dec_factor_by_speed))
		# we're moving in the forward direction
		if direction_diff_cos >= forward_arc_angle_cos:
			# we are into the forward arc: compute turning's commands
			if direction_diff_cos <= turn_min_angle_cos:
				# turn toward the target
				body.action_pressed_turn = true
				if forward_orientation.cross(desired_dir).y >= 0.0:
					if direction_diff_cos >= turn_max_angle_cos:
						body.action_strength_turn = (
								(turn_min_angle_cos - direction_diff_cos) * 
								turn_one_minus_max_angle_inv)
					else:
						body.action_strength_turn = 1.0
				else:
					if direction_diff_cos >= turn_max_angle_cos:
						body.action_strength_turn = ((
								direction_diff_cos - turn_min_angle_cos) * 
								turn_one_minus_max_angle_inv)
					else:
						body.action_strength_turn = -1.0
			# compute motion's commands: go forward
			body.action_pressed_motion = true
			body.action_strength_motion = desired_speed
		else:
			# we are outside the forward arc: let's brake
			body._set_current_brake(body.brake_size)
			# change state if necessary
			if body_speed < MIN_SPEED:
				motion_state = MotionState.BACKWARD
	elif motion_state == MotionState.BACKWARD:
		# going rear direction: compute current rear arc
		var rear_arc_angle_cos = cos(
				PI - (arc_dec_factor_by_speed * rear_arc_max_angle) /
				(rear_arc_max_angle * body_speed + arc_dec_factor_by_speed))
			# we're moving in the rear direction
		if direction_diff_cos <= rear_arc_angle_cos:
			# we are in the rear arc: compute turning's commands
			# turn opposite the target
			body.action_pressed_turn = true
			if forward_orientation.cross(desired_dir).y >= 0.0:
				body.action_strength_turn = -1.0
			else:
				body.action_strength_turn = 1.0
			# compute motion's commands: go backward
			body.action_pressed_motion = true
			body.action_strength_motion = -desired_speed				
		else:
			# we are outside the rear arc: let's brake
			body._set_current_brake(body.brake_size)
			# change state if necessary
			if body_speed < MIN_SPEED:
				motion_state = MotionState.FORWARD


func _load_behaviors(json_file):
	var saved_game = File.new()
	if saved_game.open(json_file, File.READ) != OK:
			return false
	var test_json_conv = JSON.new()
	test_json_conv.parse(saved_game.get_as_text())
	var res = test_json_conv.get_data()
	if res.error != OK:
		print("Error ID: ", res.error)
		print("Error string: ", res.error_string)
		print("Error line: ", res.error_line)
		return {}
	saved_game.close()
	return res.result


func _set_enabled(value):
	enabled = value
	visible = value
	set_physics_process(value)
	for child in get_children():
		if "visible" in child:
			child.visible = value
		if "enabled" in child:
			child.enabled = value


func _get_enabled():
	return enabled
