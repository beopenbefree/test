class_name TestScriptLibrary
extends Resource

static func get_all_children(in_node:Node, arr:=[]):
	arr.push_back(in_node)
	for child in in_node.get_children():
		arr = get_all_children(child, arr)
	return arr


static func rand_float(magnitude:float):
	# return a float in [-magnitude, magnitude]
	return (2 * randf() - 1.0) * magnitude


### INPUT: Action management

static func _add_action(action, keycode=null, button_index=null):
	InputMap.add_action(action)
	if keycode:
		var ev = InputEventKey.new()
		ev.keycode = keycode
		InputMap.action_add_event(action, ev)
	if button_index:
		var ev = InputEventMouseButton.new()
		ev.button_index = button_index
		InputMap.action_add_event(action, ev)


static func _get_scancode(action):
	var keycode = 0
	for key_event in InputMap.action_get_events(action):
		if key_event is InputEventKey:
			keycode = key_event.keycode
			return keycode
	return null


static func _get_button_index(action):
	var button_index = 0
	for mouse_event in InputMap.action_get_events(action):
		if mouse_event is InputEventMouseButton:
			button_index = mouse_event.button_index
			return button_index
	return null


static func _feed_action(name, pressed, strength=1.0):
	var ev_action = InputEventAction.new()
	ev_action.action = name
	ev_action.button_pressed = pressed
	Input.parse_input_event(ev_action)



static func find_node_by_type_name(type_name:String, root_node:Node):
	# get the first node instance with the given type name: by using tree's
	# iterative breadth search, the first ones found are returned
	var instance = null
	var stack = []
	stack.append_array(root_node.get_children())
	while not stack.is_empty():
		var child = stack.pop_front()
		if child.to_string().contains(type_name):
			instance = child
			break
		stack.append_array(child.get_children())
	return instance


static func find_node_by_type(type:Variant, root_node:Node):
	# get the first node instance with the given type: by using tree's
	# iterative breadth search, the first ones found are returned
	var instance = null
	var stack = []
	stack.append_array(root_node.get_children())
	while not stack.is_empty():
		var child = stack.pop_front()
		if is_instance_of(child, type):
			instance = child
			break
		stack.append_array(child.get_children())
	return instance
