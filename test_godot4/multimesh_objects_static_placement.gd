#tool
extends Node3D


@export var enabled: bool = false

@export var object_total_num: int = 100

@export var object_types: Array[PackedScene] = []
@export var visual_only: bool = false
@export var object_collision_shape_scales: Array[Vector3] = []
@export_range(0,1,0.05) var object_max_delta_scale: float = 0.5

@export var placement_space_center: Vector3 = Vector3.ZERO
@export var placement_space_size: Vector3 = Vector3(400, 200, 400)

@export var placement_objects_names: Array[String] = []

@export var save_scene: bool = false
@export_dir var save_scene_path = ""
@export var save_scene_name: String = ""

var ray_cast:RayCast3D = null

@onready var placements_finished:bool = false


func _ready():
	if not enabled:
		# do nothing
		return
	randomize()
	if object_types.size() < 1:
		return
	# make sure placement_space_size has no negative component
	placement_space_size = placement_space_size.abs()
	if object_collision_shape_scales.size() < object_types.size():
		var delta = object_types.size() - object_collision_shape_scales.size()
		for i in range(delta):
			object_collision_shape_scales.append(Vector3.ONE)
	print(get_child_count())
	ray_cast = RayCast3D.new()
	add_child(ray_cast)
	# create MeshInstance children
	if object_total_num > 0:
		_create_additional_children(object_total_num)
	else:
		return
	# objects dictionary are keyed by materials
	var object_by_material:Dictionary = {}
	var idx = 0
	for child in get_children():
		if not (child is MeshInstance3D):
			continue
		# hold a reference to instance
		var mesh_instance:MeshInstance3D = child 
		var mesh:Mesh = mesh_instance.mesh
		var material:StandardMaterial3D = mesh.surface_get_material(0)
		if not (material in object_by_material):
			# for each material select an object scene and setup a new multimesh 
			# instance with a new multimesh, an instance transform list 
			# and save the basic scale
			var curr_idx = idx % object_types.size()
			var object_scene:PackedScene = object_types[curr_idx]
			var object_scene_collision_shape_scale = object_collision_shape_scales[curr_idx]
			idx += 1
			var object_scene_instance = object_scene.instantiate()
			var multimesh_instance = MultiMeshInstance3D.new()
			multimesh_instance.name = "multimesh_" + str(material.get_rid().get_id())
			#
			var multimesh = MultiMesh.new()
			multimesh_instance.multimesh = multimesh
			object_by_material[material] = {
				"multimesh_instance": multimesh_instance,
				"instance_transform_list": [],
				"object_scene_scaling": object_scene_instance.scaling,
				"object_scene_collision_shape_scale": object_scene_collision_shape_scale,
			}
			# initialize the multimesh
			# find the mesh inside object scene
			var object_mesh:Mesh = null
			var object_mesh_found = false
			var stack = []
			stack.append_array(object_scene_instance.get_children())
			while ((not stack.is_empty()) and (not object_mesh_found)):
				var node = stack.pop_front()
				if node is MeshInstance3D:
					object_mesh = node.mesh
					object_mesh_found = true
				stack.append_array(node.get_children())
			multimesh.mesh = object_mesh
			multimesh.transform_format = MultiMesh.TRANSFORM_3D
			multimesh.use_colors = false
			multimesh.use_custom_data = false
			# append shape type and aabb for computing a static body 
			# (with a collision shape) for each instance
			object_by_material[material]["object_scene_shape_type"] = object_scene_instance.shape_type
			object_by_material[material]["object_scene_mesh_aabb"] = object_mesh.get_aabb()
			object_scene_instance.queue_free()
		# compute transform for the instance
		var instance_basis = Basis.IDENTITY
		var instance_origin = Vector3.ZERO
		# randomize instance rotation
		instance_basis = instance_basis.rotated(Vector3.UP, randf() * deg_to_rad(360))
		# randomize instance scale
		var scale_deviation = (1 + TestScriptLibrary.rand_float(object_max_delta_scale))
		var scaling_new = object_by_material[material]["object_scene_scaling"] * scale_deviation
		instance_basis = instance_basis.scaled(scaling_new)
		# set instance origin to the correct place on the surface
		instance_origin = mesh_instance.global_transform.origin
		var delta_pos = Vector3(0.0, 10.0 * instance_origin.y, 0.0)
		ray_cast.global_transform.origin = instance_origin + delta_pos
		ray_cast.target_position = -2 * delta_pos
		ray_cast.force_raycast_update()
		if ray_cast.is_colliding():
			instance_origin = ray_cast.get_collision_point()
		# append instance transform
		var instance_transform = Transform3D(instance_basis, instance_origin)
		object_by_material[material]["instance_transform_list"].append(instance_transform)
		if not visual_only:
			# create a static body for the instance (with a collision shape)
			var object_shape_type:int = object_by_material[material]["object_scene_shape_type"]
			var object_mesh_aabb:AABB = object_by_material[material]["object_scene_mesh_aabb"]
			var object_collision_shape_scale:Vector3 = object_by_material[material]["object_scene_collision_shape_scale"]
			var static_body = _create_static_body(object_shape_type, object_mesh_aabb, object_collision_shape_scale)
			# place and add the static body to the multimesh instance
			static_body.transform = instance_transform
			var object_multimesh_instance:MultiMeshInstance3D = object_by_material[material]["multimesh_instance"]
			object_multimesh_instance.add_child(static_body)
		# remove the original mesh instance
		child = null
		mesh_instance.queue_free()
	
	print("object by material size: ", object_by_material.size())
	
	# draw the multimeshes
	for material in object_by_material:
		var multimesh_instance = object_by_material[material]["multimesh_instance"]
		var multimesh:MultiMesh = multimesh_instance.multimesh
		var instance_transforms = object_by_material[material]["instance_transform_list"]
		multimesh.instance_count = instance_transforms.size()
		for i in multimesh.instance_count:
			multimesh.set_instance_transform(i, instance_transforms[i])
		add_child(multimesh_instance)
	
	ray_cast.queue_free()
	print(get_child_count())
	
	# save scene if requested
	if save_scene:
		await get_tree().create_timer(5.0).timeout
		# add a node to be saved as scene
		var node_to_save = Node3D.new()
		if not save_scene_name.is_empty():
			node_to_save.name = save_scene_name
		else:
			node_to_save.name = name
		add_child(node_to_save)
		# reparent all children to node_to_save and
		# set children's owner (recursively): so they will get saved with the scene
		var all_childred_but_self = TestScriptLibrary.get_all_children(self)
		all_childred_but_self.pop_front()
		for instance in all_childred_but_self:
			if instance != node_to_save:
				remove_child(instance)
				node_to_save.add_child(instance)
				instance.set_owner(node_to_save)
		#
		var current_scene = node_to_save
		var packed_scene = PackedScene.new()
		packed_scene.pack(current_scene)
		var scene_name = save_scene_path + "/"
		if not save_scene_name.is_empty():
			scene_name += save_scene_name + "_saved.tscn"
		else:
			scene_name += current_scene.name + "_saved.tscn"
		# actually save the scene 
		ResourceSaver.save(scene_name, packed_scene)
		var saved_label = Label.new()
		saved_label.text = "SAVED!"
		add_child(saved_label)
	
	# set ready is finished
	placements_finished = true


func _create_static_body(shape_type, aabb, collision_shape_scale):
	var static_body = StaticBody3D.new()
	var collision_shape = CollisionShape3D.new()
	# enum ShapeType {BOX=0, CAPSULE=1, CYLINDER=2, SPHERE=3}
	# compute collision shape
	var shape:Shape3D = null
	var longest_axis_index = aabb.get_longest_axis_index()
	var aabb_size = aabb.size * collision_shape_scale
	if shape_type == 0: #ShapeType.BOX
		shape = BoxShape3D.new()
		shape.size = aabb_size / 2.0
	elif shape_type == 1: #ShapeType.CAPSULE
		shape = CapsuleShape3D.new()
		if longest_axis_index == Vector3.AXIS_X:
			shape.radius = max(aabb_size.y / 2, aabb_size.z / 2)
			shape.height = aabb_size.x - 2 * shape.radius
			collision_shape.rotate(Vector3.UP, deg_to_rad(90))
		elif longest_axis_index == Vector3.AXIS_Y:
			shape.radius = max(aabb_size.x / 2, 
					aabb_size.z / 2)
			shape.height = aabb_size.y - 2 * shape.radius
			collision_shape.rotate(Vector3.RIGHT, deg_to_rad(90))
		else:
			# longest_axis_index == Vector3.AXIS_Z
			shape.radius = max(aabb_size.x / 2, 
					aabb_size.y / 2)
			shape.height = aabb_size.z - 2 * shape.radius
	elif shape_type == 2: #ShapeType.CYLINDER
		shape = CylinderShape3D.new()
		if longest_axis_index == Vector3.AXIS_X:
			shape.radius = max(aabb_size.y / 2, 
					aabb_size.z / 2)
			shape.height = aabb_size.x
			collision_shape.rotate(Vector3.FORWARD, deg_to_rad(90))
		elif longest_axis_index == Vector3.AXIS_Z:
			shape.radius = max(aabb_size.x / 2, 
					aabb_size.y / 2)
			shape.height = aabb_size.z
			collision_shape.rotate(Vector3.RIGHT, deg_to_rad(90))
		else:
			# longest_axis_index == Vector3.AXIS_Y
			shape.radius = max(aabb_size.x / 2, 
					aabb_size.z / 2)
			shape.height = aabb_size.y
	else: 
		#shape_type == 3: #ShapeType.CAPSULE
		shape = SphereShape3D.new()
		shape.radius = max(max(aabb_size.x / 2, 
				aabb_size.z / 2), aabb_size.y / 2)
	collision_shape.shape = shape
	# finish building the static body
	static_body.add_child(collision_shape)
	# correct collision shape's position wrt static body
	collision_shape.transform.origin += Vector3(0, aabb_size.y / 2.0, 0.0)
	#
	return static_body


func _create_additional_children(children_num):
	var total_attempts = 0
	var child_count = 0
	var dummy_meshes = []
	for i in range(object_types.size()):
		dummy_meshes.append(_create_dummy_mesh())
	#
	var space_half_size = placement_space_size / 2.0
	var space_center = placement_space_center
	while child_count < children_num:
		total_attempts += 1
		var pos_x = TestScriptLibrary.rand_float(space_half_size.x) + space_center.x
		var pos_y = space_half_size.y + space_center.y
		var pos_z = TestScriptLibrary.rand_float(space_half_size.z) + space_center.z
		# cast ray vertically
		ray_cast.global_transform.origin = Vector3(pos_x, pos_y, pos_z)
		ray_cast.target_position = Vector3(0.0, -placement_space_size.y, 0.0)
		ray_cast.force_raycast_update()
		if ray_cast.is_colliding():
			var hit_object_name = ray_cast.get_collider().get_parent().name
			if hit_object_name in placement_objects_names:
				var mesh_instance:MeshInstance3D = MeshInstance3D.new() 
				var dummy_mesh = dummy_meshes[randi() % object_types.size()]
				mesh_instance.mesh = dummy_mesh
				add_child(mesh_instance)
				var b = Basis.IDENTITY
				var o = ray_cast.get_collision_point()
				# set object transform
				mesh_instance.set_global_transform(Transform3D(b, o))
				child_count += 1
	#
	print("placements/attempts = ", children_num, "/", total_attempts, 
			" (", float(children_num)/float(total_attempts), ")" )


func _create_dummy_mesh(_scale=2.0):
	var vertices = PackedVector3Array()
	var UVs = PackedVector2Array()
	var mat = StandardMaterial3D.new()
	var color = Color(1.0, 0.5, 0.0)
	vertices.push_back(Vector3(0,0,1) * _scale)
	vertices.push_back(Vector3(0,0,0) * _scale)
	vertices.push_back(Vector3(1,0,1) * _scale)
	vertices.push_back(Vector3(1,0,0) * _scale)
	UVs.push_back(Vector2(0,0))
	UVs.push_back(Vector2(0,1))
	UVs.push_back(Vector2(1,1))
	UVs.push_back(Vector2(1,0))
	mat.albedo_color = color
	var st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLE_STRIP)
	st.set_material(mat)
	for v in vertices.size(): 
		st.set_color(color)
		st.set_uv(UVs[v])
		st.add_vertex(vertices[v])
	return st.commit()
