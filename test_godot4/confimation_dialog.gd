extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	$KeyConfirm.get_label().horizontal_alignment = HORIZONTAL_ALIGNMENT_CENTER
	var key_ok = $KeyConfirm.get_ok_button()
	key_ok.text = "Y"
	var key_cancel = $KeyConfirm.get_cancel_button()
	key_cancel.text = "N"
	$KeyConfirm.show()
