extends CollisionObject3D


enum ShapeType {BOX, CAPSULE, CYLINDER, SPHERE}
@export var shape_type: ShapeType = ShapeType.SPHERE
@export var scaling: Vector3 = Vector3.ONE 

enum Axis {X, Y, Z}

var mesh_instance:MeshInstance3D = null
var collision_shape:CollisionShape3D = null
var current_rotation:Vector3 = Vector3.ZERO
var instance_scaling = Vector3.ONE: set = _compute_shape_with_scale


func _ready():
	# get mesh instance node and collision shape: by using tree's
	# iterative breadth search, the first ones found are returned
	var mesh_instance_found = false
	var collision_shape_found = false
	var stack = []
	stack.append_array(get_children())
	while ((not stack.is_empty()) and 
			((not mesh_instance_found) or (not collision_shape_found))):
		var child = stack.pop_front()
		if (not mesh_instance_found) and (child is MeshInstance3D):
			mesh_instance = child
			mesh_instance_found = true
		elif (not collision_shape_found) and (child is CollisionShape3D):
			collision_shape = child
			collision_shape_found = true
		stack.append_array(child.get_children())
	instance_scaling = scaling
	_compute_shape_with_scale(instance_scaling)


func _compute_shape_with_scale(value):
	if not mesh_instance:
		return
	# save current rotation and reset
	current_rotation = rotation
	rotation = Vector3.ZERO
	# adjust scale
	scale = Vector3.ONE
	mesh_instance.scale = value
	# get mesh_instance (transformed) aabb
	var aabb:AABB = mesh_instance.get_aabb().abs()
	var longest_axis_index = aabb.get_longest_axis_index()
	var aabb_center = aabb.position + aabb.size / 2.0
	# compute collision shape
	var shape:Shape3D = null
	if shape_type == ShapeType.BOX:
		shape = BoxShape3D.new()
		shape.size = aabb.size / 2.0
	elif shape_type == ShapeType.CAPSULE:
		shape = CapsuleShape3D.new()
		if longest_axis_index == Vector3.AXIS_X:
			shape.radius = max(aabb.size.y / 2, aabb.size.z / 2)
			shape.height = aabb.size.x - 2 * shape.radius
			collision_shape.rotate(Vector3.UP, deg_to_rad(90))
		elif longest_axis_index == Vector3.AXIS_Y:
			shape.radius = max(aabb.size.x / 2, aabb.size.z / 2)
			shape.height = aabb.size.y - 2 * shape.radius
			collision_shape.rotate(Vector3.RIGHT, deg_to_rad(90))
		else:
			# longest_axis_index == Vector3.AXIS_Z
			shape.radius = max(aabb.size.x / 2, aabb.size.y / 2)
			shape.height = aabb.size.z - 2 * shape.radius
	elif shape_type == ShapeType.CYLINDER:
		shape = CylinderShape3D.new()
		if longest_axis_index == Vector3.AXIS_X:
			shape.radius = max(aabb.size.y / 2, aabb.size.z / 2)
			shape.height = aabb.size.x
			collision_shape.rotate(Vector3.FORWARD, deg_to_rad(90))
		elif longest_axis_index == Vector3.AXIS_Z:
			shape.radius = max(aabb.size.x / 2, aabb.size.y / 2)
			shape.height = aabb.size.z
			collision_shape.rotate(Vector3.RIGHT, deg_to_rad(90))
		else:
			# longest_axis_index == Vector3.AXIS_Y
			shape.radius = max(aabb.size.x / 2, aabb.size.z / 2)
			shape.height = aabb.size.y
	else: 
		#shape_type == ShapeType.CAPSULE
		shape = SphereShape3D.new()
		shape.radius = max(max(aabb.size.x / 2, aabb.size.z / 2), aabb.size.y / 2)
	# add shape to collision_shape
	assert(shape != null)
	collision_shape.shape = shape
	# fix translations
	var mesh_instance_global_origin = mesh_instance.global_transform.origin
	global_transform.origin = aabb_center
	mesh_instance.global_transform.origin = mesh_instance_global_origin
	# restore current rotation
	rotation = current_rotation
