class_name Aimer
extends Node3D


@export var target_path := NodePath()

@export_range(0.0, 1.0) var speed := 0.75
@export_range(0.0, 1.0) var min_weight := 0.25

@export_enum("ORBITING_QUAT", "ORBITING_BASIS", "FREEFLY") var aimer_type: String = "ORBITING_QUAT"

var basis_start := Basis()
var quat_start := Quaternion()

@onready var target_node = get_node(target_path)


func _ready():
	basis_start = global_transform.basis
	quat_start = Quaternion(basis_start)


func _physics_process(delta):
	# get the target dir
	var target_dir:Vector3 = (target_node.global_transform.origin - position).normalized()
	if aimer_type == "FREEFLY":
		compute_freefly(target_dir)
	elif aimer_type == "ORBITING_BASIS":
		# Vector3.UP preferred UP direction
		compute_orbiting_basis(target_dir)
	elif aimer_type == "ORBITING_QUAT":
		# Vector3.UP preferred UP direction
		compute_orbiting_quat(target_dir)
	
	#<DEBUG
#	# direct the rotation axis
#	if get_node("Axis"):
#		var qrot_axis = quaternion.get_axis()
#		var right = Vector3.FORWARD.cross(qrot_axis).normalized()
#		var forward = qrot_axis.cross(right)
#		$Axis.transform.basis.x = right
#		$Axis.transform.basis.y = qrot_axis
#		$Axis.transform.basis.z = forward
	#DEBUG>
	
	#<DEBUG
#	measure_performance(10000, target_dir)
#	pass
	#DEBUG>


func compute_freefly(target_dir):
	# compute normalized (ie between versors) dot product
	var target_dot = Vector3.FORWARD.dot(target_dir)
	# check if versors are (approximately) parallel
	if not is_equal_approx(target_dot, 1.0):
		# versors are not parallel: compute angle & axis of rotation
		var target_angle = acos(target_dot)
		var target_axis:Vector3 = Vector3.FORWARD.cross(target_dir).normalized()
		# compute target quaternion
		var target_quat = Quaternion(target_axis, target_angle) * quat_start
#		var weight = abs(PI - target_angle) / PI * speed
		var weight = clamp((1.0 - target_dot), min_weight, 1.0) * speed
		# interpolate current & target quaternions
		quaternion = quaternion.slerp(target_quat, weight)


func compute_orbiting_basis(target_dir):
	var curr_f_dir = (global_transform.basis * basis_start).z
	var target_dot = target_dir.dot(curr_f_dir)
	# check if versors are (approximately) parallel
	if not is_equal_approx(target_dot, 1.0):
		var f_dir = target_dir
		var r_dir = f_dir.cross(Vector3.UP).normalized()
		var u_dir = r_dir.cross(f_dir)#.normalized()
		var target_basis = Basis(r_dir, u_dir, -f_dir) * basis_start
		var weight = clamp((1.0 - target_dot), min_weight, 1.0) * speed
		global_transform.basis = global_transform.basis.slerp(target_basis, weight)


func compute_orbiting_quat(target_dir):
	var curr_f_dir = (global_transform.basis * basis_start).z
	var target_dot = target_dir.dot(curr_f_dir)
	# check if versors are (approximately) parallel
	if not is_equal_approx(target_dot, 1.0):
		var f_dir = target_dir
		var r_dir = f_dir.cross(Vector3.UP).normalized()
		var u_dir = r_dir.cross(f_dir)#.normalized()
		var target_quat = Quaternion(Basis(r_dir, u_dir, -f_dir)) * quat_start
		var weight = clamp((1.0 - target_dot), min_weight, 1.0) * speed
		quaternion = quaternion.slerp(target_quat, weight)


#<DEBUG
#func measure_performance(N, target_dir):
#	var t = Time.get_unix_time_from_system()
#	for i in range(N):
#		compute_freefly(target_dir)
#	print(Time.get_unix_time_from_system() - t, " -> freefly") 
#	t = Time.get_unix_time_from_system()
#	for i in range(N):
#		compute_orbiting_basis(target_dir)
#	print(Time.get_unix_time_from_system() - t, " -> orbiting") 
#	t = Time.get_unix_time_from_system()
#	for i in range(N):
#		compute_orbiting_quat(target_dir)
#	print(Time.get_unix_time_from_system() - t, " -> orbiting_quat")
#DEBUG>
