@tool
extends MeshInstance3D


class AABBFace:
	var aabb:AABB = AABB()
	var normal:Vector3 = Vector3.ZERO
	var points:Array = []
	var axis_name:String = ""
	var transform:Transform3D = Transform3D.IDENTITY
	
	func _init(_aabb:AABB, n:Vector3, scaling:Vector3=Vector3.ONE):
		self.aabb = _aabb.abs()
		var p = self.aabb.position
		var e = self.aabb.end
		var s0 = e - p
		var s = s0 * scaling
		var sx2 = s.x / 2.0
		var sy2 = s.y / 2.0
		var sz2 = s.z / 2.0
		var t_rot = Transform3D.IDENTITY
		var t_trans = Transform3D.IDENTITY
		# X axis
		if (abs(n.x) > abs(n.y)) and (abs(n.x) > abs(n.z)):
			self.points.append(Vector2(0,0))
			self.points.append(Vector2(s.z,0))
			self.points.append(Vector2(s.z,s.y))
			self.points.append(Vector2(0,s.y))
			t_rot = Transform3D(Basis(Vector3.UP, deg_to_rad(90)), Vector3.ZERO)
			if n.x > 0.0:
				self.axis_name = "+X"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(sx2, -sy2, sz2))
			else:
				self.axis_name = "-X"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(-sx2, -sy2, sz2))
		# Y axis
		elif (abs(n.y) > abs(n.x)) and (abs(n.y) > abs(n.z)):
			self.points.append(Vector2(0,0))
			self.points.append(Vector2(s.x,0))
			self.points.append(Vector2(s.x,s.z))
			self.points.append(Vector2(0,s.z))
			t_rot = Transform3D(Basis(Vector3.RIGHT, deg_to_rad(90)), Vector3.ZERO)
			if n.y > 0.0:
				self.axis_name = "+Y"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(-sx2, sy2, -sz2))
			else:
				self.axis_name = "-Y"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(-sx2, -sy2, -sz2))
		# Z axis
		elif (abs(n.z) > abs(n.x)) and (abs(n.z) > abs(n.y)):
			self.points.append(Vector2(0,0))
			self.points.append(Vector2(s.x,0))
			self.points.append(Vector2(s.x,s.y))
			self.points.append(Vector2(0,s.y))
			if n.z > 0.0:
				self.axis_name = "+Z"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(-sx2, -sy2, sz2))
			else:
				self.axis_name = "-Z"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(-sx2, -sy2, -sz2))
		#
		self.transform = t_trans * t_rot


func _ready():
	var occluder_node = Node3D.new()
	occluder_node.name = name + "Occluders"
	var normals = [
		Vector3.RIGHT,
		-Vector3.RIGHT,
		Vector3.UP,
		-Vector3.UP,
		Vector3.FORWARD,
		-Vector3.FORWARD,
	]
	var scaling = Vector3.ONE * 0.5
	for o in occluders_get(normals, scaling):
		occluder_node.add_child(o)
		o.set_owner(occluder_node)
	var packed_scene = PackedScene.new()
	packed_scene.pack(occluder_node)
	var scene_name = "res://test_3D_occluders/occluders/occluder_" + name + ".tscn"
	ResourceSaver.save(scene_name, packed_scene)


func occluders_get(normals, s):
	var scaling = Vector3(clamp(s.x, 0, 1.0), clamp(s.y, 0, 1.0), clamp(s.z, 0, 1.0))
	var occluders: Array = []
	for n in normals:
		occluders.append(_occluder_create(n, scaling))
	return occluders


func _occluder_create(n:Vector3, scaling:Vector3):
	var aabb_face = AABBFace.new(get_aabb(), n, scaling)
	var point_array = PackedVector2Array(aabb_face.points)
	var occluder = OccluderInstance3D.new()
	occluder.name = aabb_face.axis_name
	occluder.shape = OccluderShapePolygon.new()
	occluder.shape.polygon_points = point_array
	occluder.transform = transform * aabb_face.transform
	return occluder
