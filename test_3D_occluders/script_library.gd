extends Node

class_name ScriptLibrary

### INPUT: Action management

static func _add_action(action, keycode=null, button_index=null):
	InputMap.add_action(action)
	if keycode:
		var ev = InputEventKey.new()
		ev.keycode = keycode
		InputMap.action_add_event(action, ev)
	if button_index:
		var ev = InputEventMouseButton.new()
		ev.button_index = button_index
		InputMap.action_add_event(action, ev)


static func _get_scancode(action):
	var keycode = 0
	for key_event in InputMap.action_get_events(action):
		if key_event is InputEventKey:
			keycode = key_event.keycode
			return keycode
	return null


static func _get_button_index(action):
	var button_index = 0
	for mouse_event in InputMap.action_get_events(action):
		if mouse_event is InputEventMouseButton:
			button_index = mouse_event.button_index
			return button_index
	return null


static func _feed_action(name, pressed, strength=1.0):
	var ev_action = InputEventAction.new()
	ev_action.action = name
	ev_action.button_pressed = pressed
	Input.parse_input_event(ev_action)


### Object's Occluders management

class AABBFace:
	var aabb:AABB = AABB()
	var normal:Vector3 = Vector3.ZERO
	var points:Array = []
	var axis_name:String = ""
	var transform:Transform3D = Transform3D.IDENTITY
	
	func _init(_aabb:AABB, normal:Vector3, scaling:Vector3=Vector3.ONE):
		self.aabb = _aabb.abs()
		var p = self.aabb.position
		var e = self.aabb.end
		var s0 = e - p
		var s = s0 * scaling
		var sx2 = s.x / 2.0
		var sy2 = s.y / 2.0
		var sz2 = s.z / 2.0
		var t_rot = Transform3D.IDENTITY
		var t_trans = Transform3D.IDENTITY
		# X axis
		if (abs(normal.x) > abs(normal.y)) and (abs(normal.x) > abs(normal.z)):
			self.points.append(Vector2(0,0))
			self.points.append(Vector2(s.z,0))
			self.points.append(Vector2(s.z,s.y))
			self.points.append(Vector2(0,s.y))
			t_rot = Transform3D(Basis(Vector3.UP, deg_to_rad(90)), Vector3.ZERO)
			if normal.x > 0.0:
				self.axis_name = "+X"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(sx2, -sy2, sz2))
			else:
				self.axis_name = "-X"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(-sx2, -sy2, sz2))
		# Y axis
		elif (abs(normal.y) > abs(normal.x)) and (abs(normal.y) > abs(normal.z)):
			self.points.append(Vector2(0,0))
			self.points.append(Vector2(s.x,0))
			self.points.append(Vector2(s.x,s.z))
			self.points.append(Vector2(0,s.z))
			t_rot = Transform3D(Basis(Vector3.RIGHT, deg_to_rad(90)), Vector3.ZERO)
			if normal.y > 0.0:
				self.axis_name = "+Y"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(-sx2, sy2, -sz2))
			else:
				self.axis_name = "-Y"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(-sx2, -sy2, -sz2))
		# Z axis
		elif (abs(normal.z) > abs(normal.x)) and (abs(normal.z) > abs(normal.y)):
			self.points.append(Vector2(0,0))
			self.points.append(Vector2(s.x,0))
			self.points.append(Vector2(s.x,s.y))
			self.points.append(Vector2(0,s.y))
			if normal.z > 0.0:
				self.axis_name = "+Z"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(-sx2, -sy2, sz2))
			else:
				self.axis_name = "-Z"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(-sx2, -sy2, -sz2))
		#
		self.transform = t_trans * t_rot


const AXIS_NORMALS = [
		Vector3.RIGHT,
		-Vector3.RIGHT,
		Vector3.UP,
		-Vector3.UP,
		Vector3.FORWARD,
		-Vector3.FORWARD,
	]


static func occluders_save_scene(object:VisualInstance3D, normals:Array=AXIS_NORMALS, 
		scaling:Vector3=Vector3.ONE, save_dir:String=""):
	var occluder_node = Node3D.new()
	occluder_node.name = "Occluders"
	for o in occluders_get(object, normals, scaling):
		occluder_node.add_child(o)
		o.set_owner(occluder_node)
	var packed_scene = PackedScene.new()
	packed_scene.pack(occluder_node)
	var scene_name = "res://" + save_dir + "/" + object.name + "_occluders.tscn" 
	ResourceSaver.save(scene_name, packed_scene)


static func occluders_get(object:VisualInstance3D, normals, scaling):
	var s = Vector3(clamp(scaling.x, 0, 1.0), clamp(scaling.y, 0, 1.0), 
			clamp(scaling.z, 0, 1.0))
	var occluders: Array = []
	for n in normals:
		occluders.append(_occluder_create(object, n, s))
	return occluders


static func _occluder_create(object, n, s):
	var aabb_face = AABBFace.new(object.get_aabb(), n, s)
	var point_array = PackedVector2Array(aabb_face.points)
	var occluder = OccluderInstance3D.new()
	occluder.name = object.name + "_" + aabb_face.axis_name
	occluder.shape = OccluderShapePolygon.new()
	occluder.shape.polygon_points = point_array
	occluder.transform = object.transform * aabb_face.transform
	return occluder
