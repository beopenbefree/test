@tool
extends Node3D

var lib = preload("res://test_3D_occluders/script_library.gd")

var scene_name = "res://test_3D_occluders/test_occluders_node.tscn"

var normals = [
	Vector3.RIGHT,
	-Vector3.RIGHT,
#	Vector3.UP,
#	-Vector3.UP,
	Vector3.FORWARD,
	-Vector3.FORWARD,
]
var scaling = Vector3.ONE * 0.5

func _ready():
	var occluder_node = Node3D.new()
	occluder_node.name = "Occluders"
	for child in get_children():
		if child is VisualInstance3D:
			for o in lib.occluders_get(child, normals, scaling):
				occluder_node.add_child(o)
				o.set_owner(occluder_node)
	var packed_scene = PackedScene.new()
	packed_scene.pack(occluder_node)
	ResourceSaver.save(scene_name, packed_scene)
