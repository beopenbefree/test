extends Node2D


func _draw():
	var center = Vector2(512, 300)
	var radius = 80
	var angle_from = 0
	var angle_to = 135
	var color = Color(1.0, 0.5, 0.25)
	draw_circle_arc(center, radius, -angle_from, -angle_to, color)


func draw_circle_arc(center, radius, angle_from, angle_to, color):
	var num_points = 32
	var points_arc = PackedVector2Array()
	var delta_angle = (angle_to - angle_from) / num_points
	
	for i in range(num_points + 1):
		var angle_point = deg_to_rad(angle_from + i * delta_angle)
		points_arc.push_back(center + 
				Vector2(cos(angle_point), sin(angle_point)) * radius)
	
	for index_point in range(num_points):
		draw_line(points_arc[index_point], points_arc[index_point + 1], color)
	
