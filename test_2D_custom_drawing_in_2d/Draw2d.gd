extends Node2D


@export (Texture2D) var texture : set = set_texture


func set_texture(value):
	texture = value
	update()


func _draw():
	var pos= Vector2(get_viewport().size.x / 2 - texture.get_width() / 2, 
			get_viewport().size.y / 2 - texture.get_height() / 2)
	draw_texture(texture, pos)
