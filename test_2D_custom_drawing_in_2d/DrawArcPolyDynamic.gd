@tool
extends Node2D


var rotation_angle_speed = 50
var angle_from = 0
var angle_to = 135


func _process(delta):
	angle_from += rotation_angle_speed * delta
	angle_to += rotation_angle_speed * delta
	
	if (angle_from > 360) and (angle_to > 360):
		angle_from = wrapf(angle_from, 0.0, 360.0)
		angle_to = wrapf(angle_to, 0.0, 360.0)
	update()


func _draw():
	var center = Vector2(512, 300)
	var radius = 80
	var color = Color(1.0, 0.5, 0.25)
	draw_circle_arc_poly_dynamic(center, radius, -angle_from, -angle_to, color)


func draw_circle_arc_poly_dynamic(center, radius, angle_from, angle_to, color):
	var num_points = 32
	var points_arc = PackedVector2Array()
	points_arc.push_back(center)
	var colors = PackedColorArray([color])
	var delta_angle = (angle_to - angle_from) / num_points
	
	for i in range(num_points + 1):
		var angle_point = deg_to_rad(angle_from + i * delta_angle)
		points_arc.push_back(center + 
				Vector2(cos(angle_point), sin(angle_point)) * radius)
		draw_polygon(points_arc, colors)
