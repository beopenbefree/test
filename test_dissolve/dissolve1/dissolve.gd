extends Node3D


@export var dissolve_time: float = 16.0
@export var dissolve_objet_path: NodePath = NodePath()

@export_range(0.1, 15.0) var fade_time:float = 5.0
@export_range(0.1, 10.0) var fade_flickering_frequency:float = 4.0

var do_dissolve:bool = false
var fade_elapsed_time:float = 0.0

@onready var dissolve_object:MeshInstance3D = get_node(dissolve_objet_path)
@onready var dissolve_object_material: ShaderMaterial = dissolve_object.material_override
@onready var fade_albedo:Color = dissolve_object_material.get_shader_parameter("albedo")
@onready var fade_alpha:float = fade_albedo.a
@onready var fade_speed:float = 1.0 / fade_time
@onready var fade_phase_speed = 2 * PI * fade_flickering_frequency


### <XXX
const VEL = 10.0
const POSMAX = 10.0
var dir = -1
### XXX>


func _ready():
	do_dissolve = true


func _physics_process(delta):
	
	### <XXX
	dissolve_object.global_transform.origin.z += dir * delta * VEL
	if ((dissolve_object.global_transform.origin.z > POSMAX) or 
			(dissolve_object.global_transform.origin.z <- POSMAX)):
		dir *= -1.0
	### XXX>
		
	if do_dissolve: 
		if dissolve_time >= 0.0:
			dissolve_time -= delta
		else:
			fade_elapsed_time += delta
			fade_alpha -= fade_speed * delta
			if fade_alpha < 0.0:
				queue_free()
			var albedo = fade_albedo * (1.0 + sin(fade_phase_speed * fade_elapsed_time))
			albedo.a = fade_alpha * fade_alpha
			dissolve_object_material.set_shader_parameter("albedo", albedo)
