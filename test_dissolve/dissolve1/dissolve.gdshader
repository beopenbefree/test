shader_type spatial;
render_mode cull_disabled;// ,blend_mix,depth_draw_opaque,diffuse_burley,specular_schlick_ggx

uniform vec4 albedo : source_color = vec4(1.);
uniform sampler2D texture_albedo : source_color,filter_linear_mipmap,repeat_enable;
uniform sampler2D texture_normal : hint_roughness_normal,filter_linear_mipmap,repeat_enable;
uniform sampler2D texture_blend;
uniform sampler2D texture_noise;

uniform float offset = 0.;
uniform bool up = true;
uniform vec4 border_color: source_color = vec4(1., 1., 0., 1.);
uniform float border_height = 0.1;
uniform float wave_amplitude = 1.;
uniform float wave_frequency = 1.;
uniform float wave_phase = 0.1;
uniform float emission_intensity = 1.;
uniform float noise_speed = .01;
uniform float noise_influence = 1.;

uniform float roughness : hint_range(0,1);
uniform sampler2D texture_metallic : hint_default_white,filter_linear_mipmap,repeat_enable;
uniform vec4 metallic_texture_channel;
uniform sampler2D texture_roughness : hint_roughness_r,filter_linear_mipmap,repeat_enable;
uniform float specular;
uniform float metallic;
uniform float normal_scale : hint_range(-16,16);

uniform vec2 blend_uv_scale = vec2(1.);
uniform vec2 noise_uv_scale = vec2(1.);
uniform vec2 texture_uv_scale = vec2(1.);

uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;

const float tao = 2. * 3.14;


void vertex() {
	UV=UV*uv1_scale.xy+uv1_offset.xy;
}


void fragment() {
	vec2 base_uv = UV;
	
	vec3 position = (inverse(MODEL_MATRIX) * INV_VIEW_MATRIX * vec4(VERTEX, 1.0)).xyz;
	
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	
	vec4 blend_tex = texture(texture_blend, base_uv * blend_uv_scale);
	
	vec2 st = base_uv;
	st.y -= TIME * noise_speed;
	vec4 noise = texture(texture_noise, st * noise_uv_scale);
	
	float x = tao * position.x;
	float wave_frequency_1 = wave_frequency;
	float wave_frequency_2 = wave_frequency + 2. - wave_phase;
	float wave_frequency_3 = wave_frequency + 3. - wave_phase;
	
	position.y += wave_amplitude * (sin(x / wave_frequency_1) + sin(x / wave_frequency_2) + sin(x / wave_frequency_3));
	position.y += (noise.r * noise_influence);
	
	float direction = up ? 1. : -1.;
	float upper_border = smoothstep(offset, offset, (position.y * direction) + 1.);
	float bottom_border = smoothstep(offset, offset, (position.y * direction) - border_height + 1.);
	float border_part = upper_border - bottom_border;
	
	vec4 color = mix(blend_tex, border_color, upper_border);
	color = mix(color, albedo_tex, bottom_border);
	
	ALBEDO = albedo.rgb * color.rgb;
	
	NORMAL_MAP = texture(texture_normal,base_uv).rgb;
	NORMAL_MAP_DEPTH = normal_scale;
	
	if (!FRONT_FACING) {
			ALBEDO = border_color.rgb;
			NORMAL = VIEW;
	}
	
	ALPHA = color.a;
	ALPHA_SCISSOR_THRESHOLD = 1.0;
	EMISSION = vec3(border_part) * border_color.rgb * emission_intensity;
	
	float metallic_tex = dot(texture(texture_metallic,base_uv),metallic_texture_channel);
	METALLIC = metallic_tex * metallic;
	vec4 roughness_texture_channel = vec4(1.0,0.0,0.0,0.0);
	float roughness_tex = dot(texture(texture_roughness,base_uv),roughness_texture_channel);
	ROUGHNESS = roughness_tex * roughness;
	SPECULAR = specular;
	NORMAL_MAP = texture(texture_normal,base_uv).rgb;
	NORMAL_MAP_DEPTH = normal_scale;
}
