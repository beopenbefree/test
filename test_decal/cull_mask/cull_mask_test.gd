extends Node3D

@export_flags_2d_render var cull_mask: int = 0b1

var texture: Texture = preload("res://assets/Images/adesert_stone_d.jpg")
var dir: float = -1.0
var count: int = -1

@onready var decal := Decal.new()
@onready var timer := Timer.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	add_child(decal)
	decal.texture_albedo = texture
	decal.cull_mask = 0xffffffff
	decal.position.x = 0.0
	decal.size = Vector3(8.0, 10.0, 2.0)
	#
	add_child(timer)
	timer.timeout.connect(_toggle_cull_mask)
	timer.wait_time = 2.0
	timer.start()
	#
	set_process(false)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var dpos = delta * dir
	decal.position.x += dpos
	if (decal.position.x > 3.5) or (decal.position.x < -3.5):
		dir *= -1.0


func _toggle_cull_mask():
	count = (count + 1) % 4
	decal.cull_mask = cull_mask
	match count:
		0:
			$plane.layers |= cull_mask
			$cylinder.layers &= ~cull_mask
			$cube.layers &= ~cull_mask
		1:
			$plane.layers &= ~cull_mask
			$cylinder.layers |= cull_mask
			$cube.layers &= ~cull_mask
		2:
			$plane.layers &= ~cull_mask
			$cylinder.layers &= ~cull_mask
			$cube.layers |= cull_mask
		3:
			decal.cull_mask = 0xffffffff
	#
	timer.start()
