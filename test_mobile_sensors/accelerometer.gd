extends Node2D


var low_pass_sample_num: int = 16
var acc_sample_buffers: Array = [[], [], []]

var mass: float = 1.0
var vel: Vector3 = Vector3()


func _ready():
	for s in range(low_pass_sample_num):
		acc_sample_buffers[0].append(0.0)
		acc_sample_buffers[1].append(0.0)
		acc_sample_buffers[2].append(0.0)
	_update_text(Vector3(0.0, 0.0, 0.0))


func _process(delta):
	var acc = 0.0
	# Get our data
	var acc_raw = Input.get_accelerometer()
	acc = acc_raw
	var acc_lp = _apply_low_pass(acc, acc_sample_buffers)
	acc = acc_lp
	#
	_update_text(acc)
	vel += acc * delta
	$Sprite2D.position += Vector2(vel.x, -vel.y) * delta
	var v_rect = get_viewport_rect()
	if $Sprite2D.position.x >= v_rect.size.x:
		$Sprite2D.position.x = v_rect.size.x
	elif $Sprite2D.position.x <= 0.0:
		$Sprite2D.position.x = 0.0
	if $Sprite2D.position.y >= v_rect.size.y:
		$Sprite2D.position.y = v_rect.size.y
	elif $Sprite2D.position.y <= 0.0:
		$Sprite2D.position.y = 0.0


func _update_text(a):
	$VBoxContainer/Ax.text = "a.x = %.4f" % a.x
	$VBoxContainer/Ay.text = "a.y = %.4f" % a.y
	$VBoxContainer/Az.text = "a.z = %.4f" % a.z


func _apply_low_pass(v:Vector3, sample_buffers):
	var res  = Vector3(0.0, 0.0, 0.0)
	# x
	sample_buffers[0].remove(0)
	sample_buffers[0].append(v.x)
	# y
	sample_buffers[1].remove(0)
	sample_buffers[1].append(v.y)
	# z
	sample_buffers[2].remove(0)
	sample_buffers[2].append(v.z)
	# low pass: compute average of last low_pass_sample_num samples
	for s in range(low_pass_sample_num):
		res.x += sample_buffers[0][s]
		res.y += sample_buffers[1][s]
		res.z += sample_buffers[2][s]
	return res / low_pass_sample_num
