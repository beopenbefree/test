extends Node2D


@export var rotation_speed = 5.0 # (float, 5.0, 15.0, 1.0)


func _ready():
	_update_text(Vector3(0.0, 0.0, 0.0))


func _process(delta):
	# Get our data
	var acc = Input.get_accelerometer()
	# apply filter and compute rotation angle
	acc = $LowPassFilterBhide.apply_low_pass_vector(acc)
	$Sprite2D.rotation_degrees = acc.x * rotation_speed
	_update_text(acc, $Sprite2D.rotation_degrees)


func _update_text(a, r=0.0):
	$VBoxContainer/Ax.text = "a.x = %.4f" % a.x
	$VBoxContainer/Ay.text = "a.y = %.4f" % a.y
	$VBoxContainer/Az.text = "a.z = %.4f" % a.z
	$VBoxContainer/R.text = "r = %.4f" % r
