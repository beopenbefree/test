extends Node3D


@export var use_low_pass: bool = true
@export var low_pass_samples: int = 16

var lpf:Node = null
var text_cont:Control = null
var sprite:Sprite2D = null


func _ready():
	if use_low_pass:
		lpf = $Display/LowPassFilter
		lpf.sample_num = low_pass_samples
		lpf.initialize()
	sprite = $Display/Sprite2D
	text_cont = $Display/VBoxContainer
	_update_text(Vector3(0.0, 0.0, 0.0))


func _process(delta):
	# Get our data
	var acc = Input.get_accelerometer()
	# compute rotation angle
	var rot_angle = 0.0
	if (abs(acc.y) > 0.7) and (abs(acc.x) > 0.7):
		rot_angle = atan2(acc.x, -acc.y)
	if use_low_pass:
		rot_angle = lpf.apply_low_pass_scalar(rot_angle)
#	$Display.rotation = -rot_angle
	sprite.rotation = rot_angle
#	$Camera.rotation.z = -rot_angle
	_update_text(acc, rot_angle)


func _update_text(a, r=0.0):
	text_cont.get_node("Ax").text = "a.x = %.1f" % a.x
	text_cont.get_node("Ay").text = "a.y = %.1f" % a.y
	text_cont.get_node("Az").text = "a.z = %.1f" % a.z
	text_cont.get_node("R").text = "r = %.1f" % r
