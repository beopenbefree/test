extends Node2D


@export var use_low_pass: bool = true
@export var low_pass_samples: int = 16


func _ready():
	if use_low_pass:
		$LowPassFilter.sample_num = low_pass_samples
		$LowPassFilter.initialize()
	_update_text(Vector3(0.0, 0.0, 0.0))


func _process(delta):
	# Get our data
	var acc = Input.get_accelerometer()
	# compute rotation angle
	var rot_angle = 0.0
	if (abs(acc.y) > 0.7) and (abs(acc.x) > 0.7):
		rot_angle = atan2(acc.x, -acc.y)
	if use_low_pass:
		rot_angle = $LowPassFilter.apply_low_pass_scalar(rot_angle)
	var rot_degrees = rad_to_deg(rot_angle)
	$Sprite2D.rotation_degrees = rot_degrees
	rotation_degrees = -rot_degrees
	_update_text(acc, rot_angle)


func _update_text(a, r=0.0):
	$VBoxContainer/Ax.text = "a.x = %.4f" % a.x
	$VBoxContainer/Ay.text = "a.y = %.4f" % a.y
	$VBoxContainer/Az.text = "a.z = %.4f" % a.z
	$VBoxContainer/R.text = "r = %.4f" % r
