extends Node2D


func _ready():
	_update_text(Vector3(0.0, 0.0, 0.0))


func _process(delta):
	var gyr = Input.get_gyroscope()
	_update_text(gyr)


func _update_text(g):
	$VBoxContainer/Gx.text = "g.x = %.4f" % g.x
	$VBoxContainer/Gy.text = "g.y = %.4f" % g.y
	$VBoxContainer/Gz.text = "g.z = %.4f" % g.z
