extends Node3D


@export var use_low_pass: bool = true
@export var low_pass_samples: int = 16

var lpf:Node = null
var text_cont:Control = null
var sprite:Sprite2D = null

var old_rot_angle:float = 0
var rot_rate:float = 1.2
const ROT_ANGLE_MIN:float = deg_to_rad(-45.0)
const ROT_ANGLE_MAX:float = deg_to_rad(45.0)

func _ready():
	if use_low_pass:
		lpf = $Display/LowPassFilter
		lpf.sample_num = low_pass_samples
		lpf.initialize()
	sprite = $Display/Sprite2D
	text_cont = $Display/VBoxContainer
	_update_text(Vector3(0.0, 0.0, 0.0))


func _process(delta):
	# Get our data
	var acc = Input.get_accelerometer()
#	if use_low_pass:
#		acc = lpf.apply_low_pass_vector(acc)
	# compute rotation angle
	var rot_angle = atan2(acc.x, -acc.y)
	if use_low_pass:
		rot_angle = lpf.apply_low_pass_scalar(rot_angle)
	
	rot_angle = old_rot_angle + (rot_angle - old_rot_angle) * rot_rate
	rot_angle = clamp(rot_angle, ROT_ANGLE_MIN, ROT_ANGLE_MAX)
	old_rot_angle = rot_angle
	
	$Display.rotation = -rot_angle
	sprite.rotation = rot_angle
	$Camera3D.rotation.z = -rot_angle
	_update_text(acc, rot_angle)


func _update_text(a, r=0.0):
	text_cont.get_node("Ax").text = "a.x = %.4f" % a.x
	text_cont.get_node("Ay").text = "a.y = %.4f" % a.y
	text_cont.get_node("Az").text = "a.z = %.4f" % a.z
	text_cont.get_node("R").text = "r = %.4f" % r
