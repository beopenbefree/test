extends Node


@export var sample_num: int = 16
var sample_buffers: Array = [[], [], []]


func initialize():
	if sample_num < 1:
		sample_num = 1
	for s in range(sample_num):
		sample_buffers[0].append(0.0)
		sample_buffers[1].append(0.0)
		sample_buffers[2].append(0.0)


func apply_low_pass_vector(v:Vector3):
	var res  = Vector3(0.0, 0.0, 0.0)
	# x
	sample_buffers[0].remove(0)
	sample_buffers[0].append(v.x)
	# y
	sample_buffers[1].remove(0)
	sample_buffers[1].append(v.y)
	# z
	sample_buffers[2].remove(0)
	sample_buffers[2].append(v.z)
	# low pass: compute average of last low_pass_sample_num samples
	for s in range(sample_num):
		res.x += sample_buffers[0][s]
		res.y += sample_buffers[1][s]
		res.z += sample_buffers[2][s]
	return res / sample_num

func apply_low_pass_scalar(v:float):
	var res  = 0.0
	# x
	sample_buffers[0].remove(0)
	sample_buffers[0].append(v)
	# low pass: compute average of last low_pass_sample_num samples
	for s in range(sample_num):
		res += sample_buffers[0][s]
	return res / sample_num
