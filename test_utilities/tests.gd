extends ResourcePreloader


func _ready():
	print("Testing PriorityQueue")
	_priority_queue_test(20)
	pass


## PriorityQueue
class ObjPrio extends RefCounted:
	var prio:int
  
	func _init(p_prio:int):
		prio = p_prio

func _priority_queue_test(OBJ_NUM:int):    
	var prio_queue_max = PriorityQueue.new(true, [], "prio")
	var prio_queue_min = PriorityQueue.new(false, [], "prio")
	for i in OBJ_NUM:
		var n = randi_range(-100, 100)
		var obj = ObjPrio.new(n)
		prio_queue_max.insert(obj)
		prio_queue_min.insert(obj)
		
	print("descending ordered")
	while prio_queue_max.size() > 0:
		var head_node = prio_queue_max.head()
		print("\t", head_node.prio)
		prio_queue_max.delete_node(head_node)
	print("ascending ordered")
	while prio_queue_min.size() > 0:
		var head_node = prio_queue_min.head()
		print("\t", head_node.prio)
		prio_queue_min.delete_node(head_node)
