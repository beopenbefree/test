extends Button


enum SpeedUnit {
	METERS_PER_SECOND = 0,
	KILOMETERS_PER_HOUR = 1,
	MILES_PER_HOUR = 2,
}

@export var speed_unit: SpeedUnit = 0


func _process(_delta):
	var vehicle = globals.current_vehicle_body
	if vehicle:
		var speed = vehicle.current_speed
		if speed_unit == SpeedUnit.METERS_PER_SECOND:
			text = "Speed: " + ("%.1f" % speed) + " m/s"
		elif speed_unit == SpeedUnit.KILOMETERS_PER_HOUR:
			speed *= 3.6
			text = "Speed: " + ("%.0f" % speed) + " km/h"
		else: # speed_unit == SpeedUnit.MILES_PER_HOUR:
			speed *= 2.23694
			text = "Speed: " + ("%.0f" % speed) + " mph"
		get_parent().get_node("Gear").text = "Gear: " + vehicle.current_gear


func _on_Spedometer_pressed():
	speed_unit = (speed_unit + 1) % 3
