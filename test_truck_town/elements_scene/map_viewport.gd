extends SubViewportContainer


@export var camera_size: float = 100.0: get = _get_camera_size, set = _set_camera_size
@export var camera_position: Vector2 = Vector2(0.0, 0.0)
@export var camera_rotation_y: float = 0.0
const MAX_COLOR_RECT_SIZE = Vector2(180.0, 180.0)
@export var color_rect_size: Vector2 = MAX_COLOR_RECT_SIZE

var vehicle


func _ready():
	$SubViewport/Camera3D.size = camera_size
	$SubViewport/Camera3D.position.x = camera_position.x
	$SubViewport/Camera3D.position.z = camera_position.y
	$SubViewport/Camera3D.rotation_degrees.y = camera_rotation_y
	$SubViewport/ColorRect.size = color_rect_size
#	$Viewport/ColorRect.rect_position += (MAX_COLOR_RECT_SIZE - color_rect_size) / 2.0


func _set_camera_size(value):
	camera_size = value


func _get_camera_size():
	return camera_size


func _process(_delta):
#	var vehicle = globals.current_vehicle
	if vehicle:
		# NOTE: here we're supposing that, for both camera and vehicle's body,
		# local and global transforms are identical
		var vehiche_body = vehicle.get_node("Body")
		$SubViewport/Camera3D.global_transform.origin.x = vehiche_body.global_transform.origin.x
		$SubViewport/Camera3D.global_transform.origin.z = vehiche_body.global_transform.origin.z
		$SubViewport/Camera3D.rotation.y = vehiche_body.rotation.y + camera_rotation_y
