extends Node3D


func _ready():
	var vehicle = get_node("../Body")
	if vehicle:
		set_signal_handling(vehicle)


func set_signal_handling(vehicle:VehicleBody3D):
	# forward
	$FW_VB.connect("analog_touch_true", Callable(vehicle, "_on_VirtualMove_move").bind(Vector2(0, -1)))
	$FW_VB.connect("analog_touch_false", Callable(vehicle, "_on_VirtualMove_move_stop").bind(false))
	# backward
	$BW_VB.connect("analog_touch_true", Callable(vehicle, "_on_VirtualMove_move").bind(Vector2(0, 1)))
	$BW_VB.connect("analog_touch_false", Callable(vehicle, "_on_VirtualMove_move_stop").bind(false))
	# brake
	$BR_VB.connect("analog_touch_true", Callable(vehicle, "_on_VirtualBrake_brake").bind(true))
	$BR_VB.connect("analog_touch_false", Callable(vehicle, "_on_VirtualBrake_brake").bind(false))
	# steer
	$ST_VA.connect("analog_move", Callable(vehicle, "_on_VirtualSteer_steer"))
	$ST_VA.connect("analog_touch", Callable(vehicle, "_on_VirtualSteer_steer_stop"))
