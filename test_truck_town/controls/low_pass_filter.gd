extends Node
# see: https://github.com/Bhide/Low-Pass-Filter-To-Android-Sensors


@export var cutoff_vector = 0.016 # (float, 0.0, 1.0)
@export var cutoff_scalar = 0.016 # (float, 0.0, 1.0)
var output_value_vector: Vector3 = Vector3()
var output_value_scalar: float = 0.0


func apply_low_pass_vector(v:Vector3):
	output_value_vector.x = output_value_vector.x + cutoff_vector * (v.x - output_value_vector.x)
	output_value_vector.y = output_value_vector.y + cutoff_vector * (v.y - output_value_vector.y)
	output_value_vector.z = output_value_vector.z + cutoff_vector * (v.z - output_value_vector.z)
	return output_value_vector


func apply_low_pass_scalar(v:float):
	output_value_scalar = output_value_scalar + cutoff_scalar * (v - output_value_scalar)
	return output_value_scalar
