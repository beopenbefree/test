extends Node2D


signal analog_touch(touching)
signal analog_move(direction)

@export var enabled: bool = true: get = _get_enabled, set = _set_enabled

@export var outer_scale: Vector2 = Vector2.ONE
@export var inner_scale: Vector2 = Vector2.ONE

@export var outer_texture: Texture2D = null
@export var inner_texture: Texture2D = null

@export var region_x_ratio = 0: set = _set_region_x_ratio
@export var region_y_ratio = 0: set = _set_region_y_ratio
@export var region_width_ratio = 1: set = _set_region_width_ratio
@export var region_height_ratio = 1: set = _set_region_height_ratio
var screen_region: Rect2

var index:int = -1
@export var max_distance:float = 55.0


func _ready():
	# scale correction
	$OuterAnalog.scale = outer_scale
	$OuterAnalog/InnerAnalog.scale = inner_scale / outer_scale
	# texture correction
	$OuterAnalog.texture = outer_texture
	$OuterAnalog/InnerAnalog.texture = inner_texture
	# compute/update screen region
	_update_screen_region()


func _input(event):
	if event is InputEventScreenTouch or event is InputEventScreenDrag:
	
		# Only set index if there is nothing assigned
		if index == -1:
			if event is InputEventScreenTouch and event.pressed and correct_side(event.position):
				index = event.index
				$OuterAnalog.position = event.position
				emit_signal("analog_touch", true)
				visible = true
		
		# return early so we don't process unecessary events.
		if index == -1 || event.index != index:
			return
			
		# finish the drag
		if event is InputEventScreenTouch and not event.pressed:
			index = -1
			$OuterAnalog/InnerAnalog.position = Vector2(0,0)
			visible = false
			emit_signal("analog_touch", false)
			return
			
		$OuterAnalog/InnerAnalog.position = (event.position - 
				$OuterAnalog.position).limit_length(max_distance)
		# this will set the intensity between 0 and 1
		var intensity = $OuterAnalog/InnerAnalog.position.length() / max_distance
		var direction = $OuterAnalog/InnerAnalog.position.normalized()

		emit_signal("analog_move", direction * intensity)


func correct_side(touch_position):
	# check if touch_position is inside screen_region
	return screen_region.has_point(touch_position)


func _set_enabled(value):
	enabled = value
	set_process_input(enabled)


func _get_enabled():
	return enabled


func _set_region_x_ratio(value):
	region_x_ratio = value
	_update_screen_region()


func _set_region_y_ratio(value):
	region_y_ratio = value
	_update_screen_region()


func _set_region_width_ratio(value):
	region_width_ratio = value
	_update_screen_region()


func _set_region_height_ratio(value):
	region_height_ratio = value
	_update_screen_region()


func _update_screen_region():
	if get_viewport():
		var viewport_rect = get_viewport_rect()
		screen_region = Rect2(
				viewport_rect.position.x + viewport_rect.size.x * region_x_ratio,
				viewport_rect.position.y + viewport_rect.size.y * region_y_ratio,
				viewport_rect.size.x * region_width_ratio,
				viewport_rect.size.y * region_height_ratio)
		# debug screen region rect
		$DebugRegionRect.position = screen_region.position
		$DebugRegionRect.size = screen_region.size
