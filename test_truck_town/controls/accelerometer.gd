extends Node3D


@export var enabled: bool = true: get = _get_enabled, set = _set_enabled
@export var target: NodePath = null

var target_node: Node = null


func _ready():
	if target:
		target_node = get_node(target)


func _physics_process(delta):
	# Get our data
	var acc = Input.get_accelerometer()
	# apply filter and compute rotation angle
	acc = $LowPassFilter.apply_low_pass_vector(acc)
	# call apply_accelerometer on target (if any)
	if target_node and (target_node.has_method("apply_accelerometer")):
		target_node.apply_accelerometer(acc)


func _set_enabled(value):
	enabled = value
	set_physics_process(enabled)


func _get_enabled():
	return enabled
