extends ColorRect


func _ready():
	$CenterContainer/VBoxContainer/Score.text = "Score: " + str(globals.score)
	await get_tree().create_timer(5).timeout
	globals._show_game_over_popup()
