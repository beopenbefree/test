extends TextureButton


@onready var data = preload("res://gdnative/simple-c/bin/simple.gdns").new()


func _on_SimpleButton_pressed():
	$"../SimpleLabel".text = "Data = " + data.get_data()


func _on_SimpleButton_toggled(button_pressed):
	if button_pressed:
		$"../SimpleLabel".text = "Data = " + data.get_data()
	else:
		$"../SimpleLabel".text = "Data = "
