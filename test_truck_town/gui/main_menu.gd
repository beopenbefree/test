extends Control


var start_menu
var vehicle_select_menu
var vehicle_control_menu
var vehicle_setting_menu
var configuration_menu

var button_image:Resource = preload("res://gui/button_image.tscn")
var check_button_image:Resource = preload("res://gui/check_button_image.tscn")
var list_item:Resource = preload("res://gui/list_item.tscn")


func _ready():
	Input.add_joy_mapping("03000000300f00001101000010010000,Jess Tech Colour Rumble Pad,a:b2,b:b3,y:b1,x:b0,start:b9,back:b8,leftstick:b10,rightstick:b11,leftshoulder:b4,rightshoulder:b6,dpup:b12,dpleft:b14,dpdown:b13,dpright:b13,leftx:a0,lefty:a1~,rightx:a3,righty:a2~,lefttrigger:b5,righttrigger:b7,platform:Linux", true)
#	Input.add_joy_mapping("03000000300f00001101000010010000,Jess Tech Colour Rumble Pad,a:b2,b:b3,y:b1,x:b0,start:b9,back:b8,leftstick:b10,rightstick:b11,leftshoulder:b4,rightshoulder:b6,dpup:b12,dpleft:b14,dpdown:b13,dpright:b13,leftx:a0,lefty:a1,rightx:a3,righty:a2,lefttrigger:b5,righttrigger:b7,platform:Linux", true)
	start_menu = $StartMenu
	vehicle_select_menu = $VehicleSelectMenu
	vehicle_control_menu = $VehicleControlMenu
	vehicle_setting_menu = $VehicleSettingMenu
	configuration_menu = $ConfigurationMenu
	# StartMenu
	_initiliatize_start_menu()
	
	# VehicleSelectMenu
	_initiliatize_vehicle_select_menu()
	
	# VehicleControlMenu
	_initiliatize_vehicle_control_menu()
	
	# VehicleSettingMenu
	_initiliatize_vehicle_setting_menu()
	
	# ConfigurationMenu
	_initiliatize_configuration_menu()
	
	# set persistent options
	configuration_menu_button_pressed("vsync", true)
	configuration_menu_button_pressed("fullscreen", true)
	configuration_menu_button_pressed("debug", true)
	configuration_menu_button_pressed("low_resolution", true)
	
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func start_menu_button_pressed(button_name):
	if button_name == "start_new":
		globals._reset_game_state()
		_display_vehicle_select()
		_display_vehicle_controls()
		_display_vehicle_settings()
		vehicle_select_menu.visible = true
		start_menu.visible = false
	elif button_name == "continue_saved":
		if globals.saved_game_good() and globals.load_game():
			globals.load_new_scene(globals.current_level_path)
		else:
			vehicle_select_menu.visible = true
			start_menu.visible = false
	elif button_name == "open_godot":
		OS.shell_open("https://godotengine.org/")
	elif button_name == "options":
		configuration_menu.visible = true
		start_menu.visible = false
	elif button_name == "quit":
		globals.end_game()


func configuration_menu_button_pressed(button_name, set_from_global = false):
	var button
	var container = $ConfigurationMenu/Panel/VBoxContainer/ScrollContainer/VBoxOthers
	var conf_opts = globals.configuration_options
	if button_name == "back":
		start_menu.visible = true
		configuration_menu.visible = false
	elif button_name == "vsync":
		button = container.get_node("CheckButtonVSync")
		if set_from_global:
			button.button_pressed = conf_opts["vsync"]
		else:
			conf_opts["vsync"] = button.pressed
		DisplayServer.window_set_vsync_mode(DisplayServer.VSYNC_ENABLED if (conf_opts["vsync"]) else DisplayServer.VSYNC_DISABLED)
	elif button_name == "fullscreen":
		button = container.get_node("CheckButtonFullscreen")
		if set_from_global:
			button.button_pressed = conf_opts["full_screen"]
		else:
			conf_opts["full_screen"] = button.pressed
		get_window().mode = Window.MODE_EXCLUSIVE_FULLSCREEN if (conf_opts["full_screen"]) else Window.MODE_WINDOWED
	elif button_name == "debug":
		button = container.get_node("CheckButtonDebug")
		if set_from_global:
			button.button_pressed = conf_opts["debug_display"]
		else:
			conf_opts["debug_display"] = button.pressed
		globals.set_debug_display(conf_opts["debug_display"])
	elif button_name == "low_resolution":
		button = container.get_node("CheckLowResolution")
		var old_resolution = conf_opts["low_resolution"]
		if set_from_global:
			button.button_pressed = conf_opts["low_resolution"]
		else:
			conf_opts["low_resolution"] = button.pressed
		if conf_opts["low_resolution"]:
			get_window().size = globals.GAME_DATA["SCREEN_LOW_RES"]
			RenderingServer.texture_set_shrink_all_x2_on_set_data(true)
		else:
			get_window().size = globals.GAME_DATA["SCREEN_HIGH_RES"]
			RenderingServer.texture_set_shrink_all_x2_on_set_data(false)
		configuration_menu_button_pressed("back")


func vehicle_select_menu_button_pressed(button_name, button_type):
	if button_type == "system":
		if button_name == "back":
			start_menu.visible = true
			vehicle_select_menu.visible = false
			vehicle_control_menu.visible = false
			vehicle_setting_menu.visible = false
	elif button_type == "control":
		globals.current_control_type = load(globals.GAME_DATA["control_types"][button_name][0])
		globals.current_control_type_name = button_name
		# display
		var display = $VehicleControlMenu/Display
		display.get_node("Controls").texture = load(globals.GAME_DATA["control_types"][button_name][4])
	elif button_type == "vehicle":
		var vehicle
		for item in globals.GAME_DATA["vehicles"]:
			if item["name"] == button_name:
				vehicle = item
				break
		globals.current_vehicle_path = vehicle["path"]
		globals.current_vehicle_sound = vehicle["sound"]
		if weakref(globals.current_vehicle).get_ref():
			globals.current_vehicle.queue_free()
			globals.current_vehicle = null
		globals.current_vehicle = load(globals.current_vehicle_path).instantiate()
		globals.current_vehicle_body.get_current_settings()
		_display_vehicle_settings()
		# display
		var display = $VehicleSelectMenu/Display
		display.get_node("Vehicle").texture = load(vehicle["image"])


func _initiliatize_start_menu():
	var container = $StartMenu/Panel/VBoxContainer
	var display = $StartMenu/Display
	display.get_node("ButtonNewGame").connect("pressed", self,
			"start_menu_button_pressed", ["start_new"])
	container.get_node("ButtonContinueSavedGame").connect("pressed", self,
			"start_menu_button_pressed", ["continue_saved"])
	container.get_node("VBoxContainer/ButtonOpenGodot").connect("pressed", self,
			"start_menu_button_pressed", ["open_godot"])
	container.get_node("VBoxContainer/ButtonConfiguration").connect("pressed", self,
			"start_menu_button_pressed", ["options"])
	container.get_node("ButtonQuit").connect("pressed", self,
			"start_menu_button_pressed", ["quit"])
	if not globals.saved_game_good():
		container.get_node("ButtonContinueSavedGame").visible = false


func _initiliatize_configuration_menu():
	var container = $ConfigurationMenu/Panel/VBoxContainer
	var display = $ConfigurationMenu/Display
	container.get_node("ScrollContainer/VBoxOthers/CheckButtonFullscreen").connect("pressed", self,
			"configuration_menu_button_pressed", ["fullscreen"])
	container.get_node("ScrollContainer/VBoxOthers/CheckButtonVSync").connect("pressed", self,
			"configuration_menu_button_pressed", ["vsync"])
	container.get_node("ScrollContainer/VBoxOthers/CheckButtonDebug").connect("pressed", self,
			"configuration_menu_button_pressed", ["debug"])
	container.get_node("ScrollContainer/VBoxOthers/CheckLowResolution").connect("pressed", self,
			"configuration_menu_button_pressed", ["low_resolution"])
	display.get_node("ButtonBack").connect("pressed", self,
			"configuration_menu_button_pressed", ["back"])


func _initiliatize_vehicle_select_menu():
	var container = $VehicleSelectMenu/Panel/VBoxContainer
	# vehicle select/controls/settings postponed
	container.get_node("HBoxContainer/ButtonBack").connect("pressed", self,
			"vehicle_select_menu_button_pressed", ["back", "system"])


func _initiliatize_vehicle_control_menu():
	# vehicle select/controls/settings postponed
	pass


func _initiliatize_vehicle_setting_menu():
	# vehicle select/controls/settings postponed
	pass


func _display_vehicle_select():
	var container = $VehicleSelectMenu/Panel/VBoxContainer/VBoxVehicleSelect/ScrollContainer
	var sub_container = container.get_node("HBoxContainer/VBoxContainer")
	# reset
	container.scroll_vertical = 0.0
	for node in sub_container.get_children():
		node.free()
	# (re)create
	var check_button_group = ButtonGroup.new()
	var vehicles = globals.GAME_DATA["vehicles"]
	for vehicle in vehicles:
		var check_button_instance = check_button_image.instance()
		check_button_instance.set_name(vehicle["name"])
		check_button_instance.group = check_button_group
		var icon_on = vehicle["icon"]
		var icon_off = vehicle["icon_off"]
		check_button_instance.set_icons(icon_on, icon_off)
		check_button_instance.connect("pressed", self, 
			"vehicle_select_menu_button_pressed", [vehicle["name"], "vehicle"])
		sub_container.add_child(check_button_instance)
	sub_container.get_child(0).button_pressed = true
	vehicle_select_menu_button_pressed(sub_container.get_child(0).name, "vehicle")


func _display_vehicle_controls():
	var container = $VehicleControlMenu/Panel/VBoxContainer/VBoxVehicleControls/ScrollContainer/
	var sub_container = container.get_node("VBoxcontainer")
	# reset
	container.scroll_vertical = 0.0
	for node in sub_container.get_children():
		node.free()
	# (re)create
	var check_button_group = ButtonGroup.new()
	var contro_types = globals.GAME_DATA["control_types"]
	for control_key in contro_types:
		var check_button_instance = check_button_image.instance()
		check_button_instance.set_name(control_key)
		check_button_instance.group = check_button_group
		var icon_on = contro_types[control_key][2]
		var icon_off = contro_types[control_key][3]
		check_button_instance.set_icons(icon_on, icon_off)
		check_button_instance.connect("pressed", self, 
			"vehicle_select_menu_button_pressed", [control_key, "control"])
		sub_container.add_child(check_button_instance)
	sub_container.get_child(0).button_pressed = true
	vehicle_select_menu_button_pressed(sub_container.get_child(0).name, "control")


func _display_vehicle_settings():
	var container = $VehicleSettingMenu/Panel/VBoxContainer/VBoxVehicleSettings/ScrollContainer
	var sub_container = container.get_node("HBoxContainer/VBoxContainer")
	# reset
	container.scroll_vertical = 0.0
	for node in sub_container.get_children():
		node.free()
	# (re)create
	for item_key in globals.current_vehicle_settings:
		var item_value = globals.current_vehicle_settings[item_key]
		# check if gui enabled
		if item_value[4]:
			var item_instance = list_item.instance()
			var label_name = item_key.replace("_", " ")
			item_instance.get_node("HBoxContainer/Label").text = "%s" % label_name
			var slider:HSlider = item_instance.get_node("HBoxContainer/HSlider")
			slider.min_value = item_value[1]
			slider.max_value = item_value[2]
			slider.step = item_value[3]
			var value_gui_node = item_instance.get_node("HBoxContainer/Value")
			slider.connect("value_changed", self, "_on_ListItem_value_changed", 
					[globals.current_vehicle_settings, item_key, 0, value_gui_node])
			# initial update
			value_gui_node.text = str(slider.value)
			sub_container.add_child(item_instance)
			slider.value = item_value[0]


func _on_Start_pressed():
	globals.load_new_scene(globals.current_level_path)


func _on_BackStart_pressed(menu):
	vehicle_select_menu.visible = true
	get_node(menu).visible = false 


func _on_ButtonControl_pressed():
	vehicle_select_menu.visible = false
	vehicle_control_menu.visible = true


func _on_ButtonSetting_pressed():
	vehicle_select_menu.visible = false
	vehicle_setting_menu.visible = true


func _on_ListItem_value_changed(value, dictionary, key, idx, value_gui_node):
	dictionary[key][idx] = value
	value_gui_node.text = "%*.*f" % [4, 2, float(value)]
