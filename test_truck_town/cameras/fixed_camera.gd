extends Marker3D


@export var enabled: bool = true: get = _get_enabled, set = _set_enabled
@export var current: bool = true: get = _get_current, set = set_current

const CAMERA_TYPE = "fixed_camera"

func _set_enabled(value):
	enabled = value


func _get_enabled():
	return enabled


func set_current(value):
	current = value
	$Camera3D.current = current


func _get_current():
	return current
