extends Marker3D


@export var enabled: bool = true: get = _get_enabled, set = _set_enabled

@export var target: NodePath = null: get = _get_target, set = _set_target
@export var current: bool = true: get = _get_current, set = set_current
@export var max_delta_distance: float = 1.0
@export var angle_v_adjust: float = 0.0
@export var max_look_at_angle: float = 15.0
# inverse of the friction camera undergoes when returning to its reference 
# location/rotation (1.0 = highest friction)
@export var location_friction = 1.0 # (float, 0.0, 1.0, 0.05)
@export var rotation_friction = 1.0 # (float, 0.0, 1.0, 0.05)

@export var fixed_location_look_at: bool = false
# orbiting camera settings
@export var orbiting_camera_enabled: bool = true: get = _get_orbiting_camera_enabled, set = _set_orbiting_camera_enabled

@export var orbiting_max_yaw_angle = 30.0 # (float, 0.0, 90.0, 0.05)
@export var orbiting_max_pitch_angle = 15.0 # (float, 0.0, 90.0, 0.05)
@export var orbiting_yaw_speed = 2.0 # (float, 0.0, 10.0, 0.05)
@export var orbiting_pitch_speed = 2.0 # (float, 0.0, 10.0, 0.05)

@export var virtual_region_x_ratio = 0 # (float, 0, 1, 0.01)
@export var virtual_region_y_ratio = 0 # (float, 0, 1, 0.01)
@export var virtual_region_width_ratio = 1 # (float, 0, 1, 0.01)
@export var virtual_region_height_ratio = 1  # (float, 0, 1, 0.01)

@export var is_orthogonal: bool = false
@export var size_orthogonal: float = 1.0

const CAMERA_TYPE = "tracking_camera"

var target_node: Node = null
var camera_parent_node: Node = null

var collision_exception = []

var camera_looking_at:bool = true

var max_distance:float = 1.0
var min_distance:float = 1.0

var max_look_at_angle_rad: float = 0.0
var cos_max_look_at_angle: float = 0.0
var max_yaw_angle_rad: float = 0.0
var max_pitch_angle_rad: float = 0.0
var camera_yaw_angle:float = 0.0
var camera_pitch_axis:Vector3 = Vector3()
var camera_pitch_angle:float = 0.0

var touch_steer_direction:Vector2 = Vector2(0, 0)
var touch_touching:bool = false

var tracking_camera:Marker3D = null


func _ready():
	_set_on_ready_parameters()


func _physics_process(_delta):
	# check if orbiting camera is requested
	if orbiting_camera_enabled and (! fixed_location_look_at):
		_check_orbiting_camera_request(_delta)
	
	# get all transforms w.r.t. GLOBAL SPACE
	var target_pos = target_node.get_global_transform().origin
	var camera_pos = $Camera3D.get_global_transform().origin
	var pos = camera_parent_node.get_global_transform().origin
	
	# this is the camera's reference direction w.r.t. target_node, i.e. the camera
	# should stay along this direction
	var target_camera_ref_dir = (pos - target_pos).normalized()
	
	# the actual camera's position and direction w.r.t. target_node
	var target_camera_pos = camera_pos - target_pos
	var target_camera_dir = target_camera_pos.normalized()
	
	# Check distance ranges
	var target_camera_pos_length = target_camera_pos.length()
	if target_camera_pos_length < min_distance:
		target_camera_pos = target_camera_dir * min_distance
	elif target_camera_pos_length > max_distance:
		target_camera_pos = target_camera_dir * max_distance
	
	# check angle ranges
	var cos_look_at = target_camera_dir.dot(target_camera_ref_dir)
	if cos_look_at >= cos_max_look_at_angle:
		camera_pos = target_pos + target_camera_pos
	else: # cos_look_at < cos_max_look_at_angle
		# the camera is outside the cone formed by the 
		# target_camera_ref_pos axis and the max_look_at_angle amplitude angle
		var plane_normal = target_camera_dir.cross(target_camera_ref_dir).normalized()
		var delta_angle = (acos(cos_look_at) - max_look_at_angle_rad) * location_friction
		var delta = target_camera_dir.rotated(plane_normal, delta_angle)
		camera_pos = target_pos + delta * target_camera_pos.length()
	
	if camera_looking_at:
		if (rotation_friction < 1.0) and (! fixed_location_look_at):
			# camera start quat
			var camera_basis = $Camera3D.get_global_transform().basis
			var camera_quat = camera_basis.get_rotation_quaternion()
			if !camera_quat.is_normalized():
				camera_quat = camera_quat.normalized()
			
			# compute look at rotation
			var forward = (target_pos - camera_pos).normalized()
			var right = forward.cross(-Vector3.UP).normalized()
			var up = right.cross(forward).normalized()
			# compute target quat
			var target_basis = Basis(right, up, forward)
			var target_quat = target_basis.get_rotation_quaternion()
			if !target_quat.is_normalized():
				target_quat = target_quat.normalized()
			
			# compute interpolated quat
			var camera_quat_new =  camera_quat.slerp(target_quat, 0.05)
			var camera_new_trans = Transform3D()
			camera_new_trans.basis = Basis(camera_quat_new)
			if ! fixed_location_look_at:
				camera_new_trans.origin = camera_pos
			$Camera3D.global_transform = camera_new_trans
		else:
			if ! fixed_location_look_at:
				$Camera3D.look_at_from_position(camera_pos, target_pos, Vector3.UP)
			else:
				$Camera3D.look_at(target_pos, Vector3.UP)
	else:
		$Camera3D.global_transform.origin = camera_pos
	
	# Turn a little up or down
	var t = $Camera3D.global_transform
	t.basis = Basis(t.basis[0], deg_to_rad(angle_v_adjust)) * t.basis
	$Camera3D.global_transform = t


func _set_on_ready_parameters():
	# Find collision exceptions for ray.
	_find_collision_exceptions_for_ray()
	# This detaches the camera transform from the parent node.
	$Camera3D.set_as_top_level(true)
	$Camera3D.current = current
	tracking_camera = self
	# this identifies the target_node
	if target:
		target_node = get_node(target)
	else:
		target_node = get_parent()
	camera_parent_node = self
	# check if camera is orthogonal
	if is_orthogonal:
		$Camera3D.projection = Camera3D.PROJECTION_ORTHOGONAL
		$Camera3D.size = size_orthogonal
	# setting other parameters
	var target_pos = target_node.get_global_transform().origin
	var camera_pos = $Camera3D.get_global_transform().origin
	var base_distance = (camera_pos - target_pos).length()
	min_distance = max(base_distance - abs(max_delta_distance), 0.0)
	max_distance = max(base_distance + abs(max_delta_distance), min_distance)
	#
	max_look_at_angle_rad = deg_to_rad(max_look_at_angle / 2.0)
	cos_max_look_at_angle = cos(max_look_at_angle_rad)
	max_yaw_angle_rad = deg_to_rad(orbiting_max_yaw_angle)
	max_pitch_angle_rad = deg_to_rad(orbiting_max_pitch_angle)
	orbiting_yaw_speed *= max_yaw_angle_rad
	orbiting_pitch_speed *= max_pitch_angle_rad
	# check if there is rearview mirror(s)
	for rearview_mirror in get_children():
		if (rearview_mirror is Node3D) and (rearview_mirror.get("tracking_camera") != null):
			# set rearview mirror parameters
			var sub_tracking_camera = rearview_mirror.tracking_camera
			sub_tracking_camera.location_friction = location_friction
			sub_tracking_camera.rotation_friction = rotation_friction
	$VirtualAnalog.region_x_ratio = virtual_region_x_ratio
	$VirtualAnalog.region_y_ratio = virtual_region_y_ratio
	$VirtualAnalog.region_width_ratio = virtual_region_width_ratio
	$VirtualAnalog.region_height_ratio = virtual_region_height_ratio
	set_physics_process(enabled)


func _check_orbiting_camera_request(delta):
	var left = Input.is_action_pressed("camera_turn_left")
	var right = Input.is_action_pressed("camera_turn_right")
	var up = Input.is_action_pressed("camera_turn_up")
	var down = Input.is_action_pressed("camera_turn_down")
	# check if orbiting camera is requested
	if (left or right or up or down or touch_touching):
		if camera_looking_at:
			camera_yaw_angle = 0.0
			camera_pitch_axis = Vector3.RIGHT
			camera_pitch_angle = 0.0
		var delta_angle_yaw = 0.0
		var delta_angle_pitch = 0.0
		if left or right:
			delta_angle_yaw = delta * (Input.get_action_strength("camera_turn_left") - 
					Input.get_action_strength("camera_turn_right")) * orbiting_yaw_speed
			camera_yaw_angle += delta_angle_yaw
			if (camera_yaw_angle > -max_yaw_angle_rad) and (camera_yaw_angle < max_yaw_angle_rad):
				$Camera3D.rotate_y(delta_angle_yaw)
		if up or down:
			delta_angle_pitch = delta * (Input.get_action_strength("camera_turn_up") - 
					Input.get_action_strength("camera_turn_down")) * orbiting_pitch_speed
			camera_pitch_angle += delta_angle_pitch
			if (camera_pitch_angle > -max_pitch_angle_rad) and (camera_pitch_angle < max_pitch_angle_rad):
				$Camera3D.rotate_object_local(camera_pitch_axis, delta_angle_pitch)
		if touch_touching:
			# yaw
			var new_camera_yaw_angle = -touch_steer_direction.x * max_yaw_angle_rad
			delta_angle_yaw = (new_camera_yaw_angle - camera_yaw_angle)
			camera_yaw_angle = new_camera_yaw_angle
			if (new_camera_yaw_angle > -max_yaw_angle_rad) and (new_camera_yaw_angle < max_yaw_angle_rad):
				$Camera3D.rotate_y(delta_angle_yaw)
			# pitch
			var new_camera_pitch_angle = -touch_steer_direction.y * max_pitch_angle_rad
			delta_angle_pitch = (new_camera_pitch_angle - camera_pitch_angle)
			camera_pitch_angle = new_camera_pitch_angle
			if (new_camera_pitch_angle > -max_pitch_angle_rad) and (new_camera_pitch_angle < max_pitch_angle_rad):
				$Camera3D.rotate_object_local(camera_pitch_axis, delta_angle_pitch)
		camera_looking_at = false
	else:
		camera_looking_at = true


func _set_enabled(value):
	enabled = value
	set_physics_process(enabled)
	$VirtualAnalog.enabled = value


func _get_enabled():
	return enabled


func _set_target(value):
	target = value
	if target:
		target_node = get_node(target)


func _get_target():
	return target


func set_current(value):
	current = value
	$Camera3D.current = current


func _get_current():
	return current


func _set_orbiting_camera_enabled(value):
	orbiting_camera_enabled = value
	$VirtualAnalog.enabled = orbiting_camera_enabled


func _get_orbiting_camera_enabled():
	return orbiting_camera_enabled


func _on_VirtualSteer_steer(direction):
	if enabled:
		touch_steer_direction = direction


func _on_VirtualSteer_touch(touching):
	if enabled:
		touch_touching = touching
		if not touching:
			touch_steer_direction = Vector2(0, 0)


func _find_collision_exceptions_for_ray():
	var node = self
	while(node):
		if (node is RigidBody3D):
			collision_exception.append(node.get_rid())
			break
		elif (node == self) and target:
			node = get_node(target)
		else:
			node = node.get_parent()
