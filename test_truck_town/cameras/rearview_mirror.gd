extends Node3D


@export var enabled: bool = true: get = _get_enabled, set = _set_enabled

@export var is_visible: bool = true: get = _get_visible, set = _set_visible

enum Position {LEFT, CENTER, RIGHT}
@export var viewport_location: Position = Position.CENTER

const CAMERA_TYPE = "rearview_mirror"

var inner_camera:Marker3D = null


func _ready():
	# This is a proxy passing parameters to the inner tracking camera
	inner_camera = $SubViewport/TrackingCameraParent/InnerCamera
	# apply tracking camera parameters
	if inner_camera.CAMERA_TYPE == "tracking_camera":
		inner_camera.camera_parent_node = self
		inner_camera.target_node = $Target
		inner_camera.min_distance = abs($Target.position.z)
		inner_camera.max_distance = inner_camera.min_distance * 1.1
		inner_camera.max_look_at_angle = 5.0
		# set camera target
		inner_camera.target = @"../../../Target"
	# set viewport location
	if viewport_location == Position.LEFT:
		$CenterContainer.offset_left = -400
		$CenterContainer.offset_right = -200
	elif viewport_location == Position.RIGHT:
		$CenterContainer.offset_left = 200
		$CenterContainer.offset_right = 400


func _set_enabled(value):
	enabled = value
	$SubViewport/TrackingCameraParent/InnerCamera.enabled = enabled


func _get_enabled():
	return enabled


func _set_visible(value):
	is_visible = value
	show() if is_visible else hide()
	for child in get_children():
		if child.get("visible") != null:
			child.show() if is_visible else child.hide()


func _get_visible():
	return is_visible
