extends Node3D


@export var enabled: bool = true: get = _get_enabled, set = _set_enabled

@export var min_distance: float = 2.0
@export var max_distance: float = 4.0
@export var angle_v_adjust: float = 0.0
@export var max_look_at_angle: float = 30.0
@export var height: float = 1.5
@export var current: bool = true

const CAMERA_TYPE = "follow_camera"

var max_look_at_angle_rad = deg_to_rad(max_look_at_angle)
var cos_max_look_at_angle = cos(max_look_at_angle_rad)

var collision_exception = []

@onready var camera = $Camera3D


func _ready():
	# Find collision exceptions for ray.
	var node = self
	while(node):
		if (node is RigidBody3D):
			collision_exception.append(node.get_rid())
			break
		else:
			node = node.get_parent()
	
	# This detaches the camera transform from the parent spatial node.
	camera.set_as_top_level(true)
	camera.current = true


func _physics_process(_delta):
	var target = get_parent().get_global_transform().origin
	var target_dir = get_parent().get_global_transform().basis.z
	var pos = camera.get_global_transform().origin
	
	var from_target: Vector3 = pos - target
	
	# Check ranges.
	if from_target.length() < min_distance:
		from_target = from_target.normalized() * min_distance
	elif from_target.length() > max_distance:
		from_target = from_target.normalized() * max_distance
	
	if from_target.y < height:
		from_target.y = height
	var cosLookAt = from_target.normalized().dot(-target_dir)
	if cosLookAt >= cos_max_look_at_angle:
		pos = target + from_target
	else:
		var plane_normal = from_target.cross(-target_dir).normalized()
		var delta_angle = (acos(cosLookAt) - max_look_at_angle_rad) * 0.1
		var delta = from_target.rotated(plane_normal, delta_angle)
		pos = target + delta
	
	camera.look_at_from_position(pos, target, Vector3.UP)
	
	# Turn a little up or down
	var t = camera.get_transform()
	t.basis = Basis(t.basis[0], deg_to_rad(angle_v_adjust)) * t.basis
	camera.set_transform(t)


func _set_enabled(value):
	enabled = value
	set_physics_process(enabled)


func _get_enabled():
	return enabled
