extends Node3D


var CAMERA_TYPES = [
	preload("res://test_truck_town/cameras/tracking_camera.gd"), # default
#	preload("res://vehicles/cameras/fixed_camera.gd"),
#	preload("res://vehicles/cameras/follow_camera.gd"),
	preload("res://test_truck_town/cameras/rearview_mirror.gd"),
]
var cameras: Array = []
var current_camera_idx: int = 0
var default_camera: String = ""


func _ready():
	for child in get_children():
		if _instance_of_camera(child):
			cameras.push_back(child)
	if cameras.size() != 0:
		_set_enabled_and_current(cameras[0])
	else:
		# no viable camera
		var camera = CAMERA_TYPES[0].new()
		add_child(camera)
		_set_enabled_and_current(camera)


func _input(event):
	if Input.is_action_pressed("toggle_camera"):
		toggle_camera()


func toggle_camera():
	current_camera_idx = (current_camera_idx + 1) % cameras.size()
	var current_camera = cameras[current_camera_idx]
	if !current_camera.enabled and !current_camera.current:
		_set_enabled_and_current(current_camera)


func _set_enabled_and_current(camera):
	camera.enabled = true
	camera.current = true
	for camera_child in camera.get_children():
		# check if there is some rearview mirror(s)
		if (camera_child is Node3D) and (camera_child.get("is_visible") != null):
			camera_child.enabled = true
			camera_child.is_visible = true
	# make other camera's not current and disabled
	for other_camera in get_children():
		if (other_camera is Marker3D) and (other_camera != camera):
			other_camera.enabled = false
			other_camera.current = false
			# check if there is some rearview mirror(s)
			for other_camera_child in other_camera.get_children():
				if (other_camera_child is Node3D) and (other_camera_child.get("is_visible") != null):
					other_camera_child.enabled = false
					other_camera_child.is_visible = false


func _instance_of_camera(object):
	for camera_type in CAMERA_TYPES:
		if object is camera_type:
			return true
	return false
