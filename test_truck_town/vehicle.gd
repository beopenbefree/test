extends VehicleBody3D


@export var max_forward_speed = 10.0 # (float, 1.0, 100.0, 1)
@export var steer_speed = 1.5 # (float, 0.5, 5.0, 0.1)
@export var steer_limit = 0.4 # (float, 0.0, 1.0, 0.05)
@export var tilt_angle_max = 25 # (float, 10.0, 60.0, 1.0)
@export var tilt_engine_force_factor = 1.0 # (float, 1.0, 3.0, 0.1)
@export var accel_steer_speed = 1.0 # (float, 0.5, 2.0, 0.05)
@export var engine_force_value = 40 # (float, 2.0, 120.0, 1.0)
@export var lift_force: Vector3 = Vector3(0.0, 100.0, 0.0)
@export var lift_force_position: Vector3 = Vector3()
@export var gear_max_speed1 = 3.0 # (float, 1.0, 20.0, 1.0)
@export var gear_max_speed2 = 7.0 # (float, 1.0, 40.0, 1.0)
@export var gear_max_speed3 = 12.0 # (float, 1.0, 60.0, 1.0)
@export var gear_max_speed4 = 18.0 # (float, 1.0, 80.0, 0.0)
@export var gear_speed_factor = 0.2 # (float, 0.1, 1.0, 0.05)
@export var gear_engine_factor = 0.008 # (float, 0.001, 0.1, 0.001)

const MAX_FLOAT = 2^62
var steer_target = 0
var touch_move_direction:Vector2 = Vector2(0, 0)
var touch_steer_direction:Vector2 = Vector2(0, 0)
var accel_steer:float = 0
const max_accel_steer:float = 9.81 # Android accelerometer's component max value
const max_accel_steer_inv:float = 1.0 / max_accel_steer
var brake_size: float = 1.0
# boost area stuff
var engine_force_boost = 1.0
var boost_timer: Timer
# blast data
var do_blast = false
var blast_impulse = Vector3()
const BLAST_TORQUE_IMPULSE = 50.0
# lift data
var lift_up: bool = false
# sound data for gears
var current_sound: Node3D = null

# current data
@onready var tilt_max:float = sin(deg_to_rad(tilt_angle_max))
@onready var tilt_engine_force_factor_backward:float = tilt_engine_force_factor * 0.8
@onready var current_speed: float = 0.0
@onready var current_gear: String = "0"


func _ready():
	# boost area stuff
	boost_timer = Timer.new()
	boost_timer.connect("timeout", Callable(self, "_on_boost_timer_timeout"))
	boost_timer.one_shot = true
	add_child(boost_timer)
	# compute brake size
	brake_size = mass / 1000.0 * 30.0
	# get current engine sound
#	current_sound = globals.get_sound(globals.current_vehicle_sound, self)
	var control = load("res://test_truck_town/controls/combinations/fw_va-bw_va-br_vb-st_va.tscn").instantiate()
	add_child(control)
	control.set_signal_handling(self)


func _physics_process(delta):
	if Input.is_action_pressed("turn_left") or Input.is_action_pressed("turn_right"):
		steer_target = Input.get_action_strength("turn_left") - Input.get_action_strength("turn_right")
	else:
		steer_target = -touch_steer_direction.x
	steer_target *= steer_limit
	
	var speed = linear_velocity.length()
	var speed_inv
	if speed != 0.0:
		speed_inv = 1.0 / speed
	else:
		speed_inv = MAX_FLOAT
	if Input.is_action_pressed("accelerate") or (touch_move_direction.y < 0):
		# Increase engine force at low speeds to make the initial acceleration faster.
		if speed <= max_forward_speed:
			if get_tilt() > tilt_max:
				engine_force = engine_force_value * max(10.0 * speed_inv,
						tilt_engine_force_factor * engine_force_boost)
			else:
				engine_force = engine_force_value * engine_force_boost
		else:
			var reduction_factor = (1.0 - (speed - max_forward_speed) * speed_inv)
			engine_force = engine_force_value * reduction_factor
	else:
		engine_force = 0
	
	if Input.is_action_pressed("reverse") or (touch_move_direction.y > 0):		
		# Increase engine force at low speeds to make the initial acceleration faster.
		var fwd_mps = (linear_velocity) * transform.basis.z
		if fwd_mps >= -1.0:
			if get_tilt() < -tilt_max:
				engine_force = -engine_force_value * max(8.0 * speed_inv, 
						tilt_engine_force_factor_backward)
			else:
				engine_force = -engine_force_value
		else:
			var reduction_factor = (1.0 - (speed - 1.0) * speed_inv)
			engine_force = -engine_force_value * reduction_factor

	steering = move_toward(steering, steer_target, steer_speed * delta)
	
	# set engine sound pitch
	if current_sound:
		var pitch_scale = 1.0
		if speed < 0.15:
			current_gear = "0"
		elif linear_velocity.dot(transform.basis.z) < 0.0:
			current_gear = "reverse"
			pitch_scale = 1 + gear_speed_factor * speed + gear_engine_factor * engine_force
		elif speed < gear_max_speed1:
			current_gear = "1"
			pitch_scale = 1 + gear_speed_factor * speed + gear_engine_factor * engine_force
		elif speed < gear_max_speed2:
			current_gear = "2"
			pitch_scale = 1 + gear_speed_factor * (speed - gear_max_speed1) + gear_engine_factor * 0.95 * engine_force
		elif speed < gear_max_speed3:
			current_gear = "3"
			pitch_scale = 1 + gear_speed_factor * (speed - gear_max_speed2) + gear_engine_factor * 0.9 * engine_force
		elif speed < gear_max_speed4:
			current_gear = "4"
			pitch_scale = 1 + gear_speed_factor * (speed - gear_max_speed3) + gear_engine_factor * 0.85 * engine_force
		else:
			current_gear = "5"
			pitch_scale = 1 + gear_speed_factor * (speed - gear_max_speed4) + gear_engine_factor * 0.8 * engine_force
		current_sound.set_pitch_scale(pitch_scale)
	
	current_speed = speed


func _input(event):
	if event.is_action_pressed("brake"):
		_set_current_brake(brake_size)
	elif event.is_action_released("brake"):
		_set_current_brake(0.0)


func _integrate_forces(state):
	if do_blast:
		apply_central_impulse(blast_impulse)
		apply_torque_impulse(Vector3(0.0, abs(randf()), 0.0) * BLAST_TORQUE_IMPULSE)
		do_blast = false
	if lift_up:
		apply_force(lift_force_position, lift_force)


func get_tilt():
	var forward  = get_global_transform().basis.z
	var up = Vector3(0, 1.0, 0)
	return up.dot(forward)


func apply_accelerometer(acc):
	accel_steer = acc.x * accel_steer_speed
	accel_steer = clamp(accel_steer, -max_accel_steer, 
			max_accel_steer)
	var x = accel_steer * max_accel_steer_inv
	var y = sqrt(1.0 - x * x)
	touch_steer_direction = Vector2(x, y)


func apply_boost(factor, duration):
	if (not boost_timer.is_stopped()) or (factor == 0.0):
		return
	
	engine_force_boost = factor
	boost_timer.start(duration)


func apply_blast(magnitude):
	var impulse_dir = Vector3(randf(), abs(randf()), randf()).normalized()
	blast_impulse = impulse_dir * magnitude
	do_blast = true


func add_time(delta):
	pass
#	var scene = globals.get_current_scene()
#	scene.current_count += delta
#	if scene.current_count > scene.total_count_down:
#		scene.current_count = scene.total_count_down


func bullet_hit(damage=null, bullet_hit_pos=null):
	if damage:
		# subtract -damage to scene's current_count
		add_time(-damage)
	# play sound
#	globals.play_sound("vehicle_collision_dynamic", globals.current_vehicle)


func set_current_settings():
	pass
#	max_forward_speed = globals.current_vehicle_settings["max_forward_speed"][0]
#	steer_speed = globals.current_vehicle_settings["steer_speed"][0]
#	steer_limit = globals.current_vehicle_settings["steer_limit"][0]
#	tilt_angle_max = globals.current_vehicle_settings["tilt_angle_max"][0]
#	tilt_engine_force_factor = globals.current_vehicle_settings["tilt_engine_force_factor"][0]
#	accel_steer_speed = globals.current_vehicle_settings["accel_steer_speed"][0]
#	engine_force_value = globals.current_vehicle_settings["engine_force_value"][0]
#	lift_force = globals.current_vehicle_settings["lift_force"][0]
#	lift_force_position = globals.current_vehicle_settings["lift_force_position"][0]
#	gear_max_speed1 = globals.current_vehicle_settings["gear_max_speed1"][0]
#	gear_max_speed2 = globals.current_vehicle_settings["gear_max_speed2"][0]
#	gear_max_speed3 = globals.current_vehicle_settings["gear_max_speed3"][0]
#	gear_max_speed4 = globals.current_vehicle_settings["gear_max_speed4"][0]
#	gear_speed_factor = globals.current_vehicle_settings["gear_speed_factor"][0]
#	gear_engine_factor = globals.current_vehicle_settings["gear_engine_factor"][0]


func get_current_settings():
	pass
#	globals.current_vehicle_settings["max_forward_speed"][0] = max_forward_speed
#	globals.current_vehicle_settings["steer_speed"][0] = steer_speed
#	globals.current_vehicle_settings["steer_limit"][0] = steer_limit
#	globals.current_vehicle_settings["tilt_angle_max"][0] = tilt_angle_max
#	globals.current_vehicle_settings["tilt_engine_force_factor"][0] = tilt_engine_force_factor
#	globals.current_vehicle_settings["accel_steer_speed"][0] = accel_steer_speed
#	globals.current_vehicle_settings["engine_force_value"][0] = engine_force_value
#	globals.current_vehicle_settings["lift_force"][0] = lift_force
#	globals.current_vehicle_settings["lift_force_position"][0] = lift_force_position
#	globals.current_vehicle_settings["gear_max_speed1"][0] = gear_max_speed1
#	globals.current_vehicle_settings["gear_max_speed2"][0] = gear_max_speed2
#	globals.current_vehicle_settings["gear_max_speed3"][0] = gear_max_speed3
#	globals.current_vehicle_settings["gear_max_speed4"][0] = gear_max_speed4
#	globals.current_vehicle_settings["gear_speed_factor"][0] = gear_speed_factor
#	globals.current_vehicle_settings["gear_engine_factor"][0] = gear_engine_factor


func _on_boost_timer_timeout():
	engine_force_boost = 1.0


func _on_VirtualSteer_steer(direction):
	touch_steer_direction = direction


func _on_VirtualSteer_steer_stop(touching):
	if not touching:
		touch_steer_direction = Vector2(0, 0)


func _on_VirtualMove_move(direction):
	touch_move_direction = direction


func _on_VirtualMove_move_stop(touching):
	if not touching:
		touch_move_direction = Vector2(0, 0)


func _on_VirtualBrake_brake(enabled):
	brake = enabled


func _on_Body_body_entered(body:PhysicsBody3D):
	if body.collision_layer == 0b1:
		# static body: environment
		pass
#		globals.play_sound("vehicle_collision_static", globals.current_vehicle)
	elif body.collision_layer == 0b1000:
		# (damaging) dynamic body: bullet
		bullet_hit(body.DAMAGE)
	elif body.collision_layer == 0b10000:
		# static body: turret
		body.bullet_hit()
		pass
#		globals.play_sound("vehicle_collision_static", globals.current_vehicle)
	elif body.collision_layer == 0b100000:
		# ai vehicle
		bullet_hit()


func _set_current_brake(value):
	brake = value
