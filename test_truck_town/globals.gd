extends Node


var GAME_DATA:Dictionary = {
	"MAIN_MENU_PATH": "res://gui/main_menu.tscn",
	"RESTART_RESUME_POPUP": "res://gui/restart_resume_popup.tscn",
	"GAME_OVER_SCENE": "res://gui/game_over.tscn",
	"GAME_OVER_POPUP": "res://gui/game_over_popup.tscn",
	"WANT_QUIT_POPUP": "res://test_truck_town/gui/want_quit_popup.tscn",
	"SIMPLE_AUDIO_PLAYER_SCENE": "res://audio/simple_audio_player.tscn",
	"progress_animation_res": "res://gui/progress_animation.tscn",
	"SCREEN_HIGH_RES": Vector2(1280, 720),
	"SCREEN_LOW_RES": Vector2(640, 360),
	# levels related
	"LEVELS": [
		{
			"path": "res://levels/level_debug.tscn", # DEBUG
			"total_count_down": 501,
			"score_end_bonus": 100,
		},
		{
			"path": "res://levels/level_city1_scene.tscn",
			"total_count_down": 401,
			"score_end_bonus": 100,
		},
		{
			"path": "res://levels/level_city2_scene.tscn",
			"total_count_down": 501,
			"score_end_bonus": 100,
		},
		{
			"path": "res://levels/level_city3_scene.tscn",
			"total_count_down": 501,
			"score_end_bonus": 100,
		},
		{
			"path": "res://levels/level_city4_scene.tscn",
			"total_count_down": 301,
			"score_end_bonus": 100,
		},
	],
	# control types specified with the following syntax (from left to right):
	# inputs: FW=forward, BW=backward, BR=brake, ST=steer left/right
	# controls: VA=virtual analog, VB=virtual button, AC=accelerometer
	"control_types": {
		"FW_VB-BW_VB-BR_VB-ST_VA": ["res://vehicles/controls/combinations/fw_vb-bw_vb-br_vb-st_va.tscn",
				"Forward/Backward/Brake: button | Steer: joystick", 
				"res://assets/Images/icon_FW_VB-BW_VB-BR_VB-ST_VA.png",
				"res://assets/Images/icon_FW_VB-BW_VB-BR_VB-ST_VA_off.png",
				"res://assets/Images/FW_VB-BW_VB-BR_VB-ST_VA.png",],
		"FW_VB-BW_VB-BR_VB-ST_AC": ["res://vehicles/controls/combinations/fw_vb-bw_vb-br_vb-st_ac.tscn",
				"Forward/Backward/Brake: button | Steer: accelerometer",
				"res://assets/Images/icon_FW_VB-BW_VB-BR_VB-ST_AC.png",
				"res://assets/Images/icon_FW_VB-BW_VB-BR_VB-ST_AC_off.png",
				"res://assets/Images/FW_VB-BW_VB-BR_VB-ST_AC.png",],
		"FW_VA-BW_VA-BR_VB-ST_VA": ["res://vehicles/controls/combinations/fw_va-bw_va-br_vb-st_va.tscn",
				"Forward/Backward: joystick | Brake: button | Steer: joystick",
				"res://assets/Images/icon_FW_VA-BW_VA-BR_VB-ST_VA.png",
				"res://assets/Images/icon_FW_VA-BW_VA-BR_VB-ST_VA_off.png",
				"res://assets/Images/FW_VA-BW_VA-BR_VB-ST_VA.png",],
		"FW_VA-BW_VA-BR_VB-ST_AC": ["res://vehicles/controls/combinations/fw_va-bw_va-br_vb-st_ac.tscn",
				"Forward/Backward: joystick | Brake: button | Steer: accelerometer",
				"res://assets/Images/icon_FW_VA-BW_VA-BR_VB-ST_AC.png",
				"res://assets/Images/icon_FW_VA-BW_VA-BR_VB-ST_AC_off.png",
				"res://assets/Images/FW_VA-BW_VA-BR_VB-ST_AC.png",],
	},
	# audio related
	"audio_clips": {
		"vehicle_collision_static": "res://assets/Sounds/qubodup-crash.ogg",
		"vehicle_collision_dynamic": "res://assets/Sounds/cast iron clangs - Marker #10.ogg",
		"truck_engine_1": "res://assets/Sounds/435656__broken-head-productions__ford-ltl-9000-engine-start-and-idle.ogg",
		"truck_engine_2": "res://assets/Sounds/160442__henrique85n__start-loop-engine.ogg",
		"cannon_shot": "res://assets/Sounds/cannon_02.ogg",
		"mine_explosion": "res://assets/Sounds/explosion.ogg",
		"car_engine_loop": "res://assets/Sounds/qubodup-car-engine-loop.ogg",
		"gun_shot": "res://assets/Sounds/bang_06.ogg",
	},
	"audio_clips_loop_start": {
		"truck_engine_1": 5.0,
		"truck_engine_2": 2.5,
	},
	# vehicles related
	"vehicles": [
		{
			"name": "mini_van_test", # DEBUG
			"path": "res://vehicles/car_base_test.tscn",
			"sound": "truck_engine_2",
			"image": "res://assets/Images/choose_van_test.png",
			"icon": "res://assets/Images/icon_choose_van_test.png",
			"icon_off": "res://assets/Images/icon_choose_van_off_test.png",
		},
		{
			"name": "mini_van",
			"path": "res://vehicles/car_base.tscn",
			"sound": "truck_engine_2",
			"image": "res://assets/Images/choose_van.png",
			"icon": "res://assets/Images/icon_choose_van.png",
			"icon_off": "res://assets/Images/icon_choose_van_off.png",
		},
		{
			"name": "trailer_truck2",
			"path": "res://vehicles/trailer_truck2.tscn",
			"sound": "truck_engine_1",
			"image": "res://assets/Images/choose_trailer2.png",
			"icon": "res://assets/Images/icon_choose_trailer2.png",
			"icon_off": "res://assets/Images/icon_choose_trailer2_off.png",
		},
		{
			"name": "tow_truck_impounded",
			"path": "res://vehicles/tow_truck.tscn",
			"sound": "truck_engine_2",
			"image": "res://assets/Images/choose_tow.png",
			"icon": "res://assets/Images/icon_choose_tow.png",
			"icon_off": "res://assets/Images/icon_choose_tow_off.png",
		},
		{
			"name": "trailer_truck",
			"path": "res://vehicles/trailer_truck.tscn",
			"sound": "truck_engine_1",
			"image": "res://assets/Images/choose_trailer.png",
			"icon": "res://assets/Images/icon_choose_trailer.png",
			"icon_off": "res://assets/Images/icon_choose_trailer_off.png",
		},
	],
	"vehicle_settings": {
		# name: [value, gui_min_value, gui_max_value, gui_step, gui_enabled]
		"max_forward_speed": [10.0, 1.0, 100.0, 1, true],
		"steer_speed": [1.5, 0.5, 5.0, 0.1, true],
		"steer_limit": [0.4, 0.0, 1.0, 0.05, true],
		"tilt_angle_max": [25, 10.0, 60.0, 1.0, true],
		"tilt_engine_force_factor": [1.0, 1.0, 3.0, 0.1, true],
		"accel_steer_speed": [1.0, 0.5, 2.0, 0.05, true],
		"engine_force_value": [40, 20.0, 120.0, 1.0, true],
		"lift_force": [Vector3(0.0, 100.0, 0.0), 1.0, 1000.0, 1.0, false],
		"lift_force_position": [Vector3(), 0.0, 0.0, 0.0, false],
		"gear_max_speed1": [3.0, 1.0, 20.0, 1.0, false],
		"gear_max_speed2": [7.0, 1.0, 40.0, 1.0, false],
		"gear_max_speed3": [12.0, 1.0, 60.0, 1.0, false],
		"gear_max_speed4": [18.0, 1.0, 80.0, 0.0, false],
		"gear_speed_factor": [0.2, 0.1, 1.0, 0.05, false],
		"gear_engine_factor": [0.008, 0.001, 0.1, 0.001, false],
	},
	# score/vehicle_number related
	"starting_score": 0,
	"starting_vehicle_number": 3,
}

# configuration options
var configuration_options:Dictionary = {
	"vsync": true,
	"full_screen": false,
	"debug_display": false,
	"low_resolution": false,
}
# current options
var current_control_type:Resource = null
var current_control_type_name:String = GAME_DATA["control_types"].keys()[0]

# GUI/UI-related variables
var canvas_layer:CanvasLayer = null

var MAIN_MENU_PATH:String = ""
var GAME_OVER_SCENE:String = ""
var GAME_OVER_POPUP:Resource = null
var RESTART_RESUME_POPUP:Resource = null
var popup:Control = null
var WANT_QUIT_POPUP:Resource = null
var want_quit_popup:Control

#const DEBUG_DISPLAY_SCENE:Resource = preload("res://gui/debug_display.tscn")
var debug_display:Control = null

# Levels-related variables
var LEVELS:Array = []
var level_debug_idx:int = 0
var current_level_idx: int = 0
var num_levels:int = 0
var current_level_path:String = ""
var current_vehicle_path:String = ""
var current_vehicle:Node = null: get = _get_current_vehicle, set = _set_current_vehicle
var current_vehicle_body:VehicleBody3D = null
var current_vehicle_sound:String = ""
var current_vehicle_sound_loop_start:float = 0.0
var current_vehicle_settings:Dictionary = {}

const LEVELS_base_time:Array = [60.0, 60.0,]

# Audio-related variables
# All the audio files. You will need to provide your own sound files
var audio_clips:Dictionary = {}
var audio_clips_loop_start:Dictionary = {}

var SIMPLE_AUDIO_PLAYER_SCENE:Resource = null
var created_audios:Dictionary = {}

var all_sounds_muted:bool = false

# Score/Vehicle number-related variables
var score:int: get = _get_score, set = _set_score
var vehicle_number:int: get = _get_vehicle_number, set = _set_vehicle_number
var is_game_over:bool

# ResourceInteractiveLoader-related variables
var loader:ResourceLoader = null
var wait_frames:int
var time_max:int = 1 # msec
var new_scene_path:String
var progress_animation_res:Resource = null
var progress_animation_name:String = "progress"
var progress_animation_lenght:float = 0.0
var progress_animation:AnimationPlayer = null


func _ready():
	# initialize starting game data
	_setup_game_data()
	_reset_game_state()
	# override the default quit request behavior 
	get_tree().set_auto_accept_quit(false)
	# initialize random and GUI system
	randomize()
	canvas_layer = CanvasLayer.new()
	add_child(canvas_layer)


func _notification(what):
	if (what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST):
		_show_want_quit_popup()


func _process(_delta):
	if Input.is_action_just_pressed("ui_cancel"):
		_show_restart_resume_popup()
	if loader != null:
		# Wait for frames to let the "loading" animation show up.
		if wait_frames > 0:
			wait_frames -= 1
			return
		var t = Time.get_ticks_msec()
		# Use "time_max" to control for how long we block this thread.
		while Time.get_ticks_msec() < t + time_max:
			# Poll your loader.
			var err = loader.poll()
			var progress = float(loader.get_stage()) / loader.get_stage_count()
			if err == ERR_FILE_EOF: # Finished loading.
				var resource = loader.get_resource()
				loader = null
				update_progress(1.0)
				await get_tree().create_timer(1.0).timeout
				if progress_animation:
					progress_animation.stop()
					progress_animation.queue_free()
				set_new_scene(resource)
				setup_new_scene(new_scene_path)
				break
			elif err == OK:
				update_progress(progress)
			else: # Error during loading.
				print("ERROR: Error during loading")
				loader = null
				break


func load_new_scene(scene_path):
	# clear all sounds anyway
	clear_all_sounds()
	loader = ResourceLoader.load_threaded_request(scene_path)
	new_scene_path = scene_path
	if loader == null: # Check for errors.
		print("ERROR: Game requests to switch to this scene")
		return
	# Get rid of all the old scene.
	if current_vehicle:
		current_vehicle.queue_free()
		_set_current_vehicle(null)
	get_current_scene().queue_free()
	
	# Start your "loading..." animation.
	if progress_animation_res:
		progress_animation = progress_animation_res.instantiate()
		add_child(progress_animation)
		progress_animation.play(progress_animation_name)
		progress_animation_lenght = progress_animation.current_animation_length
	
	wait_frames = 1


func setup_new_scene(scene_path):
	if  scene_path == current_level_path:
		# wait for a frame for the scene to actually change 
		var current_level = get_current_scene()
		if not current_vehicle:
			_set_current_vehicle(load(current_vehicle_path).instantiate())
		current_vehicle.set_name("current_vehicle")
		current_level.get_node("InstancePos").add_child(current_vehicle)
		# set current control type
		current_control_type = load(GAME_DATA["control_types"][current_control_type_name][0])
		var current_control = current_control_type.instantiate()
		current_vehicle.add_child(current_control)
		_initialize_hud(current_level)
		# set current vehicle settings
		current_vehicle.get_node("Body").set_current_settings()
		# disable physics temporarily for current vehicle
		current_vehicle.get_node("Body").set_physics_process(false)
		# handle score/vehicle number
		current_level.get_node("HUD/MarginContainer/VBoxContainer/HBoxContainer2/ScoreInfos/Score").text = "Score: " + str(score)
		var available_vehicles = max(GAME_DATA["starting_vehicle_number"], vehicle_number)
		var vehicle_id = str(available_vehicles - vehicle_number + 1) + "/" + str(available_vehicles)
		current_level.get_node("HUD/MarginContainer/VBoxContainer/HBoxContainer2/ScoreInfos/VehicleNumber").text = "Vehicle: " + vehicle_id


func update_level(vehicle_body, go_new_level=true):
	# disable physics temporarily for current vehicle
	vehicle_body.lift_up = true
	# remove vehicle sounds
	clear_sound(null, vehicle_body)
	if vehicle_body.get("current_sound"):
		vehicle_body.current_sound = null
	
	if go_new_level:
		# update score with end bonus
		_set_score(score + LEVELS[current_level_idx]["score_end_bonus"])
		_set_score(score + get_current_scene().current_count)
	
	# play fade animation if any
	var fade_animation:AnimationPlayer = get_current_scene().get_node("FadeAnimation")
	if fade_animation:
		fade_animation.play("fade")
		await fade_animation.animation_finished
	
	# post-fade animation
	if go_new_level:
		# increase level index (if possible)
		current_level_idx += 1
		if (current_level_idx >= len(LEVELS)):
			# no more levels: game over
			load_new_scene(GAME_OVER_SCENE)
		else:
			_set_vehicle_number(vehicle_number + 1)
			current_level_path = LEVELS[current_level_idx]["path"]
			load_new_scene(current_level_path)
	else:
		# decrease vehicle number
		_set_vehicle_number(vehicle_number - 1)
		# stay on the same level (if possible)
		if not is_game_over:
			# restart the same level
			load_new_scene(current_level_path)
		else:
			# no more vehicles: game over
			load_new_scene(GAME_OVER_SCENE)


func play_sound(sound_name, this_vehicle = null, from_begin = true,
		loop_sound=false, sound_position=null):
	if audio_clips.has(sound_name):
		var new_audio = SIMPLE_AUDIO_PLAYER_SCENE.instantiate()
		new_audio.should_loop = loop_sound
		new_audio.sound_name = sound_name
		
		if this_vehicle != null:
			this_vehicle.get_node("Body").add_child(new_audio)
		else:
			add_child(new_audio)
		created_audios[new_audio] = sound_name
		
		if this_vehicle and (audio_clips_loop_start.has(sound_name)) and (!from_begin):
			new_audio.start_loop_position = audio_clips_loop_start[sound_name]
		else:
			new_audio.start_loop_position = 0.0
		new_audio.play_sound(audio_clips[sound_name], sound_position)
		
	else:
		print ("ERROR: cannot play sound that does not exist in audio_clips!")


func get_sound(sound_name, parent = null):
	if (sound_name == null) and parent:
		# return all sounds related to parent
		var audios_of_parent = []
		for audio in created_audios:
			if parent.is_ancestor_of(audio):
				audios_of_parent.append(audio)
		return audios_of_parent
	
	var audios_with_sound_name = []
	for audio in created_audios:
		if created_audios[audio] == sound_name:
			audios_with_sound_name.append(audio)
	if not audios_with_sound_name.is_empty():
		if parent != null:
			var audio_descendant = null
			for audio in audios_with_sound_name:
				if parent.is_ancestor_of(audio):
					audio_descendant = audio
					break
			return audio_descendant if audio_descendant != null else null
		else:
			return audios_with_sound_name
	else:
		return null


func clear_sound(sound_name, parent = null):
	if (sound_name == null) and parent:
		# remove all sounds related to parent
		for audio in created_audios:
			if parent.is_ancestor_of(audio):
				audio.clear_sound()
		return
	
	var audios_with_sound_name = []
	for audio in created_audios:
		if created_audios[audio] == sound_name:
			audios_with_sound_name.append(audio)
	if not audios_with_sound_name.is_empty():
		if parent != null:
			for audio in audios_with_sound_name:
				if parent.is_ancestor_of(audio):
					audio.clear_sound()
					break
		else:
			for audio in audios_with_sound_name:
				audio.clear_sound()


func clear_all_sounds():
	for audio in created_audios.keys():
		audio.clear_sound()


func enable_all_sounds(enable):
	AudioServer.set_bus_mute(0, not enable)


func saved_game_good():
	var saved_game = File.new()
	var res = saved_game.open("user://truck-town.save", File.READ)
	saved_game.close()
	return res == OK


func save_game():
	var saved_game = File.new()
	saved_game.open("user://truck-town.save", File.WRITE)
	var save_dict = {
		"current_level_idx": current_level_idx,
		"current_level_path": current_level_path,
		"current_vehicle_path": current_vehicle_path,
		"current_vehicle_sound": current_vehicle_sound,
		"current_control_type_name": current_control_type_name,
		"score": score,
		"vehicle_number": vehicle_number,
	}
	_save_vehicle_settings(save_dict)
	saved_game.store_line(JSON.new().stringify(save_dict))
	saved_game.close()


func load_game():
	var saved_game = File.new()
	if saved_game.open("user://truck-town.save", File.READ) != OK:
		return false
	var test_json_conv = JSON.new()
	test_json_conv.parse(saved_game.get_line())
	var saved_dict = test_json_conv.get_data()
	var res = true
	current_level_idx = saved_dict.get("current_level_idx", -1)
	res = res and (current_level_idx != -1)
	current_level_path = saved_dict.get("current_level_path", "")
	res = res and (current_level_path != "")
	current_vehicle_path = saved_dict.get("current_vehicle_path", "")
	res = res and (current_vehicle_path != "")
	current_vehicle_sound = saved_dict.get("current_vehicle_sound", "")
	res = res and (current_vehicle_sound != "")
	current_control_type_name = saved_dict.get("current_control_type_name", "")
	res = res and (current_control_type_name != "")
	_set_score(saved_dict.get("score", -1))
	res = res and (score != -1)
	_set_vehicle_number(saved_dict.get("vehicle_number", -1))
	res = res and (vehicle_number != -1)
	res = _load_vehicle_settings(saved_dict)
	saved_game.close()
	return res


func update_progress(progress):
	# Update your progress bar?
#	get_node("progress").set_progress(progress)
	# ...or update a progress animation?
	if progress_animation:
		# Call this on a paused animation. Use "true" as the second argument to
		# force the animation to update.
		# forward
		progress_animation.seek(progress * progress_animation_lenght, true)
		# backward
#		progress_animation.seek(progress_animation_lenght * (1.0 - progress), true)
	else:
		print(progress)
	var score_str = "%5d" % int(score * progress)
	progress_animation.get_node("MarginContainer/VBoxContainer/Label").text = "Score: " + score_str


func set_new_scene(scene_resource):
	var current_scene = scene_resource.instantiate()
	get_node("/root").add_child(current_scene)


func get_current_scene():
	var root = get_tree().get_root()
	return root.get_child(root.get_child_count() - 1)


func set_debug_display(display_on):
	if display_on == false:
		if debug_display != null:
			debug_display.queue_free()
			debug_display = null
	else:
		if debug_display == null:
#			debug_display = DEBUG_DISPLAY_SCENE.instance()
			canvas_layer.add_child(debug_display)


func end_game():
	# end the game
	if (not current_level_path.is_empty()) and (not current_vehicle_path.is_empty()):
		save_game()
	get_tree().quit()


func _initialize_hud(current_level):
	var container = current_level.get_node("HUD/MarginContainer/VBoxContainer")
	container.get_node("HBoxContainer/Quit").connect("pressed", Callable(self, "_show_want_quit_popup"))
	container.get_node("HBoxContainer/Restart").connect("pressed", Callable(self, "_show_restart_resume_popup"))
	container.get_node("HBoxContainer3/UnlockVehicle").connect("pressed", Callable(self, "_unlock_vehicle"))
	container.get_node("HBoxContainer3/ToggleCamera").connect("pressed", Callable(self, "_toggle_camera"))
	var mute_sound = container.get_node("HBoxContainer3/MarginContainer/MuteSounds")
	var unmute_sound = container.get_node("HBoxContainer3/MarginContainer/UnMuteSounds")
	mute_sound.connect("pressed", Callable(self, "_enable_mute_all_sounds").bind(mute_sound, unmute_sound, false))
	unmute_sound.connect("pressed", Callable(self, "_enable_mute_all_sounds").bind(mute_sound, unmute_sound, true))
	mute_sound.visible = !all_sounds_muted
	unmute_sound.visible = all_sounds_muted


func _setup_game_data():
	MAIN_MENU_PATH = GAME_DATA["MAIN_MENU_PATH"]
	#
	GAME_OVER_SCENE = GAME_DATA["GAME_OVER_SCENE"]
	#
	GAME_OVER_POPUP = load(GAME_DATA["GAME_OVER_POPUP"])
	#
	RESTART_RESUME_POPUP = load(GAME_DATA["RESTART_RESUME_POPUP"])
	#
	WANT_QUIT_POPUP = load(GAME_DATA["WANT_QUIT_POPUP"])
	#
	for item in GAME_DATA["LEVELS"]:
		LEVELS.append(item)
	#
	for key in GAME_DATA["audio_clips"]:
		audio_clips[key] = load(GAME_DATA["audio_clips"][key])
	#
	for key in GAME_DATA["audio_clips_loop_start"]:
		audio_clips_loop_start[key] = GAME_DATA["audio_clips_loop_start"][key]
	#
	SIMPLE_AUDIO_PLAYER_SCENE = load(GAME_DATA["SIMPLE_AUDIO_PLAYER_SCENE"])
	#
	progress_animation_res = load(GAME_DATA["progress_animation_res"])
	# 
	level_debug_idx = len(LEVELS) - 1
	num_levels = len(LEVELS)


func _reset_game_state():
	if weakref(current_vehicle).get_ref():
		current_vehicle.queue_free()
		_set_current_vehicle(null)
	# reset current level idx
	current_level_idx = 0
	# get current level path
	current_level_path = LEVELS[current_level_idx]["path"]
	# vehicle settings
	var settings = GAME_DATA["vehicle_settings"]
	for key in settings:
		current_vehicle_settings[key] = settings[key]
	# reset score/vehicle number
	_set_score(GAME_DATA["starting_score"])
	_set_vehicle_number(GAME_DATA["starting_vehicle_number"])
	is_game_over = false


func _enable_mute_all_sounds(mute_sound, unmute_sound, enable):
	enable_all_sounds(enable)
	all_sounds_muted = !enable
	mute_sound.visible = enable
	unmute_sound.visible = not enable


func _show_restart_resume_popup():
	if popup == null:
		popup = RESTART_RESUME_POPUP.instantiate()
		
		popup.get_node("VBoxContainer/HBoxContainer/RestartLevel").connect("pressed", Callable(self, "_popup_restart_level"))
		popup.get_node("VBoxContainer/HBoxContainer/RestartSession").connect("pressed", Callable(self, "_popup_back_to_main"))
		popup.connect("popup_hide", Callable(self, "_popup_closed"))
		popup.get_node("VBoxContainer/HBoxContainer/ButtonResume").connect("pressed", Callable(self, "_popup_closed"))
		
		canvas_layer.add_child(popup)
		popup.popup_centered()
		
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		get_tree().paused = true


func _show_game_over_popup():
	if popup == null:
		popup = GAME_OVER_POPUP.instantiate()
		
		popup.get_node("VBoxContainer/ButtonBackToMain").connect("pressed", Callable(self, "_popup_back_to_main"))
		popup.connect("popup_hide", Callable(self, "_popup_closed"))
		
		canvas_layer.add_child(popup)
		popup.popup_centered()
		
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		get_tree().paused = true


func _show_want_quit_popup():
	if want_quit_popup == null:
		want_quit_popup = WANT_QUIT_POPUP.instantiate()
		
		want_quit_popup.get_node("VBoxContainer/HBoxContainer/ButtonQuit").connect("pressed", Callable(self, "_popup_want_quit"))
		want_quit_popup.connect("popup_hide", Callable(self, "_popup_want_quit_closed"))
		want_quit_popup.get_node("VBoxContainer/HBoxContainer/ButtonResume").connect("pressed", Callable(self, "_popup_want_quit_closed"))
		
		canvas_layer.add_child(want_quit_popup)
		want_quit_popup.popup_centered()
		
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		get_tree().paused = true


func _popup_closed():
	get_tree().paused = false
	
	if popup != null:
		popup.queue_free()
		popup = null


func _popup_restart_level():
	get_tree().paused = false
	
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	if popup != null:
		popup.queue_free()
		popup = null

	get_current_scene().current_count = 0

func _popup_back_to_main():
	get_tree().paused = false
	
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	if popup != null:
		popup.queue_free()
		popup = null
	
	_reset_game_state()
	load_new_scene(MAIN_MENU_PATH)


func _popup_want_quit_closed():
	get_tree().paused = false
	
	if want_quit_popup != null:
		want_quit_popup.queue_free()
		want_quit_popup = null


func _popup_want_quit():
	get_tree().paused = false
	
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	if want_quit_popup != null:
		want_quit_popup.queue_free()
		want_quit_popup = null
	
	# end the game
	end_game()


func _unlock_vehicle():
	if current_vehicle:
		current_vehicle_body.apply_blast(current_vehicle_body.mass * 
				randf_range(5.0, 10.0) )


func _toggle_camera():
	if current_vehicle_body:
		current_vehicle_body.get_node("CameraGroup").toggle_camera()


func _set_score(value):
	score = value


func _get_score():
	return score


func _set_vehicle_number(value):
	if value <= 0:
		is_game_over = true
	else:
		vehicle_number = value
		is_game_over = false


func _get_vehicle_number():
	return vehicle_number


func _set_current_vehicle(value):
	current_vehicle = value
	if current_vehicle:
		current_vehicle_body = current_vehicle.get_node("Body")
	else:
		current_vehicle_body = null


func _get_current_vehicle():
	return current_vehicle


func _save_vehicle_settings(save_dict):
	for key in current_vehicle_settings:
		var vs_key = "VS_" + key
		save_dict[vs_key] = current_vehicle_settings[key][0] 


func _load_vehicle_settings(saved_dict):
	var res = true
	for key in current_vehicle_settings:
		var vs_key = "VS_" + key
		var value = saved_dict.get(vs_key, "VS_ERROR")
		current_vehicle_settings[key][0] = value
		res = res and (str(value) != "VS_ERROR")
	return res
