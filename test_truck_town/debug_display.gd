extends Control


func _ready():
	$OSLabel.text = "OS: " + OS.get_name()
	$EngineLabel.text = "Godot version: " + Engine.get_version_info()["string"]
	DisplayServer.window_set_vsync_mode(DisplayServer.VSYNC_ENABLED if (false) else DisplayServer.VSYNC_DISABLED)


func _process(_delta):
	$FPSLabel.text = "FPS: " + str(Engine.get_frames_per_second())
