extends CharacterBody2D


@export var speed: int = 200

var target = Vector2()
var velocity = Vector2()


func _physics_process(delta):
	velocity = position.direction_to(target) * speed
	# x axis is the character forward direction (default)
	look_at(target)
	if position.distance_to(target) > 5:
		set_velocity(velocity)
		move_and_slide()
		velocity = velocity


func _input(event):
	if event.is_action_pressed("click"):
		target = get_global_mouse_position()
