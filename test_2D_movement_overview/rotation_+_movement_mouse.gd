extends CharacterBody2D


@export var speed: int = 200

var velocity = Vector2()


func _physics_process(delta):
	get_input()
	set_velocity(velocity)
	move_and_slide()
	velocity = velocity


func get_input():
#	look_at(get_global_mouse_position())
	rotation = get_global_mouse_position().angle_to_point(position)
	#
	velocity = Vector2()
	# x axis is the character forward direction (default)
	if Input.is_action_pressed("down"):
		velocity = Vector2(-speed, 0).rotated(rotation)
	if Input.is_action_pressed("up"):
		velocity = Vector2(speed, 0).rotated(rotation)
	# -y axis is the character forward direction
#	rotation += deg2rad(90)
#	if Input.is_action_pressed("down"):
#		velocity = Vector2(0, speed).rotated(rotation)
#	if Input.is_action_pressed("up"):
#		velocity = Vector2(0, -speed).rotated(rotation)
