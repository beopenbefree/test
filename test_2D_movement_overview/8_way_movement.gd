extends CharacterBody2D


@export var speed: int = 200

var velocity = Vector2()


func _physics_process(delta):
	get_input()
	set_velocity(velocity)
	move_and_slide()
	velocity = velocity


func get_input():
	velocity = Vector2()
	if Input.is_action_pressed("right"):
		velocity.x += 1
	if Input.is_action_pressed("left"):
		velocity.x -= 1
	if Input.is_action_pressed("down"):
		velocity.y += 1
	if Input.is_action_pressed("up"):
		velocity.y -= 1
	velocity = velocity.normalized() * speed
