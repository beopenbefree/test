extends CharacterBody2D


@export var speed: int = 200
@export var rotation_speed: float = 1.5

var velocity = Vector2()
var rotation_dir = 0


func _physics_process(delta):
	get_input()
	rotation += rotation_dir * rotation_speed * delta
	set_velocity(velocity)
	move_and_slide()
	velocity = velocity


func get_input():
	rotation_dir = 0
	velocity = Vector2()
	if Input.is_action_pressed("right"):
		rotation_dir +=1
	if Input.is_action_pressed("left"):
		rotation_dir -=1
	# x axis is the character forward direction (default)
	if Input.is_action_pressed("down"):
		velocity = Vector2(-speed, 0).rotated(rotation)
	if Input.is_action_pressed("up"):
		velocity = Vector2(speed, 0).rotated(rotation)
	# -y axis is the character forward direction
#	if Input.is_action_pressed("down"):
#		velocity = Vector2(0, speed).rotated(rotation)
#	if Input.is_action_pressed("up"):
#		velocity = Vector2(0, -speed).rotated(rotation)

