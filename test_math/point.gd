extends MeshInstance3D


var grabbed:bool = false
var current_camera:Camera3D = null
var z_depth: float = 0.0
var new_pos:Vector3 = Vector3.ZERO
var move_callback:FuncRef = null

var moving_flag = null


func _ready():
	$StaticBody3D.connect("input_event", Callable(self, "_on_input_event"))


func _physics_process(delta):
	if grabbed:
		global_transform.origin = new_pos
		if move_callback:
			move_callback.call_func(new_pos)


func _input(event):
	if grabbed:
		if event is InputEventMouseMotion:
			new_pos = current_camera.project_position(event.position, 
					z_depth)
		if ((event is InputEventMouseButton) and 
				(event.button_index == MOUSE_BUTTON_LEFT)):
			if not event.pressed:
				grabbed = false
				current_camera = null


func _on_input_event(camera:Camera3D, event, position, normal, shape_idx):
	if ((event is InputEventMouseButton) and 
			(event.button_index == MOUSE_BUTTON_LEFT)):
		if event.pressed:
			grabbed = true
			current_camera = camera
			z_depth = (position - camera.global_transform.origin).length()
			new_pos = global_transform.origin
