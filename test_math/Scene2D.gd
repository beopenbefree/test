extends Sprite2D


const EPSILON = 1.0e-6


# Tests on trasform2D
func _ready():
	# 1
#	position = Vector2(350, 150)
#	rotation = -0.5
#	scale = Vector2(3.0, 3.0)
	# 2
#	var t = Transform2D()
#	t.origin = Vector2(350, 150)
#	var rot = -0.5
#	t.x.x = cos(rot)
#	t.y.y = cos(rot)
#	t.x.y = sin(rot)
#	t.y.x = -sin(rot)
#	t.x *= 3
#	t.y *= 3
#	transform = t
	# 3 (shearing)
#	var t = Transform2D()
#	t.origin = Vector2(350, 150)
#	t.y = Vector2.ONE
#	transform = t
	# 4 (transforms' composition)
	var t1 = Transform2D(deg_to_rad(90), Vector2(1, 0))
	var t2 = Transform2D(deg_to_rad(90), Vector2(2, 1))
	var p = Vector2(1.0, 0.0)
	var p1 = (t2 * t1) * (p)
	print(abs(p1.x - 1.0) < EPSILON, ", ", abs(p1.y - 2.0) < EPSILON)
