#tool
extends Node3D


@export var bezier_points = [ # (Array, Vector3)
	Vector3(-5.0, 0.0, 0.0),
	Vector3(-3.0, 0.0, -5.0),
	Vector3(3.0, 0.0, -5.0),
	Vector3(5.0, 0.0, 0.0),
]
@export var num_curve_points: int = 100
@export var closed: bool = false
@export var iterative: bool = false

var bezier_mesh_points:Array = []
var curve:ImmediateMesh = null
var point_scene:Resource = preload("res://test_math/point.tscn")


func _ready():
	if bezier_points.size() < 3:
		push_error("Insert (at least) 3 (three) control points!")
	curve = ImmediateMesh.new()
	add_child(curve)
	# add points
	for pos in bezier_points:
		var p_instance = point_scene.instance()
		add_child(p_instance)
		bezier_mesh_points.append(p_instance)
		p_instance.global_transform.origin = pos


func _physics_process(delta):
	var curve_points = _create_curve_points(num_curve_points)
	_draw_curve(curve, curve_points)


func _create_curve_points(n:int) -> PackedVector3Array:
	var points_array = PackedVector3Array()
	var t = 0.0
	var dt = 1.0 / n
	var mesh_pos = []
	mesh_pos.resize(bezier_mesh_points.size())
	for i in range(mesh_pos.size()):
		mesh_pos[i] = bezier_mesh_points[i].global_transform.origin
	for i in range(n):
		var p
		if iterative:
			p = _bezier_iterative(mesh_pos, t)
		else:
			p = _bezier_recursive(mesh_pos, t)
		points_array.append(p)
		t += dt
	return points_array


func _bezier_recursive(p:Array, t: float) -> Vector3:
	# recursive version
	if p.size() > 2:
		var q = []
		q.resize(p.size() - 1)
		for i in range(q.size()):
			q[i] = p[i].lerp(p[i + 1], t)
		return _bezier_recursive(q, t)
	elif p.size() == 2:
		return p[0].lerp(p[1], t)
	else:
		return p[0]


func _bezier_iterative(p:Array, t: float) -> Vector3:
	# iterative version
	var r:Vector3
	while true:
		if p.size() >= 2:
			var q = []
			q.resize(p.size() - 1)
			for i in range(q.size()):
				q[i] = p[i].lerp(p[i + 1], t)
			p = q
		else:
			r = p[0]
			break
	return r


func _draw_curve(curve, bezier_points:PackedVector3Array):
	curve.clear()
	if closed:
		curve.begin(Mesh.PRIMITIVE_LINE_LOOP)
	else:
		curve.begin(Mesh.PRIMITIVE_LINE_STRIP)
	for p in bezier_points:
		curve.add_vertex(p)
	curve.end()
	return curve
