extends Node3D


var t = 0.0
const SPEED = 0.05


func _process(delta):
	t += delta * SPEED
	$MeshInstance3D.transform.origin = $Path3D.get_curve().interpolate_baked(
			t * $Path3D.curve.get_baked_length(), true)

