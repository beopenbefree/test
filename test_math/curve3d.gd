@tool
extends Path3D

@export var global: bool = true
@export var points: PackedVector3Array = [
	Vector3(-10, 0, 0),
	Vector3(-8, 0, -3),
	Vector3(8, 0, -3), 
	Vector3(10, 0, 0), 
]

var point_scene:Resource = preload("res://test_math/point.tscn")
var curve_length_inv:float = 0.0

@onready var mat1 := StandardMaterial3D.new()
@onready var mat2 := StandardMaterial3D.new()
@onready var mat3 := StandardMaterial3D.new()
@onready var curve3d := Curve3D.new()
@onready var sphere := SphereMesh.new()
@onready var extern_point:Node = point_scene.instantiate()
@onready var closest_point:Node = point_scene.instantiate()
@onready var label = Label3D.new()


func _ready():
	mat1.albedo_color = Color(1,0,0,1)
	mat2.albedo_color = Color(0,1,0,1)
	mat3.albedo_color = Color(0,0,1,1)
	#
	sphere.radial_segments = 8
	sphere.rings = 4
	#
	curve = curve3d
	#
	extern_point.move_callback = funcref(self, "_move_callback")
	add_child(extern_point)
	extern_point.global_transform.origin = points[0]
	#
	closest_point.material_override = mat3
	add_child(closest_point)
	closest_point.scale = Vector3.ONE * 0.8
	closest_point.global_transform.origin = points[0]
	closest_point.add_child(label)
	label.transform.origin = Vector3(0,2,0)
	label.scale = Vector3.ONE * 5
	label.text = "%.2f" % 0.0
	label.billboard = StandardMaterial3D.BILLBOARD_ENABLED
	#
	for p in points:
		curve.add_point(p)
	curve_length_inv = (1.0 / curve.get_baked_length())
	# draw points
	_draw(points, mat1, 0.1)
	# draw baked points
	_draw(curve.get_baked_points(), mat2, 0.025)


func _move_callback(global_point_pos):
		var to_point_pos = to_local(global_point_pos)
		var closest_point_pos = curve.get_closest_point(to_point_pos)
		var closest_offset = curve.get_closest_offset(to_point_pos)
		closest_point.global_transform.origin = to_global(closest_point_pos)
		label.text = "%.2f" % (closest_offset * curve_length_inv)


func _draw(items, mat, s):
	for p in items:
		var i = MeshInstance3D.new()
		i.mesh = sphere
		add_child(i)
		i.scale = Vector3.ONE * s
		i.material_overlay = mat
		if global:
			i.global_transform.origin = p
		else:
			i.transform.origin = p
