extends MeshInstance3D


@export var axis # (int, "RIGHT", "UP", "FORWARD")
const SPEED = 4.0
const PERIOD = 2.0
var elapsed = 0.0
var gap = 0.0
var dir = 1
var start_pos
var curr_axis


func _ready():
	start_pos = transform.origin
	curr_axis = [Vector3.RIGHT, Vector3.UP, Vector3.FORWARD][axis]

func _process(delta):
	elapsed += delta
	if elapsed > PERIOD:
		elapsed = 0.0
		dir = -dir
	gap += dir * SPEED * delta
	transform = Transform3D().translated(start_pos + gap * curr_axis)
