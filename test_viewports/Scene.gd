extends Node3D


func _process(delta):
	# Retrieve the captured Image using get_data().
	var img = get_viewport().get_texture().get_data()
	# Flip on the Y axis.
	# You can also set "V Flip" to true if not on the root Viewport.
	img.flip_y()
	# Convert Image to ImageTexture.
	var tex = ImageTexture.new()
	tex.create_from_image(img)
	# Set Sprite Texture.
	$Sprite2D.texture = tex
