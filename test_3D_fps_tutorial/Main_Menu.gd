extends Control

# NOTE: The code here is only briefly explained in the tutorial, nor
#		does the code have very many comments!
#		This is because it does not really relate to making a FPS.

var start_menu
var level_select_menu
var options_menu

@export (String, FILE) var testing_area_scene
@export (String, FILE) var space_level_scene
@export (String, FILE) var ruins_level_scene


func _ready():
	start_menu = $Start_Menu
	level_select_menu = $Level_Select_Menu
	options_menu = $Options_Menu
	
	# Connect all of the start menu buttons
	$Start_Menu/Button_Start.connect("pressed", Callable(self, "start_menu_button_pressed").bind("start"))
	$Start_Menu/Button_Open_Godot.connect("pressed", Callable(self, "start_menu_button_pressed").bind("open_godot"))
	$Start_Menu/Button_Options.connect("pressed", Callable(self, "start_menu_button_pressed").bind("options"))
	$Start_Menu/Button_Quit.connect("pressed", Callable(self, "start_menu_button_pressed").bind("quit"))
	
	# Connect all of the level select menu buttons
	$Level_Select_Menu/Button_Back.connect("pressed", Callable(self, "level_select_menu_button_pressed").bind("back"))
	$Level_Select_Menu/Button_Level_Testing_Area.connect("pressed", Callable(self, "level_select_menu_button_pressed").bind("testing_scene"))
	$Level_Select_Menu/Button_Level_Space.connect("pressed", Callable(self, "level_select_menu_button_pressed").bind("space_level"))
	$Level_Select_Menu/Button_Level_Ruins.connect("pressed", Callable(self, "level_select_menu_button_pressed").bind("ruins_level"))
	
	# Connect all of the options menu buttons
	$Options_Menu/Button_Back.connect("pressed", Callable(self, "options_menu_button_pressed").bind("back"))
	$Options_Menu/Button_Fullscreen.connect("pressed", Callable(self, "options_menu_button_pressed").bind("fullscreen"))
	$Options_Menu/Check_Button_VSync.connect("pressed", Callable(self, "options_menu_button_pressed").bind("vsync"))
	$Options_Menu/Check_Button_Debug.connect("pressed", Callable(self, "options_menu_button_pressed").bind("debug"))
	
	# Some times when returning to the title screen the mouse is still captured even though it shouldn't be.
	# To prevent this from breaking the game, we just set it here too
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	# Get the globals singleton
	var globals = get_node("/root/Globals")
	# Get the mouse and joypad sensitivity
	$Options_Menu/HSlider_Mouse_Sensitivity.value = globals.mouse_sensitivity
	$Options_Menu/HSlider_Joypad_Sensitivity.value = globals.joypad_sensitivity


func start_menu_button_pressed(button_name):
	if button_name == "start":
		level_select_menu.visible = true
		start_menu.visible = false
	elif button_name == "open_godot":
		OS.shell_open("https://godotengine.org/")
	elif button_name == "options":
		options_menu.visible = true
		start_menu.visible = false
	elif button_name == "quit":
		get_tree().quit()


func level_select_menu_button_pressed(button_name):
	if button_name == "back":
		start_menu.visible = true
		level_select_menu.visible = false
	elif button_name == "testing_scene":
		set_mouse_and_joypad_sensitivity()
		get_node("/root/Globals").load_new_scene(testing_area_scene)
	elif button_name == "space_level":
		set_mouse_and_joypad_sensitivity()
		get_node("/root/Globals").load_new_scene(space_level_scene)
	elif button_name == "ruins_level":
		set_mouse_and_joypad_sensitivity()
		get_node("/root/Globals").load_new_scene(ruins_level_scene)


func options_menu_button_pressed(button_name):
	if button_name == "back":
		start_menu.visible = true
		options_menu.visible = false
	elif button_name == "fullscreen":
		get_window().mode = Window.MODE_EXCLUSIVE_FULLSCREEN if (!((get_window().mode == Window.MODE_EXCLUSIVE_FULLSCREEN) or (get_window().mode == Window.MODE_FULLSCREEN))) else Window.MODE_WINDOWED
	elif button_name == "vsync":
		DisplayServer.window_set_vsync_mode(DisplayServer.VSYNC_ENABLED if ($Options_Menu/Check_Button_VSync.pressed) else DisplayServer.VSYNC_DISABLED)
	elif button_name == "debug":
		get_node("/root/Globals").set_debug_display($Options_Menu/Check_Button_Debug.pressed)


func set_mouse_and_joypad_sensitivity():
	# Get the globals singleton
	var globals = get_node("/root/Globals")
	# Set the mouse and joypad sensitivity
	globals.mouse_sensitivity = $Options_Menu/HSlider_Mouse_Sensitivity.value
	globals.joypad_sensitivity = $Options_Menu/HSlider_Joypad_Sensitivity.value
