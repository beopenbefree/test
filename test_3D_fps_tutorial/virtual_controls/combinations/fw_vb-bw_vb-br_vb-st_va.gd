extends Node3D


var player:Node = null
var current_direction = "stop"
var current_fire = "stop"
const WEAPON_SHIFT = ["shift_weapon_positive", "shift_weapon_negative"]
var weapon_shift_idx = 0


func _ready():
	player = get_parent()
	set_signal_handling(self)


func set_signal_handling(player):
	# forward
	$FW_VB.connect("analog_touch_true", Callable(self, "_on_VirtualMove_move").bind("movement_forward"))
	$FW_VB.connect("analog_touch_false", Callable(self, "_on_VirtualMove_move").bind("stop"))
	# backward
	$BW_VB.connect("analog_touch_true", Callable(self, "_on_VirtualMove_move").bind("movement_backward"))
	$BW_VB.connect("analog_touch_false", Callable(self, "_on_VirtualMove_move").bind("stop"))
	# steer
	$ST_VA.connect("analog_move", Callable(self, "_on_VirtualSteer_steer"))
	$ST_VA.connect("analog_touch", Callable(self, "_on_VirtualSteer_steer_stop"))
	# fire
	$FR_VB.connect("analog_touch_true", Callable(self, "_on_VirtualBrake_fire").bind("fire"))
	$FR_VB.connect("analog_touch_false", Callable(self, "_on_VirtualBrake_fire").bind("stop"))
	# fire
	$WP_VB.connect("analog_touch_true", Callable(self, "_on_VirtualBrake_weapon").bind(WEAPON_SHIFT[weapon_shift_idx))
	$WP_VB.connect("analog_touch_false", Callable(self, "_on_VirtualBrake_weapon").bind("stop"))


func _on_VirtualSteer_steer(direction):
	player.touch_steer_direction = direction


func _on_VirtualSteer_steer_stop(touching):
	if not touching:
		player.touch_steer_direction = Vector2(0, 0)


func _on_VirtualMove_move(direction):
	var a = InputEventAction.new()
	if direction == "stop":
		a.action = current_direction
		a.button_pressed = false
	else:
		a.action = direction
		a.button_pressed = true
	current_direction = direction
	Input.parse_input_event(a)


func _on_VirtualBrake_fire(fire):
	var a = InputEventAction.new()
	if fire == "stop":
		a.action = current_fire
		a.button_pressed = false
	else:
		a.action = fire
		a.button_pressed = true
	current_fire = fire
	Input.parse_input_event(a)


func _on_VirtualBrake_weapon(weapon_shift):
	var a = InputEventAction.new()
	if weapon_shift == "stop":
		a.action = WEAPON_SHIFT[weapon_shift_idx]
		a.button_pressed = false
	else:
		var weapons_size = player.WEAPON_NUMBER_TO_NAME.size()
		var current_weapon_number = (player.WEAPON_NAME_TO_NUMBER[player.current_weapon_name])
		if ((current_weapon_number == weapons_size - 1) or
				(current_weapon_number == 0)):
					weapon_shift_idx = (weapon_shift_idx + 1) % 2
		a.action = WEAPON_SHIFT[weapon_shift_idx]
		a.button_pressed = true
	Input.parse_input_event(a)
