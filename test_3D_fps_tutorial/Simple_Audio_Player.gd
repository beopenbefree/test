extends Node3D


var audio_node = null
var should_loop = false
var globals = null


func _ready():
	audio_node = $Audio_Stream_Player
	audio_node.connect("finished", Callable(self, "sound_finished"))
	audio_node.stop()
	
	globals = get_node("/root/GlobalsFPS")


func play_sound(audio_stream, position=null):
	if audio_stream == null:
		print ("No audio stream passed; cannot play sound")
		globals.created_audio.remove(globals.created_audio.find(self))
		queue_free()
		return
		
	audio_node.stream = audio_stream
	
	# If you are using an AudioStreamPlayer3D, then uncomment these lines to set the position.
	#if audio_node is AudioStreamPlayer3D:
	if position != null:
		audio_node.global_transform.origin = position
	
	audio_node.play(0.0)


func sound_finished():
	if should_loop:
		audio_node.play(0.0)
	else:
		globals.created_audio.remove(globals.created_audio.find(self))
		audio_node.stop()
		queue_free()


## All of the audio files.
## You will need to provide your own sound files.
#var audio_pistol_shot = preload("res://test_3D_fps_tutorial/assets/gun_revolver_pistol_shot_04.wav")
#var audio_gun_cock = preload("res://test_3D_fps_tutorial/assets/gun_semi_auto_rifle_cock_02.wav")
#var audio_rifle_shot = preload("res://test_3D_fps_tutorial/assets/gun_rifle_sniper_shot_01.wav")
#
#var audio_node = null
#
#
#func _ready():
#	audio_node = $Audio_Stream_Player
#	audio_node.connect("finished", self, "destroy_self")
#	audio_node.stop()
#
#
#func play_sound(sound_name, position=null):
#	if audio_pistol_shot == null or audio_rifle_shot == null or audio_gun_cock == null:
#		print ("Audio not set!")
#		queue_free()
#		return
#
#	if sound_name == "Pistol_shot":
#		audio_node.stream = audio_pistol_shot
#	elif sound_name == "Rifle_shot":
#		audio_node.stream = audio_rifle_shot
#	elif sound_name == "Gun_cock":
#		audio_node.stream = audio_gun_cock
#	else:
#		print ("UNKNOWN STREAM")
#		queue_free()
#		return
#
#	# If you are using an AudioStreamPlayer3D, then uncomment these lines to set the position.
#	if audio_node is AudioStreamPlayer3D:
#		if position != null:
#			audio_node.global_transform.origin = position
#
#	audio_node.play()
#
#
#func destroy_self():
#	audio_node.stop()
#	queue_free()
