extends Node3D


const RADIUS = 16.0
var scene_switcher = null


func _ready():
	for child in $Meshes.get_children():
		child.transform.origin = get_random_pos()
	scene_switcher = $"/root/SceneSwitcherAutoload"


func get_random_pos():
	var x = randf_range(-RADIUS, RADIUS)
#	var y = rand_range(-RADIUS, RADIUS)
	var y = 0.0
	var z = randf_range(-RADIUS, RADIUS)
	return Vector3(x, y, z)


func _on_Button_pressed(scene):
	var scene_path = "res://test_io/" + scene + ".tscn"
	scene_switcher.goto_scene(scene_path)
