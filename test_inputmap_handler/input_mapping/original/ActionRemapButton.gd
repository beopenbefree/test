extends Button


@export var action: String = "ui_up"

var KeyPersistence = null


func _ready():
	assert(InputMap.has_action(action))
	set_process_unhandled_key_input(false)
	display_current_key()


func _toggled(button_pressed):
	set_process_unhandled_key_input(button_pressed)
	if button_pressed:
		text = "... Key"
		release_focus()
	else:
		display_current_key()


func _unhandled_key_input(event):
	# Note that you can use the _input callback instead, especially if
	# you want to work with gamepads.
	remap_action_to(event)
	pressed = false


func remap_action_to(event):
	# Check if event is already assigned.
	if _check_already_assigned(event):
		return
	# We first change the event in this game instance.
	InputMap.action_erase_events(action)
	InputMap.action_add_event(action, event)
	# And then save it to the keymaps file
	KeyPersistence.keymaps[action] = event
	KeyPersistence.save_keymap()
	text = "%s Key" % event.as_text()


func display_current_key():
	var current_key = InputMap.action_get_events(action)[0].as_text()
	text = "%s Key" % current_key


func _check_already_assigned(event):
	for ac in KeyPersistence.keymaps:
		var event_already_assigned = false
		var ev = KeyPersistence.keymaps[ac]
		if (ev is InputEventKey) and (event is InputEventKey):
			if (ev.keycode == event.keycode) and (ac != action):
				event_already_assigned = true
		elif (ev is InputEventMouseButton) and (event is InputEventMouseButton):
			if (ev.button_index == event.button_index) and (ac != action):
				event_already_assigned = true
		if event_already_assigned:
			_display_already_used(ev, ac)
			return true
	return false


func _display_already_used(ev, ac):
	print(OS.get_keycode_string(ev.keycode), " is already used by ", 
			str(ac))
