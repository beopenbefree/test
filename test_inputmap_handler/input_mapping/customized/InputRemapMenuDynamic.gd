# This is an autoload (singleton) which will save
# the key maps in a simple way through a dictionary.
extends Node


const keymaps_path = "user://keymaps.dat"
var keymaps: Dictionary

var action_list = [
	"movement_left",
	"movement_right",
]
var action_remap_row = preload("res://test_inputmap_handler/input_mapping/customized/ActionRemapRow.tscn")


func _ready() -> void:
	# First we create the keymap dictionary on startup with all
	# the keymap actions we have.
	for action in InputMap.get_actions():
		var keymap_list = InputMap.action_get_events(action)
		if keymap_list.size() > 0:
			keymaps[action] = keymap_list[0]
	load_keymap()
	# create the actions' rows
	for action in action_list:
		var row_node = action_remap_row.instantiate()
		row_node.name = action
		row_node.get_node("ActionName").text = action.replace("_", " ")
		var btn = row_node.get_node("ActionRemapButton")
		btn.action = action
		btn.KeyPersistence = self
		btn.AlertDialog = $AlertDialog
		$ActionsList.add_child(row_node)


func load_keymap() -> void:
	var file := File.new()
	if not file.file_exists(keymaps_path):
		save_keymap() # There is no save file yet, so let's create one.
		return
	#warning-ignore:return_value_discarded
	file.open(keymaps_path, File.READ)
	var temp_keymap: Dictionary = file.get_var(true)
	file.close()
	# We don't just replace the keymaps dictionary, because if you
	# updated your game and removed/added keymaps, the data of this
	# save file may have invalid actions. So we check one by one to
	# make sure that the keymap dictionary really has all current actions.
	for action in keymaps.keys():
		if temp_keymap.has(action):
			keymaps[action] = temp_keymap[action]
			# Whilst setting the keymap dictionary, we also set the
			# correct InputMap event
			InputMap.action_erase_events(action)
			InputMap.action_add_event(action, keymaps[action])


func save_keymap() -> void:
	# For saving the keymap, we just save the entire dictionary as a var.
	var file := File.new()
	#warning-ignore:return_value_discarded
	file.open(keymaps_path, File.WRITE)
	file.store_var(keymaps, true)
	file.close()
