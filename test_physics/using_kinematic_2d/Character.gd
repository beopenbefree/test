extends CharacterBody2D

var velocity = Vector2(100, 100)
var use_slide = true

func _physics_process(delta):
	if use_slide:
		set_velocity(velocity)
		move_and_slide()
		velocity = velocity
	else:
		var collision = move_and_collide(velocity * delta)
		if collision:
			velocity = velocity.slide(collision.normal)
