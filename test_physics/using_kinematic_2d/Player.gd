extends CharacterBody2D

var Bullet = preload("res://test_physics/using_kinematic_2d/Bullet.tscn")
var speed = 200
var velocity = Vector2()

func get_input():
	velocity = Vector2()
	if Input.is_action_pressed("movement_backward"):
		velocity = Vector2(-speed/3, 0).rotated(rotation)
	if Input.is_action_pressed("movement_forward"):
		velocity = Vector2(speed, 0).rotated(rotation)
	if Input.is_action_just_pressed("fire"):
		shoot()

func shoot():
	var b = Bullet.instantiate()
	b.start(Callable($Muzzle.global_position, rotation))
	get_parent().add_child(b)

func _physics_process(delta):
	get_input()
	var dir = get_global_mouse_position() - global_position
	if dir.length() > 5:
		rotation = dir.angle()
		set_velocity(velocity)
		move_and_slide()
		velocity = velocity
