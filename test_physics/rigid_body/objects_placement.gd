#tool
extends Node3D


@export var objects_number: int = 10

@export var object_scene: PackedScene = null

@export var container_node_path: NodePath = NodePath()
@export var container_current_scene: bool = false

@export var placement_space_center: Vector3 = Vector3.ZERO
@export var placement_space_size: Vector3 = Vector3(400, 200, 400)

@export var placement_objects_names: Array[String] = [] 

@export var delta_pos: Vector3 = Vector3.ZERO
@export_range(0, 1, 0.01) var max_delta_scale: float = 0.0

@export var save_scene: bool = false
@export_dir var save_scene_path:String = "" 
@export var save_scene_name: String = ""

@onready var placements_finished:bool = false


func _ready():
	var ray_cast = RayCast3D.new()
	add_child(ray_cast)
	var container_node
	if container_node_path.is_empty():
		container_node = self
	#elif container_current_scene:
		#container_node = globals.get_current_scene()
	else:
		container_node = get_node(container_node_path)
	var total_attempts = 0
	var object_count = 0
	if placement_objects_names.size() > 0:
		var space_half_size = placement_space_size / 2.0
		var space_center = placement_space_center
		while object_count < objects_number:
			total_attempts += 1
			var pos_x = ScriptLibrary.rand_float(space_half_size.x) + space_center.x
			var pos_y = space_half_size.y + space_center.y
			var pos_z = ScriptLibrary.rand_float(space_half_size.z) + space_center.z
			# cast ray vertically
			ray_cast.global_transform.origin = Vector3(pos_x, pos_y, pos_z)
			ray_cast.target_position = Vector3(0.0, -placement_space_size.y, 0.0)
			ray_cast.force_raycast_update()
			if ray_cast.is_colliding():
				var hit_object_name = ray_cast.get_collider().get_parent().name
				if hit_object_name in placement_objects_names:
					var object_node = object_scene.instantiate()
					container_node.add_child(object_node)
					var b = Basis.IDENTITY
					var scale_deviation = (1 + ScriptLibrary.rand_float(max_delta_scale))
					b = b.scaled(Vector3.ONE * scale_deviation)
					b = b.rotated(Vector3.UP, deg_to_rad(ScriptLibrary.rand_float(180)))
					var o = ray_cast.get_collision_point() + delta_pos
					if "center_height" in object_node:
						o.y += object_node.center_height
					# set object transform
					object_node.set_global_transform(Transform3D(b, o))
					object_count += 1
		#
		var inst_tmp = object_scene.instantiate()
		print(inst_tmp.name, ": placements/attempts = ", objects_number, "/", total_attempts, 
				" (", float(objects_number)/float(total_attempts), ")" )
		inst_tmp.free()
		ray_cast.queue_free()
		
		# save scene if requested
		if save_scene:
			await get_tree().create_timer(2.0).timeout
			# set children's owner (recursively): so they will get saved with the scene
			var all_childred_but_container = ScriptLibrary.get_all_children(container_node)
			all_childred_but_container.pop_front()
			for object_node in all_childred_but_container:
				object_node.set_owner(container_node)
			#
			var current_scene = container_node
			var packed_scene = PackedScene.new()
			packed_scene.pack(current_scene)
			var scene_name = save_scene_path + "/"
			if not save_scene_name.is_empty():
				scene_name += save_scene_name + "_saved.tscn"
			else:
				scene_name += current_scene.name + "_saved.tscn"
			ResourceSaver.save(scene_name, packed_scene)
			var saved_label = Label.new()
			saved_label.text = "SAVED!"
			add_child(saved_label)
	else:
		print("Set the 'placement_objects_names' option!")
	
	# set ready is finished
	placements_finished = true
