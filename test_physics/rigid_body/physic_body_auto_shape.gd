#tool
extends CollisionObject3D


@export var shape_type:ScriptLibrary.ShapeType = ScriptLibrary.ShapeType.SPHERE
@export var shape_scale:Vector3 = Vector3.ONE
# Center Of Mass inside the (center of) mesh
@export var com_inside_mesh:bool = true

@export var dissolve_mesh_instance_path: NodePath = NodePath()
@export var dissolve_time: float = 16.0

enum Axis {X, Y, Z}

var center_height:float = 0.0

var do_hit:bool = false
var hit_impulse:Vector3 = Vector3()
var hit_impulse_position:Vector3 = Vector3()

var do_dissolve:bool = false


func _ready():
	_set_collision_shape()


func _physics_process(delta):
	if do_hit:
		if (((self as CollisionObject3D) is RigidBody3D) and 
				(PhysicsServer3D.body_get_mode(get_rid()) == PhysicsServer3D.BODY_MODE_RIGID)):
			(self as CollisionObject3D).apply_impulse(hit_impulse, hit_impulse_position)
			do_hit = false
	if do_dissolve: 
		if dissolve_time >= 0.0:
			dissolve_time -= delta
		else:
			var dissolve_mesh_instance = get_node(dissolve_mesh_instance_path)
			if dissolve_mesh_instance:
				dissolve_mesh_instance.fade()
				await dissolve_mesh_instance.fading_done
			queue_free()


func _set_collision_shape():
	# save current rotation and reset
	var current_rotation = rotation
	rotation = Vector3.ZERO
	# get mesh instance node and collision shape: by using tree's
	# iterative breadth search, the first ones found are returned
	var mesh_instance:MeshInstance3D = null
	var collision_shape:CollisionShape3D = null
	var mesh_instance_found = false
	var collision_shape_found = false
	var stack = []
	stack.append_array(get_children())
	while ((not stack.is_empty()) and 
			((not mesh_instance_found) or (not collision_shape_found))):
		var child = stack.pop_front()
		if (not mesh_instance_found) and (child is MeshInstance3D):
			mesh_instance = child
			mesh_instance_found = true
		elif (not collision_shape_found) and (child is CollisionShape3D):
			collision_shape = child
			collision_shape_found = true
		stack.append_array(child.get_children())
	# get mesh_instance (transformed) aabb
	var aabb:AABB = mesh_instance.get_aabb().abs()
	# apply shape scale
	var scaled_aabb = AABB(aabb.position, 
			aabb.size * shape_scale * mesh_instance.scale)
	center_height = scaled_aabb.size.y * 0.5
	# set the shape of the collision shape
	ScriptLibrary.add_shape(collision_shape, shape_type, scaled_aabb)
	# Center Of Mass inside the (center of) mesh (if requested)
	if com_inside_mesh:
		var aabb_center_wrt_mesh = mesh_instance.get_aabb().get_center()
		var aabb_center_wrt_body = mesh_instance.transform * aabb_center_wrt_mesh
		mesh_instance.transform.origin -= aabb_center_wrt_body
	# restore current rotation
	rotation = current_rotation


func bullet_hit(damage, bullet_global_transform,
		raycast=false, with_hit=false):
	if (((self as CollisionObject3D) is RigidBody3D) and 
			(not self.freeze) and (raycast or with_hit)):
		# hit direction and position
		hit_impulse = -bullet_global_transform.basis.z * damage
		hit_impulse_position = bullet_global_transform.origin - global_transform.origin
		do_hit = true


func rocket_hit(damage, rocket_global_transform):
	if ((self as CollisionObject3D) is RigidBody3D) and (not self.freeze):
		#return
	#else:
		## NOTE: enhance physic collision with supplementary impulse
		# hit direction and position
		hit_impulse = rocket_global_transform.basis.z * damage
		hit_impulse_position = rocket_global_transform.origin - global_transform.origin
		do_hit = true


func dissolve(enable):
	await get_tree().create_timer(randf() * 1.5).timeout
	do_dissolve = enable
