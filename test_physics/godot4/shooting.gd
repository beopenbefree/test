class_name Shooting
extends Node3D


@export var impulse_size = 50.0
@export var bullet_mass = 1.0

var bullet_scene = preload("res://test_physics/godot4/bullet.tscn")


func _input(event):
	if event.is_action_pressed("ui_accept"):
		var bullet = bullet_scene.instantiate()
		bullet.mass = bullet_mass
		bullet.direction = -$OrbitCamera/ShootPoint.global_transform.basis.z
		bullet.impulse_size = impulse_size
		get_tree().get_root().add_child(bullet)
		bullet.global_transform = $OrbitCamera/ShootPoint.global_transform
		bullet.firing = true
