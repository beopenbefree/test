extends RigidBody3D


var impulse_size = 10.0
var direction = Vector3()

const KILL_TIMER:float = 10.0
var timer:float = 0
var firing:bool = false


func _physics_process(delta):
	if firing:
		apply_impulse(impulse_size * direction,  Vector3.ZERO)
		firing = false
	
	timer += delta
	if timer > KILL_TIMER:
		queue_free()
