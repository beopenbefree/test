extends Node3D


@export var size: float = 1.0

var body: RigidBody3D = null

@onready var apply_impulse: bool = false


# Called when the node enters the scene tree for the first time.
func _ready():
	$Plane/RigidBody3D.mass = size


func _physics_process(delta):
	if apply_impulse:
		apply_impulse = false
		var impulse = Vector3(0.0, 1.0, 0.0) * size * 100
		$Plane/RigidBody3D.apply_impulse(impulse, Vector3.ZERO)


func _on_fire_pressed():
	apply_impulse = true
