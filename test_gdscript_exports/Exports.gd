@tool
extends Node


# If the exported value assigns a constant or constant expression,
# the type will be inferred and used in the editor.
@export var number = 5
# Export can take a basic data type as an argument, which will be
# used in the editor.
@export var number1: int
# Export can also take a resource type to use as a hint.
@export var character_face: Texture2D
@export var scene_file: PackedScene
# There are many resource types that can be used this way, try e.g.
# the following to list them:
@export var resource: Resource
# Integers and strings hint enumerated values.
# Editor will enumerate as 0, 1 and 2.
@export var character_class # (int, "Warrior", "Magician", "Thief")
# Editor will enumerate with string names.
@export var character_name # (String, "Rebecca", "Mary", "Leah")
# Named enum values
# Editor will enumerate as THING_1, THING_2, ANOTHER_THING.

enum NamedEnum {THING_1, THING_2, ANOTHER_THING = -1}
@export var x: NamedEnum
# Strings as paths
# String is a path to a file.
@export(String, FILE) var f
# String is a path to a directory.
@export var f1 # (String, DIR)
# String is a path to a file, custom filter provided as hint.
@export var f3 # (String, FILE, "*.txt")
# Using paths in the global filesystem is also possible,
# but only in scripts in "tool" mode.
# String is a path to a PNG file in the global filesystem.
@export var tool_image # (String, FILE, GLOBAL, "*.png")
# String is a path to a directory in the global filesystem.
@export var tool_dir # (String, DIR, GLOBAL)
# The MULTILINE setting tells the editor to show a large input
# field for editing over multiple lines.
@export var text # (String, MULTILINE)
# Limiting editor input ranges
# Allow integer values from 0 to 20.
@export(int, 20) var i
# Allow integer values from -10 to 20.
@export(int, -10, 20) var j
# Allow floats from -10 to 20 and snap the value to multiples of 0.2.
@export(float, -10, 20, 0.2) var k
# Allow values 'y = exp(x)' where 'y' varies between 100 and 1000
# while snapping to steps of 20. The editor will present a
# slider for easily editing the value.
@export(float, EXP, 100, 1000, 20) var l
# Floats with easing hint
# Display a visual representation of the 'ease()' function
# when editing.
@export var transition_speed # (float, EASE)
# Colors
# Color given as red-green-blue value (alpha will always be 1).
@export var col # (Color, RGB)
# Color given as red-green-blue-alpha value.
@export var col1 # (Color, RGBA)
# Nodes
# Another node in the scene can be exported as a NodePath.
@export var node_path: NodePath
# Do take note that the node itself isn't being exported -
# there is one more step to call the true node:
@onready var node = get_node(node_path)
# Resources
@export var resource1: Resource
# In the Inspector, you can then drag and drop a resource file
# from the FileSystem dock into the variable slot.
# Opening the inspector dropdown may result in an
# extremely long list of possible classes to create, however.
# Therefore, if you specify an extension of Resource such as:
@export var resource2: AnimationNode
# The drop-down menu will be limited to AnimationNode and all
# its inherited classes.

# Exporting bit flags
# Set any of the given flags from the editor.
@export var spell_elements = 0 # (int, FLAGS, "Fire", "Water", "Earth", "Wind")

@export var layers_2d_physics # (int, LAYERS_2D_PHYSICS)
@export var layers_2d_render # (int, LAYERS_2D_RENDER)
@export var layers_3d_physics # (int, LAYERS_3D_PHYSICS)
@export var layers_3d_render # (int, LAYERS_3D_RENDER)

# Exporting arrays
# Default value must be a constant expression.
@export var a = [1, 2, 3]
# Exported arrays can specify type (using the same hints as before).
@export var ints = [1, 2, 3] # (Array, int)
@export var enums = [2, 1, 0] # (Array, int, "Red", "Green", "Blue")
@export var two_dimensional = [[1.0, 2.0], [3.0, 4.0]] # (Array, Array, float)
# You can omit the default value, but then it would be null if not assigned.
@export var b: Array
@export var scenes # (Array, PackedScene)
# Arrays with specified types which inherit from resource can be set by
# drag-and-dropping multiple files from the FileSystem dock.
@export var textures # (Array, Texture2D)
@export var scenes1 # (Array, PackedScene)
# Typed arrays also work, only initialized empty:
@export var vector3s = PackedVector3Array()
@export var strings = PackedStringArray()
# Default value can include run-time values, but can't
# be exported.
var c = [a, 2, 3]
