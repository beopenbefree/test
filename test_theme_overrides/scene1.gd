extends Node2D


var texture1_on = "res://assets/PNG/16x16/Green/texture_11.png"
var texture1_off = "res://assets/PNG/16x16/Red/texture_11.png"

func _ready():
	$CheckButton1.set_icons(texture1_on, texture1_off)
	$CheckButton2.set_icons(texture1_on, texture1_off)
	$CheckButton3.set_icons(texture1_on, texture1_off)
