extends Node3D


@onready var buttons = {
	"Start1": $AnimatedObject1,
	"Stop1": $AnimatedObject1,
	"Start2": $AnimatedObject2,
	"Stop2": $AnimatedObject2,
}


func _on_Start_pressed(btn):
	buttons[btn].start_anim()


func _on_Stop_pressed(btn):
	buttons[btn].stop_anim()
