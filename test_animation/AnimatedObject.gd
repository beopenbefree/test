extends Node3D


@onready var player = $MeshInstance3D/AnimationPlayer


func start_anim():
	player.play("anim1")
	player.get_animation(player.current_animation).loop = true


func stop_anim():
	player.stop(false)


func print_self():
	print(self)
