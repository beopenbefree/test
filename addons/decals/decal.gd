@tool
extends MeshInstance3D

const SHADER = preload("decal.gdshader")

@export var decal: Texture2D: set = set_decal
@export var uv_offset: Vector2 = Vector2(): set = set_uv_offset
@export var uv_scale: Vector2 = Vector2(1, 1): set = set_uv_scale
@export var emulate_lighting: bool = true: set = set_emulate_lighting
@export var brightness = 0.0: set = set_brightness

func _init():
	mesh = BoxMesh.new()
	mesh.material = ShaderMaterial.new()
	mesh.material.gdshader = SHADER

func set_decal(new_decal):
	decal = new_decal
	mesh.material.set_shader_parameter("decal", decal)

func set_uv_offset(new_offset):
	uv_offset = new_offset
	mesh.material.set_shader_parameter("offset", uv_offset)

func set_uv_scale(new_scale):
	uv_scale = new_scale
	mesh.material.set_shader_parameter("scale", uv_scale)

func set_emulate_lighting(new_value):
	emulate_lighting = new_value
	mesh.material.set_shader_parameter("emulate_lighting", emulate_lighting)

func set_brightness(new_brightness):
	brightness = new_brightness
	mesh.material.set_shader_parameter("brightness", brightness * 0.01)
