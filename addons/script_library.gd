class_name ScriptLibrary
extends Resource


### Generic helpers

enum ShapeType {BOX, CAPSULE, CYLINDER, SPHERE}


static func get_all_children(in_node:Node, recursive:bool=true,
		arr:Array=[]):
	arr.push_back(in_node)
	if recursive:
		for child in in_node.get_children():
			arr = get_all_children(child, recursive, arr)
	else:
		for child in in_node.get_children():
			arr.push_back(child)
	return arr


static func rand_float(magnitude:float):
	# return a float in [-magnitude, magnitude]
	return (2 * randf() - 1.0) * magnitude


static func find_node_by_type_name(type_name:String, root_node:Node):
	# get the first node instance with the given type name: by using tree's
	# iterative breadth search, the first one found is returned
	var instance = null
	var stack = []
	stack.append_array(root_node.get_children())
	while not stack.is_empty():
		var child = stack.pop_front()
		var child_type_name = child._get_class() if child.has_method("_get_class") else child.get_class()
		if child_type_name == type_name:
			instance = child
			break
		stack.append_array(child.get_children())
	return instance


static func find_node_by_type(type:Variant, root_node:Node):
	# get the first node instance with the given type: by using tree's
	# iterative breadth search, the first one found is returned
	var instance = null
	var stack = []
	stack.append_array(root_node.get_children())
	while not stack.is_empty():
		var child = stack.pop_front()
		if is_instance_of(child, type):
			instance = child
			break
		stack.append_array(child.get_children())
	return instance


static func create_static_body(shape_type:int, scaled_aabb:AABB):
	var static_body = StaticBody3D.new()
	# cumpute the collision shape
	var collision_shape:CollisionShape3D = compute_collision_shape(
			shape_type, scaled_aabb)
	# finish building the static body
	static_body.add_child(collision_shape)
	# correct collision shape's position wrt static body
	collision_shape.transform.origin += Vector3(
			0, scaled_aabb.size.y / 2.0, 0.0)
	#
	return static_body


static func compute_collision_shape(shape_type:ShapeType, scaled_aabb:AABB):
	# enum ShapeType {BOX=0, CAPSULE=1, CYLINDER=2, SPHERE=3}
	# compute collision shape
	var collision_shape = CollisionShape3D.new()
	# add a shape
	add_shape(collision_shape, shape_type, scaled_aabb)
	#
	return collision_shape


static func add_shape(collision_shape:CollisionShape3D, 
		shape_type:int, scaled_aabb:AABB):
	# compute shape
	var shape:Shape3D = null
	var longest_axis_index = scaled_aabb.get_longest_axis_index()
	var shape_size = scaled_aabb.size
	if shape_type == ShapeType.BOX: # 0
		shape = BoxShape3D.new()
		shape.size = shape_size
	elif shape_type == ShapeType.CAPSULE: # 1
		shape = CapsuleShape3D.new()
		var radius = 0.5
		var height = 2.0
		if longest_axis_index == Vector3.AXIS_X:
			radius = max(shape_size.y / 2, shape_size.z / 2)
			height = shape_size.x - 2 * radius
			collision_shape.rotate(Vector3.FORWARD, deg_to_rad(90))
		elif longest_axis_index == Vector3.AXIS_Z:
			radius = max(shape_size.x / 2, shape_size.y / 2)
			height = shape_size.z - 2 * radius
			collision_shape.rotate(Vector3.RIGHT, deg_to_rad(90))
		else:
			#longest_axis_index == Vector3.AXIS_Y
			radius = max(shape_size.x / 2, shape_size.z / 2)
			height = shape_size.y - 2 * radius
		shape.set_radius(radius)
		shape.set_height(height)
	elif shape_type == ShapeType.CYLINDER: # 2
		shape = CylinderShape3D.new()
		var radius = 0.5
		var height = 2.0
		if longest_axis_index == Vector3.AXIS_X:
			radius = max(shape_size.y / 2, shape_size.z / 2)
			height = shape_size.x
			collision_shape.rotate(Vector3.FORWARD, deg_to_rad(90))
		elif longest_axis_index == Vector3.AXIS_Z:
			radius = max(shape_size.x / 2, shape_size.y / 2)
			height = shape_size.z
			collision_shape.rotate(Vector3.RIGHT, deg_to_rad(90))
		else:
			# longest_axis_index == Vector3.AXIS_Y
			radius = max(shape_size.x / 2, shape_size.z / 2)
			height = shape_size.y
		shape.set_radius(radius)
		shape.set_height(height)
	else: 
		#shape_type == ShapeType.CAPSULE # 3
		shape = SphereShape3D.new()
		shape.radius = max(max(shape_size.x / 2, 
				shape_size.z / 2), shape_size.y / 2)
	#
	collision_shape.shape = shape


static func find_position_with_correct_y(instance_origin:Vector3, 
		ray_cast:RayCast3D, ray_cast_height:float):
	var correct_instance_origin = instance_origin
	var delta_y_pos = Vector3(0.0, ray_cast_height, 0.0)
	ray_cast.global_transform.origin = instance_origin + delta_y_pos
	ray_cast.target_position = -2 * delta_y_pos
	ray_cast.force_raycast_update()
	if ray_cast.is_colliding():
		correct_instance_origin = ray_cast.get_collision_point()
	return correct_instance_origin


static func  create_multimesh_objects_from_children_by_material(
		SELF:Node, object_types:Array[PackedScene],
		visual_only:bool, object_max_delta_scale:float,
		object_collision_shape_scales:Array[Vector3],
		ray_cast:RayCast3D, ray_cast_height:float,
		save_scene:bool, save_scene_name:String, save_scene_path:String):
	
	# create objects in the same place of a number of childrens 
	# each child has a material from a given set, so for each kind 
	# of material, are created a number of instances of objects 
	# of the same type and allocated in the same multimesh.
	
	# objects dictionary are keyed by materials
	var object_by_material:Dictionary = {}
	var idx = 0
	for child in SELF.get_children():
		if not (child is MeshInstance3D):
			continue
		# hold a reference to instance
		var mesh_instance:MeshInstance3D = child 
		var mesh:Mesh = mesh_instance.mesh
		var material:StandardMaterial3D = mesh.surface_get_material(0)
		if not (material in object_by_material):
			# for each material select an object scene and setup a new multimesh 
			# instance with a new multimesh, an instance transform list 
			# and save the basic scale
			var curr_idx = idx % object_types.size()
			var object_scene:PackedScene = object_types[curr_idx]
			var object_scene_collision_shape_scale = object_collision_shape_scales[curr_idx]
			idx += 1
			var object_scene_instance = object_scene.instantiate()
			var multimesh_instance = MultiMeshInstance3D.new()
			multimesh_instance.name = "multimesh_" + str(material.get_rid().get_id())
			#
			var multimesh = MultiMesh.new()
			multimesh_instance.multimesh = multimesh
			object_by_material[material] = {
				"multimesh_instance": multimesh_instance,
				"instance_transform_list": [],
				"object_scene_scaling": object_scene_instance.scaling,
				"object_scene_collision_shape_scale": object_scene_collision_shape_scale,
			}
			# initialize the multimesh
			# find the mesh inside object scene
			var object_mesh:Mesh = null
			var object_mesh_found = false
			var stack = []
			stack.append_array(object_scene_instance.get_children())
			while ((not stack.is_empty()) and (not object_mesh_found)):
				var node = stack.pop_front()
				if node is MeshInstance3D:
					object_mesh = node.mesh
					object_mesh_found = true
				stack.append_array(node.get_children())
			multimesh.mesh = object_mesh
			multimesh.transform_format = MultiMesh.TRANSFORM_3D
			multimesh.use_colors = false
			multimesh.use_custom_data = false
			# append shape type and aabb for computing a static body 
			# (with a collision shape) for each instance
			object_by_material[material]["object_scene_shape_type"] = object_scene_instance.shape_type
			object_by_material[material]["object_scene_mesh_aabb"] = object_mesh.get_aabb().abs()
			object_scene_instance.queue_free()
		# compute transform for the instance
		var instance_basis = Basis.IDENTITY
		var instance_origin = Vector3.ZERO
		# apply randomized instance rotation
		instance_basis = instance_basis.rotated(
				Vector3.UP, randf() * deg_to_rad(360))
		# set instance origin to the correct place on the surface
		instance_origin = find_position_with_correct_y(
				mesh_instance.global_transform.origin, ray_cast,
				ray_cast_height)
		# compute unscaled transform (used by static body)
		var instance_transform_unscaled = Transform3D(instance_basis,
				instance_origin)
		# apply randomized instance scale
		var scale_deviation = 1 + rand_float(object_max_delta_scale)
		var scaling_new = object_by_material[material]["object_scene_scaling"] * scale_deviation
		instance_basis = instance_basis.scaled(scaling_new)
		# compute and append instance transform
		var instance_transform = Transform3D(instance_basis, instance_origin)
		object_by_material[material]["instance_transform_list"].append(instance_transform)
		# check if we want a static body or not
		if not visual_only:
			# create a static body for the instance with a correct collision shape
			var object_shape_type:int = object_by_material[material]["object_scene_shape_type"]
			var object_mesh_aabb:AABB = object_by_material[material]["object_scene_mesh_aabb"]
			var object_collision_shape_scale:Vector3 = object_by_material[material]["object_scene_collision_shape_scale"]
			var object_scaled_aabb := AABB() 
			object_scaled_aabb.size = object_mesh_aabb.size * (
					scaling_new * object_collision_shape_scale)
			var static_body = create_static_body(object_shape_type,
					object_scaled_aabb)
			# place and add the static body to the multimesh instance
			static_body.transform = instance_transform_unscaled * static_body.transform
			var object_multimesh_instance:MultiMeshInstance3D = object_by_material[material]["multimesh_instance"]
			object_multimesh_instance.add_child(static_body)
		# remove the original mesh instance
		child = null
		mesh_instance.queue_free()
	
	print("object by material size: ", object_by_material.size())
	
	# draw the multimeshes
	for material in object_by_material:
		var multimesh_instance = object_by_material[material]["multimesh_instance"]
		var multimesh:MultiMesh = multimesh_instance.multimesh
		var instance_transforms = object_by_material[material]["instance_transform_list"]
		multimesh.instance_count = instance_transforms.size()
		for i in multimesh.instance_count:
			multimesh.set_instance_transform(i, instance_transforms[i])
		SELF.add_child(multimesh_instance)
	
	ray_cast.queue_free()
	print(SELF.get_child_count())
	
	# save scene if requested
	if save_scene:
		await SELF.get_tree().create_timer(5.0).timeout
		# add a node to be saved as scene
		var node_to_save = Node3D.new()
		if not save_scene_name.is_empty():
			node_to_save.name = save_scene_name
		else:
			node_to_save.name = SELF.name
		SELF.add_child(node_to_save)
		# reparent all first level children to node_to_save and 
		# recursively set all children's owner: so they will
		# get saved with the scene
		var all_childred_but_self = get_all_children(SELF)
		all_childred_but_self.pop_front()
		for instance:Node3D in all_childred_but_self:
			if instance != node_to_save:
				if instance.get_parent() == SELF:
					SELF.remove_child(instance)
					node_to_save.add_child(instance)
				instance.set_owner(node_to_save)
		#
		var current_scene = node_to_save
		var packed_scene = PackedScene.new()
		packed_scene.pack(current_scene)
		var scene_name = save_scene_path + "/"
		if not save_scene_name.is_empty():
			scene_name += save_scene_name + "_saved.tscn"
		else:
			scene_name += current_scene.name + "_saved.tscn"
		# actually save the scene 
		ResourceSaver.save(packed_scene, scene_name)
		var saved_label = Label.new()
		saved_label.text = "SAVED!"
		SELF.add_child(saved_label)


### INPUT: Action management

static func _add_action(action, keycode=null, button_index=null):
	InputMap.add_action(action)
	if keycode:
		var ev = InputEventKey.new()
		ev.keycode = keycode
		InputMap.action_add_event(action, ev)
	if button_index:
		var ev = InputEventMouseButton.new()
		ev.button_index = button_index
		InputMap.action_add_event(action, ev)


static func _get_scancode(action):
	var keycode = 0
	for key_event in InputMap.action_get_events(action):
		if key_event is InputEventKey:
			keycode = key_event.keycode
			return keycode
	return null


static func _get_button_index(action):
	var button_index = 0
	for mouse_event in InputMap.action_get_events(action):
		if mouse_event is InputEventMouseButton:
			button_index = mouse_event.button_index
			return button_index
	return null


static func _feed_action(name, pressed, strength=1.0):
	var ev_action = InputEventAction.new()
	ev_action.action = name
	ev_action.button_pressed = pressed
	Input.parse_input_event(ev_action)


### Object's Occluders management

class AABBFace extends RefCounted:
	var aabb:AABB = AABB()
	var normal:Vector3 = Vector3.ZERO
	var points:Array = []
	var axis_name:String = ""
	var transform:Transform3D = Transform3D.IDENTITY
	
	func _init(_aabb:AABB, normal:Vector3, scaling:Vector3=Vector3.ONE):
		self.aabb = _aabb.abs()
		var p = self.aabb.position
		var e = self.aabb.end
		var s0 = e - p
		var s = s0 * scaling
		var sx2 = s.x / 2.0
		var sy2 = s.y / 2.0
		var sz2 = s.z / 2.0
		var t_rot = Transform3D.IDENTITY
		var t_trans = Transform3D.IDENTITY
		# X axis
		if (abs(normal.x) > abs(normal.y)) and (abs(normal.x) > abs(normal.z)):
			self.points.append(Vector2(0,0))
			self.points.append(Vector2(s.z,0))
			self.points.append(Vector2(s.z,s.y))
			self.points.append(Vector2(0,s.y))
			t_rot = Transform3D(Basis(Vector3.UP, deg_to_rad(90)), Vector3.ZERO)
			if normal.x > 0.0:
				self.axis_name = "+X"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(sx2, -sy2, sz2))
			else:
				self.axis_name = "-X"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(-sx2, -sy2, sz2))
		# Y axis
		elif (abs(normal.y) > abs(normal.x)) and (abs(normal.y) > abs(normal.z)):
			self.points.append(Vector2(0,0))
			self.points.append(Vector2(s.x,0))
			self.points.append(Vector2(s.x,s.z))
			self.points.append(Vector2(0,s.z))
			t_rot = Transform3D(Basis(Vector3.RIGHT, deg_to_rad(90)), Vector3.ZERO)
			if normal.y > 0.0:
				self.axis_name = "+Y"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(-sx2, sy2, -sz2))
			else:
				self.axis_name = "-Y"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(-sx2, -sy2, -sz2))
		# Z axis
		elif (abs(normal.z) > abs(normal.x)) and (abs(normal.z) > abs(normal.y)):
			self.points.append(Vector2(0,0))
			self.points.append(Vector2(s.x,0))
			self.points.append(Vector2(s.x,s.y))
			self.points.append(Vector2(0,s.y))
			if normal.z > 0.0:
				self.axis_name = "+Z"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(-sx2, -sy2, sz2))
			else:
				self.axis_name = "-Z"
				t_trans = Transform3D(Basis.IDENTITY, Vector3(-sx2, -sy2, -sz2))
		#
		self.transform = t_trans * t_rot


const AXIS_NORMALS = [
		Vector3.RIGHT,
		-Vector3.RIGHT,
		Vector3.UP,
		-Vector3.UP,
		Vector3.FORWARD,
		-Vector3.FORWARD,
	]


static func occluders_save_scene(object:VisualInstance3D, normals:Array=AXIS_NORMALS, 
		scaling:Vector3=Vector3.ONE, save_dir:String=""):
	if object is VisualInstance3D:
		var occluder_node = Node3D.new()
		occluder_node.name = "Occluders"
		for o in occluders_get(object, normals, scaling):
			occluder_node.add_child(o)
			o.set_owner(occluder_node)
		var packed_scene = PackedScene.new()
		packed_scene.pack(occluder_node)
		var scene_name = "res://" + save_dir + "/" + object.name + "_occluders.tscn" 
		ResourceSaver.save(scene_name, packed_scene)


static func occluders_get(object:VisualInstance3D, normals:Array=AXIS_NORMALS, 
		scaling:Vector3=Vector3.ONE):
	var occluders: Array = []
	if object is VisualInstance3D:
		var s = Vector3(clamp(scaling.x, 0, 1.0), clamp(scaling.y, 0, 1.0), 
				clamp(scaling.z, 0, 1.0))
		for n in normals:
			occluders.append(_occluder_create(object, n, s))
	return occluders


static func _occluder_create(object, n, s):
	var aabb_face = AABBFace.new(object.get_aabb(), n, s)
	var point_array = PackedVector2Array(aabb_face.points)
	var occluder = OccluderInstance3D.new()
	occluder.name = object.name + "_" + aabb_face.axis_name
	occluder.occluder = PolygonOccluder3D.new()
	occluder.occluder.polygon = point_array
	occluder.transform = object.transform * aabb_face.transform
	return occluder



